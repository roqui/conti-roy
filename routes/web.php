<?php

 


Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();  
/* controladores para los usuarios */

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('newusers', 'UsersController@nuevo')->name('newusers');

Route::post('photo', 'UsersController@photo')->name('photo');


//Route::get('newusers', 'UsersController@nuevo')->name('newusers')->middleware('AddUsers');
Route::get('userexcel', 'UsersController@excel')->name('userexcel');

Route::get('manual', 'UsersController@manual')->name('manual');

Route::get('ups', 'UsersController@ups')->name('ups');

Route::get('work', 'UsersController@work')->name('work');

Route::get('compra', 'UsersController@compra')->name('compra');
Route::get('pedido', 'UsersController@pedido')->name('pedido');
Route::get('traslado', 'UsersController@traslado')->name('traslado');
Route::get('index2', 'UsersController@index2')->name('index2');


Route::get('upspermiso', 'UsersController@upspermiso')->name('upspermiso');


Route::post('create', 'UsersController@create')->name('create');

Route::post('create2', 'UsersController@create2')->name('create2');

Route::get('profile', 'UsersController@profile')->name('profile');

Route::get('editusers', 'UsersController@editar')->name('editusers');

Route::get('actuser', 'UsersController@actuser')->name('actuser');

Route::post('/update/{id}', 'UsersController@update')->name('update');
Route::post('/update2/{id}', 'UsersController@update2')->name('update2');


Route::get('{id}/destroyu', 'UsersController@destroyu')->name('destroyu');

Route::post('eliminaruser', 'UsersController@eliminaruser')->name('eliminaruser');

Route::get('allindex', 'UsersController@allindex')->name('allindex');

Route::get('searchuser', 'UsersController@searchuser')->name('searchuser');
//buscarusuarios

Route::post('buscarusuarios', 'UsersController@buscarusuarios')->name('buscarusuarios');



//

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('index','PagesController@index')->name('index');

Route::get('404','PagesController@cuatro')->name('404');

Route::get('blank','PagesController@blank')->name('blank');

Route::get('charts','PagesController@charts' )->name('charts');

Route::get('plantilla','PagesController@plantilla')->name('plantilla');

Route::get('menu','PagesController@menu')->name('menu');

// Route::get('pedidos','PagesController@pedidos')->name('pedidos');

// Route::get('verpedido','PagesController@verpedido')->name('verpedido');

//productos

Route::get('/prodview','ProductController@prodfunct');
Route::get('/findProductName','ProductController@findProductName');
Route::get('/findLastName','ProductController@findLastName');
Route::get('/findPrice','ProductController@findPrice');

//users
Route::get('/findLastUser','UsersController@findLastUser');
//sucursales
Route::get('/findLastSuc','SucursalController@findLastSuc');
//PARA PRODUCTOS 

//Lineas


Route::post('crearline','ProductController@crearline')->name('crearline');

Route::post('eliminarline', 'ProductController@eliminarline')->name('eliminarline');

Route::post('update_line', 'ProductController@update_line')->name('update_line');


//sublineas
Route::post('crearsubline','ProductController@crearsubline')->name('crearsubline');

Route::post('/eliminar_subline', 'ProductController@eliminar_subline')->name('eliminar_subsline');

Route::post('update_subline', 'ProductController@update_subline')->name('update_subline');

Route::get('verline', 'ProductController@verline')->name('verline');

//marcas
Route::post('crearmarca','ProductController@crearmarca')->name('crearmarca');

Route::post('/eliminar_marca', 'ProductController@eliminar_marca')->name('eliminar_marca');

Route::post('update_marca', 'ProductController@update_marca')->name('update_marca');

//productos

Route::post('/eliminar_producto', 'ProductController@eliminar_producto')->name('eliminar_producto');

Route::get('crear_producto', 'ProductController@crear_producto')->name('crear_producto');

Route::get('update_product', 'ProductController@update_product')->name('update_product');

Route::post('update_product2', 'ProductController@update_product2')->name('update_product2');

Route::post('new_producto', 'ProductController@new_producto')->name('new_producto');

Route::post('proveedores_producto', 'ProductController@proveedores_producto')->name('proveedores_producto');

Route::post('addprov', 'ProductController@addprov')->name('addprov');

Route::post('eliminar_proveeducto', 'ProductController@eliminar_proveeducto')->name('eliminar_proveeducto');

Route::get('productexcel', 'ProductController@excel')->name('productexcel');


//

//FIN DE PRODUCTOS

// Route::get('permisos','PermissionController@permisos')->name('permisos');

// Route::get('permisos2','PermissionController@permisos2')->name('permisos2');

Route::get('linea',['uses'=>'ProductController@linea']);
Route::get('sucursal',['uses'=>'ProductController@sucursal']);
Route::get('estado',['uses'=>'ProductController@estado']);

Route::post('select',['uses'=>'SelectController@postSelect','as'=>'postSelect']);


//DE INVENTARIOS 
Route::get('inventarios', 'InventariosController@inventarios')->name('inventarios');

Route::get('indexinventarios', 'InventariosController@indexinventarios')->name('indexinventarios');

Route::post('searchinventarios', 'InventariosController@searchinventarios')->name('searchinventarios');


Route::post('new_producto_inventario', 'InventariosController@new_producto_inventario')->name('new_producto_inventario');

Route::post('new_producto_inventario2', 'InventariosController@new_producto_inventario2')->name('new_producto_inventario2');


Route::get('inventarioexcel', 'InventariosController@excel')->name('inventarioexcel');

//eliminar_inventario_producto

Route::post('actualizar_producto_inventario', 'InventariosController@actualizar_producto_inventario')->name('actualizar_producto_inventario');

Route::post('actualizar_producto_inventario2', 'InventariosController@actualizar_producto_inventario2')->name('actualizar_producto_inventario2');


Route::post('eliminar_inventario_producto', 'InventariosController@eliminar_inventario_producto')->name('eliminar_inventario_producto');


// DE PROVEEDORES

// Route::get('newprovider','ProviderController@newprovider')->name('newprovider');

// Route::get('proveedores','ProviderController@proveedores')->name('proveedores');

Route::get('editproviders','ProviderController@editproviders')->name('editproviders');

Route::get('{id}/destroy', 'ProviderController@destroy')->name('destroy');

Route::post('crearprov','ProviderController@crearprov')->name('crearprov');

Route::post('eliminar_proveedor2','ProviderController@eliminar_proveedor2')->name('eliminar_proveedor2');


Route::get('/findLastP','ProviderController@findLastP');

Route::get('actprov', 'ProviderController@actprov')->name('actprov');

Route::post('/updatep/{id}', 'ProviderController@updatep')->name('updatep');

Route::get('providerexcel', 'ProviderController@excel')->name('providerexcel');
//eliminar_proveedor2

//DE PERMISOS

Route::get('permisos','PermissionController@permisos')->name('permisos');


Route::get('alerta','PagesController@alerta')->name('alerta');

Route::get('permiso','PermissionController@permiso')->name('permiso');

Route::get('consulta','PermissionController@consulta')->name('consulta');

Route::get('createpermiso','PermissionController@createpermiso')->name('createpermiso');

Route::get('elimpermiso','PermissionController@elimpermiso')->name('elimpermiso');

Route::get('elimall','PermissionController@elimall')->name('elimall');

Route::get('createall','PermissionController@createall')->name('createall');

//PEDIDOS PENDIENTES
Route::get('ppendientes','PedidosPendientesController@ppendientes')->name('ppendientes');

Route::get('asignarpp','PedidosPendientesController@asignarpp')->name('asignarpp');

Route::get('asignadopp','PedidosPendientesController@asignadopp')->name('asignadopp');

Route::get('conceptos','PedidosPendientesController@conceptos')->name('conceptos');


Route::get('pasignados','PedidosAsignadosController@pasignados')->name('pasignados');

Route::get('checar','PedidosAsignadosController@checar')->name('checar');

Route::get('checado','PedidosAsignadosController@checado')->name('checado');

Route::get('conceptosa','PedidosAsignadosController@conceptosa')->name('conceptosa');

Route::get('finchecar','PedidosAsignadosController@finchecar')->name('finchecar');

Route::get('finpedido','PedidosAsignadosController@finpedido')->name('finpedido');

//pedidor remisionados

Route::get('premisionados','PedidosRemisionadosController@premisionados')->name('premisionados');

Route::get('asignarpr','PedidosRemisionadosController@asignarpr')->name('asignarpr');

Route::get('asignadopr','PedidosRemisionadosController@asignadopr')->name('asignadopr');

//TRASLADOS

Route::get('traslados','TrasladosController@traslados')->name('traslados');

Route::post('datos_traslado', 'TrasladosController@datos_traslado')->name('datos_traslado');

Route::post('new_traslado', 'TrasladosController@new_traslado')->name('new_traslado');

Route::post('agregar_concepto_traslado', 'TrasladosController@agregar_concepto')->name('new_traslado');

Route::get('tabla_dinamica_traslados', 'TrasladosController@tabla_dinamica')->name('tabla_dinamica_traslados');

Route::post('eliminar_concepto_traslados', 'TrasladosController@eliminar_concepto')->name('eliminar_concepto_traslados');

Route::get('act_cantidad_traslados', 'TrasladosController@act_cantidad')->name('act_cantidad_traslados');

Route::get('nota_traslados', 'TrasladosController@nota')->name('nota_traslados');

Route::get('imprimirSiNo_traslados', 'TrasladosController@imprimirSiNo')->name('imprimirSiNo_traslados');

Route::get('observaciones_traslados', 'TrasladosController@observaciones')->name('observaciones_traslados');

Route::get('cancelar_traslado', 'TrasladosController@cancelar_nota')->name('cancelar_traslado');



//traslados pendientes

Route::get('tpendientes', 'TrasladosPendientesController@tpendientes')->name('tpendientes');

Route::get('conceptost', 'TrasladosPendientesController@conceptost')->name('conceptost');

Route::get('asignartp', 'TrasladosPendientesController@asignartp')->name('asignartp');

Route::get('asignadotp', 'TrasladosPendientesController@asignadotp')->name('asignadotp');

//traslados asignados

Route::get('tasignados', 'TrasladosAsignadosController@tasignados')->name('tasignados');

Route::get('checart', 'TrasladosAsignadosController@checart')->name('checart');

Route::get('conceptosta', 'TrasladosAsignadosController@conceptosta')->name('conceptosta');

Route::get('checadot', 'TrasladosAsignadosController@checadot')->name('checadot');

Route::get('finchecart', 'TrasladosAsignadosController@finchecart')->name('finchecart');

//traslados remisionados

Route::get('tremisionados', 'TrasladosRemisionadosController@tremisionados')->name('tremisionados');

Route::get('asignartr', 'TrasladosRemisionadosController@asignartr')->name('asignartr');

Route::get('asignadotr', 'TrasladosRemisionadosController@asignadotr')->name('asignadotr');

Route::get('todost', 'TrasladosController@todos')->name('todost');

Route::get('infotraslado', 'TrasladosController@info')->name('infotraslado');

//traslados proximos

Route::get('tproximos', 'TrasladosProximosController@tproximos')->name('tproximos');

Route::get('revisart', 'TrasladosProximosController@revisart')->name('revisart');

Route::get('conceptostp', 'TrasladosProximosController@conceptostp')->name('conceptostp');

Route::get('checadotp', 'TrasladosProximosController@checadotp')->name('checadotp');

Route::get('aceptart', 'TrasladosProximosController@aceptart')->name('aceptart');

Route::get('taceptado', 'TrasladosProximosController@taceptado')->name('taceptado');

Route::get('aceptartt', 'TrasladosProximosController@aceptartt')->name('aceptartt');

//compras proximas

Route::get('cproximas', 'ComprasProximasController@cproximas')->name('cproximas');

Route::get('conceptosc', 'ComprasProximasController@conceptosc')->name('conceptosc');

Route::get('revisarc', 'ComprasProximasController@revisarc')->name('revisarc');

Route::get('conceptoscp', 'ComprasProximasController@conceptoscp')->name('conceptoscp');

Route::get('checadocp', 'ComprasProximasController@checadocp')->name('checadocp');

Route::get('aceptarc', 'ComprasProximasController@aceptarc')->name('aceptarc');

Route::get('aceptarcc', 'ComprasProximasController@aceptarcc')->name('aceptarcc');


Route::get('caceptada', 'ComprasProximasController@caceptada')->name('caceptada');




// aqui termino de traslados

// MAXIMOS Y MINIMOS
Route::get('max_min','MaximosMinimosController@max_min')->name('max_min');

Route::get('add_max_min','ProductController@add_Max_min')->name('add_max_min');

Route::get('/findProduct','ProductController@findProduct');

Route::post('new_max_min','ProductController@new_max_min');

Route::get('updating_max_min','ProductController@updating_max_min');

Route::post('update_max_min','ProductController@update_max_min');

Route::get('eliminar_max_min','ProductController@eliminar_max_min');


// RUTAS CON LIBERACIÓN DE INTERFACES. 
// LOS NUMEROS HACEN REFERENCIA A SUS ID'S EN LA TABLA "INTERFAZS" EN LA BD


// INTERFAZ 1
Route::get('newusers', 'UsersController@nuevo')->name('newusers');
Route::get('newusers2', 'UsersController@newusers2')->name('newusers2');

// INTERFAZ 2
Route::get('users', 'UsersController@index')->name('users');

// INTERFAZ 3
 Route::get('pedidos','PedidosController@pedidos')->name('pedidos');

// INTERFAZ 4
Route::get('info_pedido','PedidosController@info_pedido')->name('verpedido');

// INTERFAZ 5
Route::get('charts','PagesController@charts' )->name('charts');

// INTERFAZ 6
Route::get('proveedores','ProviderController@proveedores')->name('proveedores');


// INTERFAZ 7
// MODIFICAR Y ELIMINAR EMPLEADOS

// INTERFAZ 8
// BOTON EMPLEADOS

// INTERFAZ 9
Route::get('permisos','PermissionController@permisos')->name('permisos');
Route::get('permisos2','PermissionController@permisos2')->name('permisos2');

// INTERFAZ 10
// MODIFICAR PERMISO

// INTERFAZ 11
// BOTON PEDIDOS

// INTERFAZ 12
// Route::get('pedpentiendes','PermissionController@pedpentiendes')->name('pedpentiendes')->middleware('MWpedpendientes');

// INTERFAZ 13
// ASIGNAR PEDIDO

// INTERFAZ 14
// Route::get('pedasignados','PermissionController@pedasignados')->name('pedasignados')->middleware('MWpedasignados');

// INTERFAZ 15
// Route::get('pedremisionados','PermissionController@pedremisionados')->name('pedremisionados')->middleware('MWpedremisionados');

// INTERFAZ 16
// ASIGNAR PEDIDO A UN CONDUCTOR

// INTERFAZ 17
// VER DEVOLUCIONES
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 18
// AGREGAR DEVOLUCION

// INTERFAZ 19
// VER CANCELACIONES
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 20
// AGREGAR CANCELACIONES

// INTERFAZ 21
// BOTON TRANSACCIONES

// INTERFAZ 22
// VER TRANSACCIONES
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 23
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 24
// ASIGNAR TRANSACCION A SURTIDOR

// INTERFAZ 25
// VER TRANSACCION ASIGNADA

// INTERFAZ 26
// VER TRANSACCIONES REMISIONADAS
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 27
// ASIGNAR TRANSACCIONES A UN CONDUCTOR

// INTERFAZ 28
// VER DEVOLUCIONES
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 29
// AGREGAR DEVOLUCION

// INTERFAZ 30
// VER CANCELACIONES
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 31
// AGREGAR CANCELACION

// INTERFAZ 32
// BOTON DE COMPRAS

// INTERFAZ 33
// VER TODAS LAS ORDENES DE COMPRAS// Route::get('','@')->name('')->middleware('');

// INTERFAZ 34
// VER ORDENES DE COMPRA PENDIENTES

// INTERFAZ 35
// ACEPTAR O RECHAZAR ORDEN DE COMPRA

// INTERFAZ 36
// CREAR ORDEN DE COMRPA

// INTERFAZ 37
// VER DEVOLUCIONES
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 38
// BOTON INVENTARIO

// INTERFAZ 39
// VER INVENTARIO

// INTERFAZ 40
// VER INVENTARIO DE OTRA SUCURSAL

// INTERFAZ 41
// VER MAXIMOS Y MINIMOS
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 42
// VER ESTADISTICAS
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 43
// BOTON PRODUCTOS

// INTERFAZ 44
// VER CATALOGO DE PRODUCTOS
Route::get('productos','ProductController@productos')->name('productos')->middleware('MWproductos');

// INTERFAZ 45
// AGREGAR PRODUCTOS
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 46
// MODIFICAR Y ELIMINAR PRODUCTO

// INTERFAZ 47
// VER MARCAS
Route::get('marca','ProductController@marca')->name('marca')->middleware('MWmarcas');

// INTERFAZ 48
// AÑADIR MARCA
// Route::get('','@')->name('')->middleware('');

// INTERFAZ 49
// MODIFICAR Y ELIMINAR MARCA

// INTERFAZ 50
// VER LINEAS Y SUBLINEAS
Route::get('lineas', 'ProductController@lineas')->name('lineas');

// INTERFAZ 51
// Route::get('','@')->name('')->middleware('');
// AÑADIR LINEA Y SUBLINEA

// INTERFAZ 52
// MODIFICAR Y ELIMINAR LINEA Y SUBLINEA

// INTERFAZ 53
// BOTON PROVEEDORES

// INTERFAZ 54
// AÑADIR PROVEEDOR
Route::get('newprovider','ProviderController@newprovider')->name('newprovider');
// INTERFAZ 55
// MODIFICAR Y ELIMINAR PROVEEDOR

// INTERFAZ 56
// MODIFICAR INVENTARIO



Route::get('sucursales', 'SucursalController@sucursales')->name('sucursales');

Route::get('editarsucursal', 'SucursalController@editarsucursal')->name('editarsucursal');

Route::post('editar_sucursal', 'SucursalController@editar_sucursal')->name('editar_sucursal');

Route::get('newsucursal', 'SucursalController@newsucursal')->name('newsucursal');

Route::get('newsucursal', 'SucursalController@newsucursal')->name('newsucursal');

Route::post('crear_sucursal', 'SucursalController@crear_sucursal')->name('crear_sucursal');

Route::post('eliminarsucursal', 'SucursalController@eliminarsucursal')->name('eliminarsucursal');


//CLIENTES
Route::get('clientes', 'ClientesController@clientes')->name('clientes');

Route::get('editar_cliente', 'ClientesController@editar_cliente')->name('editar_cliente');

Route::post('editarclient', 'ClientesController@editarclient')->name('editarclient');

Route::get('agregar_cliente', 'ClientesController@vistanewcliente')->name('vistanewcliente');

Route::post('new_cliente', 'ClientesController@new_cliente')->name('new_cliente');

Route::post('eliminarcliente', 'ClientesController@eliminarcliente')->name('eliminarcliente');

Route::get('sucursalexcel', 'SucursalController@excel')->name('sucursalexcel');


// PEDIDOS

Route::get('new_pedido', 'PedidosController@new_pedido')->name('new_pedido');

Route::post('datos_cliente', 'PedidosController@datos_cliente')->name('datos_cliente');

Route::get('todos', 'PedidosController@todos')->name('todos');

Route::get('infopedido', 'PedidosController@info')->name('infopedido');

Route::get('observaciones_pedido', 'PedidosController@observaciones_pedido')->name('observaciones_pedido');

Route::get('cancelar_pedido', 'PedidosController@cancelar_nota')->name('cancelar_pedido');



//agregar_concepto

Route::post('agregar_concepto', 'PedidosController@agregar_concepto')->name('agregar_concepto');

Route::get('ver_pedidos', 'PedidosController@ver_pedidos')->name('ver_pedidos');

Route::get('act_cantidad', 'PedidosController@act_cantidad')->name('act_cantidad');



Route::get('ver_pedidos', 'PedidosController@ver_pedidos')->name('ver_pedidos');

Route::get('tabla_dinamica', 'PedidosController@tabla_dinamica')->name('tabla_dinamica');

Route::post('eliminar_concepto', 'PedidosController@eliminar_concepto')->name('eliminar_concepto');

//imprimir_pedido

Route::get('nota_pedido', 'PedidosController@imprimir')->name('nota_pedido');

Route::post('imprimirSiNo', 'PedidosController@imprimirSiNo')->name('imprimirSiNo');





Route::get('ver_remisionados', 'PedidosController@ver_remisionados')->name('ver_remisionados');

Route::get('pedidos_pendientes', 'PedidosController@pedidos_pendientes')->name('pedidos_pendientes');

Route::get('pedidos_cancelados', 'PedidosController@pedidos_cancelados')->name('pedidos_cancelados');

//compras

Route::get('calendariocompras', 'ComprasController@calendariocompras')->name('calendariocompras');

Route::get('compras', 'ComprasController@compras')->name('compras');

Route::get('maximos', 'ComprasController@maximos')->name('maximos');

Route::post('datos_compra', 'ComprasController@datos_proveedor')->name('datos_proveedor');

Route::get('new_compra', 'ComprasController@new_compra')->name('new_compra');

Route::post('prueba_bb', 'ComprasController@prueba_bb')->name('prueba_bb');

Route::get('ordencompras', 'ComprasController@ordencompras')->name('ordencompras');

Route::get('graficas', 'ComprasController@graficas')->name('graficas');

//// rutas de compras para realizar orden de compra
Route::post('agregar_concepto_compras', 'ComprasController@agregar_concepto')->name('agregar_concepto_compras');

Route::get('tabla_dinamica_compras', 'ComprasController@tabla_dinamica')->name('tabla_dinamica_compras');

Route::post('eliminar_concepto_compras', 'ComprasController@eliminar_concepto')->name('eliminar_concepto_compras');

Route::get('act_cantidad_compras', 'ComprasController@act_cantidad')->name('act_cantidad_compras');

Route::get('nota_compras', 'ComprasController@nota')->name('nota_compras');

Route::get('imprimirSiNo_compras', 'ComprasController@imprimirSiNo')->name('imprimirSiNo_compras');

Route::get('observaciones_compras', 'ComprasController@observaciones')->name('observaciones_compras');

Route::get('cancelar_compras', 'ComprasController@cancelar_nota')->name('cancelar_compras');



//calendar 
Route::resource('tasks', 'TasksController');

//TasksController

Route::get('taskindex', 'TasksController@taskindex')->name('taskindex');

Route::post('indexcharts', 'ChartsController@indexcharts')->name('indexcharts');

Route::get('backlogindex', 'BitacoraController@backlogindex')->name('backlogindex');

Route::get('backlogAll', 'BitacoraController@backlogAll')->name('backlogAll');


//DASHBOARD 
Route::get('cuatro', 'DashboardController@cuatro')->name('cuatro');

//estadisticas

Route::get('estadisticas', 'InventariosController@estadisticas')->name('estadisticas');

Route::get('historicos', 'HistoricosController@historicos')->name('historicos');

Route::post('historicosbusqueda', 'HistoricosController@historicosbusqueda')->name('historicosbusqueda');


Route::get('exceli', 'InventariosController@excel')->name('exceli');

Route::get('excelh', 'HistoricosController@excel')->name('excelh');



Route::post('maximos_hist', 'HistoricosController@maximos_hist')->name('maximos_hist');


Route::get('generatepdf', 'HistoricosController@generatepdf')->name('generatepdf');
//maximos_hist


//productos sat
Route::get('indexsat','ProductSATController@indexsat')->name('indexsat');
Route::post('nuevosat','ProductSATController@nuevosat')->name('nuevosat');
Route::post('eliminarsat','ProductSATController@eliminarsat')->name('eliminarsat');
Route::post('editarsat','ProductSATController@editarsat')->name('editarsat');


// BUSCADOR

Route::get('buscador','PagesController@buscador')->name('buscador');