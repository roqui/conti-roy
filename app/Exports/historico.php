<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class historico implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $historico= \DB::table('historicos')->select('historico.*')->get();
        return $historico;
    }
}
