<?php

namespace App\Exports;

use App\inventario;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\User;
use Illuminate\Support\Facades\Auth;

class InventariosExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$inventarios = \DB::table('inventarios')
->join('productos', 'productos.id', '=', 'inventarios.id_producto')
->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
->join('marcas', 'marcas.id', '=', 'productos.id_marca')
->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')

->select('productos.codigo', 'productos.id as id_product', 'proveedores.nombre as proveedor',  'marcas.descripcion', 'sublineas.descrip', 'sublineas.id_linea', 'lineas.descr', 'inventarios.created_at', 'inventarios.*', 'productos.descripcion_producto')

->where('inventarios.id_sucursal', '=', Auth::user()->idSucursal)
->get();
        return $inventarios;
    }
}
