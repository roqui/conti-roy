<?php

namespace App\Exports;

use App\Provider;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProvidersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(){
    	return ['ID','Nombre', 'RFC', 'Calle', 'Numero Dir.', 'Observaciones', 'Teléfono', 'E-mail', 'Alta en: '];
    }
    public function collection()
    {
    	$proveedores = \DB::table('proveedores')->select('id','nombre','rfc','calle', 'numerodir','observaciones','telefono','correo','created_at')->get();
        return $proveedores;
    }
}
