<?php

namespace App\Exports;

use App\destino;
use Maatwebsite\Excel\Concerns\FromCollection;

class SucursalesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $sucursales = \DB::table('destinos')->get();

        return($sucursales);
    }
}
