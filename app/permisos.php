<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permisos extends Model
{
    public function scopeBuscar($query,$id)
    {
        // return $query->where('active', 1);

        // return $query->where([['idInterfaz',$view],['idUsuario',$user]])->count();

        return $query->select('idUsuario','idInterfaz','descripcion','url')->join('interfazs','interfazs.id','=','idInterfaz')->where('idUsuario',$id);

    }
}
