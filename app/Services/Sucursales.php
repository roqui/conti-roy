<?php

namespace App\Services;

use App\destino;

class Sucursales
{
    public function get()
    {
        $sucursales = destino::get();
        $sucursalesArray[''] = 'Selecciona una sucursal';
        foreach ($sucursales as $sucursal) {
            $sucursalesArray[$sucursal->id] = $sucursal->nombre;
        }
        return $sucursalesArray;
    }
}