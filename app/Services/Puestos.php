<?php

namespace App\Services;

use App\puesto;

class Puestos
{
    public function get()
    {
        $puestos = puesto::get();
        $puestosArray[''] = 'Selecciona un puesto';
        foreach ($puestos as $puesto) {
            $puestosArray[$puesto->id] = $puesto->descripcion;
        }
        return $puestosArray;
    }
}