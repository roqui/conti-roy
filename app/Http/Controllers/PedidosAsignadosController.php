<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;
use App\Exports\InventariosExport;
use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\inventaio;

class PedidosAsignadosController extends Controller
{
    public function pasignados() {

    	if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $pasignados=\DB::table('surtirs')
        					->join('pedidos', 'pedidos.id', 'surtirs.idPedido')
        					->join('destinos', 'destinos.id', 'pedidos.id_origen')
                        	->join('clientes', 'clientes.id', 'pedidos.id_destino')
                        	->select('pedidos.id','pedidos.CostoT','destinos.nombre as origen', 'clientes.nombre as destino', 'pedidos.created_at')
                        	->where([['surtirs.idSurtidor','=',$id],['surtirs.estado','=','INICIADO']])->get();

    	return view('Pedidos/pasignados', ['permisos'=>$permisos, 'pasignados'=>$pasignados]);
    }

    public function checar(Request $request) {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $conceptos=\DB::table('concepts')->select('concepts.id','concepts.idProducto','productos.codigo','productos.descripcion_producto','concepts.cantidad','productos.precio')
                                    ->join('productos', 'concepts.id', 'productos.id')
                                    ->where('idPedido','=',$request->idpedido)->get();
        $idSurtido=\DB::table('surtirs')->select('id')->where('idPedido','=',$request->idpedido)->first();

        $already=\DB::table('checars')->select('id')->where('idSurtido','=',$idSurtido->id)->first();

        if(!$already){
            \DB::table('checars')->insert(['idSurtido'=>$idSurtido->id,'created_at'=>date('Y-m-d H:i:s')]);
        }

        return view('Pedidos/checar', ['permisos'=>$permisos, 'conceptos'=>$conceptos, 'idpedido'=>$request->idpedido]);
    }

    public function conceptosa(Request $request) {
        $data="";
        $con=\DB::table('concepts')->select('concepts.id','productos.codigo','productos.descripcion_producto','concepts.cantidad','concepts.cantidadcheck','productos.precio')
                                    ->join('productos', 'concepts.idProducto', 'productos.id')
                                    ->where([['idPedido','=',$request->id],
                                             ['estado','=','1']   
                                            ])->get();

        $data="<div class='table-responsive'>
                          <table class='table table-bordered' id='dataTable' width='100%' cellspacing='0'>
                            <thead>
                    <tr>
                      <th scope='col' >ID</th>
                      <th scope='col' >Código</th>
                      <th scope='col'>Descripcion</th>
                      <th scope='col'>#</th>
                      <th scope='col'># Checada</th>
                      <th scope='col'>$/U</th>
                      <th scope='col'>$</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    ";
        $totalart=0;
        $totalcost=0;

                      foreach ($con as $c){
                        $totalcost=$totalcost+$c->precio*$c->cantidad;
                        $totalart=$totalart+$c->cantidad;
                        $data=$data."<tr><th scope='row'>".$c->id."</th>
                                <td>".$c->codigo."</td>
                                <td>".$c->descripcion_producto."</td>
                                <td>".$c->cantidad."</td>
                                <td>".$c->cantidadcheck."</td>
                                <td>".$c->precio."</td>
                                <td>$".$c->precio*$c->cantidad.".00</td>
                                </tr>";
                      }
                        
        $data=$data."<tr><td class='table-primary'></td><td class='table-primary'></td><td class='table-primary'></td><td class='table-primary'>Total de artículos:</td><td class='table-primary'>".$totalart."</td><td class='table-primary'>$ Total:</td><td class='table-primary'>$".$totalcost.".00</td></tr>
                    </tbody></table>";
        return response()->json($data); //este metodo imprime la primera tabla


    }

    public function checado(Request $request){//en este hago consultas de los productos checados 
        
        $concepto = \DB::table('concepts')->join('productos', 'productos.id', 'concepts.idProducto')->select('concepts.id', 'concepts.cantidad','concepts.cantidadcheck','concepts.estado', 'concepts.idProducto')->where([['productos.codigo', '=', $request->codigo], ['concepts.idPedido','=',$request->pedido]])->first();

        if(!$concepto){
            $data=" No lo encontré ):"; 
        }else{
            $data=" lo encontré! (:"; 
            
            $affected =  \DB::table('concepts')->where('id', $concepto->id)->update(['cantidadcheck' => $concepto->cantidadcheck+1]);//incremento en el check

            $actualcheck = \DB::table('concepts')->select( 'cantidadcheck')->where('id', $concepto->id)->first();

            $cant=\DB::table('concepts')->select('cantidad')->where([['idProducto', $concepto->idProducto], ['idPedido','=',$request->pedido]])->get();
            $totalcant=0;
            foreach ($cant as $c) {
              $totalcant= $totalcant+$c->cantidad;
            }

           $check=\DB::table('concepts')->select('cantidadcheck')->where([['idProducto', $concepto->idProducto], ['idPedido','=',$request->pedido]])->GET();
           $totalcheck=0;
           foreach ($check as $c ) {
             $totalcheck=$totalcheck+$c->cantidadcheck;
           }

            if($concepto->estado==1){

                if ($concepto->cantidad>$concepto->cantidadcheck) {
                
                    $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz checado un nuevo artículo del '".$request->codigo.".'</div>";

                    if ($concepto->cantidad==$actualcheck->cantidadcheck) {
                       $affected =  \DB::table('concepts')->where('id', $concepto->id)->update(['estado' => '2']);
                       
                        $msj="<div class='alert alert-success' role='alert'>¡Listo!, tienes todos los artículos con código '".$request->codigo."' que necesitas.</div>";
                    }

                }   

            }else{

                $concepto2 = \DB::table('concepts')->join('productos', 'productos.id', 'concepts.idProducto')->select('concepts.id', 'concepts.cantidad','concepts.cantidadcheck','concepts.estado')->where([['productos.codigo', '=', $request->codigo], ['concepts.idPedido','=',$request->pedido],['estado','=','1']])->first();
                if(!$concepto2){

                    $diferencia=$totalcheck-$totalcant;
                    $msj="<div class='alert alert-danger' role='alert'>¡Espera!, ¡Llevas ".$diferencia." producto(s) de más!.</div>";

                }else{
                    $affected =  \DB::table('concepts')->where('id', $concepto2->id)->update(['cantidadcheck' => $concepto2->cantidadcheck+1]);//incremento en el check

                    $actualcheck = \DB::table('concepts')->select( 'cantidadcheck')->where('id', $concepto2->id)->first();

                    if ($concepto2->cantidad>$concepto2->cantidadcheck) {
                
                        $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz checado un nuevo artículo del '".$request->codigo.".'</div>";

                        if ($concepto2->cantidad==$actualcheck->cantidadcheck) {
                           $affected =  \DB::table('concepts')->where('id', $concepto2->id)->update(['estado' => '2']);
                           
                            $msj="<div class='alert alert-success' role='alert'>¡Listo!, tienes todos los artículos con código '".$request->codigo."' que necesitas.</div>";
                        }

                    }else{

                    } 

                }


                
            }

        }
        

         return response()->json($msj);
    }

    public function finchecar(Request $request){

        if(Auth::guard()->check()==null){
                    return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $id_sucursal = Auth::user()->idSucursal;

        \DB::table('pedidos')->where('id', '=', $request->idpedido)->update(['Estado'=>'3']);

        $conceptos = \DB::table('concepts')->select('estado', 'idProducto', 'cantidad')->where('idPedido','=',$request->idpedido)->get();


        foreach ($conceptos as $c) {
            if($c->estado=='1'){

                return view('Pedidos/checar', ['permisos'=>$permisos, 'idpedido'=>$request->idpedido])->with('Mensajeu', '¡Lo sentimos!, al paracer aún te faltan productos.');
            }

            
        }

        foreach ($conceptos as $c) {
            
            
            $cant=\DB::table('inventarios')->select('id', 'cant_apartada')->where([['id_producto','=',$c->idProducto],['id_sucursal', '=', $id_sucursal]])->first();

            $affected =  \DB::table('inventarios')->where([['id_producto','=',$c->idProducto],['id_sucursal', '=', $id_sucursal]])->update(['cant_apartada'=>$cant->cant_apartada-$c->cantidad]);//incremento en el check  
        }

        $surtido=\DB::table('surtirs')->where('idPedido','=',$request->idpedido)->update(['estado'=>'FINALIZAD0']);

        return view('Pedidos/finpedido', ['permisos'=>$permisos]);

    }


}
