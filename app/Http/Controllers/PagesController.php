<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\permisos;

class PagesController extends Controller
{
	

// ************************************************************************************************************
// ************************************************************************************************************
	public function cuatro(){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		// Variables para liberacion de interfaces-----------------------------------
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();
		// --------------------------------------------------------------------------
		return view('404',['permisos'=>$permisos]);
	}

	public function index(){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		// Variables para liberacion de interfaces-----------------------------------
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();

		$pedidos=\DB::table('pedidos')
		->join('destinos', 'pedidos.id_origen', '=', 'destinos.id')
		->join('clientes', 'clientes.id', '=', 'pedidos.id_destino')
		->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
		->orderBy('pedidos.id', 'desc')
		->select('pedidos.id', 'destinos.nombre', 'clientes.nombre as cliente', 'estadopedidos.descripcion as Estado')
		->limit(5)
		->get();

		$cuentaclientes=\DB::table('clientes')
		->count();

		$cuentaproveedores=\DB::table('proveedores')
		->count();

		$cuentausuarios=\DB::table('users')
		->count();

		$cuentaproductos=\DB::table('productos')
		->count();


		$pedidos2=\DB::table('pedidos')
		->count();

		$pendiente = \DB::table('pedidos')
		->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
		->where('Estado', '=', 1)
		->count();

		$surtiendo = \DB::table('pedidos')
		->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
		->where('Estado', '=', 2)
		->count();

		$remisionado = \DB::table('pedidos')
		->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
		->where('Estado', '=', 3)
		->count();

		$enviado = \DB::table('pedidos')
		->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
		->where('Estado', '=', 4)
		->count();

		$cancelado = \DB::table('pedidos')
		->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
		->where('Estado', '=', 5)
		->count();



		$pieY=array();
		$pieY[]=$pendiente;
		$pieY[]=$surtiendo;
		$pieY[]=$remisionado;
		$pieY[]=$enviado;
		$pieY[]=$cancelado;

		$piejsonY=json_encode($pieY);


		$pieX=array();
		$pieX[]='Pendiente';
		$pieX[]='Surtiendo';
		$pieX[]='Remisionado';
		$pieX[]='Enviado';
		$pieX[]='Cancelado';


		$piejsonX=json_encode($pieX);


		$transaccions=\DB::table('transaccions')
		->join('destinos', 'transaccions.id_origen', '=', 'destinos.id')
		->join('clientes', 'clientes.id', '=', 'transaccions.id_destino')
		->join('estadopedidos', 'estadopedidos.id', '=', 'transaccions.Estado')
		->orderBy('transaccions.id', 'desc')
		->select('transaccions.id', 'destinos.nombre', 'clientes.nombre as cliente', 'estadopedidos.descripcion as Estado')
		->limit(5)
		->get();

		$pendiente = \DB::table('transaccions')
		->join('estadopedidos', 'estadopedidos.id', '=', 'transaccions.Estado')
		->where('Estado', '=', 1)
		->count();

		$surtiendo = \DB::table('transaccions')
		->join('estadopedidos', 'estadopedidos.id', '=', 'transaccions.Estado')
		->where('Estado', '=', 2)
		->count();

		$remisionado = \DB::table('transaccions')
		->join('estadopedidos', 'estadopedidos.id', '=', 'transaccions.Estado')
		->where('Estado', '=', 3)
		->count();

		$enviado = \DB::table('transaccions')
		->join('estadopedidos', 'estadopedidos.id', '=', 'transaccions.Estado')
		->where('Estado', '=', 4)
		->count();

		$cancelado = \DB::table('transaccions')
		->join('estadopedidos', 'estadopedidos.id', '=', 'transaccions.Estado')
		->where('Estado', '=', 5)
		->count();



		$pieY2=array();
		$pieY2[]=$pendiente;
		$pieY2[]=$surtiendo;
		$pieY2[]=$remisionado;
		$pieY2[]=$enviado;
		$pieY2[]=$cancelado;

		$piejsonY2=json_encode($pieY2);


		$pieX2=array();
		$pieX2[]='Pendiente';
		$pieX2[]='Surtiendo';
		$pieX2[]='Remisionado';
		$pieX2[]='Enviado';
		$pieX2[]='Cancelado';


		$piejsonX2=json_encode($pieX2);


		$realizada = \DB::table('ordencompras')
		->join('estadoords', 'estadoords.id', '=', 'ordencompras.estado')
		->where('estado', '=', 1)
		->count();

		$aceptada = \DB::table('ordencompras')
		->join('estadoords', 'estadoords.id', '=', 'ordencompras.estado')
		->where('estado', '=', 2)
		->count();

		$rechazada = \DB::table('ordencompras')
		->join('estadoords', 'estadoords.id', '=', 'ordencompras.estado')
		->where('estado', '=', 3)
		->count();

		$pieY3=array();
		$pieY3[]=$realizada;
		$pieY3[]=$aceptada;
		$pieY3[]=$rechazada;

		$piejsonY3=json_encode($pieY3);

		$pieX3=array();
		$pieX3[]='Realizada';
		$pieX3[]='Aceptada';
		$pieX3[]='Rechazada';
		

		$piejsonX3=json_encode($pieX3);

		$lineas = \DB::table('lineas')
		->select('descr')
		->get();



		$pieX4=array();
        $pieY4=array();

		foreach ($lineas as $linea) {
			$contar = \DB::table('productos')
			->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
			->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')
			->where('lineas.descr', '=', $linea->descr)
			->count('productos.id');

			$pieX4[]=$contar;

		}


		foreach ($lineas as $linea) {
			$contar = \DB::table('lineas')
			->where('lineas.descr', '=', $linea->descr)
			->first('lineas.descr');



			$pieY4[]=$contar->descr;

		}


		$pieX44=json_encode($pieX4);
		$pieY44=json_encode($pieY4);


		$proveedoresX=array();
        $proveedoresY=array();

        $proveedores = \DB::table('proveedores')
        ->select('nombre')
        ->get();


		foreach ($proveedores as $proveedor) {
			$contar = \DB::table('productos')
			->join('inventarios', 'productos.id', '=', 'inventarios.id_producto')
			->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
			->where('proveedores.nombre', '=', $proveedor->nombre)
			->count('productos.id');

			$proveedoresX[]=$contar;

		}


		foreach ($proveedores as $proveedore) {
			$contar = \DB::table('proveedores')
			->where('nombre', '=', $proveedore->nombre)
			->first('nombre');



			$proveedoresY[]=$contar->nombre;

		}


		$providerX=json_encode($proveedoresX);
		$providerY=json_encode($proveedoresY);


		$permiso=DB::table('permisos')->where([['idinterfaz',56],['idUsuario',Auth::user()->id]])->count();
	

		// --------------------------------------------------------------------------
		if($permisos){
		return view('index', ['permisos'=>$permisos, 'pedidos'=>$pedidos, 'cuentaproductos'=>$cuentaproductos, 'cuentaclientes'=>$cuentaclientes,  'cuentausuarios'=>$cuentausuarios, 'cuentaproveedores'=>$cuentaproveedores,  'piejsonY'=>$piejsonY, 'piejsonX'=>$piejsonX,  'piejsonY3'=>$piejsonY3, 'piejsonX3'=>$piejsonX3,  'piejsonY2'=>$piejsonY2, 'piejsonX2'=>$piejsonX2, 'providerX'=>$providerX, 'providerY'=>$providerY,'pieY4'=>$pieY44,  'pieX4'=>$pieX44, 'transaccions'=>$transaccions]);
	}else{
		return redirect()->route('index2');
	}
	}

// ************************************************************************************************************
// ************************************************************************************************************

	public function blank(){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		// Variables para liberacion de interfaces-----------------------------------
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();
		// --------------------------------------------------------------------------
		return view('blank',['permisos'=>$permisos]);
	}

// ************************************************************************************************************
// ************************************************************************************************************

	public function charts(){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		// Variables para liberacion de interfaces-----------------------------------
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();
		// --------------------------------------------------------------------------
		return view('charts',['permisos'=>$permisos]);
	}

// ************************************************************************************************************
// ************************************************************************************************************

	public function plantilla(){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		// Variables para liberacion de interfaces-----------------------------------
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();
		// --------------------------------------------------------------------------
		return view('plantilla',['permisos'=>$permisos]);
	}

// ************************************************************************************************************
// ************************************************************************************************************

	public function menu(){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		// Variables para liberacion de interfaces-----------------------------------
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();
		// --------------------------------------------------------------------------
		return view('menu',['permisos'=>$permisos]);
	}



// ************************************************************************************************************
// ************************************************************************************************************
	
	public function alerta(Request $request){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		// Variables para liberacion de interfaces-----------------------------------
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();
		// --------------------------------------------------------------------------

		return view('alerta',['permisos'=>$permisos]);
	}

	public function buscador(Request $request){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		// Variables para liberacion de interfaces-----------------------------------
		$url=$request->input('url');
		// --------------------------------------------------------------------------

		return redirect()->route($url);
	}




}
