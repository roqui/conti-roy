<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;
use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;


class BitacoraController extends Controller
{


  //para Ricardio

  public function backlogAll(){
   if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();


  $bitacoras = \DB::table('bitacoras')
  ->join('destinos', 'destinos.id', '=', 'bitacoras.id_sucursal')
  ->join('users', 'users.id', '=', 'bitacoras.id_usuario')
  ->select('bitacoras.*', 'users.usuario', 'destinos.nombre')
  ->get();

  return view ('bitacora.bitacoraAll', [ 'permisos'=>$permisos,  'bitacoras'=>$bitacoras]);

}


//para la sucursal el usuario logeado

public function backlogindex(){
  $id_sucursal = Auth::user()->idSucursal;
   if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();


  $bitacoras = \DB::table('bitacoras')
  ->join('destinos', 'destinos.id', '=', 'bitacoras.id_sucursal')
  ->join('users', 'users.id', '=', 'bitacoras.id_usuario')
  ->select('bitacoras.*', 'users.usuario', 'destinos.nombre')
  ->where('id_sucursal', '=', $id_sucursal)
  ->get();

  return view ('bitacora.bitacora', [ 'permisos'=>$permisos,  'bitacoras'=>$bitacoras]);

}


}