<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;

use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;


class MaximosMinimosController extends Controller
{

    public function max_min()
    {   
        // -------- VALIDANDO SI HAY SESION------------
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        //Variables para liberacion de interfaces
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces

        $id_sucursal = Auth::user()->idSucursal;


        $maximos = \DB::table('inventarios')
        ->join('productos', 'productos.id', '=', 'inventarios.id_producto')
        ->select('inventarios.*', 'productos.codigo',  'productos.codigosat', 'productos.descripcion_producto', 'productos.precioV', 'productos.precioC')
        ->where('inventarios.id_sucursal', '=', $id_sucursal )
        ->get();

        $productos = \DB::table('productos')
        ->get();

        return view('Maximos/maximos',['permisos'=>$permisos, 'maximos'=>$maximos, 'productos'=>$productos]);

    }

}