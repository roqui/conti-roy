<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use Barryvdh\DomPDF\Facade as PDF;

class TrasladosController extends Controller
{
    public function traslados(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

    
                $destinos=\DB::table('destinos')
                ->get();


        return view('traslados/traslados',['permisos'=>$permisos,'destinos'=>$destinos]);
    }

    public function datos_traslado(Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        $date=Carbon::now()->toDateTimeString();
        $id_cliente=$request->input('id');
        $id_origen=Auth::user()->idSucursal;
        $sucursal=\DB::table('destinos')
                ->where('id', '=', $request->input('id') )
                ->first();

        $num_pedido=\DB::table('transaccions')->max('id');
        $num_pedido=$num_pedido+1;
        // echo $num_pedido+1;
        // die();

       
       

        $productos=DB::table('productos')->select('productos.*')
        ->join('inventarios','id_producto','=','productos.id')->where('id_sucursal','=',Auth::user()->idSucursal)
        ->get();

        // \DB::table('pedidos')->insert([
        // ['CostoT' => 0,'id_destino' => $request->input('id'), 'id_origen' => $id_origen, 'Estado' => 1]
        //     ]);

        $tabla=\DB::table('conceptots')->where('id_transaccion',$num_pedido)->get();


                return view('traslados/new_traslado',['permisos'=>$permisos, 'sucursal'=>$sucursal,'date'=>$date, 'productos'=>$productos,'num_pedido'=>$num_pedido,'tabla'=>$tabla,'id_cliente'=>$id_cliente]);





    }

    public function todos()
    {

        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $id_sucursal = Auth::user()->idSucursal;

        $traslados= \DB::table('transaccions')
                        ->join('destinos as origen', 'origen.id', 'transaccions.id_origen')
                        ->join('destinos as destino', 'destino.id', 'transaccions.id_destino')
                        ->select('transaccions.id','transaccions.CostoT','origen.nombre as norigen', 'destino.nombre as ndestino', 'transaccions.created_at', 'transaccions.estado')
                        ->where([
                                 ['id_origen','=',$id_sucursal]
                                ])->get();
        

        return view('traslados/todos', ['permisos'=>$permisos, 'traslados'=>$traslados]);
    }

    public function info(Request $request){

         if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $pedido= \DB::table('transaccions')
                        ->join('destinos as origen', 'origen.id', 'transaccions.id_origen')
                        ->join('destinos as destino', 'destino.id', 'transaccions.id_destino')
                        ->join('estadopedidos', 'estadopedidos.id', 'transaccions.estado') 
                        ->select('transaccions.id','transaccions.CostoT','origen.nombre as norigen', 'destino.nombre as ndestino', 'transaccions.created_at',  'transaccions.created_at', 'transaccions.created_at', 'destino.calle as callec', 'destino.ciudad as ciudadc')
                        ->where([
                                 ['transaccions.id','=',$request->idpedido]
                                ])->first();
        $surtirs = \DB::table('surtirts')
                        ->join('users', 'surtirts.id_surtidor', 'users.id')
                        ->select('surtirts.*', 'users.name')->where('id_transaccion', '=', $request->idpedido)->first();
        $checars="";
        $envios="";

        if($surtirs){
            $checars = \DB::table('checarts')->select('checarts.*')->where('idSurtido','=',$surtirs->id)->first();

            if($checars){
                $envios =  \DB::table('enviarts')
                ->join('users', 'users.id', 'enviarts.idConductor')
                ->select('enviarts.*', 'users.name')
                ->where('idChecado','=',$checars->id)->first();
            }
        }
        

        


        return view('Traslados/infotraslado',['permisos'=>$permisos, 'p'=>$pedido, 's'=>$surtirs, 'c'=>$checars, 'e'=>$envios]);

    }


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////// A PARTIR DE AQUI COMIENZA LO CHIDO DE HACER TRASLADOS/////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////// /////////////////////////////////////////////////////////////////////////////////////////// / 


    public function agregar_concepto(Request $request){

         if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        $date=Carbon::now();
        

        // $json_array = array(); 
        
        $productos=json_decode($request->input('id_producto'));
        // $json_array = $productos;
        // $json_Array[]
        // echo json_encode($productos{0});


        // -------------------------------------------
        DB::table('transaccions')->insertOrIgnore([
            ['id'=>$request->input('num_pedido'),
            'CostoT'=>0,
            'Estado'=>1,
            'id_origen'=>Auth::user()->idSucursal,
            'id_destino'=>$request->input('id_cliente'),
            'created_at'=>$date,
            'usuario'=>Auth::user()->name]

        ]); 
        
        // ----------------------------------------------

        for ($i=0; $i < count($productos); $i++){
                $inventario=DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$productos{$i}]])->first();
                
        
                $precio=DB::table('productos')->where('id',$productos{$i})->get();
                foreach ($precio as $key => $value) {
                    $precioU=$value->precio;
                }
                // -------------------------------------------------
                $acumulado=DB::table('transaccions')->where('id', $request->input('num_pedido'))->get();

                foreach ($acumulado as $key => $value) {
                    $acumulado2=$value->CostoT;
                }
                // -------------------------------------------------
                $total=$precioU+$acumulado2;
                $cant_disponible=$inventario->cant_disponible;
                $minimo=$inventario->minimos;
                if($cant_disponible>0){
                    if($cant_disponible<$minimo-1){
                        echo "Alerta! Estas en el minímo de tu producto!";
                    }else{
                        echo "Concepto agregado. inventario disponible: ".($cant_disponible-1)."\n"; 
                    }
                        DB::table('transaccions')
                              ->where('id', $request->input('num_pedido'))
                              ->update(['CostoT' => $total]);
                        DB::table('conceptots')->insertOrIgnore([
                            ['id_transaccion' => $request->input('num_pedido'),
                            'idProducto'=> $productos{$i},
                            'cantidad'  => 1,
                            'subtotal'  => $precioU,
                            'estado'    => 1,
                            'estador'    => 'SREVISAR',
                            'created_at'=> $date]
                        ]);
                        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$productos{$i}]])
                            ->update(['cant_disponible'=>$cant_disponible-1]);
                        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$productos{$i}]])
                            ->update(['cant_apartada'=>$inventario->cant_apartada+1]); 
                        }else{
                            echo "Ya no tienes suficiente inventario! de uno de estos productos :( Realiza una compra!";
                            die();
                        } 
            }
        }
        
    // ************************************************************************************************************
// ************************************************************************************************************

    public function tabla_dinamica(Request $request){
         if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // ------------------------------------------------
        $pedido=$request->input('pedido');
        $num_pedido=$_GET['num_pedido'];
        $pedido=DB::table('conceptots')
        ->select('conceptots.id','idProducto','cantidad','unidad','p.codigo','p.descripcion_producto','p.precio','descuento')
        ->join('productos as p','p.id','=','idProducto')
        ->where('id_transaccion','=',$num_pedido)
        ->get();
        $total=DB::table('transaccions')->where('id',$num_pedido)->get();
        // return view('Pedidos.tabla_dinamica',['permisos'=>$permisos,'pedido'=>$pedido]);
        return view('traslados.tabla_dinamica',['permisos'=>$permisos,'pedido'=>$pedido,'id'=>$num_pedido,'total'=>$total]);

    }

    // ************************************************************************************************************
// ************************************************************************************************************


    public function eliminar_concepto(Request $request){
        $id_concepto=$request->input('id_concepto');
        $concepto=DB::table('conceptots')->where('id',$id_concepto)->get();

        foreach ($concepto as $key => $value) {
            $id_pedido=$value->id_transaccion;
            $subtotal=$value->subtotal;
            $cantidad=$value->cantidad;
            $id_producto=$value->idProducto;
        }

        $pedido=DB::table('transaccions')->where('id',$id_pedido)->get();

        foreach ($pedido as $key => $value) {
            # code...
            //se supone que deberia restar el subtotal de la cuenta pero ne
            $total=$value->CostoT;
        }

        $total_final=($total-$subtotal);
        $inventario=DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])->first();
        DB::table('transaccions')->where('id',$id_pedido)->update(['CostoT'=>$total_final]);
        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])->update(['cant_disponible'=>$inventario->cant_disponible+$cantidad]);
        DB::table('conceptots')->where('id',$id_concepto)->delete();
        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                            ->update(['cant_apartada'=>$inventario->cant_apartada-$cantidad]); 
        
        
    }

    
// ************************************************************************************************************
// ************************************************************************************************************



    public function nota(Request $request){
         if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // ------------------------------------------------
        $id_pedido=$request->input('id_pedido');
        $pedido=DB::table('transaccions')->where('id',$id_pedido)->get();
        foreach ($pedido as $key => $value) {
             $id=$value->id;
             $id_sucursal=$value->id_origen;
             $id_cliente=$value->id_destino;
             $date=$value->created_at;
             $total=$value->CostoT;
             $usuario=$value->usuario;

        }
        $cliente=DB::table('destinos')->where('id',$id_cliente)->first();
        $sucursal=DB::table('destinos')->where('id',$id_sucursal)->first();
        $tabla=DB::table('conceptots')
        ->select('conceptots.id','idProducto','cantidad','unidad','p.codigo','p.descripcion_producto','p.precio')
        ->join('productos as p','p.id','=','idProducto')
        ->where('id_transaccion','=',$id_pedido)
        ->get();



        
        $pdf=PDF::loadView('traslados.nota_traslados',['permisos'=>$permisos,'pedido'=>$pedido,'sucursal'=>$cliente,'date'=>$date,'tabla'=>$tabla,'total'=>$total,'usuario'=>$usuario,'num_pedido'=>$id]);

            
            // return view('traslados.nota_traslados',['permisos'=>$permisos,'pedido'=>$pedido,'sucursal'=>$cliente,'date'=>$date,'tabla'=>$tabla,'total'=>$total,'usuario'=>$usuario,'num_pedido'=>$id]);

            // return view('pedidos/new_pedido',['permisos'=>$permisos, 'cliente'=>$cliente,'date'=>$date, 'productos'=>$productos,'num_pedido'=>$num_pedido,'tabla'=>$tabla,'id_cliente'=>$id_cliente]);

            
                 return $pdf->download('nota_traslado.pdf');
            

           

    }

    public function imprimirSiNo(Request $request){
        $id_pedido=$request->id_pedido;

        $resp=DB::table('transaccions')->where('id',$id_pedido)->count();
        

        return $resp;
    }

    public function act_cantidad(Request $request){
        $id_concepto=$request->input('id_concepto');
        $cantidad_actual=$request->input('actual');
        $accion=$request->input('accion');
        $id_pedido=$request->input('id_pedido');
        $id_producto=$request->input('id_producto');
        $precio=$request->input('precio');

        // --------------------------------
        $total_concepto=DB::table('conceptots')->select('subtotal')->where('id',$id_concepto)->get();
        $total_pedido=DB::table('transaccions')->select('CostoT')->where('id',$id_pedido)->get();
        $inventario=DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])->first();
        $cant_disponible=$inventario->cant_disponible;
        $cant_apartada=$inventario->cant_apartada;
        $minimo=$inventario->minimos;
        
        foreach ($total_concepto as $key => $value) {
            $total_concepto2=($value->subtotal);
        }
        foreach ($total_pedido as $key => $value) {
            $total_pedido2=($value->CostoT);
            
        }

        if($accion==2){
                if($cant_disponible>0){
                    if($cant_disponible<=$minimo){
                        echo "Alerta! Estas en el minímo de tu producto!     ";
                    }else{
                        echo "\nConcepto agregado. inventario disponible: ".($cant_disponible-1)."\n"; 
                    }
                        DB::table('conceptots')->where('id',$id_concepto)->update(['subtotal'=>$total_concepto2+$precio]);
                        DB::table('conceptots')->where('id',$id_concepto)->update(['cantidad'=>$cantidad_actual+1]);
                        DB::table('transaccions')->where('id',$id_pedido)->update(['CostoT'=>$total_pedido2+$precio]);
                        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                            ->update(['cant_disponible'=>$inventario->cant_disponible-1]);
                        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                            ->update(['cant_apartada'=>$inventario->cant_apartada+1]);
                    }else{
                    echo "Ya no tienes suficiente inventario! de uno de estos productos :( Realiza una compra!";
                    }

        }elseif ($accion==1) {
            if($cantidad_actual<=0){
                echo "Ya no tienes podructo en tu concepto. Se eliminará automaticamente.";
                DB::table('conceptots')->where('id',$id_concepto)->delete();
            }else{
                DB::table('conceptots')->where('id',$id_concepto)->update(['subtotal'=>$total_concepto2-$precio]);
                DB::table('conceptots')->where('id',$id_concepto)->update(['cantidad'=>$cantidad_actual-1]);
                DB::table('transaccions')->where('id',$id_pedido)->update(['CostoT'=>$total_pedido2-$precio]);
                DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                        ->update(['cant_disponible'=>$inventario->cant_disponible+1]);
                DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                        ->update(['cant_apartada'=>$inventario->cant_apartada-1]);

                echo "Producto Devuelto Exitosamente!";
            }
        }
         
        



    }

    public function observaciones(Request $request){
        $observaciones=$request->input('observaciones');
        $id_pedido=$request->input('id_pedido');
        DB::table('transaccions')->where('id',$id_pedido)->update(['observaciones'=>$observaciones]);

        return redirect()->route('traslado');
    }


    // ---------------------------------------------------------------
    // ---------------------------------------------------------------
    public function cancelar_nota(Request $request){
        $id_pedido=$request->input('id_pedido');
        $pedido=DB::table('transaccion')->where('id',$id_pedido);
        $conceptos=DB::table('conceptots')->where('id_transaccion',$id_pedido)->get();

        
    foreach ($conceptos as $key => $value) {
    $id_concepto=$value->id;
    $id_producto=$value->idProducto;
    $cantidad=$value->cantidad;

    $inventario=DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])->first();
    DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
            ->update(['cant_disponible'=>$inventario->cant_disponible+$cantidad]);
            DB::table('conceptots')->where('id',$id_concepto)->delete();
    DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                            ->update(['cant_apartada'=>$inventario->cant_apartada-$cantidad]); 



    }
        DB::table('transaccions')->where('id',$id_pedido)->delete();

        return redirect()->route('index');

    }


        



}
