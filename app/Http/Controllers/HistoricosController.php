<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;
use Carbon\Carbon;
use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\Exports\historico;
use App\historicos;

class HistoricosController extends Controller
{

	public function historicos (){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();

		$sucursales=\DB::table('destinos')
		->get();

		$inventarios=\DB::table('productos')
		->select('productos.id', 'productos.codigo', 'productos.descripcion_producto')
		->get();


		return view('historicos.historicos',['permisos'=>$permisos, 'inventarios'=>$inventarios, 'sucursales'=>$sucursales]);
	}

	public function generatepdf (){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();



		return view('historicos.generatepdf',['permisos'=>$permisos]);
	}

	//maximos_hist

	public function maximos_hist (Request $request){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();

		if (!is_null($request->input('maximo'))) {
			
			if (!is_null($request->input('minimo'))) {
				

				$affected = \DB::table('inventarios')
				->where('id_producto', '=', $request->input('id_producto') )
				->where('id_sucursal', '=', $request->input('id_sucursal') )
				->update([
					'maximos' => $request->input('maximo'), 'minimos' => $request->input('minimo')
				]);

				return redirect()->route('indexinventarios',['permisos'=>$permisos])->with('Mensaje', 'Producto actualizado con éxito.'); 



			}

		}
		if (is_null($request->input('maximo'))) {
			
			if (is_null($request->input('minimo'))) {
				
				$sucursales=\DB::table('destinos')
				->get();

				$inventarios=\DB::table('productos')
				->select('productos.id', 'productos.codigo', 'productos.descripcion_producto')
				->get();


				return view('historicos.historicos',['permisos'=>$permisos, 'inventarios'=>$inventarios, 'sucursales'=>$sucursales]);


				
			}

		}

		if (!is_null($request->input('maximo'))) {
			
			if (is_null($request->input('minimo'))) {
				

				$affected = \DB::table('inventarios')
				->where('id_producto', '=', $request->input('id_producto') )
				->where('id_sucursal', '=', $request->input('id_sucursal') )
				->update([
					'maximos' => $request->input('maximo')
				]);

				return redirect()->route('indexinventarios',['permisos'=>$permisos])->with('Mensaje', 'Producto actualizado con éxito.'); 


			}

		}

		if (is_null($request->input('maximo'))) {
			
			if (!is_null($request->input('minimo'))) {


				$affected = \DB::table('inventarios')
				->where('id_producto', '=', $request->input('id_producto') )
				->where('id_sucursal', '=', $request->input('id_sucursal') )
				->update([
					'minimos' => $request->input('minimo')
				]);


				return redirect()->route('indexinventarios',['permisos'=>$permisos])->with('Mensaje', 'Producto actualizado con éxito.'); 

				


			}

		}


	}


	//


	public function historicosbusqueda (Request $Request){
		if(Auth::guard()->check()==null){
			return redirect()->route('login');
		}
		$id=Auth::user()->id;
		$permisos=permisos::buscar($id)->get();

		$sucursales=\DB::table('destinos')
		->get();

		switch ($Request->input('month')) {
			case '1':
			$mes='enero';
			break;
			case '2':
			$mes='febrero';
			break;case '3':
			$mes='marzo';
			break;case '4':
			$mes='abril';
			break;case '5':
			$mes='mayo';
			break;case '6':
			$mes='junio';
			break;case '7':
			$mes='julio';
			break;case '8':
			$mes='agosto';
			break;case '9':
			$mes='septiembre';
			break;case '10':
			$mes='octubre';
			break;case '11':
			$mes='noviembre';
			break;case '11':
			$mes='diciembre';
			break;

		}





		$inventarios=\DB::table('inventarios')
		->join('productos', 'productos.id', '=', 'inventarios.id_producto')
		->select('productos.id', 'productos.codigo', 'productos.descripcion_producto')
		->get();

		$historicos = \DB::table('concepts')
		->join('productos', 'productos.id', '=', 'concepts.idProducto')
		->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
		->join('destinos', 'destinos.id', '=', 'pedidos.id_origen')

		->select('productos.descripcion_producto', 'productos.codigo', 'concepts.created_at as fecha', 'concepts.cantidad')
		->where('productos.id', '=', $Request->input('id_producto'))
		->where('pedidos.id_origen', '=', $Request->input('id_sucursal'))
		->whereMonth('concepts.created_at',$Request->input('month'))
		->get();



		$maximo = \DB::table('concepts')
		->join('productos', 'productos.id', '=', 'concepts.idProducto')
		->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
		->where('productos.id', '=', $Request->input('id_producto'))
		->where('pedidos.id_origen', '=', $Request->input('id_sucursal'))
		->whereMonth('concepts.created_at',$Request->input('month'))
		->MAX('concepts.cantidad');

		$minimo = \DB::table('concepts')
		->join('productos', 'productos.id', '=', 'concepts.idProducto')
		->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
		->where('productos.id', '=', $Request->input('id_producto'))
		->where('pedidos.id_origen', '=', $Request->input('id_sucursal'))
		->whereMonth('concepts.created_at',$Request->input('month'))
		->MIN('concepts.cantidad');

		$name=\DB::table('productos')
		->where('id', '=', $Request->input('id_producto'))
		->select('productos.descripcion_producto')
		->first();

		$id=\DB::table('productos')
		->where('id', '=', $Request->input('id_producto'))
		->select('productos.id')
		->first();

		$infosucursal = \DB::table('destinos')
		->where('id', '=', $Request->input('id_sucursal'))
		->select('destinos.*')
		->first();



		return view('historicos.historicosbusqueda',['permisos'=>$permisos, 'inventarios'=>$inventarios, 'sucursales'=>$sucursales, 'historicos'=>$historicos, 'maximo'=>$maximo,  'infosucursal'=>$infosucursal, 'minimo'=>$minimo, 'mes'=>$mes, 'name'=>$name->descripcion_producto, 'id'=>$id->id]);



	}

	public function excel () {

  return \Excel::download(new historico, 'Inventarios.xlsx');
}





}
