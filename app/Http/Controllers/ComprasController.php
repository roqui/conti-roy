<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Closure;
use App;
use App\Exports\ProductsExport;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\inventario;
use App\destino; 
use App\max_min;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;


class ComprasController extends Controller
{


    public function calendariocompras(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();


        return view('compras.calendariocompras',['permisos'=>$permisos]);
    }


    public function maximos(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();


        $productos = \DB::table('productos')
        ->get();


        return view('compras.maximos_minimos',['permisos'=>$permisos, 'productos'=>$productos]);
    }
    public function compras(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();


        $proveedores = DB::table('proveedores')->get();


        $count = DB::table('proveedores')->count();

        return view('compras.compras',['permisos'=>$permisos, 'count'=>$count, 'proveedores'=>$proveedores]);
    }

    public function datos_proveedor(Request $request){

       if(Auth::guard()->check()==null){
        return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();
    $date=Carbon::now()->toDateTimeString();
    $id_cliente=$request->input('id');
    $id_origen=Auth::user()->idSucursal;
    $proveedor=\DB::table('proveedores')
    ->where('id', '=', $request->input('id') )
    ->first();
    $num_pedido=\DB::table('ordencompras')->max('id');
    $num_pedido=$num_pedido+1;
    $productos=DB::table('productos')->select('productos.*')
    ->join('inventarios','id_producto','=','productos.id')->where('id_sucursal','=',Auth::user()->idSucursal)
    ->get();
    $tabla=\DB::table('conceptoords')->where('idOrden',$num_pedido)->get();


    return view('compras.new_compra',['permisos'=>$permisos, 'proveedor'=>$proveedor,'date'=>$date, 'productos'=>$productos,'num_pedido'=>$num_pedido,'tabla'=>$tabla,'id_cliente'=>$id_cliente]);
}

//prueba_bb

public function prueba_bb(Request $request){

    if(Auth::guard()->check()==null){
        return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();

    dd($id);

}

public function ordencompras(){

    if(Auth::guard()->check()==null){
        return redirect()->route('login'	);
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();

    $compras = DB::table('ordencompras')
    ->join('proveedores', 'proveedores.id', '=', 'ordencompras.id_proveedor')
    ->join('estadoords', 'estadoords.id', '=', 'ordencompras.estado')
    ->select('proveedores.nombre', 'estadoords.descripcion', 'ordencompras.*')
    ->orderByRaw('ordencompras.created_at desc')

    ->get();


    return view('compras.ordenescompra',['permisos'=>$permisos, 'compras'=>$compras]);

}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////// A PARTIR DE AQUI COMIENZA LO CHIDO DE HACER TRASLADOS/////////////////
//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////// /////////////////////////////////////////////////////////////////////////////////////////// / 


public function agregar_concepto(Request $request){

   if(Auth::guard()->check()==null){
    return redirect()->route('login');
}
$id=Auth::user()->id;
$permisos=permisos::buscar($id)->get();
$date=Carbon::now();
$date2 = $date->format('Y-m-d');        
$productos=json_decode($request->input('id_producto'));
$num_pedido=$request->input('num_pedido');
$id_proveedor=$request->input('id_cliente');
DB::table('ordencompras')->where('id',$num_pedido)->get();
        // -------------------------------------------
DB::table('ordencompras')->insertOrIgnore([
    ['id'=>$num_pedido,
    'id_proveedor'=>$id_proveedor,
    'Total'=>0,
    'Estado'=>1,
    'id_origen'=>Auth::user()->idSucursal,
    'created_at'=>$date,
    'fechaC'=>$date2,
    'fechaLL'=>$date2,
    'usuario'=>Auth::user()->name]

]); 

        // ----------------------------------------------

for ($i=0; $i < count($productos); $i++){


    $precio=DB::table('productos')->where('id',$productos{$i})->get();
    foreach ($precio as $key => $value) {
        $precioU=$value->precio;
    }
                // -------------------------------------------------
    $acumulado=DB::table('ordencompras')->where('id', $request->input('num_pedido'))->get();

    foreach ($acumulado as $key => $value) {
        $acumulado2=$value->Total;
    }
                // -------------------------------------------------
    $total=$precioU+$acumulado2;


    DB::table('ordencompras')
    ->where('id', $request->input('num_pedido'))
    ->update(['Total' => $total]);


    DB::table('conceptoords')->insertOrIgnore([
        ['idOrden' => $request->input('num_pedido'),
        'idProducto'=> $productos{$i},
        'cantidad'  => 1,
        'subtotal'  => $precioU,
        'estado'    => 1,
        'created_at'=> $date]
    ]);
    echo "Concepto agregado exitosamente!";
}
}

    // ************************************************************************************************************
// ************************************************************************************************************

public function tabla_dinamica(Request $request){
   if(Auth::guard()->check()==null){
    return redirect()->route('login');
}
$id=Auth::user()->id;
$permisos=permisos::buscar($id)->get();
        // ------------------------------------------------
$pedido=$request->input('pedido');
$num_pedido=$_GET['num_pedido'];
$pedido=DB::table('conceptoords')
->select('conceptoords.id','idProducto','cantidad','unidad','p.codigo','p.descripcion_producto','p.precio')
->join('productos as p','p.id','=','idProducto')
->where('idOrden','=',$num_pedido)
->get();
$total=DB::table('ordencompras')->where('id',$num_pedido)->get();
        // return view('Pedidos.tabla_dinamica',['permisos'=>$permisos,'pedido'=>$pedido]);
return view('compras.tabla_dinamica',['permisos'=>$permisos,'compra'=>$pedido,'id'=>$num_pedido,'total'=>$total]);

}

    // ************************************************************************************************************
// ************************************************************************************************************


public function eliminar_concepto(Request $request){
    $id_concepto=$request->input('id_concepto');
    $concepto=DB::table('conceptoords')->where('id',$id_concepto)->get();

    foreach ($concepto as $key => $value) {
        $id_pedido=$value->idOrden;
        $subtotal=$value->subtotal;
    }
    $pedido=DB::table('ordencompras')->where('id',$id_pedido)->get();
    foreach ($pedido as $key => $value) {
        $total=$value->Total;
    }

    $total_final=($total-$subtotal);
    DB::table('ordencompras')->where('id',$id_pedido)->update(['Total'=>$total_final]);
    DB::table('conceptoords')->where('id',$id_concepto)->delete();
    echo "Concepto eliminado con éxito!";
}


// ************************************************************************************************************
// ************************************************************************************************************



public function nota(Request $request){
   if(Auth::guard()->check()==null){
    return redirect()->route('login');
}
$id=Auth::user()->id;
$permisos=permisos::buscar($id)->get();
        // ------------------------------------------------
$id_pedido=$request->input('id_pedido');
$pedido=DB::table('ordencompras')->where('id',$id_pedido)->get();
foreach ($pedido as $key => $value) {
   $id=$value->id;
   $id_sucursal=$value->id_origen;
   $id_cliente=$value->id_proveedor;
   $date=$value->created_at;
   $total=$value->Total;
   $usuario=$value->usuario;

}
$proveedor=DB::table('proveedores')->where('id',$id_cliente)->first();
$sucursal=DB::table('destinos')->where('id',$id_sucursal)->first();
$tabla=DB::table('conceptoords')
->select('conceptoords.id','idProducto','cantidad','unidad','p.codigo','p.descripcion_producto','p.precio')
->join('productos as p','p.id','=','idProducto')
->where('idOrden','=',$id_pedido)
->get();
$pdf=PDF::loadView('compras.nota_compras',['permisos'=>$permisos,'pedido'=>$pedido,'proveedor'=>$proveedor,'date'=>$date,'tabla'=>$tabla,'total'=>$total,'usuario'=>$usuario,'num_pedido'=>$id]);

            // return view('compras.nota_compras',['permisos'=>$permisos,'pedido'=>$pedido,'proveedor'=>$proveedor,'date'=>$date,'tabla'=>$tabla,'total'=>$total,'usuario'=>$usuario,'num_pedido'=>$id]);


      


return $pdf->download('nota_compra.pdf');




}

public function imprimirSiNo(Request $request){
    $id_pedido=$request->id_pedido;

    $resp=DB::table('ordencompras')->where('id',$id_pedido)->count();


    return $resp;
}

public function act_cantidad(Request $request){
    $id_concepto=$request->input('id_concepto');
    $cantidad_actual=$request->input('actual');
    $accion=$request->input('accion');
    $id_pedido=$request->input('id_pedido');
    $id_producto=$request->input('id_producto');
    $precio=$request->input('precio');
        // echo $id_pedido;

        // --------------------------------
    $total_concepto=DB::table('conceptoords')->select('subtotal')->where('id',$id_concepto)->get();
    $total_pedido=DB::table('ordencompras')->select('total')->where('id',$id_pedido)->get();

    foreach ($total_concepto as $key => $value) {
        $total_concepto2=($value->subtotal);
    }
    foreach ($total_pedido as $key => $value) {
        $total_pedido2=($value->total);

    }

    if($accion==2){

        DB::table('conceptoords')->where('id',$id_concepto)->update(['subtotal'=>$total_concepto2+$precio]);
        DB::table('conceptoords')->where('id',$id_concepto)->update(['cantidad'=>$cantidad_actual+1]);
        DB::table('ordencompras')->where('id',$id_pedido)->update(['total'=>$total_pedido2+$precio]);
        echo "Cantidad agregada a concepto exitosamente!";

    }elseif ($accion==1) {
        if($cantidad_actual<=0){
            echo "Ya no tienes podructo en tu concepto. Se eliminará automaticamente.";
            DB::table('conceptoords')->where('id',$id_concepto)->delete();    
        }else{
            DB::table('conceptoords')->where('id',$id_concepto)->update(['subtotal'=>$total_concepto2-$precio]);
            DB::table('conceptoords')->where('id',$id_concepto)->update(['cantidad'=>$cantidad_actual-1]);
            DB::table('ordencompras')->where('id',$id_pedido)->update(['total'=>$total_pedido2-$precio]);
            echo "Cantidad restada a la compra!!"; 
        } 
    }





}

public function observaciones(Request $request){
    $observaciones=$request->input('observaciones');
    $id_pedido=$request->input('id_pedido');
    DB::table('ordencompras')->where('id',$id_pedido)->update(['observaciones'=>$observaciones]);


      $id_sucursal = Auth::user()->idSucursal;
        $id_usuario = Auth::user()->id;

        $hora=Carbon::now();

        $bitacora = \DB::table('bitacoras')
        ->insert(['id_sucursal' => $id_sucursal, 
            'id_usuario' => $id_usuario,
            'descripcion' => "COMPRA || Nueva compra:  ".$id_pedido,
            'created_at' => $hora
        ]);

    return redirect()->route('compra');

}

public function cancelar_nota(Request $request){
    $id_pedido=$request->input('id_pedido');
    $pedido=DB::table('ordencompras')->where('id',$id_pedido);
    $conceptos=DB::table('conceptoords')->where('idOrden',$id_pedido)->get();

    foreach ($conceptos as $key => $value) {
        $id_concepto=$value->id;
        $id_producto=$value->idProducto;
        $cantidad=$value->cantidad;

        DB::table('conceptoords')->where('id',$id_concepto)->delete();
    }

    DB::table('ordencompras')->where('id',$id_pedido)->delete();


    return redirect()->route('index');

}



}




