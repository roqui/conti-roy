<?php

namespace App\Http\Controllers;

use App\proveedores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Exports\ProvidersExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\permisos;
use App\User;
use Illuminate\Support\Facades\Input;

class ComprasProximasController extends Controller
{
    public function cproximas(){
    	if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

        
		$cproximas=\DB::table('ordencompras')
                        ->join('destinos as destino', 'destino.id', 'ordencompras.id_origen')
                        ->join('proveedores', 'proveedores.id', 'ordencompras.id_proveedor')
                        ->select('ordencompras.id','proveedores.nombre as proveedor', 'destino.nombre as destino', 'ordencompras.created_at')
                        ->where([['ordencompras.estado','=','1'],['id_origen','=',Auth::user()->idSucursal]
                                ])->get();

    	return view('compras/cproximas',['permisos'=>$permisos, 'cproximas'=>$cproximas]);
    }

    public function conceptosc(Request $request) {
        $data="";
        $con=\DB::table('conceptoords')->select('conceptoords.id','productos.codigo','productos.descripcion_producto','conceptoords.cantidad')
                                    ->join('productos', 'conceptoords.idProducto', 'productos.id')
                                    ->where([['idOrden','=',$request->id]])->get();

        $data="<div class='table-responsive'>
                <table class='table'>
                  <thead class='thead-light'>
                    <tr>
                      <th scope='col' >ID</th>
                      <th scope='col' >Código</th>
                      <th scope='col'>Descripcion</th>
                      <th scope='col'>#</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    
                    ";
        

                      foreach ($con as $c){
                        $data=$data."<tr><th scope='row'>".$c->id."</th>
                                <td>".$c->codigo."</td>
                                <td>".$c->descripcion_producto."</td>
                                <td>".$c->cantidad."</td>
                                </tr>";
                      }
                        
        $data=$data."<tr><td class='table-primary'></td><td class='table-primary'></td><td class='table-primary'>Total de artículos:</td><td class='table-primary'>$ Total:</td></tr>
                    </tbody></table>";
        return response()->json($data); 

    }

    public function revisarc(Request $request){
    	if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $conceptos=\DB::table('conceptoords')->select('conceptoords.id','productos.codigo','productos.descripcion_producto','conceptoords.cantidad')
                                    ->join('productos', 'conceptoords.idProducto', 'productos.id')
                                    ->where([['idOrden','=',$request->id]])->get();

        return view('Compras/revisarc', ['permisos'=>$permisos, 'conceptos'=>$conceptos, 'idtraslado'=>$request->idtraslado]);
    }

    public function conceptoscp(Request $request) {
        $data="holis";
            $con=\DB::table('conceptoords')->select('conceptoords.id','productos.codigo','productos.descripcion_producto','conceptoords.cantidad','conceptoords.cantidadrec')
            ->join('productos', 'conceptoords.idProducto', 'productos.id')
            ->where([
                ['idOrden','=',$request->id], ['estado', '=', '1']  
           ])->get();

            

            $data="<div class='table-responsive'>
            <table class='table'>
            <thead class='thead-light'>
            <tr>
            <th scope='col' >ID</th>
            <th scope='col' >Código</th>
            <th scope='col'>Descripcion</th>
            <th scope='col'>#</th>
            <th scope='col'>Registrados</th>
            
            </tr>
            </thead>
            <tbody>
            
            ";
            

            foreach ($con as $c){

                if($c->cantidad>$c->cantidadrec){

                    $data=$data."<tr><th scope='row'>".$c->id."</th>
                    <td>".$c->codigo."</td>
                    <td>".$c->descripcion_producto."</td>
                    <td>".$c->cantidad."</td>
                    <td>".$c->cantidadrec."</td>
                    </tr>";
                }
            }
            
            $data=$data."<tr><td class='table-primary'></td><td class='table-primary'></td><td class='table-primary'>Total de artículos:</td><td class='table-primary'></td><td class='table-primary'></td></tr>
            </tbody></table>";
        
        return response()->json($data); //este metodo imprime la primera tabla
    }


     public function checadocp(Request $request){

            $concepto = \DB::table('conceptoords')->join('productos', 'productos.id', 'conceptoords.idProducto')->select('conceptoords.id', 'conceptoords.cantidad','conceptoords.cantidadrec', 'conceptoords.estado', 'conceptoords.idProducto')->where([['productos.codigo', '=', $request->codigo], ['conceptoords.idOrden','=',$request->pedido]])->first();

            if(!$concepto){
                $data=" No lo encontré ):"; 
            }else{
                $data=" lo encontré! (:";

                $actualrec = \DB::table('conceptoords')->select( 'cantidadrec')->where('id', $concepto->id)->first();
                
                if($concepto->estado=="1"){

                    if($actualrec->cantidadrec<$concepto->cantidad){
                        $cant=\DB::table('inventarios')->select('id', 'cant_disponible')->where([['id_producto','=',$concepto->idProducto],['id_sucursal', '=',  Auth::user()->idSucursal]])->first();

                        $affected =  \DB::table('inventarios')->where([['id_producto','=',$concepto->idProducto],['id_sucursal', '=', Auth::user()->idSucursal]])->update(['cant_disponible'=>$cant->cant_disponible+1]);//incremento

                        $affected =  \DB::table('conceptoords')->where('id', $concepto->id)->update(['cantidadrec' => $concepto->cantidadrec+1]);//incremento en el check

                        $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz agregado a tu inventario un nuevo artículo  del '".$request->codigo.".'</div>";

                        $actualrec = \DB::table('conceptoords')->select( 'cantidadrec')->where('id', $concepto->id)->first();
                       
                        if($actualrec->cantidadrec<$concepto->cantidad){
                           
                           $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz agregado a tu inventario un nuevo artículo  del '".$request->codigo.".'</div>";

                           

                       }else if ($concepto->cantidad==$actualrec->cantidadrec) {
                               $affected =  \DB::table('conceptoords')->where('id', $concepto->id)->update(['estado' => '2']);
                           }
                       }

                    

               }else{

                  $concepto = \DB::table('conceptoords')->join('productos', 'productos.id', 'conceptoords.idProducto')->select('conceptoords.id', 'conceptoords.cantidad','conceptoords.cantidadrec', 'conceptoords.estado', 'conceptoords.idProducto')->where([['productos.codigo', '=', $request->codigo], ['conceptoords.idOrden','=',$request->pedido],['conceptoords.estado','=','1']])->first();



                  if(!$concepto){

                     $msj="<div class='alert alert-danger' role='alert'>¡Lo siento!, ¡Al parecer no hay más productos como este en la Orden de Compra!.</div>";

                 }else{

                     $data=" lo encontré el segundo! (:";

                     $actualrec = \DB::table('conceptoords')->select( 'cantidadrec')->where('id', $concepto->id)->first();

                      if($actualrec->cantidadrec<$concepto->cantidad){
                        $cant=\DB::table('inventarios')->select('id', 'cant_disponible')->where([['id_producto','=',$concepto->idProducto],['id_sucursal', '=',  Auth::user()->idSucursal]])->first();

                        $affected =  \DB::table('inventarios')->where([['id_producto','=',$concepto->idProducto],['id_sucursal', '=', Auth::user()->idSucursal]])->update(['cant_disponible'=>$cant->cant_disponible+1]);//incremento

                        $affected =  \DB::table('conceptoords')->where('id', $concepto->id)->update(['cantidadrec' => $concepto->cantidadrec+1]);//incremento en el check

                        $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz agregado a tu inventario un nuevo artículo  del '".$request->codigo.".'</div>";

                        $actualrec = \DB::table('conceptoords')->select( 'cantidadrec')->where('id', $concepto->id)->first();
                       
                        if($actualrec->cantidadrec<$concepto->cantidad){
                           
                           $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz agregado a tu inventario un nuevo artículo  del '".$request->codigo.".'</div>";

                           

                       }else if ($concepto->cantidad==$actualrec->cantidadrec) {
                               $affected =  \DB::table('conceptoords')->where('id', $concepto->id)->update(['estado' => '2']);
                                }
                       }
      

                   }

                   

               }

           }
        
       
       return response()->json($msj);
        
     } 

      public function aceptarc(Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        
        $id_sucursal = Auth::user()->idSucursal;
        $conceptos = \DB::table('conceptoords')->select('estado', 'idProducto', 'cantidad')->where('idOrden','=',$request->idpedido)->get();

        foreach ($conceptos as $c) {
            if($c->estado=='1'){


                $msj="<div class='card border-warning mb-3'><div class='card-header'>¡Ups!<div style='float: right;' class='m-0 font-weight-bold text-warning'><i class='fas fa-exclamation-triangle'></i></div></div><div class='card-body'>  <h5 class='card-title'>Tu Orden de Compra aún no está completa</h5><p class='card-text'>Al parecer aún te faltan algunos artículos, ¿estás seguro que deseas aceptarlo así?, si lo haces <strong>no podrás volver para añadir al inventario los artículos faltantes.</strong></p><form action='aceptarcc'>
                    <input type='hidden' value=".$request->idpedido." name='idpedido'>
                    <button type='submit'class='btn btn-primary'>
                    Ir de todos modos
                    </button>
                    </form></div></div>";

                


                return response()->json($msj);    
            }
        }  

        \DB::table('ordencompras')->where('id', '=', $request->idpedido)->update(['Estado'=>'2']);

        $msj="YES";
        return response()->json($msj);    
        

    }

    public function aceptarcc(Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        \DB::table('ordencompras')->where('id', '=', $request->idpedido)->update(['Estado'=>'2']);

        return view('Compras/caceptada', ['permisos'=>$permisos]);
    }

     public function caceptada(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

        return view('compras/caceptada', ['permisos'=>$permisos]);
    }
}
