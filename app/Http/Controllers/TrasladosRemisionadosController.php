<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;

use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;

class TrasladosRemisionadosController extends Controller
{
    public function tremisionados()
    {

        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $id_sucursal = Auth::user()->idSucursal;

        $tpendientes= \DB::table('transaccions')
                        ->join('destinos as origen', 'origen.id', 'transaccions.id_origen')
                        ->join('destinos as destino', 'destino.id', 'transaccions.id_destino')
                        ->select('transaccions.id','transaccions.CostoT','origen.nombre as norigen', 'destino.nombre as ndestino', 'transaccions.created_at')
                        ->where([['transaccions.Estado','=','3'],
                                 ['id_origen','=',$id_sucursal]
                                ])->get();
        

        return view('traslados/tremisionados', ['permisos'=>$permisos, 'tpendientes'=>$tpendientes]);
    }

    public function asignartr (Request $request) {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $conductores=\DB::table('Users')->where([['puesto', '=', '6'], ['idSucursal','=',Auth::user()->idSucursal]])->get();

        return view('traslados/asignartr',['permisos'=>$permisos, 'conductores'=>$conductores, 'idTraslado'=>$request->idtraslado]);
    }

    public function asignadotr (Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $idChecado=\DB::table('checarts')->join('surtirts', 'surtirts.id', 'checarts.idSurtido')->select('checarts.id')->where('surtirts.id_transaccion','=',$request->idtraslado)->first();

        \DB::table('enviarts')->insert(['idChecado'=>$idChecado->id, 'idConductor'=>$request->idconductor, 'created_at'=>date('Y-m-d H:i:s')]);
        $nombre=\DB::table('Users')->select('name')->where('id','=', $request->idsurtidor)->first();

        \DB::table('transaccions')->where('id', '=', $request->idtraslado)->update(['Estado'=>'4']);


        $id_sucursal = Auth::user()->idSucursal;

        $tpendientes= \DB::table('transaccions')
                        ->join('destinos as origen', 'origen.id', 'transaccions.id_origen')
                        ->join('destinos as destino', 'destino.id', 'transaccions.id_destino')
                        ->select('transaccions.id','transaccions.CostoT','origen.nombre as norigen', 'destino.nombre as ndestino', 'transaccions.created_at')
                        ->where([['transaccions.Estado','=','1'],
                                 ['id_origen','=',$id_sucursal]
                                ])->get();

        return view('traslados/tpendientes',['permisos'=>$permisos, 'tpendientes'=>$tpendientes])->with('Mensaje', ' ');

    }
}
