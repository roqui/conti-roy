<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Closure;
use App;
use App\Exports\ProductsExport;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\inventario;
use App\destino; 
use App\max_min;
use App\marca;
use Carbon\Carbon;



class ProductSATController extends Controller
{

  public function indexsat(){

    if(Auth::guard()->check()==null){
      return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();

    $satproductos=\DB::table('satproductos')
    ->get();


    return view ('satproductos.satproductos', [ 'permisos'=>$permisos,  'satproductos'=>$satproductos]);

  }


  public function nuevosat(Request $request){

    if(Auth::guard()->check()==null){
      return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();

    $satproductos=\DB::table('satproductos')
    ->get();

    $hora=Carbon::now();

    $id_sucursal = Auth::user()->idSucursal;
    $id_usuario = Auth::user()->id;


    $bitacora = \DB::table('bitacoras')
    ->insert(['id_sucursal' => $id_sucursal, 
      'id_usuario' => $id_usuario,
      'descripcion' => "CATÁLOGO DE PRODUCTOS SAT || Nuevo producto ".$request->input('descripcion'),
      'created_at' => $hora

    ]);


    \DB::table('satproductos')->insert(
      ['codigo' => $request->input('codigo'), 'descripcion' => $request->input('descripcion')]);


    
    
  return redirect()->route('indexsat',['permisos'=>$permisos, 'satproductos'=>$satproductos])->with('Mensaje', 'Producto agregado con éxito.');

  }

  public function eliminarsat(Request $request){

    if(Auth::guard()->check()==null){
      return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();

    $satproductos=\DB::table('satproductos')
    ->get();

    $hora=Carbon::now();

    $id_sucursal = Auth::user()->idSucursal;
    $id_usuario = Auth::user()->id;


    $producto = \DB::table('satproductos')
    ->where('satproductos.id', '=', $request->input('id'))
      ->first();


    $bitacora = \DB::table('bitacoras')
    ->insert(['id_sucursal' => $id_sucursal, 
      'id_usuario' => $id_usuario,
      'descripcion' => "CATÁLOGO DE PRODUCTOS SAT || Eliminar producto: ".$producto->descripcion,
      'created_at' => $hora

    ]);

      \DB::table('satproductos')->where('id', '=', $request->input('id') )->delete();


  return redirect()->route('indexsat',['permisos'=>$permisos, 'satproductos'=>$satproductos])->with('Mensajee', 'Producto eliminado.');


  }

  public function editarsat(Request $request){
     if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $sucursal = \DB::table('satproductos')
  ->where('id', $request->input('id'))
  ->update([ 
    'descripcion' => $request->input('descripcion'),
    'codigo' => $request->input('codigo')
  ]);

  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE PRODUCTOS SAT || Editar producto: Código: ".$request->input('codigo').', Descripción: '.$request->input('descripcion'),
    'created_at' => $hora
  ]);


  return redirect()->route('indexsat',['permisos'=>$permisos])->with('Mensajea', 'Producto actualizado con éxito.');
  }

}




