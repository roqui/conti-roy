<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;

use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;


class TrasladosAsignadosController extends Controller
{
    public function tasignados() {

    	if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $tasignados=\DB::table('surtirts')
        ->join('transaccions', 'transaccions.id', 'surtirts.id_transaccion')
        ->join('destinos as origen', 'origen.id', 'transaccions.id_origen')
        ->join('destinos as destino', 'destino.id', 'transaccions.id_destino')
        ->select('transaccions.id','transaccions.CostoT','origen.nombre as norigen', 'destino.nombre as ndestino', 'transaccions.created_at')
        ->where([['surtirts.id_surtidor','=',$id],['surtirts.estado','=','INICIADO']])->get();

        return view('Traslados/tasignados', ['permisos'=>$permisos, 'tasignados'=>$tasignados]);
    }

    public function checart(Request $request) {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $conceptos=\DB::table('conceptots')->select('conceptots.id','productos.codigo','productos.descripcion_producto','conceptots.cantidad')
        ->join('productos', 'conceptots.idProducto', 'productos.id')
        ->where([['id_transaccion','=',$request->id]])->get();

        $idSurtido=\DB::table('surtirts')->select('id')->where('id_transaccion','=',$request->idpedido)->first();

        $already=\DB::table('checarts')->select('id')->where('idSurtido','=',$idSurtido->id)->first();

        if(!$already){
            \DB::table('checarts')->insert(['idSurtido'=>$idSurtido->id,'created_at'=>date('Y-m-d H:i:s')]);
        }

        return view('Traslados/checart', ['permisos'=>$permisos, 'conceptos'=>$conceptos, 'idpedido'=>$request->idpedido]);
    }

    public function conceptosta(Request $request) {
        $data="";
        $con=\DB::table('conceptots')->select('conceptots.id','productos.codigo','productos.descripcion_producto','conceptots.cantidad','conceptots.cantidadcheck')
        ->join('productos', 'conceptots.idProducto', 'productos.id')
        ->where([['id_transaccion','=',$request->id],
           ['estado','=','1']   
       ])->get();

        $data="<div class='table-responsive'>
        <table class='table'>
        <thead class='thead-light'>
        <tr>
        <th scope='col' >ID</th>
        <th scope='col' >Código</th>
        <th scope='col'>Descripcion</th>
        <th scope='col'>#</th>
        <th scope='col'># Checada</th>
        
        </tr>
        </thead>
        <tbody>
        
        ";
        

        foreach ($con as $c){
            $data=$data."<tr><th scope='row'>".$c->id."</th>
            <td>".$c->codigo."</td>
            <td>".$c->descripcion_producto."</td>
            <td>".$c->cantidad."</td>
            <td>".$c->cantidadcheck."</td>
            </tr>";
        }
        
        $data=$data."<tr><td class='table-primary'></td><td class='table-primary'></td><td class='table-primary'>Total de artículos:</td><td class='table-primary'></td><td class='table-primary'></td></tr>
        </tbody></table>";
        return response()->json($data); //este metodo imprime la primera tabla
    }

    public function checadot(Request $request){//en este hago consultas de los productos checados 
        
        $concepto = \DB::table('conceptots')->join('productos', 'productos.id', 'conceptots.idProducto')->select('conceptots.id', 'conceptots.cantidad','conceptots.cantidadcheck', 'conceptots.estado', 'conceptots.idProducto')->where([['productos.codigo', '=', $request->codigo], ['conceptots.id_transaccion','=',$request->pedido]])->first();

        if(!$concepto){
            $data=" No lo encontré ):"; 
        }else{
            $data=" lo encontré! (:";

            $affected =  \DB::table('conceptots')->where('id', $concepto->id)->update(['cantidadcheck' => $concepto->cantidadcheck+1]);//incremento en el check

            $actualcheck = \DB::table('conceptots')->select( 'cantidadcheck')->where('id', $concepto->id)->first();

            $cant=\DB::table('conceptots')->select('cantidad')->where([['idProducto', $concepto->idProducto], ['id_transaccion','=',$request->pedido]])->get();
            $totalcant=0;
            foreach ($cant as $c) {
              $totalcant= $totalcant+$c->cantidad;
            }

           $check=\DB::table('conceptots')->select('cantidadcheck')->where([['idProducto', $concepto->idProducto], ['id_transaccion','=',$request->pedido]])->GET();
           $totalcheck=0;
           foreach ($check as $c ) {
             $totalcheck=$totalcheck+$c->cantidadcheck;
           }
            
            if($concepto->estado==1){
               if ($concepto->cantidad>$concepto->cantidadcheck) {
                   
                   $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz checado un nuevo artículo del '".$request->codigo.".'</div>";

                   if ($concepto->cantidad==$actualcheck->cantidadcheck) {
                       $affected =  \DB::table('conceptots')->where('id', $concepto->id)->update(['estado' => '2']);
                       
                       $msj="<div class='alert alert-success' role='alert'>¡Listo!, tienes todos los artículos con código '".$request->codigo."' que necesitas.</div>";
                   }

               }

           }else{

              $concepto2 = \DB::table('conceptots')->join('productos', 'productos.id', 'conceptots.idProducto')->select('conceptots.id', 'conceptots.cantidad','conceptots.cantidadcheck', 'conceptots.estado')->where([['productos.codigo', '=', $request->codigo], ['conceptots.id_transaccion','=',$request->pedido],['conceptots.estado','=','1']])->first();

              if(!$concepto2){

                $diferencia=$totalcheck-$totalcant;
                 $msj="<div class='alert alert-danger' role='alert'>¡Espera!, ¡Llevas ".$diferencia." producto(s) de más!.</div>";

             }else{

                 $data=" lo encontré el segundo! (:";

		            $affected =  \DB::table('conceptots')->where('id', $concepto2->id)->update(['cantidadcheck' => $concepto2->cantidadcheck+1]);//incremento en el check

		            $actualcheck = \DB::table('conceptots')->select( 'cantidadcheck')->where('id', $concepto2->id)->first();
		            if ($concepto2->cantidad>$concepto2->cantidadcheck) {
                       
                       $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz checado un nuevo artículo del '".$request->codigo.".'</div>";

                       if ($concepto2->cantidad==$actualcheck->cantidadcheck) {
                           $affected =  \DB::table('conceptots')->where('id', $concepto2->id)->update(['estado' => '2']);
                           
                           $msj="<div class='alert alert-success' role='alert'>¡Listo!, tienes todos los artículos con código '".$request->codigo."' que necesitas.</div>";
                       }

                   }

               }

               

           }

       }
       

       return response()->json($msj);
   }

   public function finchecart(Request $request){

    if(Auth::guard()->check()==null){
        return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();

    $id_sucursal = Auth::user()->idSucursal;

    

    $conceptos = \DB::table('conceptots')->select('estado', 'idProducto', 'cantidad')->where('id_transaccion','=',$request->idpedido)->get();


    foreach ($conceptos as $c) {
        if($c->estado=='1'){

            return view('Traslados/checart', ['permisos'=>$permisos, 'idpedido'=>$request->idpedido])->with('Mensajeu', '¡Lo sentimos!, al paracer aún te faltan productos.');
        }

        
    }

        foreach ($conceptos as $c) {
            
            
            $cant=\DB::table('inventarios')->select('id', 'cant_apartada')->where([['id_producto','=',$c->idProducto],['id_sucursal', '=', $id_sucursal]])->first();

            $affected =  \DB::table('inventarios')->where([['id_producto','=',$c->idProducto],['id_sucursal', '=', $id_sucursal]])->update(['cant_apartada'=>$cant->cant_apartada-$c->cantidad]);//incremento en el check  
        }

        \DB::table('transaccions')->where('id', '=', $request->idpedido)->update(['Estado'=>'3']);

        $surtido=\DB::table('surtirts')->where('id_transaccion','=',$request->idpedido)->update(['estado'=>'FINALIZAD0']);

        return view('Traslados/fintraslado', ['permisos'=>$permisos]);

    }
}
