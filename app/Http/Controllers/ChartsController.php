<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Closure;
use App;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\puesto;
use App\destino;


class ChartsController extends Controller
{


  public function indexcharts (Request $request){

    if(Auth::guard()->check()==null){
      return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();


  $id_sucursal = Auth::user()->idSucursal;

  $maximo = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereYear('concepts.created_at',$request->input('year'))
  ->MAX('concepts.cantidad');

  $minimo = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereYear('concepts.created_at',$request->input('year'))
  ->MIN('concepts.cantidad');


  $uno = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','1')
  ->SUM('concepts.cantidad');

  $dos = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','2')
  ->SUM('concepts.cantidad');

  $tres = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','3')
  ->SUM('concepts.cantidad');

  $cuatro = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','4')
  ->SUM('concepts.cantidad');

  $cinco = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','5')
  ->SUM('concepts.cantidad');

  $seis = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','6')
  ->SUM('concepts.cantidad');

  $siete = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','7')
  ->SUM('concepts.cantidad');

  $ocho = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','8')
  ->SUM('concepts.cantidad');

  $nueve = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','9')
  ->SUM('concepts.cantidad');

  $diez = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','10')
  ->SUM('concepts.cantidad');

  $once = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','11')
  ->SUM('concepts.cantidad');

  $doce = \DB::table('concepts')
  ->join('productos', 'productos.id', '=', 'concepts.idProducto')
  ->join('pedidos', 'pedidos.id', '=', 'concepts.idPedido')
  ->where('productos.id', '=', $request->input('id'))
  ->where('pedidos.id_origen', '=', $id_sucursal)
  ->whereMonth('concepts.created_at','12')
  ->SUM('concepts.cantidad');


  $valoresY=array();
  $valoresY[]=$uno;
  $valoresY[]=$dos;
  $valoresY[]=$tres;
  $valoresY[]=$cuatro;
  $valoresY[]=$cinco;
  $valoresY[]=$seis;
  $valoresY[]=$siete;
  $valoresY[]=$ocho;
  $valoresY[]=$nueve;
  $valoresY[]=$diez;
  $valoresY[]=$once;
  $valoresY[]=$doce;


$valoresX=array();
$valoresX[]='Enero';
$valoresX[]='Febrero';
$valoresX[]='Marzo';
$valoresX[]='Abril';
$valoresX[]='Mayo';
$valoresX[]='Junio';
$valoresX[]='Julio';
$valoresX[]='Agosto';
$valoresX[]='Septiembre';
$valoresX[]='Octubre';
$valoresX[]='Noviembre';
$valoresX[]='Diciembre';


$datosY=json_encode($valoresY);

$datosX=json_encode($valoresX);


  $productos = \DB::table('inventarios')
  ->join('productos', 'productos.id', '=', 'inventarios.id_producto')
  ->select('productos.id', 'productos.codigo', 'productos.descripcion_producto')
  ->get();


  $producto = \DB::table('productos')
  ->join('marcas', 'marcas.id', '=', 'productos.id_marca')
  ->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
  ->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
  ->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')
  ->select('productos.id', 'productos.codigo', 'productos.codigo', 'productos.codigosat', 'productos.descripcion_producto', 'productos.costo', 'proveedores.nombre as proveedor', 'marcas.descripcion as marca', 'lineas.descr as lineas', 'sublineas.descrip as sublineas')
  ->where('productos.id', '=', $request->input('id'))
  ->first();

  
  return view('compras.busqueda',['permisos'=>$permisos, 'producto'=>$producto, 'maximo'=>$maximo, 'minimo'=>$minimo, 'productos'=>$productos,  'datosX'=>$datosX, 'datosY'=>$datosY, 'proveedor'=>$producto->proveedor, 'marca'=>$producto->marca, 'lineas'=>$producto->lineas, 'sublineas'=>$producto->sublineas, 'descripcion_producto'=>$producto->descripcion_producto,  'id'=>$producto->id,  'codigo'=>$producto->codigo, 'costo'=>$producto->costo, 'codigo'=>$producto->codigo,  'codigosat'=>$producto->codigosat, 'producto'=>$producto]);

}


}
