<?php

namespace App\Http\Controllers;

use App\proveedores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Exports\ProvidersExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\permisos;
use App\User;
use Illuminate\Support\Facades\Input;

class TrasladosProximosController extends Controller
{
    public function tproximos(){
    	if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

        
		$tproximos=\DB::table('transaccions')
                        ->join('destinos as origen', 'origen.id', 'transaccions.id_origen')
                        ->join('destinos as destino', 'destino.id', 'transaccions.id_destino')
                        ->select('transaccions.id','transaccions.CostoT','origen.nombre as norigen', 'destino.nombre as ndestino', 'transaccions.created_at')
                        ->where([['transaccions.estado','!=','6'],['id_destino','=',Auth::user()->idSucursal]
                                ])->get();

    	return view('Traslados/tproximos',['permisos'=>$permisos, 'tproximos'=>$tproximos]);
    }

    public function revisart(Request $request){
    	if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $conceptos=\DB::table('conceptots')->select('conceptots.id','productos.codigo','productos.descripcion_producto','conceptots.cantidad')
        ->join('productos', 'conceptots.idProducto', 'productos.id')
        ->where([['id_transaccion','=',$request->idtraslado]])->get();

        return view('Traslados/revisart', ['permisos'=>$permisos, 'conceptos'=>$conceptos, 'idtraslado'=>$request->idtraslado]);
    }

    public function conceptostp(Request $request) {
        $data="holis";
        
        $e=\DB::table('transaccions')->select('estado')->where('id','=',$request->id)->first();

        if($e->estado=='4'){
            $con=\DB::table('conceptots')->select('conceptots.id','productos.codigo','productos.descripcion_producto','conceptots.cantidad','conceptots.cantidadrec')
            ->join('productos', 'conceptots.idProducto', 'productos.id')
            ->where([
                ['id_transaccion','=',$request->id]  
           ])->get();

            

            $data="<div class='table-responsive'>
            <table class='table'>
            <thead class='thead-light'>
            <tr>
            <th scope='col' >ID</th>
            <th scope='col' >Código</th>
            <th scope='col'>Descripcion</th>
            <th scope='col'>#</th><th scope='col'>Registrados</th>
            
            </tr>
            </thead>
            <tbody>
            
            ";
            

            foreach ($con as $c){

                if($c->cantidad>$c->cantidadrec){

                    $data=$data."<tr><th scope='row'>".$c->id."</th>
                    <td>".$c->codigo."</td>
                    <td>".$c->descripcion_producto."</td>
                    <td>".$c->cantidad."</td>
                    <td>".$c->cantidadrec."</td>
                    </tr>";
                }
            }
            
            $data=$data."<tr><td class='table-primary'></td><td class='table-primary'></td><td class='table-primary'>Total de artículos:</td><td class='table-primary'></td><td class='table-primary'></td></tr>
            </tbody></table>";

        }else{
            $data='¡Lo sentimos!, ¡Al parecer este Traslado aún no ha sido enviado!';
        }
        return response()->json($data); //este metodo imprime la primera tabla
    }

    public function checadotp(Request $request){

        $e=\DB::table('transaccions')->select('estado')->where('id','=',$request->pedido)->first();

        if($e->estado=='4'){
            $concepto = \DB::table('conceptots')->join('productos', 'productos.id', 'conceptots.idProducto')->select('conceptots.id', 'conceptots.cantidad','conceptots.cantidadrec', 'conceptots.estador', 'conceptots.idProducto')->where([['productos.codigo', '=', $request->codigo], ['conceptots.id_transaccion','=',$request->pedido]])->first();

            if(!$concepto){
                $data=" No lo encontré ):"; 
            }else{
                $data=" lo encontré! (:";

                $actualrec = \DB::table('conceptots')->select( 'cantidadrec')->where('id', $concepto->id)->first();
                
                if($concepto->estador=="SREVISAR"){

                    if($actualrec->cantidadrec<$concepto->cantidad){
                        $cant=\DB::table('inventarios')->select('id', 'cant_disponible')->where([['id_producto','=',$concepto->idProducto],['id_sucursal', '=',  Auth::user()->idSucursal]])->first();

                        $affected =  \DB::table('inventarios')->where([['id_producto','=',$concepto->idProducto],['id_sucursal', '=', Auth::user()->idSucursal]])->update(['cant_disponible'=>$cant->cant_disponible+1]);//incremento

                        $affected =  \DB::table('conceptots')->where('id', $concepto->id)->update(['cantidadrec' => $concepto->cantidadrec+1]);//incremento en el check

                        $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz agregado a tu inventario un nuevo artículo  del '".$request->codigo.".'</div>";

                        $actualrec = \DB::table('conceptots')->select( 'cantidadrec')->where('id', $concepto->id)->first();
                       
                        if($actualrec->cantidadrec<$concepto->cantidad){
                           
                           $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz agregado a tu inventario un nuevo artículo  del '".$request->codigo.".'</div>";

                           

                       }else if ($concepto->cantidad==$actualrec->cantidadrec) {
                               $affected =  \DB::table('conceptots')->where('id', $concepto->id)->update(['estador' => 'REVISADO']);
                           }
                       }

                    

               }else{

                  $concepto = \DB::table('conceptots')->join('productos', 'productos.id', 'conceptots.idProducto')->select('conceptots.id', 'conceptots.cantidad','conceptots.cantidadrec', 'conceptots.estador', 'conceptots.idProducto')->where([['productos.codigo', '=', $request->codigo], ['conceptots.id_transaccion','=',$request->pedido],['conceptots.estador','=','SREVISAR']])->first();



                  if(!$concepto){

                     $msj="<div class='alert alert-danger' role='alert'>¡Lo siento!, ¡Al parecer no hay más productos como este en el traslado!.</div>";

                 }else{

                     $data=" lo encontré el segundo! (:";

                     $actualrec = \DB::table('conceptots')->select( 'cantidadrec')->where('id', $concepto->id)->first();

                      if($actualrec->cantidadrec<$concepto->cantidad){
                        $cant=\DB::table('inventarios')->select('id', 'cant_disponible')->where([['id_producto','=',$concepto->idProducto],['id_sucursal', '=',  Auth::user()->idSucursal]])->first();

                        $affected =  \DB::table('inventarios')->where([['id_producto','=',$concepto->idProducto],['id_sucursal', '=', Auth::user()->idSucursal]])->update(['cant_disponible'=>$cant->cant_disponible+1]);//incremento

                        $affected =  \DB::table('conceptots')->where('id', $concepto->id)->update(['cantidadrec' => $concepto->cantidadrec+1]);//incremento en el check

                        $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz agregado a tu inventario un nuevo artículo  del '".$request->codigo.".'</div>";

                        $actualrec = \DB::table('conceptots')->select( 'cantidadrec')->where('id', $concepto->id)->first();
                       
                        if($actualrec->cantidadrec<$concepto->cantidad){
                           
                           $msj="<div class='alert alert-primary' role='alert'>¡Bien!, Haz agregado a tu inventario un nuevo artículo  del '".$request->codigo.".'</div>";

                           

                       }else if ($concepto->cantidad==$actualrec->cantidadrec) {
                               $affected =  \DB::table('conceptots')->where('id', $concepto->id)->update(['estador' => 'REVISADO']);
                                }
                       }
      

                   }

                   

               }

           }
           
       }else{
        $msj="<div class='alert alert-danger' role='alert'>¡Tranquilo!, ¡Al parecer aún no ha llegado tu pedido!.</div>";
       }
       return response()->json($msj);
    }

    public function aceptart(Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        
        $id_sucursal = Auth::user()->idSucursal;
        $conceptos = \DB::table('conceptots')->select('estador', 'idProducto', 'cantidad')->where('id_transaccion','=',$request->idpedido)->get();

        foreach ($conceptos as $c) {
            if($c->estador=='SREVISAR'){


                $msj="<div class='card border-warning mb-3'><div class='card-header'>¡Espera!<div style='float: right;' class='m-0 font-weight-bold text-warning'><i class='fas fa-exclamation-triangle'></i></div></div><div class='card-body'>  <h5 class='card-title'>Tu traslado aún no está completo</h5><p class='card-text'>Al parecer aún te faltan algunos artículos, ¿estás seguro que deseas aceptarlo así?, si lo haces <strong>no podrás volver para añadir al inventario los artículos faltantes.</strong></p><form action='aceptartt'>
                    <input type='hidden' value=".$request->idpedido." name='idpedido'>
                    <button type='submit'class='btn btn-primary'>
                    Ir de todos modos
                    </button>
                    </form></div></div>";

                


                return response()->json($msj);    
            }
        }  

        \DB::table('transaccions')->where('id', '=', $request->idpedido)->update(['Estado'=>'6']);

        $msj="YES";
        return response()->json($msj);    
        

    }

    public function taceptado(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

        return view('Traslados/taceptado', ['permisos'=>$permisos]);
    }



    public function aceptartt(Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        \DB::table('transaccions')->where('id', '=', $request->idpedido)->update(['Estado'=>'6']);

        return view('Traslados/taceptado', ['permisos'=>$permisos]);
    }


    

    public function cancelart(Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        
        $id_sucursal = Auth::user()->idSucursal;
        $conceptos = \DB::table('conceptots')->select('estador', 'idProducto', 'cantidad')->where('id_transaccion','=',$request->idpedido)->get();

                $msj="<div class='card border-danger mb-3'><div class='card-header'>¡Vamos más lento!<div style='float: right;' class='m-0 font-weight-bold text-danger'><i class='fas fa-exclamation-triangle'></i></div></div><div class='card-body'>  <h5 class='card-title'>Cancelación de Traslado</h5><p class='card-text'>¿Estás seguro que quieres cancelar el traslado?, si lo haces y ya haz registrado alguno de los artículos <strong>Se restarán automáticamente de tu inventario y no podrás recuperarlos.</strong></p><form action='aceptartt'>
                    <input type='hidden' value=".$request->idpedido." name='idpedido'>
                    <button type='submit'class='btn btn-primary'>
                    Ir de todos modos
                    </button>
                    </form></div></div>";

                

                return response()->json($msj);    
            
    }  
   
        
}