<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Closure;
use App;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\puesto;
use App\destino;


class DashboardController extends Controller
{


    public function dashboard(){
         if(Auth::guard()->check()==null){
      return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();


    return view('dashboard.dashboard',['permisos'=>$permisos]);
    }





}
