<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Closure;
use App;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\puesto;
use App\destino;
use Carbon\Carbon;



class UsersController extends Controller
{

// ************************************************************************************************************
// ************************************************************************************************************
public function ups(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
 
  return view ('ups', [ 'permisos'=>$permisos]);


}

public function compra(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
 
  return view ('compra', [ 'permisos'=>$permisos]);


}

public function index2(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
 
  return view ('index2', [ 'permisos'=>$permisos]);


}

public function pedido(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
 
  return view ('pedido', [ 'permisos'=>$permisos]);


}public function traslado(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
 
  return view ('traslado', [ 'permisos'=>$permisos]);


}

public function manual(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
 
  return view ('manual', [ 'permisos'=>$permisos]);


}

public function work(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
 
  return view ('work', [ 'permisos'=>$permisos]);


}

public function upspermiso(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
 
  return view ('ups2', [ 'permisos'=>$permisos]);


}
    


    public function index()
    {   
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
            //Variables para liberacion de interfaces
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
            //Fin de variables para liberacion de interfaces

        $sucursal=\DB::table('Users')->select('idSucursal')->where('id',$id)->first();

        $users = DB::table('users')
        ->join('puestos', 'puestos.id', '=', 'users.puesto')
        ->join('destinos', 'destinos.id', '=', 'users.idSucursal')
        ->select('users.*', 'puestos.descripcion', 'destinos.nombre')
        ->orderBy('puestos.id', 'asc')->where([['idSucursal','=',Auth::user()->idSucursal]])->get();


        return view('users', ['users'=>$users],['permisos'=>$permisos]);


        

    }


    public function allindex()
    {   
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
            //Variables para liberacion de interfaces
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
            //Fin de variables para liberacion de interfaces

        $sucursal=\DB::table('Users')->select('idSucursal')->where('id',$id)->first();

        $users = DB::table('users')
        ->join('puestos', 'puestos.id', '=', 'users.puesto')
        ->join('destinos', 'destinos.id', '=', 'users.idSucursal')
        ->select('users.*', 'puestos.descripcion', 'destinos.nombre')
        ->orderBy('puestos.id', 'asc')->get();

        $sucursales = \DB::table('destinos')
        ->get();


        return view('allusers', ['users'=>$users],['permisos'=>$permisos, 'sucursales'=>$sucursales]);


        

    }

    public function buscarusuarios(Request $request)
    {   
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
            //Variables para liberacion de interfaces
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
            //Fin de variables para liberacion de interfaces

        $sucursal=\DB::table('Users')->select('idSucursal')->where('id',$id)->first();

        $users = DB::table('users')
        ->join('puestos', 'puestos.id', '=', 'users.puesto')
        ->join('destinos', 'destinos.id', '=', 'users.idSucursal')
        ->select('users.*', 'puestos.descripcion', 'destinos.nombre as sucursal')
        ->orderBy('puestos.id', 'asc')
        ->where('users.idSucursal', '=', $request->input('id_sucursal'))
        ->get();

        $sucursales = \DB::table('destinos')
        ->get();

        $sucursal=\DB::table('destinos')
        ->where('id', '=',  $request->input('id_sucursal'))
        ->first();



        return view('searchuser', ['users'=>$users, 'permisos'=>$permisos, 'sucursales'=>$sucursales, 'sucursal'=>$sucursal]);


        

    }



// ************************************************************************************************************
// ************************************************************************************************************
    
    public function photo(Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }

        //Variables para liberacion de interfaces
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces

        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/img',$name);

            $affected = DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(['foto' => 'img/'.$name]);


            return redirect()->route('profile',['permisos'=>$permisos])->with('Mensajea', 'Avatar actualizado con éxito.');
        }
    }

// ************************************************************************************************************
// ************************************************************************************************************

    public function nuevo()
    {   
        $users = \DB::table('users')
        ->select('users.*')
        ->first();
        // -------- VALIDANDO SI HAY SESION------------
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        //Variables para liberacion de interfaces
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces

        return view('newusers',['users' => $users,'permisos'=>$permisos]);

    }

    public function newusers2()
    {   
        $users = \DB::table('users')
        ->select('users.*')
        ->first();
        // -------- VALIDANDO SI HAY SESION------------
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        //Variables para liberacion de interfaces
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces

        return view('newusers2',['users' => $users,'permisos'=>$permisos]);

    }

    //newusers2

// ************************************************************************************************************
// ************************************************************************************************************

    public function profile()
    {       
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }

        //Variables para liberacion de interfaces
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces

        $puesto = DB::table('users')
        ->join('puestos', 'puestos.id', '=', 'users.puesto')
        ->join('destinos', 'destinos.id', '=', 'users.idSucursal')
        ->select('puestos.descripcion', 'destinos.nombre')
        ->where('users.id',  Auth::user()->id ) ;

        return view('profile',['permisos'=>$permisos]);

    }

// ************************************************************************************************************
// ************************************************************************************************************





// ************************************************************************************************************
// ************************************************************************************************************
    public function eliminaruser(Request $request)
    {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }

        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $usuario=\DB::table('users')
        ->where('id', '=',  $request->input('id'))
        ->first();
        
        $id_sucursal = Auth::user()->idSucursal;
        $id_usuario = Auth::user()->id;

        $hora=Carbon::now();

        $bitacora = \DB::table('bitacoras')
        ->insert(['id_sucursal' => $id_sucursal, 
            'id_usuario' => $id_usuario,
            'descripcion' => "CATÁLOGO DE USUARIOS || Eliminar usuario:  ".$usuario->usuario,
            'created_at' => $hora
        ]);

        \DB::table('users')->where('id', '=', $request->input('id') )->delete();
        // --------------------------------------------------------------------------
        return redirect()->route('users',['permisos'=>$permisos])->with('Mensajeue', 'Usuario eliminado con éxito.');
    }


// ************************************************************************************************************
// ************************************************************************************************************

       public function create2(Request $request)
    {   
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        $request->request->add([
            'password'=> Hash::make($request->input('password'))
        ]);

        $con_email = DB::table('users')->select('email')->get();
        $con_usuario = DB::table('users')->select('usuario')->get();
        $dec_Json = json_decode($con_usuario);


        for($decu=0; $decu<sizeof($dec_Json); $decu++) {
            if($request->usuario == $dec_Json[$decu]->usuario){
                return redirect()->route('users',['permisos'=>$permisos])->with('Mensajee', 'Usuario ya utilizado, verifique de nuevo.');
            }            
        }
        $dec_Jsone = json_decode($con_email);
        for($dec=0; $dec<sizeof($dec_Jsone); $dec++) {
            if($request->email == $dec_Jsone[$dec]->email){
                return redirect()->route('users',['permisos'=>$permisos])->with('Mensajee', 'Correo ya utilizado, verifique de nuevo.');
            }            
        }

        $usuario = new User();

        $usuario->name = $request->input('name');
        $usuario->foto = $request->input('foto');
        $usuario->email = $request->input('email');
        $usuario->usuario= $request->input('usuario');
        $usuario->calle =$request->input('calle');
        $usuario->foto = $request->input('foto');
        $usuario->puesto = $request->input('puesto');
        $usuario->idSucursal = $request->input('idSucursal');
        $usuario->password = $request->input('password');
        $usuario->telefono = $request->input('telefono');

        $usuario->save();

        $id_sucursal = Auth::user()->idSucursal;
        $id_usuario = Auth::user()->id;

        $hora=Carbon::now();

        $bitacora = \DB::table('bitacoras')
        ->insert(['id_sucursal' => $id_sucursal, 
            'id_usuario' => $id_usuario,
            'descripcion' => "CATÁLOGO DE USUARIOS || Nuevo usuario:  ".$request->input('usuario'),
            'created_at' => $hora
        ]);
        

        $permisos=\DB::table('rol_defaults')->select('id_interfaz')->where('id_rol',$request->puesto)->get();

        foreach ($permisos as $permiso) {

            \DB::table('permisos')->insertOrIgnore(['idInterfaz' => $permiso->id_interfaz, 'idUsuario' => $usuario->id]);
        }

        return redirect()->route('allindex',['permisos'=>$permisos])->with('Mensajeu', 'Usuario agregado con éxito.');
    }


    public function create(Request $request)
    {   
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        $request->request->add([
            'password'=> Hash::make($request->input('password'))
        ]);

        $con_email = DB::table('users')->select('email')->get();
        $con_usuario = DB::table('users')->select('usuario')->get();
        $dec_Json = json_decode($con_usuario);


        for($decu=0; $decu<sizeof($dec_Json); $decu++) {
            if($request->usuario == $dec_Json[$decu]->usuario){
                return redirect()->route('users',['permisos'=>$permisos])->with('Mensajee', 'Usuario ya utilizado, verifique de nuevo.');
            }            
        }
        $dec_Jsone = json_decode($con_email);
        for($dec=0; $dec<sizeof($dec_Jsone); $dec++) {
            if($request->email == $dec_Jsone[$dec]->email){
                return redirect()->route('users',['permisos'=>$permisos])->with('Mensajee', 'Correo ya utilizado, verifique de nuevo.');
            }            
        }

        $usuario = new User();

        $usuario->name = $request->input('name');
        $usuario->foto = $request->input('foto');
        $usuario->email = $request->input('email');
        $usuario->usuario= $request->input('usuario');
        $usuario->calle =$request->input('calle');
        $usuario->foto = $request->input('foto');
        $usuario->puesto = $request->input('puesto');
        $usuario->idSucursal = $request->input('idSucursal');
        $usuario->password = $request->input('password');
        $usuario->telefono = $request->input('telefono');

        $usuario->save();

        $id_sucursal = Auth::user()->idSucursal;
        $id_usuario = Auth::user()->id;

        $hora=Carbon::now();

        $bitacora = \DB::table('bitacoras')
        ->insert(['id_sucursal' => $id_sucursal, 
            'id_usuario' => $id_usuario,
            'descripcion' => "CATÁLOGO DE USUARIOS || Nuevo usuario:  ".$request->input('usuario'),
            'created_at' => $hora
        ]);
        

        $permisos=\DB::table('rol_defaults')->select('id_interfaz')->where('id_rol',$request->puesto)->get();

        foreach ($permisos as $permiso) {

            \DB::table('permisos')->insertOrIgnore(['idInterfaz' => $permiso->id_interfaz, 'idUsuario' => $usuario->id]);
        }

        return redirect()->route('users',['permisos'=>$permisos])->with('Mensajeu', 'Usuario agregado con éxito.');
    }


// ************************************************************************************************************
// ************************************************************************************************************

    public function actuser(Request $request)
    {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------   
        $user = DB::table('users')
        ->join('puestos', 'puestos.id', '=', 'users.puesto')
        ->join('destinos', 'destinos.id', '=', 'users.idSucursal')
        ->select('users.*','puestos.descripcion', 'destinos.nombre as sucursal')
        ->where('users.id','=',$request->id)
        ->first();

        $destinos = \DB::table('destinos')
        ->get();


        return view ('actuser', ['user'=>$user], ['permisos'=>$permisos, 'destinos'=>$destinos]);


    }

// ************************************************************************************************************
// ************************************************************************************************************


    public function update(Request $request, $id_act)
    {   
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        
        $user = User::findOrFail($id_act);

        $request->validate([
            'email' => 'required|max:255|unique:users,email,'.$user->id, 
            'usuario' => 'required|unique:users,usuario,'.$user->id 
        ]);

                $id_sucursal = Auth::user()->idSucursal;


        $newname=$request->get('name');
        $newusuario=$request->get('usuario');
        $newemail=$request->get('email');
        $newcalle=$request->get('calle');
        $newpuesto=$request->get('puesto');
        $newsucursal=  $id_sucursal;
        $newpassword= Hash::make($request['password']);
        $newtelefono=$request->get('telefono');

        
        $id_usuario = Auth::user()->id;

        $hora=Carbon::now();

        $bitacora = \DB::table('bitacoras')
        ->insert(['id_sucursal' => $id_sucursal, 
            'id_usuario' => $id_usuario,
            'descripcion' => "CATÁLOGO DE USUARIOS || Editar usuario ".$request->input('usuario'),
            'created_at' => $hora
        ]);

        $user->name=$newname;
        $user->usuario=$newusuario;
        $user->email=$newemail;
        $user->calle=$newcalle;
        $user->puesto=$newpuesto;
        $user->idSucursal=$newsucursal;
        $user->password=$newpassword;
        $user->telefono=$newtelefono;

        $user->update();

        return redirect()->route('users',['permisos'=>$permisos])->with('Mensajeu', 'Usuario actualizado con éxito.');

    }

    public function update2(Request $request, $id_act)
    {   
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        
        $user = User::findOrFail($id_act);

        $request->validate([
            'email' => 'required|max:255|unique:users,email,'.$user->id, 
            'usuario' => 'required|unique:users,usuario,'.$user->id 
        ]);


        $newname=$request->get('name');
        $newusuario=$request->get('usuario');
        $newemail=$request->get('email');
        $newcalle=$request->get('calle');
        $newpuesto=$request->get('puesto');
        $newsucursal=$request->get('idSucursal');
        $newpassword= Hash::make($request['password']);
        $newtelefono=$request->get('telefono');

        $id_sucursal = Auth::user()->idSucursal;
        $id_usuario = Auth::user()->id;

        $hora=Carbon::now();

        $bitacora = \DB::table('bitacoras')
        ->insert(['id_sucursal' => $id_sucursal, 
            'id_usuario' => $id_usuario,
            'descripcion' => "CATÁLOGO DE USUARIOS || Editar usuario ".$request->input('usuario'),
            'created_at' => $hora
        ]);

        $user->name=$newname;
        $user->usuario=$newusuario;
        $user->email=$newemail;
        $user->calle=$newcalle;
        $user->puesto=$newpuesto;
        $user->idSucursal=$newsucursal;
        $user->password=$newpassword;
        $user->telefono=$newtelefono;

        $user->update();

        return redirect()->route('allindex',['permisos'=>$permisos])->with('Mensajeu', 'Usuario actualizado con éxito.');

    }


    public function findLastUser(){


     $users = \DB::table('users')
     ->select('users.*')
     ->orderBy('created_at', 'DESC')
     ->first();
     return response()->json($users);
 }


    //funcion para obtener puestos
 public function getp()
 {
    $puestos = puesto::get();
    $puestosArray[''] = 'Selecciona un puesto';
    foreach ($puestos as $puesto) {
        $puestosArray[$puesto->id] = $puesto->descripcion;
    }
    return $puestosArray;
}

    //funcion para obtener sucursales
public function gets()
{
    $sucursales = destino::get();
    $sucursalesArray[''] = 'Selecciona una sucursal';
    foreach ($sucursales as $sucursal) {
        $sucursalesArray[$sucursal->id] = $sucursal->nombre;
    }
    return $sucursalesArray;
}

public function excel () {
    return \Excel::download(new UsersExport, 'Empleados.xlsx');
}


}
