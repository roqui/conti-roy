<?php

namespace App\Http\Controllers;

use App\proveedores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Exports\ProvidersExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\permisos;
use App\User;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;


class ProviderController extends Controller
{


 // ************************************************************************************************************
// ************************************************************************************************************   

    public function proveedores(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

        $proveedores = \DB::table('proveedores')->select('id','nombre','rfc','calle', 'num_exterior','observaciones','telefono','email', 'descuento', 'created_at')->get();
        return view('proveedores/proveedores',['proveedores'=>$proveedores],['permisos'=>$permisos]);
    }

// ************************************************************************************************************
// ************************************************************************************************************

    public function editproviders()
    {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

        return view('proveedores/editproviders',['permisos'=>$permisos]);
    }

// ************************************************************************************************************
// ************************************************************************************************************

    public function newprovider(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

        return view('proveedores/newprovider',['permisos'=>$permisos]);
    }
// ************************************************************************************************************
// ************************************************************************************************************

    public function crearprov(Request $request)
    {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------


        \DB::table('proveedores')->insert(
            ['nombre' => $request->nombre, 'rfc' => $request->rfc, 'calle'=>$request->calle,'num_exterior'=>$request->num_exterior,'codigo_postal'=>$request->codigo_postal,'telefono'=>$request->telefono, 'estado'=>$request->estado, 'pais'=>$request->pais, 'ciudad'=>$request->ciudad, 'foto'=>$request->foto, 'rfc'=>$request->rfc, 'email'=>$request->email, 'observaciones'=>$request->observaciones, 'descuento'=>$request->descuento, 'dias_plazo'=>$request->dias_plazo,]);

        $id_sucursal = Auth::user()->idSucursal;
        $id_usuario = Auth::user()->id;

        $hora=Carbon::now();

        $bitacora = \DB::table('bitacoras')
        ->insert(['id_sucursal' => $id_sucursal, 
            'id_usuario' => $id_usuario,
            'descripcion' => "CATÁLOGO DE PROVEEDORES || Nuevo proveedor: ".$request->nombre,
            'created_at' => $hora

        ]);

        return redirect()->route('proveedores',['permisos'=>$permisos])->with('Mensaje', 'Proveedor agregado con exito.');

    }

// ************************************************************************************************************
// ************************************************************************************************************

    public function eliminar_proveedor2(Request $request)
    {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        $productos=\DB::table('proveedores')
        ->where('id', '=', $request->input('id'))
        ->first();


        $id_sucursal = Auth::user()->idSucursal;
        $id_usuario = Auth::user()->id;

        $hora=Carbon::now();

        $bitacora = \DB::table('bitacoras')
        ->insert(['id_sucursal' => $id_sucursal, 
            'id_usuario' => $id_usuario,
            'descripcion' => "CATÁLOGO DE PROVEEDORES || Eliminar proveedor: ".$productos->nombre,
            'created_at' => $hora
        ]);

                DB::table('proveedores')->where('id',$request->input('id'))->delete();

        

        return redirect()->route('proveedores',['permisos'=>$permisos])->with('Mensajee', 'Proveedor eliminado con exito.');



    }

// ************************************************************************************************************
// ************************************************************************************************************

    public function actprov(Request $request)
    {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

        $proveedor =proveedores::where('id',$request->id)->first();
        return view ('proveedores/actprov', ['proveedor'=>$proveedor],['permisos'=>$permisos]);


    }

// ************************************************************************************************************
// ************************************************************************************************************

    public function updatep(Request $request, $id_editar)
    {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------

        $proveedor = proveedores::find($id_editar);

        $newnombre=$request->input('nombre');
        $newrfc=$request->input('rfc');
        $newcalle=$request->input('calle');
        $newestado=$request->input('estado');
        $newpais=$request->input('pais');
        $newdias_plazo=$request->input('dias_plazo');
        $newnumext=$request->input('num_exterior');
        $newobservaciones=$request->input('observaciones');
        $newtelefono=$request->input('telefono');
        $newcorreo=$request->input('email');
        $newcodigo=$request->input('codigo_postal');
        $newciudad=$request->input('ciudad');
        $newdescuento=$request->input('descuento');


        $proveedor->nombre=$newnombre;
        $proveedor->rfc=$newrfc;
        $proveedor->calle=$newcalle;
        $proveedor->num_exterior=$newnumext;
        $proveedor->observaciones=$newobservaciones;
        $proveedor->telefono=$newtelefono;
        $proveedor->email=$newcorreo;
        $proveedor->ciudad=$newciudad;
        $proveedor->codigo_postal=$newcodigo;
        $proveedor->descuento=$newdescuento;
        $proveedor->estado=$newestado;
        $proveedor->pais=$newpais;
        $proveedor->dias_plazo=$newdias_plazo;


        $proveedor->save();

        $id_sucursal = Auth::user()->idSucursal;
        $id_usuario = Auth::user()->id;

        $hora=Carbon::now();

        $bitacora = \DB::table('bitacoras')
        ->insert(['id_sucursal' => $id_sucursal, 
            'id_usuario' => $id_usuario,
            'descripcion' => "CATÁLOGO DE PROVEEDORES || Editar proveedor: ".$request->input('nombre'),
            'created_at' => $hora

        ]);

        return redirect()->route('proveedores',['permisos'=>$permisos])->with('Mensaje', 'Proveedor actualizado con exito.');

    }

     public function findLastP(){
        $proveedores = \DB::table('proveedores')
             ->select('proveedores.nombre', 'proveedores.rfc', 'proveedores.email', 'proveedores.telefono', 'proveedores.calle', 'proveedores.num_exterior', 'proveedores.codigo_postal', 'proveedores.ciudad', 'proveedores.estado', 'proveedores.pais', 'proveedores.dias_plazo', 'proveedores.descuento')
             ->orderBy('created_at', 'DESC')
             ->first();
        return response()->json($proveedores);
  }

// ************************************************************************************************************
// ************************************************************************************************************
    
    public function excel () {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        

        return \Excel::download(new ProvidersExport, 'proveedores.xlsx');
    }

}