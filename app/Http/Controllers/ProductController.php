<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Services\PayUService\Exception;
use Illuminate\Database\QueryException;

use Closure;
use App;
use App\Exports\ProductsExport;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\inventario;
use App\destino; 
use App\max_min;
use App\marca;
use Carbon\Carbon;



class ProductController extends Controller
{

 public function crearline(Request $request)
 {
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();


  $conlineas = DB::table('lineas')->select('descr')->get();
  $dec_lineas = json_decode($conlineas);
  echo $dec_lineas[0]->descr;
  for($dec=0; $dec<sizeof($dec_lineas); $dec++) {
    echo $dec_lineas[$dec]->descr;
    echo "\n";
    if($request->descr == $dec_lineas[$dec]->descr){
      return redirect()->route('lineas',['permisos'=>$permisos])->with('Mensajeu', 'Línea ya agregada, verifique de nuevo.');
    }            
  }

  \DB::table('lineas')->insert(
    ['descr' => $request->descr, 'created_at' => date('Y-m-d H:i:s')]);


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE LÍNEAS || Nueva línea: ".$request->descr,
    'created_at' => $hora
  ]);

  return redirect()->route('lineas',['permisos'=>$permisos])->with('Mensaje', 'Línea agregada con exito.');

}

public function lineas(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
  $lineas = \DB::table('lineas')
  ->get();
  return view ('productos.lineas', ['lineas'=>$lineas, 'permisos'=>$permisos]);


}



public function update_line(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();  

  $conlineas = DB::table('lineas')->select('descr')->get();
  $dec_lineas = json_decode($conlineas);
  echo $dec_lineas[0]->descr;
  for($dec=0; $dec<sizeof($dec_lineas); $dec++) {
    echo $dec_lineas[$dec]->descr;
    echo "\n";
    if($request->descr == $dec_lineas[$dec]->descr){
      return redirect()->route('lineas',['permisos'=>$permisos])->with('Mensajeu', 'Línea ya agregada, verifique de nuevo.');
    }            
  }

  $lineas = \DB::table('lineas')
  ->where('id', $request->input('id'))
  ->update(['descr' => $request->input('descr'), 'updated_at' => date('Y-m-d H:i:s')]
);

  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE LÍNEAS || Editar línea: ".$request->input('descr'),
    'created_at' => $hora
  ]);

  return redirect()->route('lineas',['permisos'=>$permisos] )->with('Mensajea', 'Línea actualizada con exito.');
}

public function eliminarline(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }


 try {

   $lineas=\DB::table('lineas')
  ->where('id', '=', $request->input('id'))
  ->first();


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE LÍNEAS || Eliminar línea: ".$lineas->descr,
    'created_at' => $hora
  ]);


  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
  \DB::table('lineas')->where('id', '=', $request->input('id') )->delete();

   
 } catch (Exception $e) {

  dd($hello);
   
 }



  return redirect()->route('lineas',['permisos'=>$permisos])->with('Mensajee', 'Línea eliminada con éxito.');

}

public function verline(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $sublineas = \DB::table('sublineas')

  ->where('id_linea', '=', $request->input('id') )
  ->get();
  $lineas = \DB::table('lineas')
  ->where('id', $request->input('id'))
  ->first();

  return view ('productos.verline', ['sublineas'=>$sublineas, 'lineas'=>$lineas, 'permisos'=>$permisos]);

}

public function crearsubline(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------

  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $conlineas = DB::table('sublineas')->select('descrip')->get();
  $dec_lineas = json_decode($conlineas);
  for($dec=0; $dec<sizeof($dec_lineas); $dec++) {
    if($request->descrip == $dec_lineas[$dec]->descrip){
      return redirect()->route('lineas',['permisos'=>$permisos])->with('Mensajee', 'Sublínea ya agregada, verifique de nuevo.');
    }            
  }

  $line = \DB::table('sublineas')
  ->where('id', $request->input('id'))
  ->update(['descrip' => $request->input('descrip')]);



  $conlineas = DB::table('sublineas')->select('descrip')->get();
  $dec_lineas = json_decode($conlineas);
  for($dec=0; $dec<sizeof($dec_lineas); $dec++) {
    if($request->descrip == $dec_lineas[$dec]->descrip){
      return redirect()->route('lineas',['permisos'=>$permisos])->with('Mensajee', 'Sublínea ya agregada, verifique de nuevo.');
    }            
  }

  \DB::table('sublineas')->insert(['descrip' =>  $request->descrip, 'id_linea' => $request->id_linea,  'created_at' => date('Y-m-d H:i:s') ]);


  $lineas=\DB::table('lineas')
  ->where('id', '=', $request->id_linea)
  ->select('lineas.descr')
  ->first();

  
  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CÁTALOGO DE SUBLÍNEAS || Nueva sublínea: ".$request->descrip.'; Línea '.$lineas->descr,
    'created_at' => $hora
  ]);


  return redirect()->route('lineas',['permisos'=>$permisos])->with('Mensaje', 'Sublínea agregada con exito.'); 
}

public function update_subline(Request $request)
{
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $line = \DB::table('sublineas')
  ->where('id', $request->input('id'))
  ->update(['descrip' => $request->input('descrip')]);



  $sublineas=\DB::table('sublineas')
  ->where('sublineas.id', '=', $request->input('id'))
  ->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')
  ->select('sublineas.*', 'lineas.descr')
  ->first();


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE SUBLÍNEAS || Editar sublínea: ".$request->input('descrip')."; Línea ".$sublineas->descr,
    'created_at' => $hora
  ]);



  return redirect()->route('lineas')->with('Mensajea', 'Sublínea   actualizada con éxito.');

}

public function eliminar_subline(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }


  $sublineas=\DB::table('sublineas')
  ->where('sublineas.id', '=', $request->input('id'))
  ->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')
  ->select('sublineas.*', 'lineas.descr')
  ->first();


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE SUBLÍNEAS || Eliminar sublínea: ".$sublineas->descrip.'; Línea '.$sublineas->descr,
    'created_at' => $hora
  ]);


  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
  \DB::table('sublineas')->where('id', '=', $request->input('id') )->delete();

  return redirect()->route('lineas',['permisos'=>$permisos])->with('Mensajee', 'Sublínea eliminada con éxito.');

}



public function versubline(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $sublineas = \DB::table('sublineas')
  ->where('id_linea', $request->input('id'))
  ->get();


  return view ('productos.verline', ['sublineas'=>$sublineas, 'permisos'=>$permisos]);


}


        /////////////////inicio de marcas


public function marca()
{
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $marcas = \DB::table('marcas')
  ->get();        
  return view ('productos/marca', ['marcas'=>$marcas, 'permisos'=>$permisos]);
}





public function update_marca(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
  $marcas = \DB::table('marcas')
  ->where('id', $request->input('id'))
  ->update(['descripcion' => $request->input('descripcion')]);


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CÁTALOGO DE MARCAS || Editar marca: ".$request->input('descripcion'),
    'created_at' => $hora
  ]);

  return redirect()->route('marca',['permisos'=>$permisos])->with('Mensajea', 'Marca   actualizada con éxito.');
}

public function eliminar_marca(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $marcas=\DB::table('marcas')
  ->where('id', '=', $request->input('id'))
  ->first();


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CÁTALOGO DE MARCAS || Eliminar marca: ".$marcas->descripcion,
    'created_at' => $hora
  ]);

  \DB::table('marcas')->where('id', '=', $request->input('id') )->delete();


  return redirect()->route('marca',['permisos'=>$permisos])->with('Mensajee', 'Marca  eliminada con éxito.');
}

public function crearmarca(Request $request)
{
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $conlineas = DB::table('marcas')->select('descripcion')->get();
  $dec_lineas = json_decode($conlineas);
  for($dec=0; $dec<sizeof($dec_lineas); $dec++) {
    if($request->descripcion == $dec_lineas[$dec]->descripcion){
      return redirect()->route('lineas',['permisos'=>$permisos])->with('Mensajee', 'Marca ya agregada, verifique de nuevo.');
    }            
  }


  \DB::table('marcas')->insert(
    ['descripcion' => $request->descripcion]);


  \DB::table('marcas')->insert(
    ['descripcion' => $request->descripcion]);


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CÁTALOGO DE MARCAS || Nueva marca: ".$request->descripcion,
    'created_at' => $hora
  ]);

  return redirect()->route('marca',['permisos'=>$permisos])->with('Mensaje', 'Marca agregada con exito.');

}




    /////////////////fin de marcas

        /////////////////inicio de productos


public function productos(){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
  $productos = \DB::table('productos')
  ->leftjoin('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
  ->join('marcas', 'marcas.id', '=', 'productos.id_marca')
  ->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
  ->join('satproductos', 'satproductos.id', '=', 'productos.codigosat')
  ->join('lineas', 'sublineas.id_linea', '=', 'lineas.id')
  ->select('productos.*', 'lineas.descr', 'satproductos.codigo as sat',  'marcas.descripcion','sublineas.descrip', 'proveedores.descuento')
  ->orderBy('productos.id')->get();

  $proveedores = \DB::table('proveedores')->select('id','nombre','rfc','calle', 'num_exterior','observaciones','telefono','email', 'descuento', 'created_at')->get();




  return view ('productos/productos', ['proveedores'=>$proveedores, 'productos'=>$productos, 'permisos'=>$permisos]);
}





public function eliminar_producto(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $productos=\DB::table('productos')
  ->where('id', '=', $request->input('id'))
  ->first();


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();


  try {

   $bitacora = \DB::table('bitacoras')
   ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE PRODUCTOS || Eliminar Producto: ".$productos->descripcion_producto,
    'created_at' => $hora
  ]);


   \DB::table('productos')->where('id', '=', $request->input('id') )->delete();


 } catch (Exception $e) {

  return redirect()->route('productos',['permisos'=>$permisos])->with('Mensajee', 'Este producto está en inventario, no se puede eliminar.');


}

return redirect()->route('productos',['permisos'=>$permisos])->with('Mensajee', 'Producto eliminado con éxito.');
}


public function crear_producto(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();


  $concodigo = DB::table('productos')->select('codigo')->get();
  $dec_lineas = json_decode($concodigo);

  for($dec=0; $dec<sizeof($dec_lineas); $dec++) {
    if($request->codigo == $dec_lineas[$dec]->codigo){
      return redirect()->route('crear_producto',['permisos'=>$permisos])->with('Mensajee', 'Código de producto ya existente, verifique de nuevo.');
    }            
  }
  $consat = DB::table('productos')->select('codigosat')->get();
  $dec_codigo = json_decode($consat);
  for($decc=0; $decc<sizeof($dec_codigo); $decc++) {
    if($request->codigosat == $dec_codigo[$decc]->codigosat){
      return redirect()->route('crear_producto',['permisos'=>$permisos])->with('Mensajee', 'Código SAT ya usado, verifique de nuevo.');
    }            
  }
  $productos = \DB::table('productos')
  ->select('productos.*')
  ->first();


  $proveedores = \DB::table('proveedores')
  ->select('proveedores.id', 'proveedores.nombre')
  ->get();

  $marcas = \DB::table('marcas')
  ->select('id', 'descripcion')
  ->get();

  $lineass = \DB::table('lineas')
  ->select('id', 'descr')
  ->get();

  $sublienas = \DB::table('sublineas')
  ->select('id', 'descrip')
  ->get();

  $sat = \DB::table('satproductos')
  ->get();

  return view ('productos.crear_producto', ['productos' => $productos, 'proveedores'=>$proveedores, 'sublienas'=>$sublienas, 'marcas'=>$marcas, 'permisos'=>$permisos, 'lineass'=>$lineass, 'sat'=>$sat]);


}


public function update_product(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $concodigo = DB::table('productos')->select('codigo')->get();
  $dec_lineas = json_decode($concodigo);

  for($dec=0; $dec<sizeof($dec_lineas); $dec++) {
    if($request->codigo == $dec_lineas[$dec]->codigo){
      return redirect()->route('productos',['permisos'=>$permisos])->with('Mensajee', 'Código de producto ya existente, verifique de nuevo.');
    }            
  }
  $consat = DB::table('productos')->select('codigosat')->get();
  $dec_codigo = json_decode($consat);
  for($decc=0; $decc<sizeof($dec_codigo); $decc++) {
    if($request->codigosat == $dec_codigo[$decc]->codigosat){
      return redirect()->route('productos',['permisos'=>$permisos])->with('Mensajee', 'Código SAT ya existente, verifique de nuevo.');
    }            
  }



  $productos = \DB::table('productos')
  ->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
  ->join('satproductos', 'satproductos.id', '=', 'productos.codigosat')

  ->join('marcas', 'marcas.id', '=', 'productos.id_marca')
  ->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
  ->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')
  ->select('productos.*', 'satproductos.codigo as codigosat_des',  'satproductos.id as codigosat',    'marcas.descripcion','sublineas.descrip', 'proveedores.nombre', 'lineas.descr')
  ->where('productos.id', $request->input('id'))
  ->first();

  $info = \DB::table('lineas')
  ->join('sublineas', 'lineas.id', '=', 'sublineas.id_linea')
  ->join('productos', 'sublineas.id', '=', 'productos.id_sublinea')
  ->select('lineas.id', 'lineas.descr')
  ->where('productos.id', $request->input('id'))
  ->first();


  $proveedores = \DB::table('proveedores')
  ->select('proveedores.id', 'proveedores.nombre')
  ->get();

  $lineass = \DB::table('lineas')
  ->select('id', 'descr')
  ->get();

  $marcas = \DB::table('marcas')
  ->select('id', 'descripcion')
  ->get();

  $sublienas = \DB::table('sublineas')
  ->select('id', 'descrip')
  ->get();


  $sat = \DB::table('satproductos')
  ->get();


  return view ('productos/updateproducto', ['lineas'=>$info->descr, 'productos'=>$productos, 'sat'=>$sat, 'proveedores'=>$proveedores, 'sublienas'=>$sublienas, 'marcas'=>$marcas, 'permisos'=>$permisos, 'lineass'=>$lineass ]);


}




public function update_product2(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();




  $affected = \DB::table('productos')
  ->where('id', $request->input('id'))
  ->update(['codigo' => $request->input('codigo'), 
    'codigosat' => $request->input('codigosat'),
    'descripcion_producto' => $request->input('descripcion_producto'),
    'precio' => $request->input('precio'),
    'costo' => $request->input('total'),
    'iva' => $request->input('iva'),
    'id_proveedor' => $request->input('id_proveedor'),
    'id_marca' => $request->input('id_marca'),
    'id_sublinea' => $request->input('id_sublinea'),
    'unidad' => $request->input('unidad')  ]);



  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE PRODUCTOS || Editar producto: ".$request->input('descripcion_producto'),
    'created_at' => $hora

  ]);



  return redirect()->route('productos',['permisos'=>$permisos])->with('Mensajea', 'Producto actualizado con éxito.');
  
}
public function new_producto(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $consulta_codigo = \DB::table('productos')->select('codigo')->get();
        // $consulta_sat = DB::table('productos')->select('codigosat')->get();

  foreach ($consulta_codigo as $conco => $value) {
    if($request->codigo == $conco){
      return redirect()->route('crear_producto',['permisos'=>$permisos])->with('Mensaje', 'Error, Código repetido, verifique de nuevo.'); 
    }
  }

  $nuevo= \DB::table('productos')->insert(['codigo' => $request->input('codigo'), 
    'codigosat' => $request->input('codigosat'),
    'descripcion_producto' => $request->input('descripcion_producto'),
    'precio' => $request->input('precio'),
    'costo' => $request->input('total'),
    'iva' => $request->input('iva'),
    'id_proveedor' => $request->input('id_proveedor'),
    'id_marca' => $request->input('id_marca'),
    'id_sublinea' => $request->input('id_sublinea'),
    'unidad' => $request->input('unidad')  ]);


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE PRODUCTOS || Nuevo producto: ".$request->input('descripcion_producto'),
    'created_at' => $hora

  ]);



  if($nuevo){

    $concodigo = DB::table('productos')->select('codigo')->get();
    $dec_lineas = json_decode($concodigo);

    for($dec=0; $dec<sizeof($dec_lineas); $dec++) {
      if($request->codigo == $dec_lineas[$dec]->codigo){
        return redirect()->route('productos',['permisos'=>$permisos])->with('Mensaje', 'Producto agregado con éxito.');
      }            
    }
    $consat = DB::table('productos')->select('codigosat')->get();
    $dec_codigo = json_decode($consat);
    for($decc=0; $decc<sizeof($dec_codigo); $decc++) {
      if($request->codigosat == $dec_codigo[$decc]->codigosat){
        return redirect()->route('crear_producto',['permisos'=>$permisos])->with('Mensajee', 'Código SAT ya usado, verifique de nuevo.');
      }            
    }
    $nuevo= \DB::table('productos')->insert(['codigo' => $request->input('codigo'), 
      'codigosat' => $request->input('codigosat'),
      'descripcion_producto' => $request->input('descripcion_producto'),
      'precio' => $request->input('precio'),
      'costo' => $request->input('total'),
      'iva' => $request->input('iva'),
      'id_proveedor' => $request->input('id_proveedor'),
      'id_marca' => $request->input('id_marca'),
      'id_sublinea' => $request->input('id_sublinea'),
      'unidad' => $request->input('unidad')
    ]);

    

    if($nuevo){

      // SI LA CONSULTA SE EJECUTA CORRECTAMENTE:

      \DB::commit();
      return redirect()->route('productos',['permisos'=>$permisos])->with('Mensaje', 'Producto añadido con éxito.');        
    }else{

      // SI LA CONSULTA -NO- SE EJECUTA CORRECTAMENTE:

      \DB::rollBack();
      return redirect()->route('productos',['permisos'=>$permisos])->with('Mensaje', 'OCurrió un error, inténtalo más tarde.');        
    }

  }


}

public function proveedores_producto(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;

  $permisos=permisos::buscar($id)->get();
  $proveeducts = \DB::table('proveeducts')
  ->join('proveedores', 'proveedores.id', '=', 'proveeducts.id_proveedor')
  ->join('productos', 'proveeducts.id_productos', '=', 'productos.id')
  ->select('proveeducts.id', 'proveeducts.id_proveedor', 'proveedores.nombre','productos.descripcion_producto')
  ->where('productos.id', $request->input('id'))
  ->get();



  $productos = \DB::table('productos')
  ->select('productos.descripcion_producto', 'id')
  ->where('productos.id', $request->input('id'))
  ->first();

  $proveedores = \DB::table('proveedores')
  ->select('id', 'nombre')
  ->get();

  return view ('productos/proveedores_producto', ['proveeducts'=>$proveeducts, 'productos'=>$productos, 'proveedores'=>$proveedores, 'permisos'=>$permisos]);

}

public function addprov (Request $request){

  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
  \DB::table('proveeducts')
  ->insert(['id_proveedor' => $request->input('id_proveedor'), 
    'id_productos' => $request->input('id_productos')]);



  return redirect()->route('productos',['permisos'=>$permisos])->with('Mensaje', 'Proveedor agregado con éxito.');

}

public function eliminar_proveeducto (Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
  \DB::table('proveeducts')->where('id', '=', $request->input('id') )->delete();


  return redirect()->route('productos',['permisos'=>$permisos])->with('Mensajee', 'Proveedor eliminado con éxito.');
}


    ////////////////termina productos





//who knows

public function prodfunct(){

 $prod = \DB::table('lineas')
 ->get();
 return view ('productos/crear_producto', ['prod'=>$prod]);



}

public function findProductName(Request $request){


      //if our chosen id and products table prod_cat_id col match the get first 100 data 

        //$request->id here is the id of our crearchosen option id
 $data = \DB::table('sublineas')
 ->where('id_linea',$request->id)
 ->get();
        return response()->json($data);//then sent this data to ajax success
      }
      public function findLastName(){


       $productos = \DB::table('productos')
       ->select('productos.*')
       ->orderBy('created_at', 'DESC')
       ->first();
       return response()->json($productos);
     }

     public function findBorrado(){


       $productos = \DB::table('productos')
       ->select('productos.*')
       ->orderBy('created_at', 'DESC')
       ->first();
       return response()->json($productos);
     }



// ----------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------



     PUBLIC function Max_min(){
       if(Auth::guard()->check()==null){
        return redirect()->route('login');
      }
      $id=Auth::user()->id;
      $permisos=permisos::buscar($id)->get();

      $productos=\DB::table('productos')
      ->select('*','max_mins.id as id_maxx')
      ->join('max_mins', 'productos.id', '=', 'max_mins.id_producto')
      ->join('inventarios','inventarios.id_sucursal','=','max_mins.id_sucursal')
      ->join('destinos','max_mins.id_sucursal','=','destinos.id')
      ->get();

        // echo $productos;
        // die();

      return view('Maximos/max_min',['permisos'=>$permisos,'productos'=>$productos]);

    }
// ----------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------

    public function add_Max_min(){
      // -------------------------------------------------------------------
     if(Auth::guard()->check()==null){
      return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();
        // ------------------------------------------------------------------
    $sucursal=destino::all();
    $inventarios=inventario::all();




    return view('Maximos/add_max_min',['permisos'=>$permisos,'sucursal'=>$sucursal,'inventarios'=>$inventarios]);

  }
// ----------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------

  public function findProduct(Request $request){


      //if our chosen id and products table prod_cat_id col match the get first 100 data 

        //$request->id here is the id of our crearchosen option id
    $data = \DB::table('inventarios')
    ->join('productos', 'productos.id', '=', 'inventarios.id_producto')
    ->where('id_sucursal',$request->id)
    ->get();
        return response()->json($data);//then sent this data to ajax success
      }



// ----------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------


      public function new_max_min(Request $request){
      // -------------------------------------------------------------------
        if(Auth::guard()->check()==null){
          return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // ------------------------------------------------------------------

        $hora=Carbon::now();
        // echo $hora;
        if($request->id_sucursal == null){
          return redirect()->route('add_max_min')->with('Mensajeua', 'Error al seleccionar sucursal, intente de nuevo');
        }elseif ($request->id_producto == null) {
         return redirect()->route('add_max_min')->with('Mensajeua', 'Error al seleccionar producto, intente de nuevo');
       }elseif ($request->minimo >= $request->maximo) {
        return redirect()->route('add_max_min')->with('Mensajeua', 'Error, el minimo es más grande que el máximo, intente de nuevo');
      }
      else{
        \DB::beginTransaction();
        
        if(
          \DB::table('max_mins')
          ->insert(['id_sucursal' => $request->input('id_sucursal'), 
            'id_producto' => $request->input('id_producto'),
            'maximo' => $request->input('maximo'),
            'minimo' => $request->input('minimo'),
            'created_at' => $hora
          ])
        ){

          \DB::commit();

        }else{
          DB::rollBack();
        }



        return redirect()->route('max_min')->with('Mensajea', 'Datos agregados con exito.');
      }


    }

// ----------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------

    public function updating_max_min(Request $request){

      if(Auth::guard()->check()==null){
        return redirect()->route('login');
      }
      $id=Auth::user()->id;
      $permisos=permisos::buscar($id)->get();
        // ------------------------------------------------------------------

      $hora=Carbon::now();
        // echo $hora;

      $datos=\DB::table('productos')
      ->select('*','max_mins.id as id_maxx')
      ->join('max_mins', 'productos.id', '=', 'max_mins.id_producto')
      ->join('inventarios','inventarios.id_sucursal','=','max_mins.id_sucursal')
      ->join('destinos','max_mins.id_sucursal','=','destinos.id')
      ->where('max_mins.id',$request->input('product_max_min'))
      ->get();
      $sucursal=destino::all();
      $inventarios=inventario::all();





      return view('Maximos/updating_max_min',['datos'=>$datos,'sucursal'=>$sucursal,'permisos'=>$permisos]);
    }

// ----------------------------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------

    public function update_max_min(Request $request){

      if(Auth::guard()->check()==null){
        return redirect()->route('login');
      }
      $id=Auth::user()->id;
      $permisos=permisos::buscar($id)->get();
        // ------------------------------------------------------------------

      $hora=Carbon::now();
        // echo $hora;
        // echo $request->input('id_max_min');
        // die();


      \DB::beginTransaction();

      if(
        \DB::table('max_mins')
        ->where('id',$request->input('id_max_min'))
        ->update(['id_sucursal' => $request->input('id_sucursal'), 
          'id_producto' => $request->input('id_producto'),
          'maximo' => $request->input('maximo'),
          'minimo' => $request->input('minimo'),
          'updated_at' => $hora
        ])
      ){

        \DB::commit();
        return redirect()->route('max_min')->with('Mensaje', 'Datos actualizados con exito.');

      }else{
        \DB::rollBack();
        return redirect()->route('max_min')->with('Mensajee', 'Ups! Algo salio mal :(            Datos NO actualizados');
      }





    }

    public function eliminar_max_min(Request $request){

    // echo "hola, soy el delete";
    // die();
      \DB::beginTransaction();
      $delete =\DB::table('max_mins')->where('id',$request->input('id_delete'))->delete();
      echo $delete;
    // echo $request->input('id_delete');
    // die();

      if($delete){

      // SI LA CONSULTA SE EJECUTA CORRECTAMENTE:

        \DB::commit();
        return redirect()->route('max_min')->with('Mensajee', 'Datos eliminados con exito.');
      }else{

      // SI LA CONSULTA -NO- SE EJECUTA CORRECTAMENTE:

        \DB::rollBack();
        return redirect()->route('max_min')->with('Mensajee', 'Datos NO eliminados');
      }



    }

  //funcion para obtener marcas
    public function getm()
    {
      $marcas = marca::get();
      $marcasArray[''] = 'Marcas';
      foreach ($marcas as $marca) {
        $marcasArray[$marca->id] = $marca->descripcion;
      }
      return $marcasArray;
    }


    public function excel () {
      return \Excel::download(new ProductsExport, 'Productos.xlsx');
    }



  }




