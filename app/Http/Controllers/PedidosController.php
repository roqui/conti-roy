<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use Barryvdh\DomPDF\Facade as PDF;

class PedidosController extends Controller
{
    public function new_pedido(){

        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        $clientes=\DB::table('clientes')
        ->get();

        $destinos=\DB::table('destinos')
        ->get();


        return view('Pedidos.new_pedido',['permisos'=>$permisos]);
    }

    // ************************************
// ************************************

    public function datos_cliente(Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        $date=Carbon::now()->toDateTimeString();
        $id_cliente=$request->input('id');
        $id_origen=Auth::user()->idSucursal;

        $cliente=DB::table('clientes')
                ->where('id', '=', $request->input('id') )
                ->first();

        


        $num_pedido=DB::table('pedidos')->max('id');
        $num_pedido=$num_pedido+1;
        // echo $num_pedido+1;
        // die();

       
       

        $productos=DB::table('productos')->select('productos.*')
        ->join('inventarios','id_producto','=','productos.id')->where('id_sucursal','=',Auth::user()->idSucursal)
        ->get();
        // ->join('clientes','id_origen','=','clientes.id')

        // \DB::table('pedidos')->insert([
        // ['CostoT' => 0,'id_destino' => $request->input('id'), 'id_origen' => $id_origen, 'Estado' => 1]
        //     ]);

        $tabla=DB::table('concepts')->where('idPedido',$num_pedido)->get();


        return view('pedidos/new_pedido',['permisos'=>$permisos, 'cliente'=>$cliente,'date'=>$date, 'productos'=>$productos,'num_pedido'=>$num_pedido,'tabla'=>$tabla,'id_cliente'=>$id_cliente]);





    }

    public function pedidos(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------


        $pedidos=DB::table('pedidos')
        ->where('Estado', '=', 1)
        ->count();

                $clientes=DB::table('clientes')
                ->get();



        return view('pedidos/pedidos',['permisos'=>$permisos,'clientes'=>$clientes, 'pedidos'=>$pedidos]);
    }

// ************************************
// ************************************

    public function info_pedido(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        return view('pedidos/info_pedido',['permisos'=>$permisos]);
    }
// ************************************
// ************************************

    public function ver_pedidos(){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        // Variables para liberacion de interfaces-----------------------------------
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // --------------------------------------------------------------------------
        $pedidos=DB::table('pedidos')
        ->select('pedidos.id','CostoT','Estado','id_origen','id_destino','pedidos.created_at','clientes.calle','clientes.colonia','clientes.num_exterior','clientes.ciudad','clientes.codigo_postal','estadopedidos.descripcion as estado','clientes.nombre','destinos.nombre as sucursal')
        ->join('clientes','id_origen','=','clientes.id')
        ->join('estadopedidos','Estado','=','estadopedidos.id')
        ->join('destinos','id_origen','=','destinos.id')
        ->get();
        return view('Pedidos/ver_pedidos',['permisos'=>$permisos,'pedidos'=>$pedidos]);
    }



    public function ver_remisionados(){

     if(Auth::guard()->check()==null){
        return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)
    ->get();

    return view('Pedidos/ver_remisionados',['permisos'=>$permisos]);

}
// ************************************************************************************************************
// ************************************************************************************************************

   public function pedidos_pendientes(){

     if(Auth::guard()->check()==null){
        return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)
    ->get();

    return view('Pedidos/ppendientes',['permisos'=>$permisos]);



}

// ************************************************************************************************************
// ************************************************************************************************************
 public function pedidos_cancelados(){


     if(Auth::guard()->check()==null){
        return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)
    ->get();

    return view('Pedidos/pedidos_cancelados',['permisos'=>$permisos]);



}

// ************************************************************************************************************
// ************************************************************************************************************

    public function agregar_concepto(Request $request){

         if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        $date=Carbon::now();        
        $productos=json_decode($request->input('id_producto'));

        


        // -------------------------------------------
        DB::table('pedidos')->insertOrIgnore([
            ['id'=>$request->input('num_pedido'),
            'CostoT'=>0,
            'Estado'=>1,
            'id_origen'=>Auth::user()->idSucursal,
            'id_destino'=>$request->input('id_cliente'),
            'created_at'=>$date,
            'usuario'=>Auth::user()->name]

        ]); 
        
        // ----------------------------------------------

        for ($i=0; $i < count($productos); $i++){
                $inventario=DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$productos{$i}]])->first();
                

                $precio=DB::table('productos')->where('id',$productos{$i})->get();
                foreach ($precio as $key => $value) {
                     
                    $precioU=$value->precio;
                }
                // -------------------------------------------------
                $acumulado=DB::table('pedidos')->where('id', $request->input('num_pedido'))->get();

                foreach ($acumulado as $key => $value) {
                    $acumulado2=$value->CostoT;
                }
                // -------------------------------------------------
                $total=$precioU+$acumulado2;
                $cant_disponible=$inventario->cant_disponible;
                $minimo=$inventario->minimos;
                if($cant_disponible>0){
                    if($cant_disponible<$minimo){
                        echo "Alerta! Estas en el minímo de tu producto!";
                    }else{
                        echo "Concepto agregado. inventario disponible: ".($cant_disponible-1); 
                    }
                    DB::table('pedidos')
                      ->where('id', $request->input('num_pedido'))
                      ->update(['CostoT' => $total]);
                    DB::table('concepts')->insertOrIgnore([
                    ['idPedido' => $request->input('num_pedido'),
                    'idProducto'=> $productos{$i},
                    'cantidad'  => 1,
                    'subtotal'  => $precioU,
                    'estado'    => 1,
                    'created_at'=> $date]
                ]);
                    DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$productos{$i}]])->update(['cant_disponible'=>$cant_disponible-1]);
                    DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$productos{$i}]])
                            ->update(['cant_apartada'=>$inventario->cant_apartada+1]);   
                
                }else{
                    echo "Ya no tienes suficiente inventario! de uno de estos productos :( Realiza una compra!";
                    die();
                }

                 
                

            }
        
            


        

    }

// ************************************************************************************************************
// ************************************************************************************************************

    public function tabla_dinamica(Request $request){
         if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // ------------------------------------------------
        $pedido=$request->input('pedido');
        $num_pedido=$_GET['num_pedido'];
        $pedido=DB::table('concepts')
        ->select('concepts.id','idProducto','cantidad','unidad','p.codigo','p.descripcion_producto','p.precio','descuento')
        ->join('productos as p','p.id','=','idProducto')
        ->where('idPedido','=',$num_pedido)
        ->get();
        $total=DB::table('pedidos')->where('id',$num_pedido)->get();
        // return view('Pedidos.tabla_dinamica',['permisos'=>$permisos,'pedido'=>$pedido]);
        return view('Pedidos.tabla_dinamica',['permisos'=>$permisos,'pedido'=>$pedido,'id'=>$num_pedido,'total'=>$total]);

    }

    // ************************************************************************************************************
// ************************************************************************************************************


    public function eliminar_concepto(Request $request){
        $id_concepto=$request->input('id_concepto');
        $concepto=DB::table('concepts')->where('id',$id_concepto)->get();
        

        foreach ($concepto as $key => $value) {
            $id_pedido=$value->idPedido;
            $subtotal=$value->subtotal;
            $cantidad=$value->cantidad;
            $id_producto=$value->idProducto;
        }

        $pedido=DB::table('pedidos')->where('id',$id_pedido)->get();

        foreach ($pedido as $key => $value) {
            # code...
            //se supone que deberia restar el subtotal de la cuenta pero ne
            $total=$value->CostoT;
        }

        $total_final=($total-$subtotal);
        $inventario=DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])->first();
        DB::table('pedidos')->where('id',$id_pedido)->update(['CostoT'=>$total_final]);
        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])->update(['cant_disponible'=>$inventario->cant_disponible+$cantidad]);
        DB::table('concepts')->where('id',$id_concepto)->delete();
        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                            ->update(['cant_apartada'=>$inventario->cant_apartada-$cantidad]); 
    }

    
// ************************************************************************************************************
// ************************************************************************************************************



    public function imprimir(Request $request){
         if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        // ------------------------------------------------
        $id_pedido=$request->input('id_pedido');
        $pedido=DB::table('pedidos')->where('id',$id_pedido)->get();
        foreach ($pedido as $key => $value) {
             $id=$value->id;
             $id_sucursal=$value->id_origen;
             $id_cliente=$value->id_destino;
             $date=$value->created_at;
             $total=$value->CostoT;
             $usuario=$value->usuario;

        }
        $cliente=DB::table('clientes')->where('id',$id_cliente)->first();
        $sucursal=DB::table('destinos')->where('id',$id_sucursal)->first();
        $tabla=DB::table('concepts')
        ->select('concepts.id','idProducto','cantidad','unidad','p.codigo','p.descripcion_producto','p.precio')
        ->join('productos as p','p.id','=','idProducto')
        ->where('idPedido','=',$id_pedido)
        ->get();



        // $pdf=PDF::loadView('Pedidos.nota_pedido',['permisos'=>$permisos,'pedido'=>$pedido,'cliente'=>$cliente,'date'=>$date,'tabla'=>$tabla,'total'=>$total])->setPaper('a4', 'landscape')->setWarnings(false);
        $pdf=PDF::loadView('Pedidos.nota_pedido',['permisos'=>$permisos,'pedido'=>$pedido,'cliente'=>$cliente,'date'=>$date,'tabla'=>$tabla,'total'=>$total,'usuario'=>$usuario]);

            
            // return view('Pedidos.nota_pedido',['permisos'=>$permisos,'pedido'=>$pedido,'cliente'=>$cliente,'date'=>$date,'tabla'=>$tabla,'total'=>$total,'usuario'=>$usuario]);

            // return view('pedidos/new_pedido',['permisos'=>$permisos, 'cliente'=>$cliente,'date'=>$date, 'productos'=>$productos,'num_pedido'=>$num_pedido,'tabla'=>$tabla,'id_cliente'=>$id_cliente]);

            
                 return $pdf->download('nota_pedido.pdf');
            

           

    }

    public function imprimirSiNo(Request $request){
        $id_pedido=$request->id_pedido;

        $resp=DB::table('pedidos')->where('id',$id_pedido)->count();
        

        return $resp;
    }

    public function act_cantidad(Request $request){
        $id_concepto=$request->input('id_concepto');
        $cantidad_actual=$request->input('actual');
        $accion=$request->input('accion');
        $id_pedido=$request->input('id_pedido');
        $id_producto=$request->input('id_producto');
        $precio=$request->input('precio');

        // --------------------------------
        $total_concepto=DB::table('concepts')->select('subtotal')->where('id',$id_concepto)->get();
        $total_pedido=DB::table('pedidos')->select('CostoT')->where('id',$id_pedido)->get();
        $inventario=DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])->first();
        $cant_disponible=$inventario->cant_disponible;
        $cant_apartada=$inventario->cant_apartada;
        $minimo=$inventario->minimos;

        
        foreach ($total_concepto as $key => $value) {
            $total_concepto2=($value->subtotal);
        }
        foreach ($total_pedido as $key => $value) {
            $total_pedido2=($value->CostoT);
        }
        
        

            if($accion==2){
                if($cant_disponible>0){
                    if($cant_disponible<=$minimo){
                        echo "Alerta! Estas en el minímo de tu producto!";
                    }else{
                        echo "Concepto agregado. inventario disponible: ".($cant_disponible-1); 
                    }
                    DB::table('concepts')->where('id',$id_concepto)->update(['subtotal'=>$total_concepto2+$precio]);
                    DB::table('concepts')->where('id',$id_concepto)->update(['cantidad'=>$cantidad_actual+1]);
                    DB::table('pedidos')->where('id',$id_pedido)->update(['CostoT'=>$total_pedido2+$precio]);
                    DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                    ->update(['cant_disponible'=>$inventario->cant_disponible-1]);
                    DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                            ->update(['cant_apartada'=>$inventario->cant_apartada+1]);
                }else{
                    echo "Ya no tienes suficiente inventario! de uno de estos productos :( Realiza una compra!";
                }

            }elseif ($accion==1) {
                if($cantidad_actual<=0){
                    echo "Ya no tienes podructo en tu concepto. Se eliminará automaticamente.";
                    DB::table('concepts')->where('id',$id_concepto)->delete();
                }else{
                    DB::table('concepts')->where('id',$id_concepto)->update(['subtotal'=>$total_concepto2-$precio]);
                    DB::table('concepts')->where('id',$id_concepto)->update(['cantidad'=>$cantidad_actual-1]);
                    DB::table('pedidos')->where('id',$id_pedido)->update(['CostoT'=>$total_pedido2-$precio]);
                    DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                        ->update(['cant_disponible'=>$inventario->cant_disponible+1]);
                        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                            ->update(['cant_apartada'=>$inventario->cant_apartada-1]);
                    echo "Producto Devuelto Exitosamente!";
                }
            }   
        



    }

    public function observaciones_pedido(Request $request){
        $observaciones=$request->input('observaciones');
        $id_pedido=$request->input('id_pedido');
        DB::table('pedidos')->where('id',$id_pedido)->update(['observaciones'=>$observaciones]);
         return redirect()->route('pedido');
    }

    

     public function todos(){

        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $id_sucursal = Auth::user()->idSucursal;

        $pedidos= \DB::table('pedidos')
                        ->join('destinos', 'destinos.id', 'pedidos.id_origen')
                        ->join('clientes', 'clientes.id', 'pedidos.id_destino')
                        ->join('estadopedidos', 'estadopedidos.id', 'pedidos.estado') 
                        ->select('pedidos.id','pedidos.CostoT','destinos.nombre as origen', 'clientes.nombre as destino', 'pedidos.created_at', 'pedidos.estado')
                        ->where([
                                 ['id_origen','=',$id_sucursal]
                                ])->get();

        return view('Pedidos/todos',['permisos'=>$permisos, 'pedidos'=>$pedidos]);
    }

    public function info(Request $request){

         if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $pedido= \DB::table('pedidos')
                        ->join('destinos', 'destinos.id', 'pedidos.id_origen')
                        ->join('clientes', 'clientes.id', 'pedidos.id_destino')
                        ->join('estadopedidos', 'estadopedidos.id', 'pedidos.estado') 
                        ->select('pedidos.id','pedidos.CostoT','destinos.nombre as origen', 'clientes.nombre as destino', 'pedidos.created_at', 'pedidos.estado', 'pedidos.created_at', 'clientes.calle as callec', 'clientes.ciudad as ciudadc', 'clientes.estado as estadoc', 'clientes.pais as paisc', 'clientes.rfc')
                        ->where([
                                 ['pedidos.id','=',$request->idpedido]
                                ])->first();
        $surtirs = \DB::table('surtirs')
                        ->join('users', 'surtirs.idsurtidor', 'users.id')
                        ->select('surtirs.*', 'users.name')->where('idPedido', '=', $request->idpedido)->first();
        $checars="";
        $envios="";

        if($surtirs){
            $checars = \DB::table('checars')->select('checars.*')->where('idSurtido','=',$surtirs->id)->first();

            if($checars){
                $envios =  \DB::table('sends')
                        ->join('users', 'sends.idconductor', 'users.id')
                        ->select('sends.*', 'users.name')->where('idchecado', '=', $checars->id)->first();
            }
        }
        

        



        return view('Pedidos/infopedido',['permisos'=>$permisos, 'p'=>$pedido, 's'=>$surtirs, 'c'=>$checars, 'e'=>$envios]);

    }

    public function cancelar_nota(Request $request){
        $id_pedido=$request->input('id_pedido');
        $pedido=DB::table('pedidos')->where('id',$id_pedido);
        $conceptos=DB::table('concepts')->where('idPedido',$id_pedido)->get();

        
    foreach ($conceptos as $key => $value) {
    $id_concepto=$value->id;
    $id_producto=$value->idProducto;
    $cantidad=$value->cantidad;

    $inventario=DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])->first();
    DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
            ->update(['cant_disponible'=>$inventario->cant_disponible+$cantidad]);
            DB::table('concepts')->where('id',$id_concepto)->delete();
        DB::table('inventarios')->where([['id_sucursal',Auth::user()->idSucursal],['id_producto',$id_producto]])
                            ->update(['cant_apartada'=>$inventario->cant_apartada-$cantidad]); 



    }
        DB::table('pedidos')->where('id',$id_pedido)->delete();

        return redirect()->route('index');

    }


}