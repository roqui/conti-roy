<?php

namespace App\Http\Controllers;

use App\Task;
use App\Tickets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\permisos;



class TasksController extends Controller
{
 
   
  

    public function taskindex()
    {


        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $tasks = Task::all();


        return view('compras.calendariocompras',['permisos'=>$permisos, 'tasks'=>$tasks]);
    }

    public function edit ()
    {
        dd('hello');
    }

    public function store(Request $request)
    {
        Task::create($request->all());
        return redirect()->route('tasks.index');
    }
}
