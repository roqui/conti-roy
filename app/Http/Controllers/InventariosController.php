<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Exports\InventariosExport;
use App\inventario;
use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\inventaio;
use Carbon\Carbon;



class InventariosController extends Controller
{

  public function indexinventarios (){
   if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();


  $id_sucursal = Auth::user()->idSucursal;


  $inventarios = \DB::table('inventarios')
  ->join('productos', 'productos.id', '=', 'inventarios.id_producto')
  ->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
  ->join('marcas', 'marcas.id', '=', 'productos.id_marca')
  ->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
  ->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')

  ->join('destinos', 'destinos.id', '=', 'inventarios.id_sucursal')

  ->select('productos.codigo', 'productos.id as id_product', 'proveedores.nombre as proveedor',  'marcas.descripcion', 'sublineas.descrip', 'sublineas.id_linea', 'lineas.descr', 'inventarios.created_at', 'inventarios.*', 'productos.descripcion_producto', 'destinos.nombre', 'destinos.nombre')

  ->get();



  $productos = \DB::table('productos')
  ->leftJoin('inventarios', 'productos.id', '=', 'inventarios.id_producto')
  ->get();

  $sucursales = \DB::table('destinos')
  ->get();

  
  return view('inventarios.indexinventarios', ['inventarios'=>$inventarios, 'sucursales'=>$sucursales, 'permisos'=>$permisos, 'productos'=>$productos]);

}

public function searchinventarios(Request $request){

 if(Auth::guard()->check()==null){
  return redirect()->route('login');
}
$id=Auth::user()->id;
$permisos=permisos::buscar($id)->get();


$id_sucursal = Auth::user()->idSucursal;


$inventarios = \DB::table('inventarios')
->join('productos', 'productos.id', '=', 'inventarios.id_producto')
->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
->join('marcas', 'marcas.id', '=', 'productos.id_marca')
->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')

->join('destinos', 'destinos.id', '=', 'inventarios.id_sucursal')
->where('inventarios.id_sucursal', '=', $request->input('id_sucursal'))

->select('productos.codigo', 'productos.id as id_product', 'proveedores.nombre as proveedor',  'marcas.descripcion', 'sublineas.descrip', 'sublineas.id_linea', 'lineas.descr', 'inventarios.created_at', 'inventarios.*', 'productos.descripcion_producto', 'destinos.nombre', 'destinos.nombre', 'destinos.id as id_sucursal')

->get();



$productos = \DB::table('inventarios')
->join('productos', 'productos.id', '=', 'inventarios.id_producto')
->select('productos.id', 'productos.codigo', 'productos.descripcion_producto')
->get();

$sucursales = \DB::table('destinos')
->get();

$sucursal = \DB::table('destinos')
->where('id', '=', $request->input('id_sucursal'))
->first();


return view('inventarios.inventariosucursal', ['inventarios'=>$inventarios, 'sucursales'=>$sucursales, 'permisos'=>$permisos, 'productos'=>$productos, 'sucursal'=>$sucursal]);

}



public function inventarios (){
 if(Auth::guard()->check()==null){
  return redirect()->route('login');
}
$id=Auth::user()->id;
$permisos=permisos::buscar($id)->get();


$id_sucursal = Auth::user()->idSucursal;


$inventarios = \DB::table('inventarios')
->join('productos', 'productos.id', '=', 'inventarios.id_producto')
->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
->join('marcas', 'marcas.id', '=', 'productos.id_marca')
->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')

->select('productos.codigo', 'productos.id as id_product', 'proveedores.nombre as proveedor',  'marcas.descripcion', 'sublineas.descrip', 'sublineas.id_linea', 'lineas.descr', 'inventarios.created_at', 'inventarios.*', 'productos.descripcion_producto')

->where('inventarios.id_sucursal', '=', $id_sucursal)
->get();


$productos = \DB::table('productos')
->get();




return view('inventarios.inventarios', ['inventarios'=>$inventarios, 'permisos'=>$permisos, 'productos'=>$productos]);

}

public function new_producto_inventario(Request $request){

  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $conproductos = DB::table('productos')->select('id')->get();
  $dec_producto = json_decode($conproductos);

  $id_sucursal = Auth::user()->idSucursal;


  $nuevo= \DB::table('inventarios')
  ->insert(['id_producto' => $request->input('id_producto') , 
    'id_sucursal' => $id_sucursal , 
    'maximos' => $request->input('maximos') , 
    'minimos'=> $request->input('minimos'), 
    'cant_disponible' => $request->input('cant_disponible'),
    'cant_apartada' => '0' ]);

  $producto = \DB::table('productos')
  ->where('productos.id', '=', $request->input('id_producto'))
  ->first();


  if ($nuevo) {

    $id_sucursal = Auth::user()->idSucursal;
    $id_usuario = Auth::user()->id;

    $hora=Carbon::now();

    $bitacora = \DB::table('bitacoras')
    ->insert(['id_sucursal' => $id_sucursal, 
      'id_usuario' => $id_usuario,
      'descripcion' => "INVENTARIO || Nuevo producto  ".$producto->descripcion_producto.', Código: '.$producto->codigo,
      'created_at' => $hora
    ]);

    $productos = \DB::table('productos')
    ->get();

    $inventarios = \DB::table('inventarios')
    ->join('productos', 'productos.id', '=', 'inventarios.id_producto')
    ->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
    ->join('marcas', 'marcas.id', '=', 'productos.id_marca')
    ->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
    ->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')

    ->select('productos.*', 'productos.id as id_product', 'proveedores.nombre as proveedor',  'marcas.descripcion', 'sublineas.descrip', 'sublineas.id_linea', 'lineas.descr', 'inventarios.created_at', 'inventarios.*')

    ->where('inventarios.id_sucursal', '=', $id_sucursal)
    ->get();



    return redirect()->route('inventarios',['permisos'=>$permisos, 'inventarios'=>$inventarios,  'productos'=>$productos])->with('Mensajeu', 'Producto agregado con éxito.');
  }
  else{

    $productos = \DB::table('productos')
    ->get();

    $inventarios = \DB::table('inventarios')
    ->join('productos', 'productos.id', '=', 'inventarios.id_producto')
    ->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
    ->join('marcas', 'marcas.id', '=', 'productos.id_marca')
    ->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
    ->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')

    ->select('productos.*', 'productos.id as id_product', 'proveedores.nombre as proveedor',  'marcas.descripcion', 'sublineas.descrip', 'sublineas.id_linea', 'lineas.descr', 'inventarios.created_at', 'inventarios.*')

    ->where('inventarios.id_sucursal', '=', $id_sucursal)
    ->get();



    return redirect()->route('inventarios',['permisos'=>$permisos, 'inventarios'=>$inventarios,  'productos'=>$productos])->with('Mensajee', 'Algo salió mal, inténta más tarde.'); 


  }

  



}

public function new_producto_inventario2(Request $request){

  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $conproductos = DB::table('productos')->select('id')->get();
  $dec_producto = json_decode($conproductos);

  $id_sucursal = Auth::user()->idSucursal;


  $nuevo= \DB::table('inventarios')
  ->insert(['id_producto' => $request->input('id_producto') , 
    'id_sucursal' => $request->input('id_sucursal') , 
    'maximos' => $request->input('maximos') , 
    'minimos'=> $request->input('minimos'), 
    'cant_disponible' => $request->input('cant_disponible'),
    'cant_apartada' => '0' ]);

  $productos = \DB::table('productos')
  ->get();

  $inventarios = \DB::table('inventarios')
  ->join('productos', 'productos.id', '=', 'inventarios.id_producto')
  ->join('proveedores', 'proveedores.id', '=', 'productos.id_proveedor')
  ->join('marcas', 'marcas.id', '=', 'productos.id_marca')
  ->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
  ->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')

  ->select('productos.*', 'productos.id as id_product', 'proveedores.nombre as proveedor',  'marcas.descripcion', 'sublineas.descrip', 'sublineas.id_linea', 'lineas.descr', 'inventarios.created_at', 'inventarios.*')

  ->where('inventarios.id_sucursal', '=', $id_sucursal)
  ->get();



  return redirect()->route('indexinventarios',['permisos'=>$permisos, 'inventarios'=>$inventarios,  'productos'=>$productos])->with('Mensaje', 'Producto agregado con éxito.');




}

public function actualizar_producto_inventario(Request $request){



  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $producto = \DB::table('inventarios')
  ->join ('productos', 'productos.id', '=', 'inventarios.id_producto')
  ->where('inventarios.id', '=', $request->input('id'))
  ->select('productos.descripcion_producto', 'productos.codigo', 'inventarios.cant_disponible', 'inventarios.maximos', 'inventarios.minimos')
  ->first();


  $affected = \DB::table('inventarios')
  ->where('id', $request->input('id'))
  ->update(['cant_disponible' => $request->input('cant_disponible'), 'maximos' => $request->input('maximos'), 'minimos' => $request->input('minimos')]);

  if ($affected) {

    $id_sucursal = Auth::user()->idSucursal;
    $id_usuario = Auth::user()->id;

    $hora=Carbon::now();

    $bitacora = \DB::table('bitacoras')
    ->insert(['id_sucursal' => $id_sucursal, 
      'id_usuario' => $id_usuario,
      'descripcion' => "INVENTARIO || Editar producto  ".$producto->descripcion_producto.', Código: '.$producto->codigo.', Cantidad Disponible: '.$producto->cant_disponible.' >> '.$request->input('cant_disponible').', Mínimo: '.$producto->minimos.' >> '.$request->input('minimos').', Máximo: '.$producto->maximos.' >> '.$request->input('maximos') ,
      'created_at' => $hora
    ]);

    return redirect()->route('inventarios',['permisos'=>$permisos])->with('Mensajea', 'Producto actualizado con éxito.'); 
  }
  else{

    return redirect()->route('inventarios',['permisos'=>$permisos])->with('Mensajee', 'Algo salió mal, inténta más tarde.'); 


  }
}


public function actualizar_producto_inventario2(Request $request){


  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $producto = \DB::table('inventarios')
  ->join ('productos', 'productos.id', '=', 'inventarios.id_producto')
  ->where('inventarios.id', '=', $request->input('id'))
  ->select('productos.descripcion_producto', 'productos.codigo', 'inventarios.cant_disponible', 'inventarios.maximos', 'inventarios.minimos')
  ->first();


  $affected = \DB::table('inventarios')
  ->where('id', $request->input('id'))
  ->update(['cant_disponible' => $request->input('cant_disponible'), 'maximos' => $request->input('maximos'), 'minimos' => $request->input('minimos')]);

  if ($affected) {

    $id_sucursal = Auth::user()->idSucursal;
    $id_usuario = Auth::user()->id;

    $hora=Carbon::now();

    $bitacora = \DB::table('bitacoras')
    ->insert(['id_sucursal' => $id_sucursal, 
      'id_usuario' => $id_usuario,
      'descripcion' => "INVENTARIO || Editar producto  ".$producto->descripcion_producto.', Código: '.$producto->codigo.', Cantidad Disponible: '.$producto->cant_disponible.' >> '.$request->input('cant_disponible').', Mínimo: '.$producto->minimos.' >> '.$request->input('minimos').', Máximo: '.$producto->maximos.' >> '.$request->input('maximos') ,
      'created_at' => $hora
    ]);

    return redirect()->route('indexinventarios',['permisos'=>$permisos])->with('Mensajea', 'Producto actualizado con éxito.'); 
  }
  else{

    return redirect()->route('indexinventarios',['permisos'=>$permisos])->with('Mensajee', 'Algo salió mal, inténta más tarde.'); 


  }




}

public function eliminar_inventario_producto(Request $request){

 if(Auth::guard()->check()==null){
  return redirect()->route('login');
}
$id=Auth::user()->id;
$permisos=permisos::buscar($id)->get();

$cant_disponible = \DB::table('inventarios')
->where('id', '=', $request->input('id'))
->select('cant_disponible')
->first();

$producto = \DB::table('inventarios')
->join ('productos', 'productos.id', '=', 'inventarios.id_producto')
->where('inventarios.id', '=', $request->input('id'))
->select('productos.descripcion_producto', 'productos.codigo', 'productos.id')
->first();



if ($cant_disponible->cant_disponible<=0) {

  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "INVENTARIO || Eliminar producto:  ".$producto->descripcion_producto.'; Código: '.$producto->codigo,
    'created_at' => $hora
  ]);

  \DB::table('inventarios')
  ->where('id', '=', $request->input('id'))
  ->delete();

  return redirect()->route('inventarios',['permisos'=>$permisos])->with('Mensajee', 'Producto eliminado con éxito.'); 

  
}
else{

  return redirect()->route('inventarios',['permisos'=>$permisos])->with('Mensajee', 'ERROR. El producto aún está en existencia.'); 


}






}

public function excel () {

  return \Excel::download(new InventariosExport, 'Inventarios.xlsx');
}


public function estadisticas(){
 if(Auth::guard()->check()==null){
  return redirect()->route('login');
}
$id=Auth::user()->id;
$permisos=permisos::buscar($id)->get();

$valoresY=array();
$valoresX=array();

$lineas = \DB::table('lineas')
->get();

foreach ($lineas as $linea ) {

 $valoresY[]=$linea->descr;

}

$datosY=json_encode($valoresY);

foreach ($lineas as $linea) {

 $sub = \DB::table('productos')
 ->join('sublineas', 'sublineas.id', '=', 'productos.id_sublinea')
 ->join('lineas', 'lineas.id', '=', 'sublineas.id_linea')
 ->where('id_linea', '=', $linea->id)
 ->count();

 $valoresX[]=$sub;


}


$datosX=json_encode($valoresX);


   //piechart 

$pedidos=\DB::table('pedidos')
->count();

$pendiente = \DB::table('pedidos')
->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
->where('Estado', '=', 1)
->count();

$surtiendo = \DB::table('pedidos')
->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
->where('Estado', '=', 2)
->count();

$remisionado = \DB::table('pedidos')
->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
->where('Estado', '=', 3)
->count();

$enviado = \DB::table('pedidos')
->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
->where('Estado', '=', 4)
->count();

$cancelado = \DB::table('pedidos')
->join('estadopedidos', 'estadopedidos.id', '=', 'pedidos.Estado')
->where('Estado', '=', 5)
->count();

$pendiente = ($pendiente*100)/$pedidos;
$surtiendo = ($surtiendo*100)/$pedidos;
$remisionado = ($remisionado*100)/$pedidos;
$enviado = ($enviado*100)/$pedidos;
$cancelado = ($cancelado*100)/$pedidos;


$pieY=array();
$pieY[]=$pendiente;
$pieY[]=$surtiendo;
$pieY[]=$remisionado;
$pieY[]=$enviado;
$pieY[]=$cancelado;

$piejsonY=json_encode($pieY);


$pieX=array();
$pieX[]='Pendiente';
$pieX[]='Surtiendo';
$pieX[]='Remisionado';
$pieX[]='Enviado';
$pieX[]='Cancelado';

$piejsonX=json_encode($pieX);


return view('inventarios.charts', ['permisos'=>$permisos, 'datosY'=>$datosY, 'datosX'=>$datosX, 'piejsonX'=>$piejsonX, 'piejsonY'=>$piejsonY]);




}

}