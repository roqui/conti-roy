<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;
use App\Exports\InventariosExport;
use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\inventaio;

class PedidosPendientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ppendientes()
    {

        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $id_sucursal = Auth::user()->idSucursal;

        $ppendientes= \DB::table('pedidos')
                        ->join('destinos', 'destinos.id', 'pedidos.id_origen')
                        ->join('clientes', 'clientes.id', 'pedidos.id_destino')
                        ->select('pedidos.id','pedidos.CostoT','destinos.nombre as origen', 'clientes.nombre as destino', 'pedidos.created_at')
                        ->where([['pedidos.Estado','=','1'],
                                 ['id_origen','=',$id_sucursal]
                                ])->get();
        

        return view('Pedidos/ppendientes', ['permisos'=>$permisos, 'ppendientes'=>$ppendientes]);
    }

    public function asignarpp (Request $request) {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        $surtidores=\DB::table('Users')->where([['puesto','=', '1'],['idSucursal','=',Auth::user()->idSucursal]])->get();

        return view('Pedidos/asignarpp',['permisos'=>$permisos, 'surtidores'=>$surtidores, 'idPedido'=>$request->idpedido]);
    }

    public function asignadopp (Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        \DB::table('surtirs')->insert(['idPedido'=>$request->idpedido, 'idSurtidor'=>$request->idsurtidor,'estado'=>'INICIADO', 'created_at'=>date('Y-m-d H:i:s')]);
        $nombre=\DB::table('Users')->select('name')->where([['id','=', $request->idsurtidor]])->first();

        \DB::table('pedidos')->where('id', '=', $request->idpedido)->update(['Estado'=>'2']);


        $id_sucursal = Auth::user()->idSucursal;

        $ppendientes= \DB::table('pedidos')
                        ->join('destinos', 'destinos.id', 'pedidos.id_origen')
                        ->join('clientes', 'clientes.id', 'pedidos.id_destino')
                        ->select('pedidos.id','pedidos.CostoT','destinos.nombre as origen', 'clientes.nombre as destino', 'pedidos.created_at')
                        ->where([['pedidos.estado','=','1'],
                                 ['id_origen','=',$id_sucursal]
                                ])->get();

        return view('Pedidos/ppendientes',['permisos'=>$permisos, 'ppendientes'=>$ppendientes])->with('Mensaje', ' ');

    }

    public function conceptos(Request $request) {
        $data="";
        $con=\DB::table('concepts')->select('concepts.id','productos.codigo','productos.descripcion_producto','concepts.cantidad','productos.precio')
                                    ->join('productos', 'concepts.idProducto', 'productos.id')
                                    ->where([['idPedido','=',$request->id]])->get();

        $data="<div class='table-responsive'>
                <table class='table'>
                  <thead class='thead-light'>
                    <tr>
                      <th scope='col' >ID</th>
                      <th scope='col' >Código</th>
                      <th scope='col'>Descripcion</th>
                      <th scope='col'>#</th>
                      <th scope='col'>$/U</th>
                      <th scope='col'>$</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    ";
        $totalart=0;
        $totalcost=0;

                      foreach ($con as $c){
                        $totalcost=$totalcost+$c->precio*$c->cantidad;
                        $totalart=$totalart+$c->cantidad;
                        $data=$data."<tr><th scope='row'>".$c->id."</th>
                                <td>".$c->codigo."</td>
                                <td>".$c->descripcion_producto."</td>
                                <td>".$c->cantidad."</td>
                                <td>".$c->precio."</td>
                                <td>$".$c->precio*$c->cantidad.".00</td>
                                </tr>";
                      }
                        
        $data=$data."<tr><td class='table-primary'></td><td class='table-primary'></td><td class='table-primary'>Total de artículos:</td><td class='table-primary'>".$totalart."</td><td class='table-primary'>$ Total:</td><td class='table-primary'>$".$totalcost.".00</td></tr>
                    </tbody></table>";
        return response()->json($data); 

    }
}
