<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;
use Carbon\Carbon;
use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;


class ClientesController extends Controller
{


  public function clientes(){

    if(Auth::guard()->check()==null){
      return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();
    $clientes=\DB::table('clientes')
    ->get();
    return view('clientes.clientes',['permisos'=>$permisos, 'clientes'=>$clientes]);

  }


  public function editar_cliente(Request $request){

    if(Auth::guard()->check()==null){
      return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();
    $cliente=\DB::table('clientes')
    ->where('id', '=', $request->input('id'))
    ->first();
    return view('clientes.editcliente',['permisos'=>$permisos, 'cliente'=>$cliente]);
  }



  public function new_cliente (Request $request){
   if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();



  \DB::table('clientes')->
  insert(['nombre' => $request->input('nombre'), 
    'calle' => $request->input('calle'),
    'ciudad' => $request->input('ciudad'),
    'num_exterior' => $request->input('num_exterior'),
    'codigo_postal' => $request->input('codigo_postal'),
    'estado' => $request->input('estado'),
    'pais' => $request->input('pais'),
    'descuento' => $request->input('descuento'),
    'observaciones' => $request->input('observaciones'),
    'telefono' => $request->input('telefono'),
    'rfc' => $request->input('rfc'),
    'email' => $request->input('email'),
    'dias_plazo' => $request->input('dias_plazo'),
    'foto' => $request->input('foto')
  ]);

  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE CLIENTES || Nuevo cliente: ".$request->input('nombre'),
    'created_at' => $hora
  ]);

  return redirect()->route('clientes',['permisos'=>$permisos])->with('Mensaje', 'Cliente agregado con éxito.');      




}

public function vistanewcliente(){
 if(Auth::guard()->check()==null){
  return redirect()->route('login');
}
$id=Auth::user()->id;
$permisos=permisos::buscar($id)->get();
return view('clientes.newcliente',['permisos'=>$permisos]);
}

public function editarclient(Request $request){
 if(Auth::guard()->check()==null){
  return redirect()->route('login');
}
$id=Auth::user()->id;
$permisos=permisos::buscar($id)->get();

$sucursal = \DB::table('clientes')
->where('id', $request->input('id'))
->update(['nombre' => $request->input('nombre'), 
  'calle' => $request->input('calle'),
  'ciudad' => $request->input('ciudad'),
  'num_exterior' => $request->input('num_exterior'),
  'codigo_postal' => $request->input('codigo_postal'),
  'estado' => $request->input('estado'),
  'pais' => $request->input('pais'),
  'descuento' => $request->input('descuento'),
  'observaciones' => $request->input('observaciones'),
  'telefono' => $request->input('telefono'),
  'rfc' => $request->input('rfc'),
  'email' => $request->input('email'),
  'dias_plazo' => $request->input('dias_plazo')

]);

$id_sucursal = Auth::user()->idSucursal;
$id_usuario = Auth::user()->id;

$hora=Carbon::now();

$bitacora = \DB::table('bitacoras')
->insert(['id_sucursal' => $id_sucursal, 
  'id_usuario' => $id_usuario,
  'descripcion' => "CATÁLOGO DE CLIENTES || Editar cliente: ".$request->input('nombre'),
  'created_at' => $hora
]);



return redirect()->route('clientes',['permisos'=>$permisos])->with('Mensajea', 'Cliente actualizado con éxito.');
}

public function eliminarcliente( Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $cliente=\DB::table('clientes')
  ->where('id', '=', $request->input('id'))
  ->first();




  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "CATÁLOGO DE CLIENTES || Eliminar cliente: ".$cliente->nombre,
    'created_at' => $hora
  ]);

  \DB::table('clientes')->where('id', '=', $request->input('id') )->delete();

  return redirect()->route('clientes',['permisos'=>$permisos])->with('Mensajee', 'Cliente eliminado con éxito.');
} 


}