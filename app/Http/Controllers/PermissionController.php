<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Request as Req;
use App\permisos;


class PermissionController extends Controller
{
	

// ************************************************************************************************************
// ************************************************************************************************************


    public function permiso(Request $request){  
    if(Auth::guard()->check()==null){
            return redirect()->route('login');
        } 
	   
        //Variables para liberacion de interfaces
            $id=Auth::user()->id;
            $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces

        //PARA SURTIDORES----------------------------------------

        $user = Req::get('usuario');
        $nombre=User::all()->where('id',$user);
        $surtidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','1')
                    ->get();
        $sur=array();
        $cont=0;

        foreach ($surtidores as $interfaz) {
            $sur[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA DISTRIBUIDORES----------------------------------------

        $distribuidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','2')
                    ->get();
        $dist=array();
        $cont=0;

        foreach ($distribuidores as $interfaz) {
            $dist[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA JEFE DE ALMACÉN----------------------------------------

        $almacenistas=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','3')
                    ->get();
        $alm=array();
        $cont=0;

        foreach ($almacenistas as $interfaz) {
            $alm[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA COMPRADOR----------------------------------------

        $compradores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','5')
                    ->get();
        $comp=array();
        $cont=0;

        foreach ($compradores as $interfaz) {
            $comp[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA GERENTE DE OPERACIONES----------------------------------------

        $gerente=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','4')
                    ->get();
        $ger=array();
        $cont=0;

        foreach ($gerente as $interfaz) {
            $ger[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }


        
       return view('Permisos/permisos',['nombre'=>$nombre,'user'=>$user,'sur'=>$sur, 'dist'=>$dist, 'alm'=>$alm, 'comp'=>$comp, 'ger'=>$ger,'surtidores'=>$surtidores, 'distribuidores'=>$distribuidores, 'almacenistas'=>$almacenistas, 'compradores'=>$compradores, 'gerente'=>$gerente],['permisos'=>$permisos]);
    }

// ************************************************************************************************************
// ************************************************************************************************************



    public function createpermiso(Request $request) {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }

        //Variables para liberacion de interfaces
            $id=Auth::user()->id;
            $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces

        \DB::table('permisos')->insert(['idInterfaz' => $request->idinterfaz, 'idUsuario' => $request->idusuario]);

        $user =  $request->idusuario;
        $nombre=User::all()->where('id',$user);

        //IMPRESION DE LAS TABLAS---------------------------------------------
            //PARA SURTIDORES----------------------------------------
        $surtidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','1')
                    ->get();
        $sur=array();
        $cont=0;

        foreach ($surtidores as $interfaz) {
            $sur[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA DISTRIBUIDORES----------------------------------------

        $distribuidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','2')
                    ->get();
        $dist=array();
        $cont=0;

        foreach ($distribuidores as $interfaz) {
            $dist[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA JEFE DE ALMACÉN----------------------------------------

        $almacenistas=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','3')
                    ->get();
        $alm=array();
        $cont=0;

        foreach ($almacenistas as $interfaz) {
            $alm[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA COMPRADOR----------------------------------------

        $compradores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','5')
                    ->get();
        $comp=array();
        $cont=0;

        foreach ($compradores as $interfaz) {
            $comp[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA GERENTE DE OPERACIONES----------------------------------------

        $gerente=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','4')
                    ->get();
        $ger=array();
        $cont=0;

        foreach ($gerente as $interfaz) {
            $ger[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }
        //FIN IMPRESION DE LAS TABLAS-----------------------------------------

        return view('Permisos/permisos',['nombre'=>$nombre,'user'=>$user,'permisos'=>$permisos,'sur'=>$sur, 'dist'=>$dist, 'alm'=>$alm, 'comp'=>$comp, 'ger'=>$ger,'surtidores'=>$surtidores, 'distribuidores'=>$distribuidores, 'almacenistas'=>$almacenistas, 'compradores'=>$compradores, 'gerente'=>$gerente]);

        
      
        
    }

// ************************************************************************************************************
// ************************************************************************************************************


    public function elimpermiso(Request $request) {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }

        //Variables para liberacion de interfaces
            $id=Auth::user()->id;
            $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces

            

        \DB::table('permisos')->where([
            ['idInterfaz','=', $request->idinterfaz],
            ['idUsuario' ,'=', $request->idusuario] 
        ])->delete();
        
        $user =  $request->idusuario;
        $nombre=User::all()->where('id',$user);
        //IMPRESION DE LAS TABLAS---------------------------------------------
            //PARA SURTIDORES----------------------------------------

        
        $surtidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','1')
                    ->get();
        $sur=array();
        $cont=0;

        foreach ($surtidores as $interfaz) {
            $sur[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA DISTRIBUIDORES----------------------------------------

        $distribuidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','2')
                    ->get();
        $dist=array();
        $cont=0;

        foreach ($distribuidores as $interfaz) {
            $dist[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA JEFE DE ALMACÉN----------------------------------------

        $almacenistas=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','3')
                    ->get();
        $alm=array();
        $cont=0;

        foreach ($almacenistas as $interfaz) {
            $alm[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA COMPRADOR----------------------------------------

        $compradores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','5')
                    ->get();
        $comp=array();
        $cont=0;

        foreach ($compradores as $interfaz) {
            $comp[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA GERENTE DE OPERACIONES----------------------------------------

        $gerente=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','4')
                    ->get();
        $ger=array();
        $cont=0;

        foreach ($gerente as $interfaz) {
            $ger[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }
        //FIN IMPRESION DE LAS TABLAS-----------------------------------------

       return view('Permisos/permisos',['nombre'=>$nombre,'user'=>$user,'permisos'=>$permisos,'sur'=>$sur, 'dist'=>$dist, 'alm'=>$alm, 'comp'=>$comp, 'ger'=>$ger,'surtidores'=>$surtidores, 'distribuidores'=>$distribuidores, 'almacenistas'=>$almacenistas, 'compradores'=>$compradores, 'gerente'=>$gerente]);
    }


    //******************************************************************************************************************************************************************************************************************

    public function elimall(request $request ) {

        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }

        //Variables para liberacion de interfaces
            $id=Auth::user()->id;
            $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces


        $tablas = \DB::table('rol_defaults')->select('id_interfaz')->where('id_rol','=',$request->idrol)->get();

        foreach ($tablas as $tabla) {
            \DB::table('permisos')->where([
            ['idInterfaz','=', $tabla->id_interfaz],
            ['idUsuario' ,'=', $request->idusuario] 
            ])->delete();
        }

        
        
        $user =  $request->idusuario;
        $nombre=User::all()->where('id',$user);

        //IMPRESION DE LAS TABLAS---------------------------------------------
            //PARA SURTIDORES----------------------------------------

        
        $surtidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','1')
                    ->get();
        $sur=array();
        $cont=0;

        foreach ($surtidores as $interfaz) {
            $sur[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA DISTRIBUIDORES----------------------------------------

        $distribuidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','2')
                    ->get();
        $dist=array();
        $cont=0;

        foreach ($distribuidores as $interfaz) {
            $dist[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA JEFE DE ALMACÉN----------------------------------------

        $almacenistas=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','3')
                    ->get();
        $alm=array();
        $cont=0;

        foreach ($almacenistas as $interfaz) {
            $alm[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA COMPRADOR----------------------------------------

        $compradores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','5')
                    ->get();
        $comp=array();
        $cont=0;

        foreach ($compradores as $interfaz) {
            $comp[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA GERENTE DE OPERACIONES----------------------------------------

        $gerente=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','4')
                    ->get();
        $ger=array();
        $cont=0;

        foreach ($gerente as $interfaz) {
            $ger[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }
        //FIN IMPRESION DE LAS TABLAS-----------------------------------------

       return view('Permisos/permisos',['nombre'=>$nombre,'user'=>$user,'permisos'=>$permisos,'sur'=>$sur, 'dist'=>$dist, 'alm'=>$alm, 'comp'=>$comp, 'ger'=>$ger,'surtidores'=>$surtidores, 'distribuidores'=>$distribuidores, 'almacenistas'=>$almacenistas, 'compradores'=>$compradores, 'gerente'=>$gerente]);
    }

     //******************************************************************************************************************************************************************************************************************

    public function createall(request $request ) {

        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }

        //Variables para liberacion de interfaces
            $id=Auth::user()->id;
            $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces


        $tablas = \DB::table('rol_defaults')->select('id_interfaz')->where('id_rol','=',$request->idrol)->get();

        foreach ($tablas as $tabla) {
            \DB::table('permisos')->insert(['idInterfaz' => $tabla->id_interfaz, 'idUsuario' => $request->idusuario]);
        }

        
        
        $user =  $request->idusuario;
        $nombre=User::all()->where('id',$user);

        //IMPRESION DE LAS TABLAS---------------------------------------------
            //PARA SURTIDORES----------------------------------------

        
        $surtidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','1')
                    ->get();
        $sur=array();
        $cont=0;

        foreach ($surtidores as $interfaz) {
            $sur[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA DISTRIBUIDORES----------------------------------------

        $distribuidores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','2')
                    ->get();
        $dist=array();
        $cont=0;

        foreach ($distribuidores as $interfaz) {
            $dist[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA JEFE DE ALMACÉN----------------------------------------

        $almacenistas=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','3')
                    ->get();
        $alm=array();
        $cont=0;

        foreach ($almacenistas as $interfaz) {
            $alm[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA COMPRADOR----------------------------------------

        $compradores=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','5')
                    ->get();
        $comp=array();
        $cont=0;

        foreach ($compradores as $interfaz) {
            $comp[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }

        //PARA GERENTE DE OPERACIONES----------------------------------------

        $gerente=\DB::table('interfazs')
                    ->join('rol_defaults', 'rol_defaults.id_interfaz', 'interfazs.id')
                    ->select('interfazs.id','interfazs.descripcion')
                    ->where('rol_defaults.id_rol','4')
                    ->get();
        $ger=array();
        $cont=0;

        foreach ($gerente as $interfaz) {
            $ger[$cont]= DB::table('permisos')
            ->join('interfazs', 'permisos.idInterfaz', '=', 'interfazs.id')
            ->select('permisos.id', 'interfazs.descripcion')
            ->where([
                    ['permisos.idUsuario',"$user"],
                    ['interfazs.id',$interfaz->id]
                ])
            ->get();
            $cont++;
        }
        //FIN IMPRESION DE LAS TABLAS-----------------------------------------

       return view('Permisos/permisos',['nombre'=>$nombre,'user'=>$user,'permisos'=>$permisos,'sur'=>$sur, 'dist'=>$dist, 'alm'=>$alm, 'comp'=>$comp, 'ger'=>$ger,'surtidores'=>$surtidores, 'distribuidores'=>$distribuidores, 'almacenistas'=>$almacenistas, 'compradores'=>$compradores, 'gerente'=>$gerente]);
    }

        




}