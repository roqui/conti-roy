<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;

use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;

class PedidosRemisionadosController extends Controller
{
 
	public function premisionados(){
		if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        //Variables para liberacion de interfaces
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        //Fin de variables para liberacion de interfaces

        $id_sucursal = Auth::user()->idSucursal;

        $premisionados= \DB::table('pedidos')
                        ->join('destinos', 'destinos.id', 'pedidos.id_origen')
                        ->join('clientes', 'clientes.id', 'pedidos.id_destino')
                        ->select('pedidos.id','pedidos.CostoT','destinos.nombre as origen', 'clientes.nombre as destino', 'pedidos.created_at')
                        ->where([['pedidos.Estado','=','3'],
                                 ['id_origen','=',$id_sucursal]
                                ])->get();

		return view ('Pedidos/premisionados',['permisos'=>$permisos, 'premisionados'=>$premisionados]);
	}

    public function asignarpr (Request $request) {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();
        $conductores=\DB::table('Users')->where([['puesto', '=', '6'], ['idSucursal','=',Auth::user()->idSucursal]])->get();

        return view('Pedidos/asignarpr',['permisos'=>$permisos, 'conductores'=>$conductores, 'idPedido'=>$request->idpedido]);
    }

    public function asignadopr (Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $idChecado=\DB::table('checars')->join('surtirs', 'surtirs.id', 'checars.idSurtido')->select('checars.id')->where('surtirs.idPedido','=',$request->idpedido)->first();

        \DB::table('sends')->insert(['idChecado'=>$idChecado->id, 'idConductor'=>$request->idconductor, 'created_at'=>date('Y-m-d H:i:s')]);
        $nombre=\DB::table('Users')->select('name')->where('id','=', $request->idconductor)->first();

        \DB::table('pedidos')->where('id', '=', $request->idpedido)->update(['Estado'=>'4']);


        $id_sucursal = Auth::user()->idSucursal;

        $premisionados= \DB::table('pedidos')
                        ->join('destinos', 'destinos.id', 'pedidos.id_origen')
                        ->join('clientes', 'clientes.id', 'pedidos.id_destino')
                        ->select('pedidos.id','pedidos.CostoT','destinos.nombre as origen', 'clientes.nombre as destino', 'pedidos.created_at')
                        ->where([['pedidos.estado','=','3'],
                                 ['id_origen','=',$id_sucursal]
                                ])->get();

        return view('Pedidos/premisionados',['permisos'=>$permisos, 'premisionados'=>$premisionados]);

    }
	

}
