<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;
use App\Exports\InventariosExport;
use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\permisos;
use App\user;
use App\inventaio;

class TrasladosPendientesController extends Controller
{
    public function tpendientes()
    {

        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $id_sucursal = Auth::user()->idSucursal;

        $tpendientes= \DB::table('transaccions')
                        ->join('destinos as origen', 'origen.id', 'transaccions.id_origen')
                        ->join('destinos as destino', 'destino.id', 'transaccions.id_destino')
                        ->select('transaccions.id','transaccions.CostoT','origen.nombre as norigen', 'destino.nombre as ndestino', 'transaccions.created_at')
                        ->where([['transaccions.Estado','=','1'],
                                 ['id_origen','=',$id_sucursal]
                                ])->get();
        

        return view('traslados/tpendientes', ['permisos'=>$permisos, 'tpendientes'=>$tpendientes]);
    }

     public function conceptost(Request $request) {
        $data="";
        $con=\DB::table('conceptots')->select('conceptots.id','productos.codigo','productos.descripcion_producto','conceptots.cantidad')
                                    ->join('productos', 'conceptots.idProducto', 'productos.id')
                                    ->where([['id_transaccion','=',$request->id]])->get();

        $data="<div class='table-responsive'>
                <table class='table'>
                  <thead class='thead-light'>
                    <tr>
                      <th scope='col' >ID</th>
                      <th scope='col' >Código</th>
                      <th scope='col'>Descripcion</th>
                      <th scope='col'>#</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    
                    ";
        

                      foreach ($con as $c){
                        $data=$data."<tr><th scope='row'>".$c->id."</th>
                                <td>".$c->codigo."</td>
                                <td>".$c->descripcion_producto."</td>
                                <td>".$c->cantidad."</td>
                                </tr>";
                      }
                        
        $data=$data."<tr><td class='table-primary'></td><td class='table-primary'></td><td class='table-primary'>Total de artículos:</td><td class='table-primary'>$ Total:</td></tr>
                    </tbody></table>";
        return response()->json($data); 

    }

    public function asignartp (Request $request) {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        $surtidores=\DB::table('Users')->where([['puesto','=', '1'],['idSucursal','=',Auth::user()->idSucursal]])->get();

        return view('traslados/asignartp',['permisos'=>$permisos, 'surtidores'=>$surtidores, 'idTraslado'=>$request->idtraslado]);
    }

    public function asignadotp (Request $request){
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id=Auth::user()->id;
        $permisos=permisos::buscar($id)->get();

        \DB::table('surtirts')->insert(['id_transaccion'=>$request->idtraslado, 'id_surtidor'=>$request->idsurtidor,'estado'=>'INICIADO', 'created_at'=>date('Y-m-d H:i:s')]);
        $nombre=\DB::table('Users')->select('name')->where('id','=', $request->idsurtidor)->first();

        \DB::table('transaccions')->where('id', '=', $request->idtraslado)->update(['Estado'=>'2']);


        $id_sucursal = Auth::user()->idSucursal;

        $tpendientes= \DB::table('transaccions')
                        ->join('destinos as origen', 'origen.id', 'transaccions.id_origen')
                        ->join('destinos as destino', 'destino.id', 'transaccions.id_destino')
                        ->select('transaccions.id','transaccions.CostoT','origen.nombre as norigen', 'destino.nombre as ndestino', 'transaccions.created_at')
                        ->where([['transaccions.Estado','=','1'],
                                 ['id_origen','=',$id_sucursal]
                                ])->get();

        return view('traslados/tpendientes',['permisos'=>$permisos, 'tpendientes'=>$tpendientes])->with('Mensaje', ' ');

    }
}
