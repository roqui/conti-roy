<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\DB;

use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use App\Exports\SucursalesExport;
use App\permisos;
use App\destino;
use Carbon\Carbon;


class SucursalController extends Controller
{
  public function sucursales(){
    if(Auth::guard()->check()==null){
      return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();

    $sucursales = \DB::table('destinos')
    ->get();

    return view ('sucursales.sucursales', ['permisos'=>$permisos, 'sucursales'=>$sucursales]);
  }

  public function editarsucursal(Request $request){
    if(Auth::guard()->check()==null){
      return redirect()->route('login');
    }
    $id=Auth::user()->id;
    $permisos=permisos::buscar($id)->get();

    $sucursal = \DB::table('destinos')
    ->where('id', '=', $request->input('id') )
    ->first();

    return view ('sucursales.editsucursal', ['permisos'=>$permisos, 'sucursal'=>$sucursal]);


  }

  public function newsucursal(){
   if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();
  $sucursales = \DB::table('destinos')
  ->select('destinos.*')
  ->first();

  return view ('sucursales.newsucursal', ['permisos'=>$permisos, 'sucursales'=>$sucursales]);
}

public function crear_sucursal(Request $request){

  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();


  \DB::table('destinos')->insert(['nombre' => $request->input('nombre'), 
    'calle' => $request->input('calle'),
    'colonia' => $request->input('colonia'),
    'ciudad' => $request->input('ciudad'),
    'num_exterior' => $request->input('num_exterior'),
    'codigo_postal' => $request->input('codigo_postal'),
    'telefono' => $request->input('telefono')
  ]);


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "SUCURSALES || Nueva sucursal: ".$request->input('nombre'),
    'created_at' => $hora
  ]);

  return redirect()->route('sucursales',['permisos'=>$permisos])->with('Mensaje', 'Sucursal agregada con éxito.');      
}


public function editar_sucursal(Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

  $sucursal = \DB::table('destinos')
  ->where('id', $request->input('id'))
  ->update(['codigo_postal' => $request->input('codigo_postal'), 
    'nombre' => $request->input('nombre'),
    'calle' => $request->input('calle'),
    'colonia' => $request->input('colonia'),
    'num_exterior' => $request->input('num_exterior'),
    'ciudad' => $request->input('ciudad'),
    'telefono' => $request->input('telefono')
  ]);

  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "SUCURSALES || Editar sucursal: ".$request->input('nombre'),
    'created_at' => $hora
  ]);


  return redirect()->route('sucursales',['permisos'=>$permisos])->with('Mensajea', 'Sucursal actualizada con éxito.');


}

public function eliminarsucursal( Request $request){
  if(Auth::guard()->check()==null){
    return redirect()->route('login');
  }
        // Variables para liberacion de interfaces-----------------------------------
  $id=Auth::user()->id;
  $permisos=permisos::buscar($id)->get();

   $destinos=\DB::table('destinos')
  ->where('id', '=', $request->input('id'))
  ->first();


  $id_sucursal = Auth::user()->idSucursal;
  $id_usuario = Auth::user()->id;

  $hora=Carbon::now();

  $bitacora = \DB::table('bitacoras')
  ->insert(['id_sucursal' => $id_sucursal, 
    'id_usuario' => $id_usuario,
    'descripcion' => "SUCURSALES || Eliminar sucursal:".$destinos->nombre,
    'created_at' => $hora
  ]);

  \DB::table('destinos')->where('id', '=', $request->input('id') )->delete();

  return redirect()->route('sucursales',['permisos'=>$permisos])->with('Mensajee', 'Sucursal eliminada con éxito.');
} 

public function excel () {
  return \Excel::download(new SucursalesExport, 'Sucursales.xlsx');
}

public function findLastSuc(){


 $sucursales = \DB::table('destinos')
 ->select('destinos.*')
 ->orderBy('created_at', 'DESC')
 ->first();
 return response()->json($sucursales);
}

}