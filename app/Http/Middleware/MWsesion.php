<?php

namespace App\Http\Middleware;
use Closure;
use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MWsesion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }

        if(Auth::guard()->check()){
            die();
        }
        return $next($request);
    }
}
