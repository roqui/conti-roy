<?php

namespace App\Http\Middleware;

use Closure;

class MWbitacora
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
            
         $id_sesion = Auth::user()->id;

         $user = DB::table('permisos')
            ->where('idInterfaz', '55')
            ->where('idUsuario', $id_sesion)
            ->count();
         if ($user>0) 
        {
            return $next($request);
        } else{
            return redirect()->route('upspermiso');
        }

        
    }
}
