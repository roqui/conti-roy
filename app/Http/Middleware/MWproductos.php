<?php

namespace App\Http\Middleware;
use Closure;
use App;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MWproductos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard()->check()==null){
            return redirect()->route('login');
        }
        $id_sesion = Auth::user()->id;

         if ($user = DB::table('permisos')
            ->where('idInterfaz', '4')
            ->where('idUsuario', $id_sesion)
            ->first()
            ) 
        {
            return $next($request);
        }else{
            return redirect()->route('upspermiso');
        }


        

    }
}
