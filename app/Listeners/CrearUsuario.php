<?php

namespace App\Listeners;

use App\Events\CrearUsuario;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CrearUsuario
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CrearUsuario  $event
     * @return void
     */
    public function handle(CrearUsuario $event)
    {
        //
    }

    public function created(UserRequest $userRequest){

    }
}
