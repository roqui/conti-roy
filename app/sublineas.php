<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sublineas extends Model
{
    protected $fillable = ['descrip', 'id_linea', 'id']
}
