<head>
	<!-- Plotly.js -->
	<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
	<!-- Numeric JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
</head>



<div>
  <ul class="nav nav-pills mb-3  nav-justified navbar-light mb-3 pt-1" style="background-color: #e3f2fd;" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><label>PEDIDOS</label></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><label>COMPRAS</label></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-traslados-tab" data-toggle="pill" href="#pills-traslados" role="tab" aria-controls="pills-profile" aria-selected="false"><label>TRASLADOS</label></a>
    </li>
  </ul>
  <div class="tab-content">
    <div class="card">
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">                  
          <div class="card-body">
            <h5 class="card-title"> </h5>
            <p class="card-text"> 
              <div class="row">
                <div class="col-sm">
                  <h4>Pedidos Recientes</h4>
                  <br>


                  <table class="table">
                    <thead class="table-striped">
                      <tr>
                        <th scope="col">Folio</th>
                        <th scope="col">Origen</th>
                        <th scope="col">Cliente</th>
                        <th scope="col">Estado</th>
                      </tr>
                    </thead>                     
                    <tbody>
                     @foreach($pedidos as $pedido)

                     <tr>
                      <th scope="row">{{$pedido->id}}</th>
                      <td>{{$pedido->nombre}}</td>
                      <td>{{$pedido->cliente}}</td>

                      <?php 
                      switch ($pedido->Estado) {
                        case 'Pendiente':
                        $color = '#ffe0b2';
                        break;

                        case 'Surtiendo':
                        $color = '#ffb74d';
                        break;

                        case 'Remisionado':
                        $color = '#ffb300';
                        break;

                        case 'Enviado':
                        $color = '#00e676';
                        break;

                        case 'Cancelado':
                        $color = '#d84315';
                        break;


                      }
                      ?>

                      <td >
                        {{$pedido->Estado}}

                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
                <small>
                  <a href="pedidos">
                    Haz click aquí para ver todos los pedidos
                  </a>
                </small>

              </div>

              <div class="col-sm" style="width: 400px;">
                <h4>Estado de los pedidos</h4>
                <div  id="charpedidos"></div>
              </div>

            </div>


          </p>
        </div>
      </div>
      <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <div class="card-body">
          <h5 class="card-title"></h5>
          <p class="card-text"> 
            <div class="row">
              <div class="col-sm">
                <h4>Compras Recientes</h4>
                <br>


                <table class="table">
                  <thead class="table-striped">
                    <tr>
                      <th scope="col">Folio</th>
                      <th scope="col">Origen</th>
                      <th scope="col">Cliente</th>
                      <th scope="col">Estado</th>
                    </tr>
                  </thead>                     
                  <tbody>
                   @foreach($pedidos as $pedido)

                   <tr>
                    <th scope="row">{{$pedido->id}}</th>
                    <td>{{$pedido->nombre}}</td>
                    <td>{{$pedido->cliente}}</td>

                    <?php 
                    switch ($pedido->Estado) {
                      case 'Pendiente':
                      $color = '#ffe0b2';
                      break;

                      case 'Surtiendo':
                      $color = '#ffb74d';
                      break;

                      case 'Remisionado':
                      $color = '#ffb300';
                      break;

                      case 'Enviado':
                      $color = '#00e676';
                      break;

                      case 'Cancelado':
                      $color = '#d84315';
                      break;


                    }
                    ?>

                    <td >
                      {{$pedido->Estado}}

                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
              <small>
                <a href="compras">
                  Haz click aquí para ver todos las compras
                </a>
              </small>

            </div>

            <div class="col-sm" style="width: 400px;">
              <h4>Estado de las compras</h4>
              <div id="plotly-div"></div>
            </div>


          </div>




        </p>
      </div>
    </div>

    <div class="tab-pane fade" id="pills-traslados" role="tabpanel" aria-labelledby="pills-traslados-tab">
      <div class="card-body">
        <h5 class="card-title"></h5>
        <p class="card-text"> 

          <div class="row">
            <div class="col-sm">

              <h4>Taslados Recientes</h4>


              <table class="table">
                <thead class="table-striped">
                  <tr>
                    <th scope="col">Folio</th>
                    <th scope="col">Origen</th>
                    <th scope="col">Cliente</th>
                    <th scope="col">Estado</th>
                  </tr>
                </thead>                     
                <tbody>
                 @foreach($transaccions as $pedido)

                 <tr>
                  <th scope="row">{{$pedido->id}}</th>
                  <td>{{$pedido->nombre}}</td>
                  <td>{{$pedido->cliente}}</td>

                  <?php 
                  switch ($pedido->Estado) {
                    case 'Pendiente':
                    $color = '#ffe0b2';
                    break;

                    case 'Surtiendo':
                    $color = '#ffb74d';
                    break;

                    case 'Remisionado':
                    $color = '#ffb300';
                    break;

                    case 'Enviado':
                    $color = '#00e676';
                    break;

                    case 'Cancelado':
                    $color = '#d84315';
                    break;


                  }
                  ?>

                  <td >
                    {{$pedido->Estado}}

                  </td>
                </tr>
                @endforeach

              </tbody>
            </table>

            <small>
              <a href="traslados">
                Haz click aquí para ver todos los traslados
              </a>
            </small>

          </div>

          <div class="col-sm" style="width: 400px;">
            <h4>Estado de los traslados</h4>
            <div id="myDiv"></div>
          </div>

        </div>



      </div>


    </div>



  </p>
</div>
</div>
</div>
</div>

<div class="card">
  <div class="card-body">
    <h3>Relación de productos con líneas</h3>
    <div id="myDiv2"></div>

  </div>
</div>

<div class="card">
  <div class="card-body">
    <h3>Relación de productos con proveedores</h3>
    <div id="myDiv3"></div>

  </div>
</div>


</div>
</div>





</div>    


