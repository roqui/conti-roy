<head>
	<!-- Plotly.js -->
	<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
	<!-- Numeric JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
</head>

<body>

	<div  style="width: 50%;" id="charpedidos"><!-- Plotly chart will be drawn inside this DIV --></div>

	<script type="text/javascript">

  function crearCadenaLineal(json){
    var parsed = JSON.parse(json);
    var arr = [];
    for(var y in parsed){
      arr.push(parsed[y]);
    }
    return arr;
  }
  
</script>
	<script>

		 piejsonY=crearCadenaLineal('<?php echo $piejsonY ?>');

  piejsonX=crearCadenaLineal('<?php echo $piejsonX ?>');

		var data = [{
			values: piejsonY,
			labels: piejsonX,
			type: 'pie'
		}];

	Plotly.newPlot('charpedidos', data, {}, {showSendToCloud:true})  
</script>
</body>