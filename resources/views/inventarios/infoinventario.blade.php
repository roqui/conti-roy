<!-- Button trigger modal -->
<button type="button"  data-toggle="modal" data-target="#info{{$inventario->id}}" class="btn"><i class="fa fa-info-circle"></i></button>

<!-- Modal -->
<div class="modal fade" id="info{{$inventario->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Información  <b>{{$inventario->descripcion_producto}}</b></h5> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">

       <div class="container">

        <b><label>Descripción del producto:</label></b> {{$inventario->descripcion_producto}} <br>

        <b><label>Código:</label></b> {{$inventario->codigo}} <br>

        <b><label>Proveedor:</label></b> {{$inventario->proveedor}} <br>


        <b><label>Línea:</label></b> {{$inventario->descr}} <br>



        <div class="row">
          <div class="col-sm">
            <b><label>Sublínea:</label></b> {{$inventario->descrip}} <br>

            <b><label>Cantidad Apartada:</label></b> {{$inventario->cant_apartada}} <br>
            <b><label>Mínimo:</label></b> {{$inventario->minimos}} <br>
          </div>
          <div class="col-sm">
            <b><label>Marca:</label></b> {{$inventario->descripcion}} <br>
            <b><label>Cantidad Disponible:</label></b> {{$inventario->cant_disponible}} <br>
            <b><label>Máximo:</label></b> {{$inventario->maximos}} <br>


          </div>
        </div>


        <?php 

        $procentaje=(($inventario->cant_disponible)*100/$inventario->maximos);
        $procentaje= bcdiv($procentaje, '1', 0);

        $porcentaje_minimo=(($inventario->minimos)*100/$inventario->maximos);
        $porcentaje_minimo= bcdiv($porcentaje_minimo, '1', 0);

        $color = 'green';
        $link="";
        $mensaje='Las existencias de este producto superan el promedio';

        if ($porcentaje_minimo < $procentaje ){
          if ($procentaje < 50) {
            $color='orange';
            $mensaje='Las existencias de este producto están por debajo del promedio';

          }
        }

        elseif ($procentaje <= $porcentaje_minimo ){
          $color='red';
          $mensaje='Las existencias de este producto están al mínimo ';
          $link=' <small><a href="compras">Haz click aquí para procesar una nueva compra</a></small>';

        }



        ?>


        <div style="height: 30px;" class="progress">
          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"  style="width: {{$procentaje}}%; height: 30px; background-color: {{$color}};" aria-valuenow="{{$inventario->cant_disponible}}" aria-valuemin="{{$inventario->minimos}}" aria-valuemax="{{$inventario->maximos}}">
            {{$procentaje}}%
          </div>
        </div>
        <br>
        <label> {{$mensaje}}</label> <br>
        <?php
        echo $link;
        ?>

      </div>


    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
</div>
</div>



<!-- Button trigger modalmmmmmksdfjñlkdjfñlkajsdflñkdnfñlsdnflñkdnflkñdnf -->