<button href="#" class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#inventario">
  <span class="icon text-white-50">
    <i class="fas fa-plus"></i>
  </span>
  <span class="text">Agregar producto a inventario</span>
</button>

<!-- Modal -->
<div class="modal fade" id="inventario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Agregar producto a inventario</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="new_producto_inventario2" method="POST">
          {{csrf_field()}} 
          <b>      <label>Producto:</label></b> <br>
          <select name="id_producto" style="width: 460px;" id="id_producto1" required class="select2">
            <option>Selecciona una producto</option>

            @foreach($productos as $producto)
            <option name="id_producto" value="{{$producto->id}}">
             {{ $producto->descripcion_producto }} || {{$producto->codigo}}
           </option>
           @endforeach
         </select>

         <br>
         <div>
           <b><label>Sucursal:</label></b> <br>
           <select name="id_sucursal" style="width: 460px;" id="id_producto2" required class="select2">
            <option>Selecciona una sucursal</option>

            @foreach($sucursales as $sucursal)
            <option name="id_sucursal" value="{{$sucursal->id}}">
             {{ $sucursal->nombre }} 
           </option>
           @endforeach
         </select>
       </div>
       <br>

       <b> <label>Cantidad de productos disponible:</label></b> <br>
       <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1"> </span>
        </div>
        <input type="number" required class="form-control" name="cant_disponible" placeholder="Cantidad disponible" aria-label="Username" aria-describedby="basic-addon1">
      </div>

      <div class="container">
        <div class="row">
          <div class="col-sm">
            <label>Establecer mínimo:</label></b> <br>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> </span>
              </div>
              <input type="number" required class="form-control" name="minimos" placeholder="Mínimo" aria-label="Username" aria-describedby="basic-addon1">
            </div>
          </div>
          <div class="col-sm">
            <label>Establecer máximo:</label></b> <br>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> </span>
              </div>
              <input type="number" required class="form-control" name="maximos" placeholder="Máximo" aria-label="Username" aria-describedby="basic-addon1">
            </div>
          </div>

        </div>
      </div>


    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      <button type="submit" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Agregar inventario</span>
      </button>
    </div>
  </form>
</div>
</div>
</div>


<script type="text/javascript">
  function vlaidarcodigo(codigo) {
  //if (fono.value.lenght < 8) {
    if (codigo.value.length > 13 || codigo.value.length < 13 ) {
      alert("Error: El código debe tener exactamente 13 dígitos.");
      fono.focus();
      fono.select();
    }
  }
</script>