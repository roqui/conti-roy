@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')




<div style="margin: 50px;">


     @if(session('Mensaje'))

     <div class="alert alert-success" id="success-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensaje')}}</div>

    @endif
    @if(session('Mensajee'))

    <div class="alert alert-danger" id="danger-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajee')}}</div>

    @endif
    @if(session('Mensajea'))

    <div class="alert alert-primary" id="warning-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajea')}}</div>

    @endif



  <div class="card shadow mb-4">

    <div class="card-body">
      <h3>Inventario</h3>
      <h4>Inventario de la sucursal <b>
       <?php
       $id = Auth::user()->idSucursal; 

       $sucursal = DB::table('destinos')->select('nombre')->where('id', '=', $id)->first();?>
       @foreach($sucursal as $key)
       {{$key}}
       @endforeach


     


    
    @foreach($permisos as $item)
    @if($item->idInterfaz==3)

    <div style="float: right; margin-bottom: 20px;">
      @include('inventarios.newinventario')
    </div>
    @break
    @endif
    @endforeach
    <div class="table-responsive">


      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ID</th>
            <th scope="col">Descripción</th>
            <th>Código</th>
            <th>Marca</th>
            <th >Proveedor</th>
            <th style="width: 50px;">Productos Apartados</th>
            <th style="width: 50px;">Productos Disponibles</th>
            <th>Porcentaje de Existencia</th>
            <th></th>
            <th></th>
            <th></th>

          </tr>
        </thead>

        <tbody>

          @foreach($inventarios as $inventario)
          <tr id="color">
            <th>{{$inventario->id_producto}}</th>
            <th>{{$inventario->descripcion_producto}}</th>
            <th>{{$inventario->codigo}}</th>
            <th>{{$inventario->descripcion}}</th>
            <th>{{$inventario->proveedor}}</th>
            <th>{{$inventario->cant_apartada}}</th>
            <th>{{$inventario->cant_disponible}}</th>

            <th>
             <?php 

             $procentaje=(($inventario->cant_disponible)*100/$inventario->maximos);
             $procentaje= bcdiv($procentaje, '1', 0);

             $porcentaje_minimo=(($inventario->minimos)*100/$inventario->maximos);
             $porcentaje_minimo= bcdiv($porcentaje_minimo, '1', 0);

             $color = '#00e676';
             $link="";
             $mensaje='Las existencias de este producto superan el promedio';

             if ($porcentaje_minimo < $procentaje ){
              if ($procentaje < 50) {
                $color='#ffb300';
                $mensaje='Las existencias de este producto están por debajo del promedio';

              }
            }

            elseif ($procentaje <= $porcentaje_minimo ){
              $color='#d84315';
              $mensaje='Las existencias de este producto están al mínimo ';
              $link=' <small><a href="compras">Haz click aquí para procesar una nueva compra</a></small>';

            }



            ?>


            <div style="height: 30px; width: 200px;" class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"  style="width: {{$procentaje}}%; height: 30px; background-color: {{$color}};" aria-valuenow="{{$inventario->cant_disponible}}" aria-valuemin="{{$inventario->minimos}}" aria-valuemax="{{$inventario->maximos}}">
                {{$procentaje}}%
              </div>
            </div>
            <br>

          </th>

          <th>@include('inventarios.infoinventario')</th>
          <th>@include('inventarios.editinventario')</th>
          <th>@include('inventarios.deleteinventario')</th>
        </tr>
        @endforeach

      </tbody>
    </table>

    <a href="exceli" style="float: right; margin: 10px;" class="btn btn-success"><i class="fas fa-file-excel"></i> Generar Excel</a>
  </div>

</div>
</div>




</center>

<div class="card">
  <div class="card-body">
    <h4>Código de Colores</h4>
    <b><label>Cuando el procentaje de existencias supera el 50%, la barra será color verde</label></b>
    <div style="width: 400px;">
      <div class="progress" style="height: 30px;">
        <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 85%; background-color:#00e676; height: 30px;" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">85%</div>
      </div>
    </div>

    <br>

    <b><label>Cuando el procentaje de existencias sea menor que el 50% pero mayor al porcentaje mínimo, la barra será color naranja</label></b>
    <div style="width: 400px;">
      <div class="progress" style="height: 30px;">
        <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 45%; background-color:#ffb300; height: 30px;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100">45%</div>
      </div>
    </div>

     <br>

    <b><label>Cuando el procentaje de existencias sea menor que el porcentaje mínimo, la barra será color rojo</label></b>
    <div style="width: 400px;">
      <div class="progress" style="height: 30px;">
        <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 15%; background-color:#d84315; height: 30px;" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">15%</div>
      </div>
    </div>
  </div>
</div>

<div>



  @endsection


  @section('script')

  <script>

    $(document).ready(function() {
      $('.select2').select2();
    });
  </script>

  @endsection


  @section('script')

  <script>

    function cambiarColor(){
      document.getElementById('#color').style.backgroundColor = "red";
    }
  </script>

  @endsection