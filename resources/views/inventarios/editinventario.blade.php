<!-- Button trigger modal -->
<button  class="btn" data-toggle="modal"
data-target="#inventario{{$inventario->id}}"><i class="fas fa-edit" data-toggle="tooltip" data-placement="top" title="Editar inventario"></i>

</button>

<!-- Modal -->
<div class="modal fade" id="inventario{{$inventario->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Editar Inventario</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="actualizar_producto_inventario" method="POST">
          <input hidden value="{{$inventario->id}}" name="id">
          {{csrf_field()}} 
          <b><label>Producto: </label></b>  {{$inventario->descripcion_producto}} <br>

          <b>      <label>Código:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> </span>
            </div>
            <input type="number" readonly value="{{$inventario->codigo}}" required onchange="return vlaidarcodigo(this)" class="form-control" name="codigo" placeholder="Código del producto" aria-label="Username" aria-describedby="basic-addon1">
          </div>


          <b> <label>Cantidad de productos disponible:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> </span>
            </div>
            <input type="number" value="{{$inventario->cant_disponible}}" required class="form-control" name="cant_disponible" placeholder="Cantidad disponible" aria-label="Username" aria-describedby="basic-addon1">
          </div>

          <div class="row">
            <div class="col-sm">
              <label>Mínimo:</label></b> <br>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"> </span>
                </div>
                <input type="number" value="{{$inventario->minimos}}" required class="form-control" name="minimos" placeholder="Mínimo" aria-label="Username" aria-describedby="basic-addon1">
              </div>
            </div>
            <div class="col-sm">
              <label>Máximo:</label></b> <br>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"> </span>
                </div>
                <input type="number" value="{{$inventario->maximos}}" required class="form-control" name="maximos" placeholder="Máximo" aria-label="Username" aria-describedby="basic-addon1">
              </div>
            </div>

          </div>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-save"></i>
            </span>
            <span class="text">Guardar</span>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
  function vlaidarcodigo(codigo) {
  //if (fono.value.lenght < 8) {
    if (codigo.value.length > 13 || codigo.value.length < 13 ) {
      alert("Error: El código debe tener exactamente 13 dígitos.");
      fono.focus();
      fono.select();
    }
  }
</script>