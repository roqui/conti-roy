
<!-- Button trigger modal -->
<button class="card"  style=" background-color: white; margin:20px;" data-toggle="modal" data-target="#cliente">

            <a style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/new.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Nuevo</label></b>
                <h5>¡Haz click aquí para procesar un nuevo pedido!</h5>
              </div>
      
</button>

<div class="modal fade" id="cliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>¿Para quién será el pedido?</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      <form action="datos_cliente" method="POST">
                {{csrf_field()}} 


        <b><label>Selecciona un cliente: </label></b> <br>
       <select name="id" style="width: 460px;" id="sucursal" required class="select2">
        <option selected="disabled"></option>
       @foreach($clientes as $cliente)
         <option name="id" value="{{ $cliente->id}}">
          {{ $cliente->nombre }}
        </option>
        @endforeach
      </select>

      
      </div>
      <div class="modal-footer">
         <small>
           <a href="agregar_cliente">
             ¿Te gustaría agregar un nuevo cliente?
           </a>
         </small>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Seleccionar</span>
                  </button>
                        </form>

      </div>
    </div>
  </div>
</div>

