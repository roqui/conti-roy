@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="margin:30px;">

  <div class="card shadow mb-4" >
    <div class="card">


      <div style="text-decoration: none; color: black;" class="card-body">
        <b><label style="font-size: 20px;">Pedidos</label></b>
        <h4>Pedidos de la sucursal
          <b>
            <?php
            $id = Auth::user()->idSucursal; 

            $sucursal = DB::table('destinos')->select('nombre')->where('id', '=', $id)->first();?>
            @foreach($sucursal as $key)
            {{$key}}
            @endforeach


          </b>
        </h4>


      </div>
      <div class="row" >

        <div class="col-sm">
          
          @foreach($permisos as $item)
          @if($item->idInterfaz==26)
            @include('pedidos.infonewpedido')
          @break
          @endif
          @endforeach

     
          @foreach($permisos as $item)
          @if($item->idInterfaz==30)
          <div class="card" style="margin:20px;  ">
            <a href="premisionados" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/reports.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">
                <b><label style="font-size: 20px;">Remisionados</label></b>
                <h5>Échale un vistaso a todos los pedidos remisionados</h5>
              </div>
            </a>
          </div>
          @break
          @endif
          @endforeach


        </div> 

        <div class="col-sm" style=" ;">
          
          @foreach($permisos as $item)
          @if($item->idInterfaz==27)
          <div class="card" style="margin:20px;  ">
            <a href="ppendientes" style="text-decoration: none; color: black; " >
              <div class="card-body">
                <img src="img/pendiente.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px; text-align: center;">Pendientes</label></b>
                <h5>Esta semana tienes <b>{{$pedidos}}</b> pedidos pendientes</h5>
              </div>
            </a>
          </div>
          @break
          @endif
          @endforeach


          @foreach($permisos as $item)
          @if($item->idInterfaz==27)
          <div class="card" style="margin:20px;  ">
            <a href="work" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/cancel.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Cancelaciones</label></b>
                <h5>Aquí podrás ver los pedidos cancelados</h5>
              </div>
            </a>
          </div>
          @break
          @endif
          @endforeach


        </div>
        <div class="col-sm">

          @foreach($permisos as $item)
          @if($item->idInterfaz==32)
          <div class="card" style="margin:20px;  ">
            <a href="todos" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/delivered.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Todos</label></b>
                <h5>¿Estás buscando un pedido en específico?, ¡Encuentralo aquí!</h5>
              </div>
            </a>
          </div>
          @break
          @endif
          @endforeach

          @foreach($permisos as $item)
          @if($item->idInterfaz==32)
          <div class="card" style="margin:20px;  ">
            <a href="work" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/return.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Devoluciones</label></b>
                <h5>Aquí podrás ver las devoluciones de pedidos</h5>

              </div>
            </a>
          </div>
          @break
          @endif
          @endforeach

        </div> 
      </div>


    </div>
  </div>

  <div>


    @endsection

    @section('script')

    <script>
      
      $(document).ready(function() {
        $('.select2').select2();
      });
    </script>

    @endsection