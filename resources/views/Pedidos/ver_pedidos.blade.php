@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

	<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          

          <!-- Content Row -->
          <div class="row">

          	<!-- DataTales Example -->
          <div class="card shadow mb-4" style="margin: auto;">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Pedidos</h6>
            </div>
            <div class="card-body">
              <h1 class="h3 mb-1 text-gray-800">Registro de Pedidos:</h1>
          <p class="mb-4">Resgistro de todos los pedidos realizados, ya sea por sucursales o clientes registrados.</p>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Num. Pedido</th>
                      <th>Sucursal Origen</th>
                      <th>Cliente</th>
                      <th>Destino</th>
                      <th>Fecha</th>
                      <th>Total</th>
                      <th>Estado</th>
                      @foreach($permisos as $info_ped)
                      @if($info_ped->idInterfaz)
                      <th width="200">Información</th>
                      <th width="50"></th>
                      @break
                      @endif
                      @endforeach
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Num. Pedido</th>
                      <th>Sucursal Origen</th>
                      <th>Cliente</th>
                      <th>Destino</th>
                      <th>Fecha</th>
                      <th>Total</th>
                      <th>Estado</th>
                      @foreach($permisos as $info_ped)
                      @if($info_ped->idInterfaz)
                      <th width="200">Información</th>
                      <th width="50"></th>
                      @break
                      @endif
                      @endforeach
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($pedidos as $item)
                    	<tr>
                      <td>{{$item->id}}</td>
                      <td>{{$item->sucursal}}</td>
                      <td>{{$item->nombre}}</td>
                      <td>{{$item->calle}} {{$item->num_exterior}} {{$item->colonia}} CP. {{$item->codigo_postal}}  {{$item->ciudad}}</td>
                      <td>{{$item->created_at}}</td>
                      <td>{{$item->CostoT}}</td>
                      <td>{{$item->estado}}</td>
                      
                      @foreach($permisos as $info_ped)
                      @if($info_ped->idInterfaz)
                      <td> <a href="verpedido"><button type="button" style="width: 200px;" class="btn btn-info"> Ver Info</button></a>
                      </td>
                      <td> 
                        <a href="verarticulos"><button type="button" style="width: 50px;" class="btn btn-info"><i class="fas fa-list"></i></button></a>
                      </td>
                      @break
                      @endif
                      @endforeach
                    @endforeach
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
            

            

            

          </div>

        </div>
        <!-- /.container-fluid -->
 

@endsection
