@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')
  
<div class="container">
    
    <div class="card shadow mb-4">
        <div class="card-body">
          <h3>Pedidos Remisionados</h3>
    <p class="mb-4">Listado de los Pedidos en estado 'Remisionado', es decir que ya han pasado por el proceso de 'Surtido' y 'Checado'. En éste apartado se tiene la capacidad de asignar el pedido a un conductor designado para que éste lo lleve a su destino.</p>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                          <th scope="col" >ID-Folio</th>
                          <th scope="col">Sucursal Origen</th>
                          <th scope="col">Destino</th>
                          <th scope="col">Fecha de Registro</th>
                          <th scope="col"><i class="fa fa-usd"></i></th>
                          <th scope="col" ></th>
                          <th></th>
                        </tr>
                    </thead>
                          <tbody>
                                @foreach ($premisionados as $p)
                                  <tr>
                                    <th scope="row">{{$p->id}}</th>
                                    <td>{{$p->origen}}</td>
                                    <td>{{$p->destino}}</td>
                                    <td>{{$p->created_at}}</td>
                                    <td>{{$p->CostoT}}</td>
                                    <td>@include('Pedidos.editppendientes')</td>
                                    
                                    <td>
                                      <form action="asignarpr">
                                          <input type="hidden" value="{{$p->id}}" name="idpedido">
                                          <button type="submit" class="btn btn-outline-info">Asignar</button>
                                      </form>
                                    </td>
                                    

                                  </tr>
                              
                              @endforeach
                </table>
                        
            </div>

        </div>
    </div>
</div>


@endsection()