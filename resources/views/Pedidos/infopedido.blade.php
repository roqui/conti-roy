@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          

          <!-- Content Row -->
          <div class="row">
          	<!--CARD PARA LA ETAMA DE PENDIENTE -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-warning" style="width: 100%">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-warning"><i class="fas fa-exclamation-triangle"></i> Pendiente</h6>

                  <?php 
                    if ($p!="") {
                      ?>
                        <div style="float: right;" class="m-0 font-weight-bold text-primary"><i class="fas fa-thumbs-up"></i></div>
                      <?php
                    }
                  ?>
                  
                </div>
                <div class="card-body">

                  <h1 class="h3 mb-1 text-gray-800">Seguimiento del pedido {{$p->id}}</h1>
          <p class="mb-4">Resgistro del seguimiento del pedido con número {{$p->id}}.</p>

                  <?php 
                    if ($p!="") {
                      ?>
                      <p>Fecha y hora de expedición: {{$p->created_at}}.</p>
                      <p>Cliente: {{$p->destino}}. RFC: {{$p->rfc}}.</p>
                      <p>Dirección de Envío: {{$p->callec}}, {{$p->ciudadc}} {{$p->estadoc}}, {{$p->paisc}}</p>
                      <?php
                    }else{
                      ?>
                        <p>No hay datos disponibles. Etapa no Realizada</p>
                      <?php
                    }
                  ?>
                  
                  
                </div>
              </div>

              <br>

              <!--CARD PARA LA ETAMA DE SURTIENDO -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-primary" style="width: 100%">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-primary"><i class="fas fa-spinner"></i> Surtiendo</h6>
                  <?php 
                    if ($s!="") {
                      ?>
                        <div style="float: right;" class="m-0 font-weight-bold text-primary"><i class="fas fa-thumbs-up"></i></div>
                      <?php
                    }
                  ?>
                </div>
                <div class="card-body">
                  <?php 
                    if ($s!="") {
                      ?>
                      <p>Etapa comenzada en: {{$s->created_at}}</p>
                      <p>Empleado Encargado: {{$s->name}}. </p>
                      <?php
                    }else{
                      ?>
                        <p>No hay datos disponibles. Etapa no Realizada</p>
                      <?php
                    }
                  ?>
                  
                  
                </div>
              </div>

              <!--CARD PARA LA ETAMA DE REMISIONADO -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-info" style="width: 100%">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-info"><i class="fas fa-file"></i> Remisionado</h6>
                  <?php 
                    if ($c!="") {
                      ?>
                        <div style="float: right;" class="m-0 font-weight-bold text-primary"><i class="fas fa-thumbs-up"></i></div>

                      <?php
                    }
                  ?>
                </div>
                <div class="card-body">
                  <?php 
                    if ($c!="") {
                      ?>
                        <p>Etapa comenzada en: {{$c->created_at}}</p>
                        <p>Empleado Encargado: {{$s->name}}. </p>
                        <form action="nota_pedido">
                          <input type="hidden" value="{{$p->id}}" name="id_pedido">
                          <center><button class="btn btn-info" type="submit"><i class="fas fa-print"></i>Ver Remisión</button></center>
                        </form>
                      <?php
                    }else{
                      ?>
                        <p>No hay datos disponibles. Etapa no Realizada</p>
                      <?php
                    }
                  ?>
                  

                  
                  
                </div>
              </div>

              <br>

              <!--CARD PARA LA ETAMA DE ENVIADO -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-success" style="width: 100%">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-success"><i class="fas fa-truck"></i> Enviado</h6>
                  <?php 
                    if ($e!="") {
                      ?>
                        <div style="float: right;" class="m-0 font-weight-bold text-primary"><i class="fas fa-thumbs-up"></i></div>
                      <?php
                    }
                  ?>
                </div>
                <div class="card-body">

                  <?php 
                    if ($e!="") {
                      ?>
                        <p>El pedido se asignó al conductor a las: {{$e->created_at}}</p>
                       <p>Conductor Asignado: {{$e->name}}. </p>
                      <?php
                    }else{
                      ?>
                        <p>No hay datos disponibles. Etapa no Realizada</p>
                      <?php
                    }
                  ?>
                  
                </div>
              </div>

               <!--CARD PARA LA ETAMA DE CANCELADO -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-danger" style="width: 100%">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-danger"><i class="fas fa-times"></i> Cancelado</h6>
                  <div style="float: right;" class="m-0 font-weight-bold text-danger"><!--<i class="fas fa-thumbs-up"></i>--></div>
                </div>
                <div class="card-body">
                  <p>Etapa realizada en: 30 de Septiembre del 2019 a las 6:00 pm.</p>
                  <p>Empleado Encargado: Ricardo Contreras. </p>



                  <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text"> Notas:</h6>
                  
                </div>
                <div class="card-body">
                  <p>El vehículo en el que se transportaba la mercancía sufrió un accidente por lo que la mercancía resultó golpeada y el cliente no la quiso recibir.</p>
                  	

                  
                </div>


                  
                </div>
              </div>


          </div>
      	</div>
 

@endsection