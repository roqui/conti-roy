<!-- <?php echo $pedido; ?> -->

<div id="tabla">
  <table class="table" style="text-decoration: none; color: black;">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Código</th>
        <th scope="col">Cantidad</th>
        <th scope="col">Unidad</th>
        <th scope="col">Descripción</th>
        <th>Precio U.</th>
        <th>Descuento</th>
        <th>Importe</th>
        <th><i class="fas fa-trash"></i></th>
      </tr>
    </thead>
    <tbody>
    @foreach($pedido as $item)
      <tr>
        <td><?php echo $item->codigo ?></td>
        <td> 
          <!-- <select name="cantidad" style="width: 75px;" id="cantidad_actual" required class="select2" onchange="cantidad_actual(<?php echo $item->cantidad ?>)">
              <option value="<?php echo $item->cantidad ?>" selected><?php echo $item->cantidad ?></option>
              @for($i=0;$i<101;$i++)
                <option value="<?php echo $i ?>">
                 <?php echo $i ?>
                </option>
              @endfor
            </select> -->
          <!-- <div class="input-group mb-3" style="width: 100px;">
              <input type="number" required class="form-control" value="<?php echo $item->cantidad ?>" name="cantidad" placeholder="Cant." aria-label="Username" aria-describedby="basic-addon1" id="cantidad" min="1">
          </div> -->
          <button type="button" class="btn btn-outline-dark btn-sm" 
          onclick="cantidad_actual(<?php echo $item->precio ?>,<?php echo $item->idProducto ?>,<?php echo $item->id ?>,<?php echo $item->cantidad ?>,1)">
              <i class="fas fa-minus"  ></i>
          </button>  <?php echo $item->cantidad ?>
          <button type="button" class="btn btn-outline-dark btn-sm" 
          onclick="cantidad_actual(<?php echo $item->precio ?>,<?php echo $item->idProducto ?>,<?php echo $item->id ?>,<?php echo $item->cantidad ?>,2)">
              <i class="fas fa-plus"></i>
          </button>
        </td>
        <td><?php echo $item->unidad ?></td>
        <td><?php echo $item->descripcion_producto ?></td>
        <td>$<?php echo $item->precio ?></td>
        <td><?php echo $item->descuento ?>%</td>
        <td scope="row">$<?php echo ($item->cantidad)*($item->precio); ?></td>
        <td>
            <button  onclick="eliminar(<?php echo $item->id; ?>)" type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
              <i class="fas fa-trash"></i>
            </button>
        </td>
      </tr>
    @endforeach
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Total: </td>
        <td>$@foreach($total as $item)<?php echo $item->CostoT ?>@endforeach</td>
      </tr>    
   </tbody>
  </table>
</div>

