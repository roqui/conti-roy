@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')
  
<div class="container">
    

    <!--ALERTAS-->
    @if(session('Mensaje'))
        <div class="alert alert-success" id="success-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensaje')}}</div>

    @endif
    @if(session('Mensajee'))
      
      <div class="alert alert-danger" id="danger-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajee')}}</div>

    @endif
    @if(session('Mensajea'))
        <div class="alert alert-warning" id="warning-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajea')}}</div>

    @endif

    <!--FIN ALERTAS-->

    <div class="card shadow mb-4">
           
              <div class="card-body">
                <h3>Pedidos Asignados</h3>
    <p class="mb-4">Listado de los Pedidos en estado 'Surtiendo' que te han sido asignados para que los lleves a través del proceso de 'Surtido' y 'Checado'.</p>
                <div class="table-responsive">
                          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                              <tr>
                                    <th scope="col" >ID-Folio</th>
                                    <th scope="col">Sucursal Origen</th>
                                    <th scope="col">Destino</th>
                                    <th scope="col">Fecha de Registro</th>
                                    <th scope="col"><i class="fa fa-usd"></i></th>
                                    <th scope="col" ></th>

                                    
                            </thead>
                            
                            <tbody>
                                  @foreach ($pasignados as $p)
                                <tr>
                                  <th scope="row">{{$p->id}}</th>
                                  <td>{{$p->origen}}</td>
                                  <td>{{$p->destino}}</td>
                                  <td>{{$p->created_at}}</td>
                                  <td>{{$p->CostoT}}</td>
                                  
                                 
                                  <td>
                                    <form action="checar">
                                        <input type="hidden" value="{{$p->id}}" name="idpedido">
                                        <button type="submit" class="btn btn-outline-info">Checar</button>
                                    </form>
                                  </td>
                                
                                </tr>
                              
                              @endforeach
                            </tbody>
                          </table>
                        
                </div>

              

  
              </div>
    </div>
</div>
@endsection()