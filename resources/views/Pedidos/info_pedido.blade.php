@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-1 text-gray-800">Seguimiento del pedido #4562</h1>
          <p class="mb-4">Resgistro del seguimiento del pedido con número #4562.</p>

          <!-- Content Row -->
          <div class="row">
          	<!--CARD PARA LA ETAMA DE PENDIENTE -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-warning" style="width: 100%">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-warning"><i class="fas fa-exclamation-triangle"></i> Pendiente</h6>
                  <div style="float: right;" class="m-0 font-weight-bold text-primary"><i class="fas fa-thumbs-up"></i></div>
                </div>
                <div class="card-body">
                  <p>Pedido Recibido: el 28 de Septiembre de 2019.</p>
                  <p>Destino en: Cale Pintor José Jara no.25</p>
                  <p>Tipo de Pedido: a Sucursal.</p>
                  
                </div>
              </div>

              <br>

              <!--CARD PARA LA ETAMA DE SURTIENDO -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-primary" style="width: 1000px">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-primary"><i class="fas fa-spinner"></i> Surtiendo</h6>
                  <div style="float: right;" class="m-0 font-weight-bold text-primary"><i class="fas fa-thumbs-up"></i></div>
                </div>
                <div class="card-body">
                  <p>Etapa realizada en: 30 de Septiembre del 2019 a las 9:55 am.</p>
                  <p>Empleado Encargado: Rodrigo Quintana. </p>
                  
                </div>
              </div>

              <!--CARD PARA LA ETAMA DE REMISIONADO -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-info" style="width: 1000px">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-info"><i class="fas fa-file"></i> Remisionado</h6>
                  <div style="float: right;" class="m-0 font-weight-bold text-primary"><i class="fas fa-thumbs-up"></i></div>
                </div>
                <div class="card-body">
                  <p>Etapa realizada en: 30 de Septiembre del 2019 a las 9:55 am.</p>
                  <p>Empleado Encargado: Rodrigo Quintana. </p>

                  <center><a href="#"><button type="button" class="btn btn-info" >Ver Remisión <i class="fas fa-print"></i></button></a></center>
                  
                </div>
              </div>

              <br>

              <!--CARD PARA LA ETAMA DE ENVIADO -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-success" style="width: 1000px">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-success"><i class="fas fa-truck"></i> Enviado</h6>
                  <div style="float: right;" class="m-0 font-weight-bold text-primary"><i class="fas fa-thumbs-up"></i></div>
                </div>
                <div class="card-body">
                  <p>Etapa realizada en: 30 de Septiembre del 2019 a las 12:00 pm.</p>
                  <p>Empleado Encargado: Jose Miguel Trejo. </p>
                  
                </div>
              </div>

               <!--CARD PARA LA ETAMA DE CANCELADO -->
          	<!-- Basic Card Example -->
              <div class="card shadow mb-4 border-left-danger" style="width: 1000px">
                <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text-danger"><i class="fas fa-times"></i> Cancelado</h6>
                  <div style="float: right;" class="m-0 font-weight-bold text-danger"><!--<i class="fas fa-thumbs-up"></i>--></div>
                </div>
                <div class="card-body">
                  <p>Etapa realizada en: 30 de Septiembre del 2019 a las 6:00 pm.</p>
                  <p>Empleado Encargado: Ricardo Contreras. </p>



                  <div class="card-header py-3">	
                  <h6 style="float: left;" class="m-0 font-weight-bold text"> Notas:</h6>
                  
                </div>
                <div class="card-body">
                  <p>El vehículo en el que se transportaba la mercancía sufrió un accidente por lo que la mercancía resultó golpeada y el cliente no la quiso recibir.</p>
                  	

                  
                </div>


                  
                </div>
              </div>


          </div>
      	</div>
 

@endsection