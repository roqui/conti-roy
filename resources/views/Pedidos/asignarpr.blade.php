@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')
  
<div class="container">
    

    <div class="card shadow mb-4">
           
              <div class="card-body">
                <h3>Conductores Disponibles para el pedido con no. de Folio: #{{$idPedido}}</h3>
    <p class="mb-4">Listado de los Empleados con el Rol 'Conductor' a los cuales es posible asignar un pedido para que lleve a su destino. </p>
                <div class="table-responsive">
                          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                              <tr>
                                    <th scope="row">ID</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Rol</th>
                                    <th scope="col" width="30"></th>
                               </tr>     
                            </thead>
                            
                            <tbody>
                                  @foreach ($conductores as $s)
                                <tr>
                                  <th scope="row">{{$s->id}}</th>
                                  <td>{{$s->name}}</td>
                                  <td>Conductor</td>
                                  
                                 
                                  <td>
                                    <center>
                                        <form action="asignadopr">
                                          <input type="hidden" value="{{$idPedido}}" name="idpedido">
                                          <input type="hidden" value="{{$s->id}}" name="idconductor">
                                          <button type="submit" class="btn btn-secondary">Asignar</button>
                                        </form>
                                    </center>
                                  </td>
                                </tr>
                              
                              @endforeach
                            </tbody>
                          </table>
                        
                </div>

              

  
              </div>
    </div>
</div>
@endsection()