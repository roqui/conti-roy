<button href="#" class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#inventario" style="float: right; margin-bottom: 10px;">
  <span class="icon text-white-50">
    <i class="fas fa-plus"></i>
  </span>
  <span class="text">Producto</span>
</button>

<!-- Modal -->
<div class="modal fade" id="inventario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Concepto</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <b><label>Seleccione un producto: </label></b> <br>
            <select name="id_prod[]" style="width: 460px;" id="id_producto" required class="select2" multiple="multiple">
              @foreach($productos as $producto)
                <option name="idProducto" value="{{$producto->id}}">
                {{ $producto->descripcion_producto }} || {{ $producto->codigo }} 
                </option>
              @endforeach
            </select>
            
            <input hidden value="{{$num_pedido}}" name="idPedido" id="id_pedido">
              <input hidden value="{{$id_cliente}}" name="id_cliente" id="id_cliente">
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary btn-icon-split" data-dismiss="modal" id="guardarnuevo">
                <span class="icon text-white-50">
                  <i class="fas fa-plus"></i>
                </span>
                <span class="text">Agregar</span>
              </button>
            </div>
      </div>
    </div>
  </div>

