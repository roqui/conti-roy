@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="text-decoration: none; color: black; margin:50px;">
<!-- ------------------- cancelar pedido    -->
  <form id="cancelar" action="cancelar_pedido" method="get" style="float: right; margin-right: 15px; margin-bottom: 10px;">
      <input type="text" hidden="" value="{{$num_pedido}}" name="id_pedido">
      <button class="btn btn-danger btn-icon-split" >
        <span class="icon text-white-50">
          <i class="far fa-times-circle"></i>
        </span>
        <span class="text">Cancelar</span>
      </button>
    </form>

  <!-- ----------------------- cancelar pedido -->

  <div  class="card shadow mb-4" style="width: 100%; margin: 0;">
    <div class="card-body">

      <div style="margin: 30px; background-color: white;">
        <div class="row">
          <div class="col-sm">
            <img style="width: 200px;" src="img/logo2.jpg"><br>
          </div>
          <div class="col-sm">
            <b><label>Email: </label></b> contipapelerias@gmail.com <br>
            <b><label>Teléfono: </label></b> +52 443 313 0740<br>
            <b><label>Sitio web: </label></b> contipapelerias.com
          </div>
          <div class="col-sm">
            <b><label>Producciones Conti, S.A. De C.V.</label></b><br>
            <b><label>Dirección: </label></b> Allende 1013, Centro histórico<br>
            CP. 58000 Morelia, Mich.
          </div>
        </div>
      </div>
      <hr class="sidebar-divider">

      
      <h4>Pedido</h4>

      <div class="row">
        <div class="col-sm">
          <b><label>Cliente: </label></b> {{$cliente->nombre}}<br>
          <b><label>RFC: </label></b> {{$cliente->rfc}} <br>
          <b><label>Dirección: </label></b> {{$cliente->calle}} #{{$cliente->num_exterior}} CP. {{$cliente->codigo_postal}}, 
          <br>{{$cliente->ciudad}} {{$cliente->estado}}, {{$cliente->pais}} <br>
          <b><label>Teléfono: </label></b> +52 {{$cliente->telefono}}
        </div>
        <div class="col-sm">
          <b><label>Pedido: </label></b> #{{$num_pedido}} <br>
          <b><label>Fecha y Hora: </label></b> {{$date}}<br>
          <b><label>Empleado que expidió: </label></b> {{ Auth::user()->name }}<br>
          <!-- <b><label>Pedido: </label></b> #54321 -->
          <br>
          @include('pedidos.new_concepto')
        </div>
      </div>

      <hr class="sidebar-divider">
      <div id="tabla" style="width: 100%;">
        
      </div>
      <br>
      <div style=" width: -webkit-fill-available;" id="observaciones" hidden="" >
        <form action="observaciones_pedido" method="get">
            <div class="input-group" >
              <div class="input-group-prepend">
                <span class="input-group-text">Observaciones</span>
              </div>
              <textarea class="form-control" placeholder="Las observaciones se guardaran al finalizar la compra" id="observaciones_input"></textarea>
            </div>
          <br>
              <button class="btn btn-success btn-icon-split" style="float: right; margin-right: 5px;" onclick="add_obs()" >
              <span class="icon text-white-50">
                <i class="fas fa-save"></i>
              </span>
              <span class="text">Finalizar</span>
            </button>
        </form>
        <!-- --------------------------------------------- IMPRIMIR -->
        <form id="imprimir" hidden=""  action="nota_pedido" method="get" style="float: right; margin-right: 10px;">
          <input type="text" hidden="" value="{{$num_pedido}}" name="id_pedido">
              <button class="btn btn-primary btn-icon-split" onclick="add_obs()">
                <span class="icon text-white-50">
                  <i class="fas fa-print"></i>
                </span>
                <span class="text">PDF</span>
              </button>
        </form>
      </div>
   
 </div>


</div>

@endsection


@section('script')
<script>
  
  $(document).ready(function() {

    //CARGANDO LA TABLA POR PRIMERA VEZ 
    // LE MANDO LOS PARAMETROS ATRAVEZ DE GET "A LA PHP" xd

    num_pedido=$('#id_pedido').val();
    $('#tabla').load('tabla_dinamica?num_pedido='+num_pedido);
    // $('#tabla').on('load','tabla_dinamica');
  //   $('#tabla').load('tabla_dinamica',{
  //   'pedido': num_pedido});
  });

  $(document).ready(function() {
    
    $('#guardarnuevo').click(function(event) {
      // AQUI JALO LOS VALORES CON .val() DE LOS INPUTS Y LOS IMPRIMO PARA VERIFICAR QUE EXISTEN
            id_producto=JSON.stringify($('#id_producto').val());
            num_pedido=$('#id_pedido').val();
            id_cliente=$('#id_cliente').val();
            console.log('id_producto: '+id_producto);
            // console.log('cantidad: '+cantidad);
            // console.log('id_PEDIDO: '+num_pedido);
            // console.log('id_cliente: '+id_cliente);

            // CREANDO EL AJAX PARA MANDAR LA SOLICITUD A UN METODO PARA AGREGAR LOS DATOS A LA BD
            $.ajax({
              url: 'agregar_concepto',
              type: 'post',
              data: {'id_producto': id_producto,
              'num_pedido':num_pedido, 
              _token: '{{csrf_token()}}',
              'id_cliente':id_cliente
               }, 
               success:function(response) {
                response.idproducto; 
               }
            })
            .done(function(r) {
              // CUANDO SE TERMINA DE EJECUTAR LA FUNCION RECARGO LA TABLA
              if(r=="Alerta! Estas en el minímo de tu producto!"){
                alertify.warning(r);
              }
              if(r=="Ya no tienes suficiente inventario! de uno de estos productos :( Realiza una compra!"){
                alertify.error(r);
              }else{
                alertify.success(r);
              }
              $('#tabla').load('tabla_dinamica?num_pedido='+num_pedido);
              imprimirSiNo(num_pedido);
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              // console.log("complete");
            });

    });
  });

  // ----------------------------- ELIMINAR
    // AQUI, AL IMPRIMIR EL BOTON DE ELIMINAR EN CADA TUPLA LE ASIGNO EL ID DE
    // LA TUPLA EN LA TABLA CONCEPTOS Y EJECUTO UN AJAX PARECIDO AL DE ARRIBA  

  function eliminar(id){
    console.log(id);
    num_pedido=$('#id_pedido').val();
    
    $.ajax({
       url: 'eliminar_concepto',
       type: 'POST',
       data: {'id_concepto': id, _token: '{{csrf_token()}}' },
     })
     .done(function() {
      $('#tabla').load('tabla_dinamica?num_pedido='+num_pedido);
       console.log("success delete: " + id);
     })
     .fail(function() {
       console.log("error");
     })
     .always(function() {
       // console.log("complete");
     });
      

  }

  // --------------------------------------------------------------
  function imprimirSiNo(id){

    console.log('imprimirSiNo')
    $.ajax({
      url: 'imprimirSiNo',
      type: 'post',
      data: {'id_pedido': id,_token: '{{csrf_token()}}'},
    })
    .done(function(r) {
      // console.log("tuplas: " + r);
      if (r==1) {
        $('#imprimir').removeAttr('hidden');
        $('#observaciones').removeAttr('disabled');
        $('#observaciones').removeAttr('hidden');

      }
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    

  }
// -----------------------------------------

// -----------------------------------------
$(document).ready(function() {
        $('.select2').select2();
      });
// ----------------------------------------------------

  function cantidad_actual(precio,id_producto,id_concepto,cant,accion){
    num_pedido=$('#id_pedido').val();
    console.log('pedido: '+num_pedido);
    // console.log( "pedido: "+ num_pedido+" id_concepto: "+id_concepto+" Cantidad actual: " + cant + " accion: "+ accion );

    $.ajax({
      url: 'act_cantidad',
      type: 'get',
      data: { 
        'id_concepto':id_concepto,
        'actual': cant, 
        'accion': accion, 
        'id_pedido': num_pedido,
        'id_producto': id_producto,
        'precio': precio }
    })
    .done(function(r) {
      if(r=="Alerta! Estas en el minímo de tu producto!"){
                alertify.warning(r);
        }else if(r=="Ya no tienes suficiente inventario! de uno de estos productos :( Realiza una compra!" || r=="Ya no tienes podructo en tu concepto. Se eliminará automaticamente."){
                alertify.error(r);
        }else{ r=="Ya no tienes podructo en tu concepto. Se eliminará automaticamente."
                alertify.success(r);
              }
      $('#tabla').load('tabla_dinamica?num_pedido='+num_pedido);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  }
// -----------------------------------------------------
  
  
    

    function add_obs(){
    num_pedido=$('#id_pedido').val();
    observaciones=$('#observaciones_input').val();
    console.log('pedido: '+num_pedido);
    console.log('observaciones: '+observaciones);
    
    $.ajax({
      url: 'observaciones_pedido',
      type: 'get',
      data: {'observaciones': observaciones, 'id_pedido':num_pedido },
    })
    .done(function() {
      console.log("success");
      // alert('Observaciones guardadas con exito');
      
    })
    .always(function() {
      console.log("complete");
      alertify.success('Pedido Finalizado.');
    });
    

    

    
    
  }
  



</script>


@endsection