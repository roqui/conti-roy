@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div class="container-fluid pt-3">


  <div class="row pt-1" >
    <div class="col-xl-3 col-lg-6">
      <div class="card card-stats mb-4">
        <div class="card-body">
          <a  style="text-decoration: none;" href="clientes">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0">Clientes</h5>
                <span class="h2 font-weight-bold mb-0">
                  <h2 class="timer count-title count-number" data-to="{{$cuentaclientes}}" data-speed="1500"></h2>
                </span>
              </div>
              <div class="col-auto">
               <div class="icon icon-shape ">
                <img style="width:80px; "src="img/ordencompra.png">
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-6">
    <div class="card card-stats mb-4">
      <div class="card-body">
        <a  style="text-decoration: none;" href="proveedores">

          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Proveedores</h5>
              <span class="h2 font-weight-bold mb-0"><h2 class="timer count-title count-number" data-to="{{$cuentaproveedores}}" data-speed="1500"></h2></span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape ">
                <img style="width:100px; "src="img/provider.jpg">
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-6">
    <div class="card card-stats mb-4">
      <div class="card-body">
        <a  style="text-decoration: none;" href="users">

          <div class="row">
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Usuarios</h5>
              <span class="h2 font-weight-bold mb-0"><h2 class="timer count-title count-number" data-to="{{$cuentausuarios}}" data-speed="1500"></h2> </span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape ">
                <img style="width:80px; "src="img/user.png">
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-lg-6">
    <div class="card card-stats mb-4">
      <div class="card-body">
        <a  style="text-decoration: none;" href="productos">

          <div class="row">
            <div class="counter"></div>
            <div class="col">
              <h5 class="card-title text-uppercase text-muted mb-0">Productos</h5>
              <span class="h2 font-weight-bold mb-0"><h2 class="timer count-title count-number" data-to="{{$cuentaproductos}}" data-speed="1500"></h2>
              </span>
            </div>
            <div class="col-auto">
              <div class="icon icon-shape ">
                <img style="width:80px; "src="img/new.png">
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>

</div>
  @include('chartpedidos')


</div>






@endsection


@section('script')
<script type="text/javascript">
  (function ($) {
    $.fn.countTo = function (options) {
      options = options || {};

      return $(this).each(function () {
      // set options for current element
      var settings = $.extend({}, $.fn.countTo.defaults, {
        from:            $(this).data('from'),
        to:              $(this).data('to'),
        speed:           $(this).data('speed'),
        refreshInterval: $(this).data('refresh-interval'),
        decimals:        $(this).data('decimals')
      }, options);
      
      // how many times to update the value, and how much to increment the value on each update
      var loops = Math.ceil(settings.speed / settings.refreshInterval),
      increment = (settings.to - settings.from) / loops;
      
      // references & variables that will change with each update
      var self = this,
      $self = $(this),
      loopCount = 0,
      value = settings.from,
      data = $self.data('countTo') || {};
      
      $self.data('countTo', data);
      
      // if an existing interval can be found, clear it first
      if (data.interval) {
        clearInterval(data.interval);
      }
      data.interval = setInterval(updateTimer, settings.refreshInterval);
      
      // initialize the element with the starting value
      render(value);
      
      function updateTimer() {
        value += increment;
        loopCount++;
        
        render(value);
        
        if (typeof(settings.onUpdate) == 'function') {
          settings.onUpdate.call(self, value);
        }
        
        if (loopCount >= loops) {
          // remove the interval
          $self.removeData('countTo');
          clearInterval(data.interval);
          value = settings.to;
          
          if (typeof(settings.onComplete) == 'function') {
            settings.onComplete.call(self, value);
          }
        }
      }
      
      function render(value) {
        var formattedValue = settings.formatter.call(self, value, settings);
        $self.html(formattedValue);
      }
    });
    };

    $.fn.countTo.defaults = {
    from: 0,               // the number the element should start at
    to: 0,                 // the number the element should end at
    speed: 10000,           // how long it should take to count between the target numbers
    refreshInterval: 100,  // how often the element should be updated
    decimals: 0,           // the number of decimal places to show
    formatter: formatter,  // handler for formatting the value before rendering
    onUpdate: null,        // callback method for every time the element is updated
    onComplete: null       // callback method for when the element finishes updating
  };
  
  function formatter(value, settings) {
    return value.toFixed(settings.decimals);
  }
}(jQuery));

  jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
    formatter: function (value, options) {
      return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
    }  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
    var $this = $(this);
    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
    $this.countTo(options);
  }
});
</script>


<script type="text/javascript">

  function crearCadenaLineal(json){
    var parsed = JSON.parse(json);
    var arr = [];
    for(var y in parsed){
      arr.push(parsed[y]);
    }
    return arr;
  }
  
</script>
<script>

 piejsonY=crearCadenaLineal('<?php echo $piejsonY ?>');

 piejsonX=crearCadenaLineal('<?php echo $piejsonX ?>');

 var data = [
 {
  x: piejsonX,
  y: piejsonY,
  mode: 'lines',
  line: {
    color: 'rgb(44, 230, 220)',
    width: 5
  }
}
];

Plotly.newPlot('charpedidos', data, {}, {showSendToCloud:true})  
</script>



<script>

  piejsonY2=crearCadenaLineal('<?php echo $piejsonY2 ?>');

  piejsonX2=crearCadenaLineal('<?php echo $piejsonX2 ?>');

  var trace1 = {
   x: piejsonX2,
   y: piejsonY2,
   line: {
    color: 'rgb(44, 230, 220)',
    width: 5
  }
};


var data = [trace1];

Plotly.newPlot('myDiv', data, {}, {showSendToCloud: true});
</script>

<script type="text/javascript">

  piejsonY3=crearCadenaLineal('<?php echo $piejsonY3 ?>');

  piejsonX3=crearCadenaLineal('<?php echo $piejsonX3 ?>');

  var trace0 = {
    type: 'scatter',
    x: piejsonX3,
    y: piejsonY3,
    line: {
      color: 'rgb(44, 230, 220)',
      width: 5
    }
  };
  var data = [trace0]

  Plotly.plot('plotly-div', data, {}, {showSendToCloud:true});
</script>

<script type="text/javascript">


   pieY4=crearCadenaLineal('<?php echo $pieY4 ?>');

   pieX4=crearCadenaLineal('<?php echo $pieX4 ?>');


var trace2 = {
  x: pieY4,
  y: pieX4,
  type: 'bar'
};

var data = [trace2];

Plotly.newPlot('myDiv2', data, {}, {showSendToCloud: true})
</script>

<script type="text/javascript">


   providerY=crearCadenaLineal('<?php echo $providerY ?>');

   providerX=crearCadenaLineal('<?php echo $providerX ?>');


var trace2 = {
  x: providerY,
  y: providerX,
  type: 'bar'
};

var data = [trace2];

Plotly.newPlot('myDiv3', data, {}, {showSendToCloud: true})
</script>






@endsection


