@extends('layouts.plantilla')
@extends('layouts.menu')
@section('main')
<div style="margin:50px;">
 
  @if(session('Mensaje'))

  <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensaje')}}</div>

  @endif
  @if(session('Mensajee'))

  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajee')}}</div>

  @endif
  @if(session('Mensajea'))

  <div class="alert alert-warning" id="warning-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajea')}}</div>

  @endif
  <div>
  





    <div class="card shadow mb-4">

      <div class="card-body">

         <h3>Proveedores</h3>
  <h4 style="float: left;">Listado de proveedores de Papelerías CONTI</h4>

  @foreach($permisos as $mod_delete)
  @if($mod_delete->idInterfaz==14)
   <div style="float: right; margin-bottom: 10px; margin-top: 10px;">
    <a href="newprovider"  class="btn btn-primary btn-icon-split">
      <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
      </span>
      <span class="text">Agregar proveedor</span>
    </a>
  </div>
  @break
  @endif
  @endforeach


        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th scope="col" >ID</th>
                <th scope="col">Nombre</th>
                <th scope="col" >RFC</th>
                <th scope="col" width="50"></th>
                @foreach($permisos as $mod_delete)
                @if($mod_delete->idInterfaz==15)
                <th scope="col" width="50"></th>
                <th scope="col" width="50"></th>
                @break
                @endif
                @endforeach
              </tr>
            </thead>

            <tbody>
              @foreach ($proveedores as $proveedor)
              <tr>
                <th scope="row">{{$proveedor->id}}</th>
                <td>{{$proveedor->nombre}}</td>
                <td>{{$proveedor->rfc}}</td>
                <td width="50">@include('proveedores.editproviders')</td>

                @foreach($permisos as $mod_delete)
                @if($mod_delete->idInterfaz==15)
                <td width="50">
               
                 <form action="actprov">
                   <input type="hidden" name="id" value="{{$proveedor->id}}">
                   <button class="btn " type="submit"><i class="fa fa-edit"></i></button>
                 </form>
               </td>

               <td width="50">
              @include('proveedores.deleteproveedor')  
               
              </td>
              @break
              @endif
              @endforeach 
            </tr>

            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>









</div>
@foreach($permisos as $mod_delete)
@if($mod_delete->idInterfaz==16)
<div style="float: right;">
  <a href="providerexcel"  class="btn btn-success btn-icon-split"><span class="icon text-white-50"><i class="fas fa-download"></i> </span>
    <span class="text">Obtener Excel</span> </a> <br>
  </div>
@break
@endif
@endforeach

</div>


<!-- /.container-fluid -->


@endsection
