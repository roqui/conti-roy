
@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="margin: 50px;">
  

<div class="card shadow mb-6">
           
  <div class="card-body">
  <div>
 
<div style="margin-bottom: 20px;">
   <h3>Editar Proveedor </h3>

</div>
    <form  action="/updatep/{{ $proveedor -> id}}" method="POST">
      @method('PUT')

  
          {{csrf_field()}} 
          <input type="hidden" name="_method" value="POST">

    <div class="row">
        <div class="col-sm">

          <b><label>Nombre:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> </span>
            </div>

            <input required  type="text" placeholder="{{$proveedor->nombre}}" value="{{$proveedor->nombre}}" id = "nombre" name="nombre" class="form-control" aria-nombreibedby="basic-addon1">
          </div>   

          <b><label>Correo Electrónico:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> @ </span>
            </div>

            <input required  type="email" placeholder="{{$proveedor->email}}" value="{{$proveedor->email}}" id ="email" name="email" class="form-control" aria-nombreibedby="basic-addon1">
          </div>



           <input name="foto" value="img/users.png" hidden>
        </div>
        <div class="col-sm">


          <b><label>Número de Teléfono:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> +52</span>
            </div>

            <input required  type="number" placeholder="{{$proveedor->telefono}}" value="{{$proveedor->telefono}}" id ="telefono" name="telefono" class="form-control" aria-nombreibedby="basic-addon1">
          </div>

          <b><label>RFC:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> </span>
            </div>

            <input required type="text" placeholder="{{$proveedor->rfc}}" value="{{$proveedor->rfc}}" id ="rfc" name="rfc" pattern="^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\d]{3}))?$" class="form-control" aria-nombreibedby="basic-addon1">
          </div>

          
        </div>
      </div>
      <hr>
      <h5>Dirección del Proveedor</h5>

      <div class="row">
        <div class="col-sm">
         <b><label>Calle:</label></b> <br>
         <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"> </span>
          </div>

          <input required  type="text" placeholder="{{$proveedor->calle}}" value="{{$proveedor->calle}}" id="calle" name="calle" class="form-control" aria-nombreibedby="basic-addon1">
        </div>

      </div>
      
      <div class="col-sm">
       <b><label>Num. Exterior:</label></b> <br>
       <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1"> # </span>
        </div>
        
        <input required  type="number" placeholder="{{$proveedor->num_exterior}}" value="{{$proveedor->num_exterior}}" id="num_exterior" name="num_exterior" class="form-control" aria-nombreibedby="basic-addon1">
      </div>    
    </div>
    <div class="col-sm">
     <b><label>Código Postal:</label></b> <br>
     <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> CP.</span>
      </div>
      
      <input required  type="text" placeholder="{{$proveedor->codigo_postal}}" value="{{$proveedor->codigo_postal}}" id="codigo_postal" name="codigo_postal" class="form-control" aria-nombreibedby="basic-addon1">
    </div>    
  </div>
</div>

<div class="row">
  <div class="col-sm">
    <b><label>País:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  type="text" placeholder="{{$proveedor->pais}}" value="{{$proveedor->pais}}" id ="País" name="pais" class="form-control" aria-nombreibedby="basic-addon1">
    </div>
  </div>
  <div class="col-sm">
    <b><label>Estado:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  type="text" placeholder="{{$proveedor->estado}}" value="{{$proveedor->estado}}" id="estado" name="estado" class="form-control" aria-nombreibedby="basic-addon1">
    </div>
  </div>
  <div class="col-sm">
    <b><label>Ciudad:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  type="text" placeholder="{{$proveedor->ciudad}}" value="{{$proveedor->ciudad}}" id ="ciudad" name="ciudad" class="form-control" aria-nombreibedby="basic-addon1">
    </div>
  </div>
</div>
<hr>
<h5>Información Adicional</h5>

<div class="row">
  <div class="col-sm">
    <b><label>Días de Plazo:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  type="text" placeholder="{{$proveedor->dias_plazo}}" value="{{$proveedor->dias_plazo}}" id ="dias_plazo" name="dias_plazo" class="form-control" aria-nombreibedby="basic-addon1">
    </div>

    <b><label>Observaciones:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  class="form-control" placeholder="{{$proveedor->observaciones}}" value="{{$proveedor->observaciones}}" id = "observaciones" name="observaciones" aria-nombreibedby="basic-addon1">

      </input>
    </div>

        <div style="margin-top: 10px;">
              <a href="proveedores" class="btn btn-outline-danger"><i class="fa fa-arrow-left"></i>   Cancelar  </a>

            </div>
    
  </div>
  <div class="col-sm">
    <b><label>Descuento:</label></b> <br>
    <div class="input-group mb-3">

<div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> %</span>
      </div>
      <input type="text" required placeholder="{{$proveedor->descuento}}" value="{{$proveedor->descuento}}" id="descuento" name="descuento" class="form-control" aria-nombreibedby="basic-addon1">

      
    </div>


    <div style="float: right; margin-top: 90px;">
    <button type="submit"  class="btn btn-primary btn-icon-split">
      <span class="icon text-white-50">
        <i class="fas fa-save"></i>
      </span>
      <span class="text">Guardar</span>
    </button>
    </div>
    </div>
  </div>



  </form>
  </div>

</div>
</div>
  

</div>

@endsection