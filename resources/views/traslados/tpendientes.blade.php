@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')
  
<div class="container">
    

    <!--ALERTAS-->
    @if(session('Mensaje'))
        <div class="alert alert-success" id="success-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensaje')}}</div>

    @endif
    @if(session('Mensajee'))
      
      <div class="alert alert-danger" id="danger-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajee')}}</div>

    @endif
    @if(session('Mensajea'))
        <div class="alert alert-warning" id="warning-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajea')}}</div>

    @endif

    <!--FIN ALERTAS-->

    <div class="card shadow mb-4">

           
              <div class="card-body">
                <h3>Traslados Pendientes</h3>
    <p class="mb-4">Listado de los Traslados en estado 'Pendiente', es decir que no han pasado al proceso de 'Checado'.</p>
                <div class="table-responsive">
                          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                              <tr>
                                    <th scope="col" >ID-Folio</th>
                                    <th scope="col">Sucursal Origen</th>
                                    <th scope="col">Destino</th>
                                    <th scope="col">Fecha de Registro</th>
                                    <th scope="col"><i class="fa fa-usd"></i></th>
                                    <th scope="col" ></th>
                                    @foreach($permisos as $item)
                                    @if($item->idInterfaz==36)
                                    <th scope="col" ></th>
                                    @break
                                    @endif
                                    @endforeach

                                    
                            </thead>
                            
                            <tbody>
                                  @foreach ($tpendientes as $t)
                                <tr>
                                  <th scope="row">{{$t->id}}</th>
                                  <td>{{$t->norigen}}</td>
                                  <td>{{$t->ndestino}}</td>
                                  <td>{{$t->created_at}}</td>
                                  <td>{{$t->CostoT}}</td>
                                  <td>@include('traslados.edittpendientes')</td>
                                
                                @foreach($permisos as $item)
                                @if($item->idInterfaz==36)
                               <td>
                                  <form action="asignartp">
                                      <input type="hidden" value="{{$t->id}}" name="idtraslado">
                                      <button type="submit" class="btn btn-outline-info">Asignar</button>
                                  </form>
                                </td>
                                @break
                                @endif
                                @endforeach
                                
                                </tr>
                              
                              @endforeach
                            </tbody>
                          </table>
                        
                </div>

              

  
              </div>
    </div>
</div>
@endsection()

