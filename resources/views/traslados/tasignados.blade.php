@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')
  
<div class="container">
    <h3>Traslados Asignados</h3>
    <p class="mb-4">Listado de los Traslados en estado 'Surtiendo' que te han sido asignados para que los lleves a través del proceso de 'Surtido' y 'Checado'.</p>

    <!--ALERTAS-->
    @if(session('Mensaje'))
        <div class="alert alert-success" id="success-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensaje')}}</div>

    @endif
    @if(session('Mensajee'))
      
      <div class="alert alert-danger" id="danger-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajee')}}</div>

    @endif
    @if(session('Mensajea'))
        <div class="alert alert-warning" id="warning-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajea')}}</div>

    @endif

    <!--FIN ALERTAS-->

    <div class="card shadow mb-4">
           
              <div class="card-body">
                <div class="table-responsive">
                          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                              <tr>
                                    <th scope="col" >ID-Folio</th>
                                    <th scope="col">Sucursal Origen</th>
                                    <th scope="col">Destino</th>
                                    <th scope="col">Fecha de Registro</th>
                                    <th scope="col" ></th>
                                    <th></th>

                                    
                            </thead>
                            
                            <tbody>
                                  @foreach ($tasignados as $t)
                                <tr>
                                  <th scope="row">{{$t->id}}</th>
                                  <td>{{$t->norigen}}</td>
                                  <td>{{$t->ndestino}}</td>
                                  <td>{{$t->created_at}}</td>
                                  <td>@include('traslados.edittpendientes')</td>
                                  <td>
                                    <form action="checart">
                                        <input type="hidden" value="{{$t->id}}" name="idpedido">
                                        <button type="submit" class="btn btn-outline-info">Checar</button>
                                    </form>
                                  </td>
                                     

                                </tr>
                              
                              @endforeach
                            </tbody>
                          </table>
                        
                </div>

              

  
              </div>
    </div>
</div>
@endsection()