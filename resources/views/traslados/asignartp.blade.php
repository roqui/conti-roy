@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')
  
<div class="container">
    <h3>Surtidores Disponibles para el traslado con no. de Folio: #{{$idTraslado}}</h3>
    <p class="mb-4">Listado de los Empleados con el Rol 'Surtidor' a los cuales es posible asignar un Traslado para surtir. </p>

    <div class="card shadow mb-4">
           
              <div class="card-body">
                <div class="table-responsive">
                          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                              <tr>
                                    <th scope="row">ID</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Rol</th>
                                    <th scope="col" width="30"></th>
                               </tr>     
                            </thead>
                            
                            <tbody>
                                  @foreach ($surtidores as $s)
                                <tr>
                                  <th scope="row">{{$s->id}}</th>
                                  <td>{{$s->name}}</td>
                                  <td>Surtidor</td>
                                  
                                 
                                  <td>
                                    <center>
                                        <form action="asignadotp">
                                          <input type="hidden" value="{{$idTraslado}}" name="idtraslado">
                                          <input type="hidden" value="{{$s->id}}" name="idsurtidor">
                                          <button type="submit" class="btn btn-secondary">Asignar</button>
                                        </form>
                                    </center>
                                  </td>
                                </tr>
                              
                              @endforeach
                            </tbody>
                          </table>
                        
                </div>

              

  
              </div>
    </div>
</div>
@endsection()