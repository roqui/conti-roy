<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Imprimiendo</title>
  <link rel="stylesheet" href="">
    <!-- <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"> -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <!-- <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet"> -->


  



  
</head>
<body>




      <div class="contenedor1" style="height: 75px; font: small">
          <div style="float: left; ">
            <img style="width: 150px;" src="img/logo2.jpg"><br>
          </div>
          <div style="float: right;">
            <b><label>Email: </label></b> contipapelerias@gmail.com <br>
            <b><label>Teléfono: </label></b> +52 443 313 0740<br>
            <b><label>Sitio web: </label></b> contipapelerias.com
          </div>
          <div style="float: right; margin-right: 20px; ">
            <b><label>Producciones Conti, S.A. De C.V.</label></b><br>
            <b><label>Dirección: </label></b> Allende 1013, Centro <br>histórico
            CP. 58000 Morelia, Mich.
          </div>
      </div>
      <!-- <hr class="sidebar-divider"> -->
      <h4>Traslado</h4>
      <div class="contenedor2" style="width: 100%; height: 150px; font: small;">
        <div style="float: left;">
          
          <b><label>Destino: </label></b> {{$sucursal->nombre}}<br>
          <b><label>Dirección: </label></b> {{$sucursal->calle}} #{{$sucursal->num_exterior}} CP. {{$sucursal->codigo_postal}}, 
          <br>{{$sucursal->ciudad}},  México <br>
          <b><label>Teléfono: </label></b> +52 {{$sucursal->telefono}}
        </div>
        <div class="col-sm">
          <b><label>Traslado: </label></b> #{{$num_pedido}} <br>
          <b><label>Fecha y Hora: </label></b> {{$date}}<br>
          <b><label>Empleado que expidió: </label><br></b> {{ $usuario }}<br>
          <br>
        </div>
      </div>

     

      <!-- <hr class="sidebar-divider"> -->

        <div class="table">
       
          <table class="table" style="text-decoration: none; color: black;">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Código</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Unidad</th>
                <th scope="col">Descripción</th>
                <th>P.U.</th>
                <th>Descuento</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
            @foreach($tabla as $item)
              <tr>
                <td><?php echo $item->codigo ?></td>
                <td><?php echo $item->cantidad ?></td>
                <td><?php echo $item->unidad ?></td>
                <td><?php echo $item->descripcion_producto ?></td>
                <td>$<?php echo $item->precio ?></td>
                <td> 0 %</td>
                <td scope="row">$<?php echo ($item->cantidad)*($item->precio); ?></td>
              </tr>
            @endforeach
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Total:</td>
                <td>${{$total}}</td>
              </tr>
           </tbody>
          </table>
        </div>
        @foreach($pedido as $item)
        @if($item->observaciones)
        <div>
          Observaciones
          <br>
          <p style="color: black;">
           {{$item->observaciones}}
          </p>
        </div>
        @endif
        @endforeach
  
</body>
</html>