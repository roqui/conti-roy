@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="margin:30px;">

  <div class="card shadow mb-4" >
    <div class="card">


      <div style="text-decoration: none; color: black;" class="card-body">
        <b><label style="font-size: 20px;">Traslados</label></b>
        <h4>Traslados de la sucursal
          <b>
            <?php
            $id = Auth::user()->idSucursal; 

            $sucursal = DB::table('destinos')->select('nombre')->where('id', '=', $id)->first();?>
            @foreach($sucursal as $key)
            {{$key}}
            @endforeach


          </b>
        </h4>


      </div>
      <div class="row" >
        
        <div class="col-sm">

        @foreach($permisos as $item)
        @if($item->idInterfaz==34)
          @include('traslados.infonewtraslado')
        @break
        @endif
        @endforeach

     
        @foreach($permisos as $item)
        @if($item->idInterfaz==38)
          <div class="card" style="margin:20px;  ">
            <a href="tremisionados" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/document.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Remisionados</label></b>
                <h5>Échale un vistaso a todos los Traslados remisionados</h5>
              </div>
            </a>
          </div>
        @break
        @endif
        @endforeach


        </div> 

        <div class="col-sm" style=" ;">
        @foreach($permisos as $item)
        @if($item->idInterfaz==35)
          <div class="card" style="margin:20px;  ">
            <a href="tpendientes" style="text-decoration: none; color: black; " >
              <div class="card-body">
                <img src="img/box.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px; text-align: center;">Pendientes</label></b>
                <h5>Actualmente tienes <b>14</b> Traslados pendientes</h5>
              </div>
            </a>
          </div>
        @break
        @endif
        @endforeach


        @foreach($permisos as $item)
        @if($item->idInterfaz==35)
          <div class="card" style="margin:20px;  ">
            <a href="work" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/return.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Devoluciones</label></b>
                <h5>Aquí podrás ver los pedidos devoluciones</h5>
              </div>
            </a>
          </div>
        @break
        @endif
        @endforeach


        </div>
        <div class="col-sm">

        @foreach($permisos as $item)
        @if($item->idInterfaz==40)
          <div class="card" style="margin:20px;  ">
            <a href="todost" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/traslados.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Todos</label></b>
                <h5>¿Estás buscando un traslado en específico?, ¡Encuentralo aquí!</h5>
              </div>
            </a>
          </div>
        @break
        @endif
        @endforeach
        
        @foreach($permisos as $item)
        @if($item->idInterfaz==41)
          <div class="card" style="margin:20px;  ">
            <a href="tproximos" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/truck.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Próximos</label></b>
                <h5>Aquí podrás ver los Traslados próximos por llegar</h5>

              </div>
            </a>
          </div>
        @break
        @endif
        @endforeach

        </div> 
      </div>


    </div>
  </div>

  <div>


    @endsection

    @section('script')

    <script>
      
      $(document).ready(function() {
        $('.select2').select2();
      });
    </script>

    @endsection