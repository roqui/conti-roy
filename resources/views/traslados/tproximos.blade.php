@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')
  
<div class="container">
    <h3>Traslados Próximos por Llegar</h3>
    <p class="mb-4">Listado de los traslados próximos a llegar a la Sucursal.</p>

    <!--ALERTAS-->
    @if(session('Mensaje'))
        <div class="alert alert-success" id="success-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensaje')}}</div>

    @endif
    @if(session('Mensajee'))
      
      <div class="alert alert-danger" id="danger-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajee')}}</div>

    @endif
    @if(session('Mensajea'))
        <div class="alert alert-warning" id="warning-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajea')}}</div>

    @endif

    <!--FIN ALERTAS-->



    <div id="datos2" class="datos2"></div>

    <div class="card shadow mb-4">
           
              <div class="card-body">
                <div class="table-responsive">
                          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                              <tr>
                                    <th scope="col" >ID-Folio</th>
                                    <th scope="col">Sucursal Origen</th>
                                    <th scope="col">Fecha de Registro</th>
                                    <th scope="col"><i class="fa fa-usd"></i></th>
                                    <th scope="col" ></th>
                                    <th scope="col" ></th>

                                    
                            </thead>
                            
                            <tbody>
                                  @foreach ($tproximos as $t)
                                <tr>
                                  <th scope="row">{{$t->id}}</th>
                                  <td>{{$t->norigen}}</td>
                                  <td>{{$t->created_at}}</td>
                                  <td>{{$t->CostoT}}</td>
                                  <td>@include('traslados.edittpendientes')</td>

                               <td>
                                  <form action="revisart">
                                      <input type="hidden" value="{{$t->id}}" name="idtraslado">
                                      <button type="submit" class="btn btn-outline-info">Revisar</button>
                                  </form>
                              </td>
                               

                                
                                </tr>
                              
                              @endforeach
                            </tbody>
                          </table>
                        
                </div>

              

  
              </div>
    </div>
</div>
@endsection()

@section('script')
<script type="text/javascript">
  function cancelar(){
    $(document).ready(function(){
      var idpedido = $('#pedido').val();
      var div=$('#datos2').parent();
      $.ajax({
            type:'get',
            url:'{!!URL::to('cancelart')!!}',
            dataType: "json",
            data:{'idpedido': idpedido},
            success:function($msj){
              console.log('Aquí va  al mensaje: ...');
              console.log($msj);
              
                div.find("#datos2").html($msj);
               
            },
            error:function(xhr, status, error){
              console.log(xhr);
              console.log(status);
              console.log(error);
            }

        });

    });
  }
</script>
@endsection

