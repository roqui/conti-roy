@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div class="container">
    <h3>Proceso de Checado para el Traslado #{{$idpedido}}</h3>
    <p class="mb-4">Listado de los artículos asociados con el pedido con número de folio {{$idpedido}}.</p>

    @if(session('Mensajeu'))
    
    <div class="alert alert-warning" id="warning-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajeu')}}</div>

    @endif  

    <div id="alert" class="alert"></div>

    <div class="form-group col-md-6">
      <label for="inputEmail4">Código del Producto:</label>
      <input type="email" style="width: 300px;"  class="codigo form-control" id="caja" placeholder="">
    </div>

    <input type="hidden" value="{{$idpedido}}" id="pedido" name="pedido">
    

    <div class="card shadow mb-4">
           
              <div class="card-body">
                <div class="table-responsive">
                          
                       <div id="datos" class="datos"></div> 
                </div>

              

  
              </div>
    </div>

    <form action="finchecart">
    	<input type="hidden" value="{{$idpedido}}" name="idpedido">
    	<button type="submit" style="margin-bottom: 10px; float: right;"  class="btn btn-primary btn-icon-split">
		    <span class="icon text-white-50">
		      	<i class="fas fa-arrow-circle-right"></i>
		    </span>
		    <span class="text">Continuar Proceso</span>
  		</button>
    </form>
    
           
              
</div>

 

@endsection()

@section('script')
<script type="text/javascript">
  $(document).ready(function(){//en este script estoy cargando la tabla 
    	var id = $('#pedido').val();
    	var div=$('#datos').parent();
        $.ajax({
	        type:'get',
	        url:'{!!URL::to('conceptosta')!!}',
	        dataType: "json",
	        data:{'id': id},
	        success:function($datos){
	          console.log('Primera tabla...');
	          div.find("#datos").html($datos);
	          
	        },
	        error:function(xhr, status, error){
	          console.log(xhr);
	          console.log(status);
	          console.log(error);
	        }

   		});
  
});
</script>

<script type="text/javascript">
  $(document).ready(function(){
    	$(".codigo").keypress(function(){
    		var codigo=$(this).val();
    		var pedido = $('#pedido').val();
    		var div=$('#alert').parent();
    		 $.ajax({
		        type:'get',
		        url:'{!!URL::to('checadot')!!}',
		        dataType: "json",
		        data:{'codigo':codigo, 'pedido': pedido},
		        success:function($msj){
		          console.log('Aquí va  al mensaje: ...');
		          console.log($msj);
		          div.find("#alert").html($msj);
		          tabla();
		        },
		        error:function(xhr, status, error){
		          console.log(xhr);
		          console.log(status);
		          console.log(error);
		        }

		    });

    		$('#caja').val('');
    	});
    
  });
</script>
<script type="text/javascript">
	function tabla (){
		$(document).ready(function(){//en este script estoy cargando la tabla 
	    	var id = $('#pedido').val();
	    	var div=$('#datos').parent();
	        $.ajax({
		        type:'get',
		        url:'{!!URL::to('conceptosta')!!}',
		        dataType: "json",
		        data:{'id': id},
		        success:function($datos){
		          console.log('Imprimiendo tabla...');
		          div.find("#datos").html($datos);
		          
		        },
		        error:function(xhr, status, error){
		          console.log(xhr);
		          console.log(status);
		          console.log(error);
		        }

	   		});
  
		});
	}
</script>
@endsection







