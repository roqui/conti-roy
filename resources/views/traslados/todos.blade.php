@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

  <!-- Begin Page Content -->
        <div class="container">

          <!-- Page Heading -->
          <h1 class="h3 mb-1 text-gray-800">Registro de Traslados</h1>
          <p class="mb-4">Resgistro de todos los pedidos realizados con origen en la susucursal: </p>

          <!-- Content Row -->
          <div class="row">

            <!-- DataTales Example -->
          <div class="card shadow mb-4" style="width: 100%">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Traslados</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable"  cellspacing="0">
                    <thead>
                        <tr>
                          <th scope="col" >ID-Folio</th>
                          <th scope="col">Sucursal Origen</th>
                          <th scope="col">Destino</th>
                          <th scope="col">Fecha de Registro</th>
                          <th scope="col"><i class="fa fa-usd"></i></th>
                          <th scope="col" ></th>
                          <th scope="col" ></th>
                          <th scope="col" >Etapa</th>
                        </tr>
                    </thead>
                          <tbody>
                                @foreach ($traslados as $t)
                                <?php switch ($t->estado) {
                                  case '1':
                                    $clase="table-warning";
                                    $desc="Pendiente";
                                    break;
                                  case '2':
                                    $clase="table-info";
                                    $desc="Surtiendo";
                                    break;
                                  case '3':
                                    $clase="table-primary";
                                    $desc="Remisionado";
                                    break;
                                  case '4':
                                    $clase="table-success";
                                    $desc="Enviado";
                                    break;
                                  
                                  default:
                                    $clase="table-danger";
                                    $desc="Cancelado";
                                    break;
                                } ?>
                                  <tr class=<?php echo $clase; ?>>
                                    <th scope="row">{{$t->id}}</th>
                                  <td>{{$t->norigen}}</td>
                                  <td>{{$t->ndestino}}</td>
                                  <td>{{$t->created_at}}</td>
                                  <td>{{$t->CostoT}}</td>
                                  <td>@include('traslados.edittpendientes')</td>
                                    <td>
                                      <form action="infotraslado">
                                        <input type="hidden" name="idpedido" value="{{$t->id}}">
                                        <button class="btn" type="submit"><i class="fas fa-info-circle"></i></button>
                                      </form>
                                    </td>
                                    <td>
                                      <?php echo $desc; ?>
                                    </td>
                                  </tr>
                              
                              @endforeach
                </table>
              </div>
            </div>
          </div>
            

            

            

          </div>

        </div>
        <!-- /.container-fluid -->
 

@endsection