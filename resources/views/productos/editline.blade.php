<!-- Button trigger modal -->
<button  class="btn" data-toggle="modal"

 data-target="#editline{{$line->id}}"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Editar línea"></i>
  
</button>

<!-- Modal -->
<div class="modal fade" id="editline{{$line->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar línea <b>{{$line->descr}}</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form  action="update_line" method="POST">
          {{csrf_field()}} 
          <input type="hidden" name="_method" value="POST">

          <label>Nueva descripción:</label> <br>
          <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1"> </span>
  </div>
  <input value="{{$line->id}}" hidden name="id">
  <input type="text" name="descr" class="form-control" placeholder="{{$line->descr}}" value="{{$line->descr}}"  aria-describedby="basic-addon1">
</div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</span>
                  </button>
      </div>
        </form>
    </div>
  </div>
</div>
