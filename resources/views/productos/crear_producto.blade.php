@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

@if(session('Mensaje'))

<div class="alert alert-success" id="success-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
{{session('Mensaje')}}</div>

@endif
@if(session('Mensajee'))

<div class="alert alert-danger" id="danger-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
{{session('Mensajee')}}</div>

@endif
@if(session('Mensajea'))

<div class="alert alert-primary" id="warning-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
{{session('Mensajea')}}</div>

@endif
@if(session('Mensajeu'))

<div class="alert alert-primary" id="warning-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
{{session('Mensajeu')}}</div>

@endif   


<div style="margin: 50px;">


  <div  class="card shadow mb-6">

    <div class="card-body">

     <h3>Agregar Producto</h3>

     <form  name="formu" action="new_producto" method="POST">
      {{csrf_field()}}  

      <div class="row">
        <div class="col-sm">
          <b><label>Código</label></b>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"></span>
            </div>

            <input required type="number" name="codigo" class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="codigo" placeholder="Código">

          </div>


          <b><label>Código SAT: </label></b> <br>
          <select name="codigosat" style="width: 660px; height: 30px;" id="sucursal" required class="select2">
                        <option >Elige un código SAT</option>

            @foreach($sat as $sats)
            <option name="id" value="{{ $sats->id}}">
            {{$sats->descripcion}}  || {{ $sats->codigo }}
            </option>
            @endforeach
          </select>

        </div>
        <div class="col-sm">
          <b> <label>Unidad</label></b>

          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"></span>
            </div>
            <input required type="text"  required name="unidad" class="form-control" aria-label="Username" aria-describedby="basic-addon1" placeholder="Unidad" id="unidad">
          </div>

          <b> <label>Descripción</label></b>

          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"></span>
            </div>
            <input required type="text"  required name="descripcion_producto" class="form-control" aria-label="Username" aria-describedby="basic-addon1" placeholder="Descripción del Producto" id="descripcion_producto">
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm">
          <div class="form-group">
            <b>                <label for="">Selecciona una marca</label><br></b>                
            <select required  name="id_marca" required style="width: 265px;" class="btn btn-secondary dropdown-toggle">
              <option value="0" disabled="true" selected="true"> Marcas </option>
              @foreach ($marcas as $marca)
              <option value="{{$marca->id}}">{{$marca->descripcion}}</option>
              @endforeach
            </select>
            <hr>
            <b>  <label for="">Selecciona un proveedor</label><br></b>                
            <select required  name="id_proveedor" required style="width: 265px;" class="btn btn-secondary dropdown-toggle">
              <option value="0" disabled="true" selected="true"> Proveedores </option>
              @foreach ($proveedores as $proveedor)
              <option value="{{$proveedor->id}}">{{$proveedor->nombre}}</option>
              @endforeach
            </select>

          </div>

        </div>
        <div class="col-sm">
          <label><b>Selecciona una linea</b></label>  <br>
          <select required  style="width: 250px" required class="productcategory btn btn-secondary dropdown-toggle" id="prod_cat_id">

            <option value="0" disabled="true" selected="true">-- Líneas --</option>
            @foreach($lineass as $lineasss)
            <option value="{{$lineasss->id}}">{{$lineasss->descr}}</option>
            @endforeach

          </select>
          <hr>
          <label><b>Selecciona una sublinea</b></label>  <br>
          <select required style="width: 250px" name="id_sublinea"  id="productname" class="btn btn-secondary dropdown-toggle" >

            <option value="0" disabled="true" selected="true">-- Sublíneas --</option>
          </select>
        </div>

      </div>
      <hr>

      <div class="row">
        <div class="col-sm">

          <b><label>Precio de Compra</label></b>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">$</span>
            </div>
            <input required type="number" name="costo" class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="costo" onkeyup="return iva(event);" placeholder="Precio de Compra" step="0.01">
          </div>

          <b><label>IVA</label></b>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">%</span>
            </div>
            <input required type="" name="iva" class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="iva" value="{{$productos->iva}}" readonly="">
          </div>

          <input hidden name="proveedor" value="1">
          <div class="checkbox checkbox-success">
            <input name="iva" id="iva" type="checkbox" value="iva" onclick="return Cambia(this);" >
            <label for="test" style="padding-left: 15px!important;">Exento de IVA</label>
          </div>
        </div>
        <div class="col-sm">
          <b>
            <label>IVA del Producto</label>

          </b>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">$</span>
            </div>
            <input required type="number" name="ive" class="form-control" aria-label="Username" aria-describedby="basic-addon1" readonly="" >
          </div>

          <b>
            <label>Total con/sin IVA</label>

          </b>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1">$</span>
            </div>
            <input required type="number" required name="total" class="form-control" aria-label="Username" aria-describedby="basic-addon1" readonly="" id="costott" >
          </div>
        </div>

      </div>
      <hr>
      <div class="row">
       <div class="col-sm">
         <b>
          <label>Precio de venta</label>

        </b>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">$</span>
          </div>
          <input required type="number" id="precio"required name="precio" class="form-control" aria-label="Username" aria-describedby="basic-addon1" placeholder="Precio de Venta" >
        </div>




      </div> 
      <div class="col-sm">

      </div>

    </div>
    <div class="row">
      <div class="col-sm">
        <div style="margin-top: 65px;">
          <a href="productos" class="btn btn-outline-danger"><i class="fa fa-arrow-left"></i>   Cancelar  </a>
        </div>
      </div>
      <div class="col-sm">

       <button type="submit" style=" margin-top: 60px; float: right;" class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Agregar producto</span>
      </button>
          </div>

        </div>



      </div>




    </form>
    <!--
     <div class="col-sm">
        <button  style=" margin: 10px; float: right;" class="lastinsert btn btn-primary btn-icon-split" id="carga" value="carga" name="carga">
          <span class="icon text-white-50">
            <i class="fas fa-history"></i>
          </span>
          <span class="text">Último Registro</span>
        </button>
      </div>
    -->

  </div>

</div>
</div>



</div>
@endsection

<!-- ----------------------------------------- SCRIPTS ------------------------------ -->

@section('script')

<script type="text/javascript">
  $(document).ready(function() {
        $('.select2').select2();
      });
</script>
<script type="text/javascript">
  $(document).ready(function(){

    $(document).on('change','.productcategory',function(){
      // console.log("hmm its change");

      var cat_id=$(this).val();
      // console.log(cat_id);
      var div=$(this).parent();

      var op=" ";

      $.ajax({
        type:'get',
        url:'{!!URL::to('findProductName')!!}',
        data:{'id':cat_id},
        success:function(data){
          //console.log('success');

          //console.log(data);

          //console.log(data.length);
          op+='<option value="0" selected disabled>Elige una sublínea</option>';
          for(var i=0;i<data.length;i++){
            op+='<option value="'+data[i].id+'">'+data[i].descrip+'</option>';
          }

          div.find('#productname').html(" ");
          div.find('#productname').append(op);
        },
        error:function(){

        }
      });
    });


  });

</script>
<script type="text/javascript">
  $(document).ready(function(){

    $("#carga").click('.lastinsert',function(){
      $.ajax({
       type:'get',
       url:'{!!URL::to('findLastName')!!}',
       dataType:'json', 
       success:function(productos){
        console.log('Holi, llegué al success');
        console.log(productos);


          //if($("#codigo").is(":empty")){
            $.each(productos, function(index, obj){
             $('#codigo').val("{{$productos->codigo}}");
             $('#codigosat').val("{{$productos->codigosat}}");
             $('#unidad').val("{{$productos->unidad}}");
             $('#descripcion_producto').val("{{$productos->descripcion_producto}}");
             $('#costo').val("{{$productos->costo}}");
             $('#costott').val("{{$productos->costo}}");
             $('#precio').val("{{$productos->precio}}");
           });
          //}
        },
        error:function(xhr, status, error){
          console.log(xhr);
          console.log(status);
          console.log(error);
        }
      });
    });


  });

</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#limpia').click(function() {
      $('input[type="text"]').val('');
      $('input[type="number"]').val('');
    });
  });
</script>



<script type="text/javascript">
  function vlaidarcodigo(codigo) {
  //if (fono.value.lenght < 8) {
    if (codigo.value.length > 13 || codigo.value.length < 13 ) {
      alert("Error: El código debe tener exactamente 13 dígitos.");
      fono.focus();
      fono.select();
    }
  }
</script>

<script type="text/javascript">
  function iva(e){
    var isChecked = document.getElementById('iva').checked;
    if(isChecked){

  //tasa de impuesto
  var tasa = 0;
  
  //monto a calcular el impuesto
  var monto = $("input[name=costo]").val();
  
  //calsulo del impuesto
  var iva = (monto * tasa)/100;
  
  //se carga el iva en el campo correspondien te
  $("input[name=ive]").val(iva);
  
  //se carga el total en el campo correspondiente
  $("input[name=total]").val(parseFloat(monto)+parseFloat(iva));
}else{
  //tasa de impuesto
  var tasa = 16;
  
  //monto a calcular el impuesto
  var monto = $("input[name=costo]").val();
  
  //calsulo del impuesto
  var iva = (monto * tasa)/100;
  
  //se carga el iva en el campo correspondien te
  $("input[name=ive]").val(iva);
  
  //se carga el total en el campo correspondiente
  $("input[name=total]").val(parseFloat(monto)+parseFloat(iva));

}

}

$(function(){
  // Registro controladores de eventos
  $('#costo').on('keyup', iva);
  
});
</script>
<script type="text/javascript">
 function Cambia(txek)
 {   
   if(txek.checked)
   {

  //tasa de impuesto
  var tasa = 0;
  
  //monto a calcular el impuesto
  var monto = $("input[name=costo]").val();
  
  //calsulo del impuesto
  var iva = (monto * tasa)/100;
  
  //se carga el iva en el campo correspondien te
  $("input[name=ive]").val(iva);
  
  //se carga el total en el campo correspondiente
  $("input[name=total]").val(parseFloat(monto)+parseFloat(iva));
}
else
{

  //tasa de impuesto
  var tasa = 16;
  
  //monto a calcular el impuesto
  var monto = $("input[name=costo]").val();
  
  //calsulo del impuesto
  var iva = (monto * tasa)/100;
  
  //se carga el iva en el campo correspondien te
  $("input[name=ive]").val(iva);
  
  //se carga el total en el campo correspondiente
  $("input[name=total]").val(parseFloat(monto)+parseFloat(iva));
}
}
</script>

@endsection

