@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div class="container">
	<h3>Proveedores del producto <b>{{$productos->descripcion_producto}}</b></h3>

	@if(session('Mensaje'))
      
      <div class="alert alert-success" id="success-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensaje')}}</div>

    @endif
    @if(session('Mensajee'))
      
      <div class="alert alert-danger" id="danger-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajee')}}</div>

    @endif
    @if(session('Mensajea'))
      
      <div class="alert alert-primary" id="warning-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajea')}}</div>

    @endif

	<div  style="position: relative; left: 81.5%; margin-bottom: 15px; ">
		@include('productos.agregarproveedor')
	</div>

	  <div class="card shadow mb-4"  >
           
            <div class="card-body" >
              <div class="table-responsive" >
                <table  class="table table-bordered" id="dataTable"   cellspacing="0">
                  <thead>
    <tr>
      <th scope="col">Id_Proveedor</th>
      <th scope="col">Proveedor</th>
      <th scope="col">Proveedor activo</th>
      <th scope="col">Eliminar proveedor</th>
    
    </tr>
  </thead>
                  
                  <tbody>
        @foreach ($proveeducts as $proveeducto)
      <tr>
      <th scope="row">{{$proveeducto->id}}</th>
      <th scope="row">{{$proveeducto->nombre}}</th>
      <th scope="row"><input type="radio" name="prove_activo" value="$proveeducto->id_proveedor"></th>
      <th style="width: 50px;">
      	@include('productos.deleteproveeducto')
      </th>
      
    </tr>
        @endforeach
  </tbody>
                </table>
                
              </div>
              
            </div>
          </div>




</div>
 

@endsection