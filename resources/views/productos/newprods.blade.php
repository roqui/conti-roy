@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div class="container">



  <h1>Agregar Producto</h1>


  <div class="container">
    <form  action="{{route('crearprod')}}" method="POST">

  
          {{csrf_field()}} 

  <div class="row">
    <div class="col-sm">
      <div class="form-group">
                <label for="">Código:</label>
                <input type="number" required name="codigo" class="form-control"  >   
      </div>
      <div class="form-group">
                <label for="">Código SAT:</label>
                <input type="number" required name="codigosat" class="form-control" >   
      </div>

      <div class="col-sm">
       
         <div class="form-group">
                <label for="">Selecciona una linea</label><br>
                {!! Form::open(['route'=>'postSelect','method'=>'POST']) !!}
             <select name="linea">
                    <option disabled selected>Elige la linea</option>
                    @foreach($data as $linea)
                    <option value="{{$linea->descripcion}}">{{$linea->descripcion}}</option>
                    @endforeach
                </select>

              </div>
      <div class="form-group">
                      <label for="">Descripción</label>
                      <input type="text" required name="descripcion" class="form-control"  >   
      </div>

      <div class="form-group">
                <label for="">Precio de Venta</label>
                <input type="text" required name="precioV" class="form-control" >   
              </div>

              <div class="form-group">
                <label for="">Precio de Compra</label>
                <input required type="text" name="obs" class="form-control"  >   
              </div>


  </div>
  <div class="col-sm">
       

              <div class="form-group">
                <label for="">Proveedor</label>
                <input required type="number" name="proveedor" class="form-control"  >   
              </div>

              <div class="form-group">
                <label for="">Selecciona una sucursal: </label><br>
                <select name="sucursal">
                    <option disabled selected>Elige una sucursal</option>
                    @foreach($data as $sucursal)
                    <option value="{{$sucursal->nombre}}">{{$sucursal->nombre}}</option>
                    @endforeach
                </select>
              </div>

              <div class="form-group">
                <label for="">Selecciona una estado:</label><br>
                
             <select name="estado">
                    <option disabled selected>Elige el estado del producto</option>
                    @foreach($data as $estado)
                    <option value="{{$estado->descripcion}}">{{$estado->descripcion}}</option>
                    @endforeach
                </select>
              </div>

    </div>
    
 
  </div>
</div>

<button  type="submit" style="width: 150px; position: absolute; left: 85.5%;" class="btn btn-primary" style="background-color: #00bcd4;">Agregar   <i class="fa fa-plus-circle"></i></button> 

   </form>
  
</div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('linea').material_select();
        });
        $(document).ready(function() {
            $('sucursal').material_select();
        });
        $(document).ready(function() {
            $('estado').material_select();
        });
    </script>
@endsection