
<!-- Button trigger modal -->
<button class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#subline{{$lineas->id}}">
  <span class="icon text-white-50">
                      <i class="fas fa-plus"></i>
                    </span>
                    <span class="text">Agregar sublínea</span>

  
</button>

<div class="modal fade" id="subline{{$lineas->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Agregar sublínea</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4>Agregar nueva sublínea a {{$lineas->descr}}</h4>
        <form action="crearsubline" method="POST">
           {{csrf_field()}} 
          <label>Descripción:</label> <br>
          <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1"> </span>
  </div>
  <input hidden="hidden" value="{{$lineas->id}}" name="id_linea">
  <input type="text" name="descrip" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
</div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Agregar sublínea</span>
                  </button>
      </div>
        </form>
    </div>
  </div>
</div>

