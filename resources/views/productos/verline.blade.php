@extends('layouts.plantilla')
@extends('layouts.menu')
@section('main')
<div style="margin:50px;">



 <div class="card shadow mb-4">
   
  <div class="card-body">

    <h3>Sublíneas de la línea <b>{{$lineas->descr}}</b></h3>
    @foreach($permisos as $item)
    @if($item->idInterfaz==8)
    <div style="margin-bottom: 10px; float: right;">
     @include('productos.newsubline2')
   </div>
   @break
   @endif
   @endforeach

    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead >
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Descripción</th>
            @foreach($permisos as $item)
            @if($item->idInterfaz==9)
            <th style="width: 50px;" scope="col"><center><i class="fas fa-edit"></center></i></th>
            <th style="width: 50px;" scope="col"><center><i class="fas fa-trash-alt"></i></center></th>
            @break
            @endif
            @endforeach
          </tr>
        </thead>
        
        @foreach ($sublineas as $subline)
        <tr>
          <th>{{$subline->id}}</th>

          <th>{{$subline->descrip}}</th>

          
          @foreach($permisos as $item)
          @if($item->idInterfaz==9)
          <th>
            <center>
            @include('productos.editsubline')
            </center>
          </th>
          
          <th>
            <center>
            @include('productos.deletesubline')
            </center>

          </th>
          @break
          @endif
          @endforeach
          
        </tr>

        @endforeach

        
      </tbody>
    </table>

    <div style="margin-top:10px; float: left;">
          <a href="lineas" class="btn btn-outline-secondary"><i class="fa fa-arrow-left"></i>   Regresar  </a>
        </div>
    
  </div>
  
</div>
</div>


</div>





@endsection