
<!-- Button trigger modal -->
<button class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#proveedor{{$productos->id}}">
  <span class="icon text-white-50">
                      <i class="fas fa-plus"></i>
                    </span>
                    <span class="text">Agregar proveedor</span>

  
</button>

<div class="modal fade" id="proveedor{{$productos->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Agregar proveedor</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4>Agregar proveedor para {{$productos->descripcion_producto}} </h4>
        <form action="addprov" method="POST">
           {{csrf_field()}} 
          <div class="input-group mb-3">
 
  <input hidden="hidden" value="{{$productos->id}}" name="id_productos">
                      <label for="">Selecciona un proveedor</label><br>    

  <center>

      <div class="form-group">
             <select  name="id_proveedor" style="width: 300px;" class=" btn btn-secondary dropdown-toggle">
              
            @foreach ($proveedores as $proveedor)
            <option value="{{$proveedor->id}}">{{$proveedor->nombre}}</option>
             @endforeach
             </select>

              </div>
  </center>
</div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Agregar proveedor</span>
                  </button>
      </div>
        </form>
    </div>
  </div>
</div>

