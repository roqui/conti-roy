@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="margin:50px;">
	<div style="margin-bottom: 10px;">

   @if(session('Mensaje'))

   <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensaje')}}</div>

  @endif
  @if(session('Mensajee'))

  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajee')}}</div>

  @endif
  @if(session('Mensajea'))

  <div class="alert alert-primary" id="warning-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajea')}}</div>

  @endif

</div>


<div class="card shadow mb-4">

  <div class="card-body">
    <h3 >Marcas</h3>
    <h4>Listado de marcas disponibles.</h4>
    @foreach($permisos as $item)
    @if($item->idInterfaz==11)
    <div style="margin-top: 10px; margin-bottom: 10px; float: right;">
      @include('productos.newmarca')
    </div>
    <!-- Divider -->
    @break
    @endif
    @endforeach

    <div class="table-responsive">
      <table class="table table-bordered centered table-hover " id="dataTable"  cellspacing="0">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Descripción</th>
            @foreach($permisos as $item)
            @if($item->idInterfaz==12)
            <th style="width: 50px;" scope="col"><center><i class="fas fa-edit"></i></center></th>
            <th style="width: 50px;" scope="col"><center><i class="fas fa-trash-alt"></i></center></th>
            @break
            @endif
            @endforeach
          </tr>
        </thead>


        @foreach($marcas as $marca)         
        <tr>
          <th>{{$marca->id}}</th>
          <th>{{$marca->descripcion}}</th>

          @foreach($permisos as $item)
          @if($item->idInterfaz==12)
          <th>

            @include('productos.editmarca')

          </th>
          <th>

            @include('productos.deletemarca')

          </th>
          @break
          @endif
          @endforeach
        </tr>
        @endforeach

      </tbody>
    </table>

     <div style="margin-top:10px; float: left;">
          <a href="marca" class="btn btn-outline-secondary"><i class="fa fa-arrow-left"></i>   Regresar  </a>
        </div>

  </div>

</div>
</div>


</div>




@endsection