@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="margin: 50px;">
	<div style="margin-bottom: 10px;">

    @if(session('Mensaje'))
    
    <div class="alert alert-success" id="success-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensaje')}}</div>

    @endif
    @if(session('Mensajee'))
    
    <div class="alert alert-danger" id="danger-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajee')}}</div>

    @endif
    @if(session('Mensajea'))
    
    <div class="alert alert-primary" id="warning-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajea')}}</div>

    @endif
    @if(session('Mensajeu'))
    
    <div class="alert alert-primary" id="warning-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajeu')}}</div>

    @endif   



    <div class="card shadow mb-4">

      <div class="card-body">

        <h3>Líneas</h3>
        <p class="mb-4">Listado de líneas disponibles para cada producto.</p>


        <div>

          @foreach($permisos as $item)
          @if($item->idInterfaz==6)
          <div style="float: right; margin-bottom: 10px;">
           @include('productos.newline')

         </div>
         @break
         @endif
         @endforeach

       </div>


       <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">


          <thead>
            <tr>
              <th style="width: 80px;"  scope="col">ID</th>
              <th scope="col">Descripción</th>
              @foreach($permisos as $item)
              @if($item->idInterfaz==7)
              <th style="width: 50px;" scope="col"></th>
              @break
              @endif
              @endforeach

              @foreach($permisos as $item)
              @if($item->idInterfaz==8)
              <th style="width: 50px;" scope="col"></th>
              @break
              @endif
              @endforeach
              @foreach($permisos as $item)
              @if($item->idInterfaz==9)
              <th style="width: 50px;" scope="col"></th>
              <th style="width: 50px;" scope="col"></th>
              @break
              @endif
              @endforeach
            </tr>
          </thead>
          <tbody>


            @foreach ($lineas as $line)

            <tr>
              <td>{{$line->id}}</td>

              <td>{{$line->descr}}</td>
              @foreach($permisos as $item)
              @if($item->idInterfaz==7)
              <td>
                <center>
                  <form action="verline" method="GET">

                    <input type="text" hidden="hidden" name="id" value="{{$line->id}}">
                    {{csrf_field()}} 
                    <button type="submit"
                    data-toggle="tooltip" data-placement="top" title="Ver sublíneas"
                    class="btn"><i class="fa fa-list"></i>  </button> 
                  </form>
                </center>
              </td>
              @break
              @endif
              @endforeach

              @foreach($permisos as $item)
              @if($item->idInterfaz==8)
              <td>
                @include('productos.newsubline')
              </td>
              @break
              @endif
              @endforeach

              @foreach($permisos as $item)
              @if($item->idInterfaz==9)
              <td>
                @include('productos.editline')
              </td>
              <td>
                @include('productos.deleteline')
              </td>
              @break
              @endif
              @endforeach
            </tr>
            @endforeach

          </tbody>
        </table>

      </div>

    </div>
  </div>


</div>




@endsection