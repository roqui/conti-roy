<!-- Button trigger modal -->
<button  class="btn" data-toggle="modal" data-target="#editmarca{{$marca->id}}"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Editar marca"></i>
  
</button>

<!-- Modal -->
<div class="modal fade" id="editmarca{{$marca->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar <b>{{$marca->descripcion}}</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="update_marca" method="POST">
          {{csrf_field()}}
          <input  name="id" hidden value="{{$marca->id}}">
          <label>Descripción:</label> <br>
          <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1"> </span>
  </div>
  <input type="text" class="form-control" name="descripcion" aria-label="Username" value="{{$marca->descripcion}}" placeholder="{{$marca->descripcion}}" aria-describedby="basic-addon1">
</div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</span>
                  </button>
      </div>
        </form>
    </div>
  </div>
</div>
