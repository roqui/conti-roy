@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="margin:50px;" >
  


  @if(session('Mensaje'))
  
  <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensaje')}}</div>

  @endif
  @if(session('Mensajee'))
  
  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajee')}}</div>

  @endif
  @if(session('Mensajea'))
  
  <div class="alert alert-primary" id="warning-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajea')}}</div>

  @endif

  <div hidden >
       

               <a href="crear_producto"  style="margin-top:10px; margin-bottom: 10px;" class="btn btn-success btn-icon-split">
                      <span class="icon text-white-50">
                        <i class="fas fa-download"></i>
                      </span>
                      <span class="text">Obtener Excel</span>
  </a>
  </div>

  

  <div class="card shadow mb-4" style="width: 100%; margin: 0;">
   
    <div class="card-body" >


    <h3>Productos</h3>
  <h5 style="float: left;">Listado de Productos de Papelerías CONTI</h5>

  @foreach($permisos as $add_producto)
  @if($add_producto->idInterfaz==2)
  <a href="crear_producto" style="margin-bottom: 20px; float: right;"  class="btn btn-primary btn-icon-split">
    <span class="icon text-white-50">
      <i class="fas fa-plus"></i>
    </span>
    <span class="text">Agregar producto</span>
  </a>
  @break
  @endif
  @endforeach

 


      <div class="table-responsive" >
        <table  class="table table-bordered" id="dataTable"   cellspacing="0">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Código</th>
              <th>Código Sat</th>
              <th scope="col">Descripción</th>
              <th>Marca</th>
              <th>Sublínea</th>
              <th scope="col">Costo + IVA</th>
              <th scope="col">Precio de Venta</th>
              <th></th>
              @foreach($permisos as $item)
              @if($item->idInterfaz==3) 
              <th></th>
              @break
              @endif
              @endforeach
              @foreach($permisos as $item)
              @if($item->idInterfaz==1) 
              <th></th>
              <th></th>
              @break
              @endif
              @endforeach
            </tr>
          </thead>
          
          <tbody>
            @foreach ($productos as $producto)
            <tr>
              <td scope="row">{{$producto->id}}</td>
              <td scope="row">{{$producto->codigo}}</td>
              <td scope="row" >{{$producto->sat}}</td>
              <td scope="row">{{$producto->descripcion_producto}}</td>
              <td scope="row">{{$producto->descripcion}}</td>
              <td scope="row">{{$producto->descrip}}</td>
                            <?php  
                $preciofinal = $producto->costo  + ($producto->costo*.16);
              ?>
                
              <td scope="row">${{$preciofinal}}</td>
              <td scope="row">${{$producto->precio}}</td>

              @foreach($proveedores as $proveedor)
              @if($proveedor->id == $producto->id_proveedor) 
              <td>@include('productos.infoproducto')</td>
              @break
              @endif
              @endforeach
              
              @foreach($permisos as $item)
              @if($item->idInterfaz==3)
             <td>
               @include('productos.newinventario_producto')
             </td>
              @break
              @endif
              @endforeach

              @foreach($permisos as $item)
              @if($item->idInterfaz==1) 
             <td>
              <form action="update_product" method="GET">
               {{csrf_field()}} 

               <input hidden value="{{$producto->id}}" name="id">

               <button type="submit" data-toggle="tooltip" data-placement="top" title="Editar producto"
                class="btn"><i class="fa fa-edit"></i></button>
             </form>
             
              </td>
              <td>
                @include('productos.deleteprodcuto')
              </td>
              @break
              @endif
              @endforeach
          
        </tr>
        @endforeach
      </tbody>
    </table>
    
  </div>
  
</div>
</div>





</div>



@endsection