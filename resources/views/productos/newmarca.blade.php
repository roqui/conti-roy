<button href="#" class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#subline">
                    <span class="icon text-white-50">
                      <i class="fas fa-plus"></i>
                    </span>
                    <span class="text">Agregar marca</span>
                  </button>

<!-- Modal -->
<div class="modal fade" id="subline" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar marca</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="crearmarca" method="POST">
                        {{csrf_field()}} 
          <b><label>Descripción:</label></b> <br>
          <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1"> </span>
  </div>
  <input type="text" class="form-control" name="descripcion" placeholder="Nueva marca" aria-label="Username" aria-describedby="basic-addon1">
</div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Agregar marca</span>
                  </button>
      </div>
        </form>
    </div>
  </div>
</div>


