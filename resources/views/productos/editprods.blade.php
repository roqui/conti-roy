<!-- Button trigger modal -->
<button type="button"  data-toggle="modal" data-target="#info{{$producto->id}}" type="submit" data-toggle="tooltip" data-placement="top" title="Información del producto"
                class="btn">><i class="fa fa-info-circle"></i></button>

<!-- Modal -->
<div class="modal fade" id="info{{$producto->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Información del producto</h5> 
                  <a href="productos" class="btn btn-outline-secondary"><i class="fa fa-arrow-left"></i>   Regresar  </a>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
        
       <div class="container">

<center>
	
    	<div>
  		<img class="img-profile rounded-circle" width="200px" src="img/proveedor.jpg">
  	</div>
</center>
<br>
			    <b>Código: </b> {{$producto->codigo}} <br>

          <b>Descripción:</b> {{$producto->descripcion}} <br>
          

          <b>Precio Venta:</b> {{$producto->precioV}} <br>
      
          <b>Precio Compra: </b> {{$producto->precioC}} <br>
          <b>Proveedor: </b> {{$producto->proveedor}} <br>
          <b>Agregado en: </b> {{$producto->created_at}}<br> 
      
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

