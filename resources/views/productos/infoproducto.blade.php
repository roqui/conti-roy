<!-- Button trigger modal -->
<center><button type="button"  data-toggle="modal" data-target="#info{{$proveedor->id}}" class="btn"><i class="fa fa-info-circle"></i></button></center>

<!-- Modal -->
<div class="modal fade" id="info{{$proveedor->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Información del producto</h5> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">

       <div class="container">

        <h5>Producto</h5>

        <div class="row">
          <div class="col-sm">
           <b><label>Código:</label></b> {{$producto->codigo}} <br>
            <b><label>Descripción:</label></b> {{$producto->descripcion_producto}} <br>
             <b><label>Línea:</label></b> {{$producto->descr}} <br>
              <b><label>Costo:</label></b> {{$producto->costo}} <br>
          </div>
          <div class="col-sm">
           <b><label>Código SAT:</label></b> {{$producto->sat}} <br>
            <b><label>Marca:</label></b> {{$producto->descripcion}} <br>
             <b><label>Sublínea:</label></b> {{$producto->descrip}} <br>
              <b><label>Precio:</label></b> {{$producto->precio}} <br>

          </div>
        </div>

        <h5>Proveedor</h5>
      
        <b><label>Nombre: </label></b> {{$proveedor->nombre}} <br>
         <b><label>Descuento:</label></b> {{$proveedor->descuento}}% <br>
 

        <small>
          <a href="proveedores.proveedores">
            Haz click aquí para ver los todos los proveedores
          </a>
        </small>

      </div>

    </div>
    <div class="modal-footer">

      <button type="button" style="position:relative; margin-top: 60px;" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
</div>
</div>



<!-- Button trigger modalmmmmmksdfjñlkdjfñlkajsdflñkdnfñlsdnflñkdnflkñdnf -->
