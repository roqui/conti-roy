@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

  @if(session('Mensaje'))
    
    <div class="alert alert-success" id="success-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensaje')}}</div>

    @endif
    @if(session('Mensajee'))
    
    <div class="alert alert-danger" id="danger-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajee')}}</div>

    @endif
    @if(session('Mensajea'))
    
    <div class="alert alert-primary" id="warning-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajea')}}</div>

    @endif
    @if(session('Mensajeu'))
    
    <div class="alert alert-primary" id="warning-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajeu')}}</div>

    @endif   


<div style="margin: 50px;">


  <div  class="card shadow mb-6">

    <div class="card-body">

     <h3>Editar Producto</h3>

       <form  name="formu" action="update_product2" method="POST">
        {{csrf_field()}}  

        <input hidden value="{{$productos->id}}" name="id">

          <div class="row">
            <div class="col-sm">
              <b><label>Código</label></b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"></span>
                </div>

                <input required type="number" class="form-control" aria-label="Username" aria-describedby="basic-addon1" name="codigo" placeholder="{{$productos->codigo}}" value="{{$productos->codigo}}">

              </div>

              <b><label>Código SAT: </label></b> <br>
          <select name="codigosat" style=" height: 30px;" id="sucursal" required class="select2">
            <option >Elige un código SAT</option>
            @foreach($sat as $sats)
            <option name="id" value="{{ $sats->id}}">
            {{$sats->descripcion}}  || {{ $sats->codigo }}
            </option>
            @endforeach
          </select>
                        <p style="font-size: 80%">Código actual: <b>{!! $productos-> codigosat_des !!}. </b> Si desea mantener el mismo código seleccione de nuevo.</p>

            </div>
            <div class="col-sm">
              <b> <label>Unidad</label></b>

              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"></span>
                </div>
                <input required type="text"  required name="unidad" class="form-control" aria-label="Username" aria-describedby="basic-addon1" placeholder="{{$productos->unidad}}" value="{{$productos->unidad}}" >
              </div>

              <b> <label>Descripción</label></b>

              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"></span>
                </div>
                <input required type="text"  required name="descripcion_producto" class="form-control" aria-label="Username" aria-describedby="basic-addon1" placeholder="{{$productos->descripcion_producto}}" value="{{$productos->descripcion_producto}}">
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm">
              <div class="form-group">
                <b>                <label for="">Selecciona una marca</label><br></b>                
                <select required  name="id_marca" required style="width: 265px;" class="btn btn-secondary dropdown-toggle">
                  <option value="0" disabled="true" selected="true"> Marcas </option>
                  @foreach ($marcas as $marca)
                  <option value="{{$marca->id}}">{{$marca->descripcion}}</option>
                  @endforeach
                </select>
                <p style="font-size: 80%">Marca actual: <b>{!! $productos-> descripcion !!}. </b> Si desea mantener la misma marca seleccione de nuevo.</p>
              </div>
                <hr>
                <b>  <label for="">Selecciona un proveedor</label><br></b>                
                <select required  name="id_proveedor" required style="width: 265px;" class="btn btn-secondary dropdown-toggle">
                  <option value="0" disabled="true" selected="true"> Proveedores </option>
                  @foreach ($proveedores as $proveedor)
                  <option value="{{$proveedor->id}}">{{$proveedor->nombre}}</option>
                  @endforeach
                </select>
                <p style="font-size: 80%">Proveedor actual: <b>{!! $productos-> nombre !!}. </b> Si desea mantener la misma marca seleccione de nuevo.</p>

              </div>
              
          
            <div class="col-sm">
              <label><b>Selecciona una linea</b></label>  <br>
              <select required  style="width: 250px" required class="productcategory btn btn-secondary dropdown-toggle" id="prod_cat_id">

                <option value="0" disabled="true" selected="true">-- Líneas --</option>
                @foreach($lineass as $lineasss)
                <option value="{{$lineasss->id}}">{{$lineasss->descr}}</option>
                @endforeach

              </select>
              <p style="font-size: 80%">Linea actual: <b>{!! $lineas !!}. </b> Si desea mantener la misma linea seleccione de nuevo.</p>
              <hr>
              <label><b>Selecciona una sublinea</b></label>  <br>
              <select required style="width: 250px" name="id_sublinea"  id="productname" class="btn btn-secondary dropdown-toggle" >

                <option value="0" disabled="true" selected="true">-- Sublíneas --</option>
              </select>
              <p style="font-size: 80%">Sublinea actual: <b>{!! $productos-> descrip !!}. </b> Si desea mantener la misma sublinea seleccione de nuevo.</p>
            </div>
            
          </div>
          <hr>

          <div class="row">
            <div class="col-sm">

              <b><label>Precio de Compra</label></b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input required type="number" name="costo" class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="costo" onkeyup="return iva(event);" placeholder="{{$productos->costo}}" value="{{$productos->costo}}">
              </div>

              <b><label>IVA</label></b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">%</span>
                </div>
                <input required type="number" name="iva" class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="iva" value="{{$productos->iva}}" readonly="">
              </div>

              <input hidden name="proveedor" value="1">
              <div class="checkbox checkbox-success">
                <input name="iva" id="iva" type="checkbox" value="iva" onclick="return Cambia(this);" >
                <label for="test" style="padding-left: 15px!important;">Exento de IVA</label>
              </div>
            </div>
            <div class="col-sm">
              <b>
                <label>IVA del Producto</label>

              </b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">%</span>
                </div>
                <input type="number" value="16" name="ive" class="form-control" aria-label="Username" aria-describedby="basic-addon1" readonly="">
              </div>


              <b>
                <label>Total con/sin IVA</label>

              </b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input required type="number" required name="total" class="form-control" aria-label="Username" aria-describedby="basic-addon1" placeholder="{{$productos->costo}}" value="{{$productos->costo}}" readonly="">
              </div>
            </div>
            
          </div>
          <hr>
          <div class="row">
           <div class="col-sm">
             <b>
                <label>Precio de venta</label>

              </b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input required type="number" id="precio"required name="precio" class="form-control" aria-label="Username" aria-describedby="basic-addon1" placeholder="{{$productos->precio}}" value="{{$productos->precio}}">
              </div>
           </div> 
           <div class="col-sm">
             
           </div>
            
          </div>
          <div class="row">
            <div class="col-sm">
              <div style="margin-top: 65px;">
              <a href="productos" class="btn btn-outline-danger"><i class="fa fa-arrow-left"></i>   Cancelar  </a>
              </div>
            </div>
            <div class="col-sm">

           <button type="submit" style=" margin-top: 60px;  float: right;" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50">
                  <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar</span>
              </button>
            </div>

            </div>



          </div>




        </form>


      </div>

    </div>
  </div>



</div>
@endsection

<!-- ----------------------------------------- SCRIPTS ------------------------------ -->

@section('script')

<script type="text/javascript">
  $(document).ready(function() {
        $('.select2').select2();
      });
</script>

<script type="text/javascript">
  $(document).ready(function(){

    $(document).on('change','.productcategory',function(){
      // console.log("hmm its change");

      var cat_id=$(this).val();
      // console.log(cat_id);
      var div=$(this).parent();

      var op=" ";

      $.ajax({
        type:'get',
        url:'{!!URL::to('findProductName')!!}',
        data:{'id':cat_id},
        success:function(data){
          //console.log('success');

          //console.log(data);

          //console.log(data.length);
          op+='<option value="0" selected disabled>Elige una sublínea</option>';
          for(var i=0;i<data.length;i++){
            op+='<option value="'+data[i].id+'">'+data[i].descrip+'</option>';
          }

          div.find('#productname').html(" ");
          div.find('#productname').append(op);
        },
        error:function(){

        }
      });
    });


  });

</script>
<script type="text/javascript">
  function vlaidarcodigo(codigo) {
  //if (fono.value.lenght < 8) {
    if (codigo.value.length > 13 || codigo.value.length < 13 ) {
      alert("Error: El código debe tener exactamente 13 dígitos.");
      fono.focus();
      fono.select();
    }
  }
</script>

<script type="text/javascript">
  function iva(e){
  var isChecked = document.getElementById('iva').checked;
  if(isChecked){
  
  //tasa de impuesto
  var tasa = 0;
  
  //monto a calcular el impuesto
  var monto = $("input[name=costo]").val();
  
  //calsulo del impuesto
  var iva = (monto * tasa)/100;
  
  //se carga el iva en el campo correspondien te
  $("input[name=ive]").val(iva);
  
  //se carga el total en el campo correspondiente
  $("input[name=total]").val(parseFloat(monto)+parseFloat(iva));
}else{
  //tasa de impuesto
  var tasa = 16;
  
  //monto a calcular el impuesto
  var monto = $("input[name=costo]").val();
  
  //calsulo del impuesto
  var iva = (monto * tasa)/100;
  
  //se carga el iva en el campo correspondien te
  $("input[name=ive]").val(iva);
  
  //se carga el total en el campo correspondiente
  $("input[name=total]").val(parseFloat(monto)+parseFloat(iva));

}
    
}

$(function(){
  // Registro controladores de eventos
  $('#costo').on('keyup', iva);
  
});
</script>
<script type="text/javascript">
 function Cambia(txek)
  {   
   if(txek.checked)
    {
     
  //tasa de impuesto
  var tasa = 0;
  
  //monto a calcular el impuesto
  var monto = $("input[name=costo]").val();
  
  //calsulo del impuesto
  var iva = (monto * tasa)/100;
  
  //se carga el iva en el campo correspondien te
  $("input[name=ive]").val(iva);
  
  //se carga el total en el campo correspondiente
  $("input[name=total]").val(parseFloat(monto)+parseFloat(iva));
    }
   else
    {
     
  //tasa de impuesto
  var tasa = 16;
  
  //monto a calcular el impuesto
  var monto = $("input[name=costo]").val();
  
  //calsulo del impuesto
  var iva = (monto * tasa)/100;
  
  //se carga el iva en el campo correspondien te
  $("input[name=ive]").val(iva);
  
  //se carga el total en el campo correspondiente
  $("input[name=total]").val(parseFloat(monto)+parseFloat(iva));
    }
  }
</script>

@endsection


















