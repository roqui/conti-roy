  @extends('layouts.plantilla')
  @extends('layouts.menu')

  @section('main')
  @inject('sucursales', 'App\Http\Controllers\UsersController')
  @inject('puestos', 'App\Http\Controllers\UsersController')
  @if(session('Mensajeu'))

  <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajeu')}}</div>

  @endif
  @if(session('Mensajeue'))

  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajeue')}}</div>

  @endif
  @if(session('Mensajeua'))

  <div class="alert alert-warning" id="warning-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajeua')}}</div>

  @endif


  <div style="margin: 50px;">

    <div class="card shadow mb-4">

      <div class="card-body">

        <h3>Editar Usuario</h3> <br>

        <form  action="/update2/{{ $user -> id}}" method="POST">


          {{csrf_field()}} 

          <div class="row">
            <div class="col-sm">

              <b><label>Nombre</label></b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"></span>
                </div>

                <input required type="text" name="name" class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="name"
                value="{{$user->name}}" placeholder="{{$user->name}}">

              </div>

              <b><label>Usuario</label></b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"></span>
                </div>

                <input required type="text" name="usuario" value="{{$user->usuario}}" placeholder="{{$user->usuario}}" class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="usuario">

              </div>
              <b><label>Email</label></b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"></span>
                </div>

                <input required type="text" name="email" value="{{$user->email}}" placeholder="{{$user->email}}" class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="email">

              </div>
              <b><label>Dirección</label></b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"></span>
                </div>

                <input required type="text" name="calle" value="{{$user->calle}}" placeholder="{{$user->calle}}"  class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="calle">

              </div>

              <input name="foto" value="img/user.png" hidden>

              <div>
                <a href="users" class="btn btn-outline-danger"><i class="fa fa-arrow-left"></i>   Cancelar  </a>

              </div>


            </div>
            <div class="col-sm">
             <div class="form-group">
              <b><label for="puesto">Puesto</label></b>


              <select id="puesto" required name="puesto" class="productcategory btn btn-secondary dropdown-toggle form-control{{ $errors->has('puesto') ? ' is-invalid' : '' }}" >
               @foreach($puestos->getp() as $index => $puesto)
               <option value="{{ $index }}" {{ old('puesto') == $index ? 'selected' : '' }}>
                {{ $puesto }}
              </option>
              @endforeach
            </select>

            <p>El puesto actual es: <b>{{$user->descripcion}}</b></p>

            @if ($errors->has('puesto'))
            <span class="invalid-feedback" role="alert">
             <strong>{{ $errors->first('puesto') }}</strong>
           </span>
           @endif

         </div>
         <div class="form-group">
          <b><label for="sucursal">Sucursal</label></b> 

          <div>
            <select style="width: 650px;" id="sucursal" required name="idSucursal" class="productcategory btn btn-secondary dropdown-toggle">
             @foreach($destinos as $destino)
             <option value="{{$destino->id}}" >
              {{ $destino->nombre }}
            </option>
            @endforeach
          </select>
        </div>

        <p>La sucursal actual es: <b>{{$user->sucursal}}</b> </p>

        @if ($errors->has('idSucursal'))
        <span class="invalid-feedback" role="alert">
         <strong>{{ $errors->first('idSucursal') }}</strong>
       </span>
       @endif

     </div>

     <b><label>Contraseña</label></b>
     <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"></span>
      </div>

      <input required type="password" name="password" placeholder="Contraseña" class="form-control" aria-label="Username" aria-describedby="basic-addon1">

    </div>

    <b><label>Número de teléfono</label></b>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1">+54</span>
      </div>

      <input required type="number" name="telefono" value="{{$user->telefono}}" placeholder="{{$user->telefono}}" class="form-control" aria-label="Username" aria-describedby="basic-addon1" id="telefono">



    </div>


    

    <div style="float: right; ">
      <button type="submit"  class="btn btn-primary btn-icon-split">
        <span class="icon text-white-50">
          <i class="fas fa-save"></i>
        </span>
        <span class="text">Guardar</span>
      </button>
    </div>


  </div>

</div>







</div>


</div>
</div>


</form>
</div> 
</div>

@endsection

