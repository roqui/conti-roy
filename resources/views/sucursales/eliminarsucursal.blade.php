<!-- Button trigger modal -->
<button  class="btn" data-toggle="modal" data-target="#deletesucursal{{$sucursal->id}}"><i class="fas fa-trash-alt" data-toggle="tooltip" data-placement="top" title="Eliminar sucursal"></i>
  
</button>

<!-- Modal -->
<div class="modal fade" id="deletesucursal{{$sucursal->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar sucursal</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>¿Estás seguro de eliminar la sucursal <b>{{$sucursal->nombre}}</b>?</h5>
         <form action="eliminarsucursal" method="POST">
                        {{csrf_field()}} 

                        <input value="{{$sucursal->id}}" name="id" hidden>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-danger btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-times"></i>
                    </span>
                    <span class="text">Eliminar</span>
                  </button>
      </div>
        </form>
    </div>
  </div>
</div>