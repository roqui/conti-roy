@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')



	<div style="margin: 50px;">
  

		@if(session('Mensaje'))
  
  <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensaje')}}</div>

  @endif
  @if(session('Mensajee'))
  
  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajee')}}</div>

  @endif
  @if(session('Mensajea'))
  
  <div class="alert alert-primary" id="warning-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajea')}}</div>

  @endif


<div class="card shadow mb-4">           
  <div class="card-body">
    <h3>Sucursales</h3>
    <h4>Listado de sucursales de Papelerías Conti</h4>


	<div style="margin-bottom: 20px; float: right; ">
		<a href="newsucursal"  class="btn btn-primary btn-icon-split">
      <span class="icon text-white-50">
      <i class="fas fa-plus"></i>
      </span>
      <span class="text">Agregar sucursal</span>
    </a>
   </div>

  

    <div class="table-responsive">
      <table class="table table-bordered centered table-hover " id="dataTable"  cellspacing="0">
          <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Dirección</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Ciudad</th>
            <th></th>
            <th></th>
            <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach($sucursales as $sucursal)
            <tr>       
              <td>{{$sucursal->id}}</td>
              <td>{{$sucursal->nombre}}</td>
              <td>{{$sucursal->calle}}</td>
              <td>{{$sucursal->telefono}}</td>
              <td>{{$sucursal->ciudad}}</td>
              <td>
              @include('sucursales.infosucursal')
              </td>
          
              <td>
                <form action="editarsucursal">
                    <input type="hidden" name="id" value="{{$sucursal->id}}">
                    <button class="btn" type="submit"><i class="fa fa-edit"></i></button>
                 </form> 
              </td>
              <td>
              @include('sucursales.eliminarsucursal')
              </td>
              
            </tr>  
            @endforeach
          </tbody>
      </table>
    </div>
  </div>
    </div>



  <div>
       <a href="{{route('sucursalexcel')}}" style="float: right; " class="btn btn-success btn-icon-split">
                      <span class="icon text-white-50">
                        <i class="fas fa-download"></i>
                      </span>
                      <span class="text">Obtener Excel</span>
                      
                    </a> <br>
  </div>

</div>


 

@endsection