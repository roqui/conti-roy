@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="margin: 50px;">
  <div class="card shadow mb-6">

    <div class="card-body">


     <h3>Actualizar Sucursal

      <form style="font-size: 15px;" action="editar_sucursal" method="POST">
        {{csrf_field()}} 


          <input value="{{$sucursal->id}}" name="id" hidden>

            <b><label>Nombre:</label></b> <br>
            <div style="width: 650px;" class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> </span>
              </div>

              <input type="text" name="nombre" class="form-control" placeholder="{{$sucursal->nombre}}" value="{{$sucursal->nombre}}"  aria-nombreibedby="basic-addon1">
            </div>


          <div class="row">
            <div class="col-sm">



              <div>
               <b> <label>Calle:</label></b> <br>
               <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"> </span>
                </div>

                <input type="text" name="calle" class="form-control" placeholder="{{$sucursal->calle}}" value="{{$sucursal->calle}}"  aria-nombreibedby="basic-addon1">
              </div>

            </div>

            <div>
              <b><label>Colonia:</label></b> <br>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"> </span>
                </div>

                <input type="text" name="colonia" class="form-control" placeholder="{{$sucursal->colonia}}" value="{{$sucursal->colonia}}"  aria-nombreibedby="basic-addon1">
              </div>

            </div>

            <div>
              <b><label>Ciudad:</label></b> <br>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"> </span>
                </div>

                <input type="text" name="ciudad" class="form-control" placeholder="{{$sucursal->ciudad}}" value="{{$sucursal->ciudad}}"  aria-nombreibedby="basic-addon1">
              </div>

            </div>

            <div style="margin-top: 50px;">
              <a href="sucursales" class="btn btn-outline-secondary"><i class="fa fa-arrow-left"></i>   Regresar  </a>

            </div>

          </div>
          <div class="col-sm">

            <div>
              <b><label>Num. exterior:</label> </b><br>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"></span>
                </div>

                <input type="text" name="num_exterior" class="form-control" placeholder="{{$sucursal->num_exterior}}" value="{{$sucursal->num_exterior}}"  aria-nombreibedby="basic-addon1">
              </div>

            </div>

            <div>
             <b> <label>Código Postal:</label></b> <br>
             <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> </span>
              </div>

              <input type="number" name="codigo_postal" class="form-control" placeholder="{{$sucursal->codigo_postal}}" value="{{$sucursal->codigo_postal}}"  aria-nombreibedby="basic-addon1">
            </div>

          </div>

          <div>
            <b><label>Teléfono:</label></b> <br>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> +52</span>
              </div>

              <input type="number" name="telefono" class="form-control" placeholder="{{$sucursal->telefono}}" value="{{$sucursal->telefono}}"  aria-nombreibedby="basic-addon1">
            </div>

          </div>

          <button type="submit" style="position: absolute; left: 68%; margin-top: 30px; " class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-check"></i>
            </span>
            <span class="text">Actualizar sucursal</span>
          </button>

        </div>
      </div>



      </form>



    </div>

  </div>
</div>







@endsection