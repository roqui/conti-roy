@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')


<div style="margin: 50px;">


  <div  class="card shadow mb-6">

    <div class="card-body">
      <div>

        <div style="margin-bottom: 20px;">
         <h3>Agregar Sucursal </h3>

       </div>

       <form name="formu" action="crear_sucursal" method="POST">
        {{csrf_field()}} 


         <div style="width: 50%;">
          <b><label>Nombre:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> </span>
            </div>
            
            <input type="text" name="nombre" class="form-control" aria-nombreibedby="basic-addon1" id="nombre">
          </div>

        </div>
        <div class="row">
          <div class="col-sm">



            <div>
             <b> <label>Calle:</label></b> <br>
             <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> </span>
              </div>

              <input type="text" name="calle" class="form-control"  aria-nombreibedby="basic-addon1" id="calle">
            </div>

          </div>

          <div>
            <b><label>Colonia:</label></b> <br>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> </span>
              </div>

              <input type="text" name="colonia" class="form-control" aria-nombreibedby="basic-addon1" id="colonia">
            </div>

          </div>

          <div>
            <b><label>Ciudad:</label></b> <br>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"> </span>
              </div>

              <input type="text" name="ciudad" class="form-control"  aria-nombreibedby="basic-addon1" id="ciudad">
            </div>

          </div>

          <div style="margin-top: 40px;">
            <a href="sucursales" class="btn btn-outline-danger"><i class="fa fa-arrow-left"></i>   Cancelar  </a>

          </div>

        </div>
        <div class="col-sm">

          <div>
            <b><label>Num. exterior:</label> </b><br>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"></span>
              </div>

              <input type="text" name="num_exterior" class="form-control"  aria-nombreibedby="basic-addon1" id="num_exterior">
            </div>

          </div>

          <div>
           <b> <label>Código Postal:</label></b> <br>
           <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> </span>
            </div>
            
            <input type="number" name="codigo_postal" class="form-control" aria-nombreibedby="basic-addon1" id="codigo_postal">
          </div>

        </div>

        <div>
          <b><label>Teléfono:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> +52</span>
            </div>
            
            <input type="number" name="telefono" class="form-control"  aria-nombreibedby="basic-addon1" id="telefono">
          </div>

        </div>
        <div>
          <button type="submit" style=" margin-top:30px; float: right;" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-plus"></i>
            </span>
            <span class="text">Añadir sucursal</span>
          </button>
        </div>
      </div>
    </div>
  </div>

</form>


</div>

</div>
</div>





@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function(){

    $("#carga").click('.lastinsert',function(){
      $.ajax({
       type:'get',
       url:'{!!URL::to('findLastSuc')!!}',
       dataType:'json', 
       success:function(sucursales){
        console.log('Holi, llegué al success');
        console.log(sucursales);


          //if($("#codigo").is(":empty")){
            $.each(sucursales, function(index, obj){
             $('#nombre').val("{{$sucursales->nombre}}");
             $('#calle').val("{{$sucursales->calle}}");
             $('#colonia').val("{{$sucursales->colonia}}");
             $('#ciudad').val("{{$sucursales->ciudad}}");
             $('#num_exterior').val("{{$sucursales->num_exterior}}");
             $('#codigo_postal').val("{{$sucursales->codigo_postal}}");
             $('#telefono').val("{{$sucursales->telefono}}");
           });
          //}
        },
        error:function(xhr, status, error){
          console.log(xhr);
          console.log(status);
          console.log(error);
        }
      });
    });


  });

</script>
@endsection