@extends('layouts.plantilla')
@extends('layouts.menu')
@section('main')



<div class="container">

  <h3>Máximos y Mínimos</h3>


  @if(session('Mensaje'))
  
  <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensaje')}}</div>

  @endif
  @if(session('Mensajee'))
  
  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajee')}}</div>

  @endif
  @if(session('Mensajea'))
  
  <div class="alert alert-primary" id="warning-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajea')}}</div>

  @endif

  @foreach($permisos as $item)
  @if($item->idInterfaz==58)
  <div style="margin-bottom: 20px; position: relative; left: 76%;">
    <a href="add_max_min"  class="btn btn-primary btn-icon-split">
      <span class="icon text-white-50">
      <i class="fas fa-plus"></i>
      </span>
      <span class="text">Agregar Máximos y Mínimos</span>
    </a>
   </div>
   @break
   @endif
   @endforeach



  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"></h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th style="width: 200px; max-width: 100px;">Sucursal</th>
              <th style="width: 200px;">Producto</th>
              <th style="width: 50px;">Máximo</th>
              <th style="width: 50px; margin: auto;">Mínimo</th>
              <th>Cantidad en % de existencia</th>
              <th><center><i class="fas fa-check-square"> </i></center></th>
              <th><center><i class="fas fa-check-square"> </i></center></th>

            </tr>
          </thead>
          <tfoot>
            <tr>
              <th style="width: 200px; max-width: 100px;">Sucursal</th>
              <th style="width: 200px;">Producto</th>
              <th style="width: 50px;">Máximo</th>
              <th style="width: 50px; margin: auto;">Mínimo</th>
              <th>Cantidad en % de existencia</th>
              <th><center><i class="fas fa-check-square"></i></center></th>
              <th><center><i class="fas fa-check-square"> </i></center></th>

            </tr>
          </tfoot>
          <tbody>

            @foreach($productos as $item)
            <tr>
              <td>{{$item->nombre}}</td>
              <td>{{$item->descripcion_producto}}</td>
              <td>{{$item->maximo}}</td>
              <td>{{$item->minimo}}</td>
              <td>
                <div class="progress">
                    <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                    <!-- <div class="progress-bar-animated" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div> -->
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 50%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              @foreach($permisos as $item)
              @if($item->idInterfaz==59)
              <td>
                <center>
                  <form action="updating_max_min">
                    <input type="hidden" name="product_max_min" value="{{$item->id_maxx}}">
                    <button class="btn " type="submit"> <i class="fas fa-edit"> </i></button>
                  </form> 
                </center>
              </td>
              <td>
                <center>
                    <form action="eliminar_max_min">
                      <input type="hidden" name="id_delete" value="{{$item->id_maxx}}">
                      <button class="btn " type="submit"> <i class="fas fa-trash"> </i></button>
                    </form>   
                  </center>
              </td>
              @break
              @endif
              @endforeach
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

  







@endsection

@section('scripts')

@endsection