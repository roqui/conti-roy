@extends('layouts.plantilla')
@extends('layouts.menu')
@section('main')


<div class="container">

	<h3>Máximos y mínimos de la sucursal
		<b>
			<?php
                  $id = Auth::user()->idSucursal; 

                  $sucursal = DB::table('destinos')->select('nombre')->where('id', '=', $id)->first();?>
                  @foreach($sucursal as $key)
                    {{$key}}
                  @endforeach
		</b>
	</h3>

		<div class="card shadow mb-4">
           
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Producto</th>
      <th>Código</th>
      <th>Mínimo</th>
      <th>Máximo</th>
      @foreach($permisos as $item)
      @if($item->idInterfaz==49)
      <th scope="col"></th>
      <th scope="col"></th>
      @break
      @endif
      @endforeach
    </tr>
    </thead>

    
     	@foreach($maximos as $maximo)
     	     <tr>

     	<td>{{$maximo->id}}</td>
     	<td>{{$maximo->descripcion_producto}}</td>
     	<td>{{$maximo->codigo}}</td>
     	<td>{{$maximo->minimos}}</td>
     	<td>{{$maximo->maximos}}</td>
     	<td>
     		@include('maximos.updating_max_min')
     	</td>
     	<td>
     		@include('maximos.reestablecer')
     	</td>
     	     </tr>

     	@endforeach
                </table>
                
              </div>
              
            </div>
          </div>

	
</div>

@endsection

@section('scripts')

@endsection