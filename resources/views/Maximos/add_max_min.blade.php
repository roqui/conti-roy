@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')
@if(session('Mensajeu'))
      
      <div class="alert alert-success" id="success-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajeu')}}</div>

    @endif
    @if(session('Mensajeue'))
      
      <div class="alert alert-danger" id="danger-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajeue')}}</div>

    @endif
    @if(session('Mensajeua'))
      
      <div class="alert alert-warning" id="warning-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajeua')}}</div>

    @endif
    




<div style="width: auto; max-width: 75%; margin: auto; height: 500px; " class="card shadow mb-6">

  <div class="card-body">
    <div>
      <form  action="new_max_min" method="POST">
        {{csrf_field()}} 

        <div> 
          <h3>Establecer Máximos y Mínimos</h3>
          <div class="row">
            <div class="col-sm">
             <div class="form-group">

              <!-- ------------------------------ SUCURSAL --------------------- -->
              <b><label for="">Selecciona una sucursal y un producto:</label><br></b>                
              <select style="width: 260px" class="class_sucursal btn btn-secondary dropdown-toggle" id="sucursal" name="id_sucursal">
                
                <option value="0" disabled="true" selected>-- Sucursal --</option>
                @foreach($sucursal as $sucursal)
                <option value="{{$sucursal->id}}">{{$sucursal->nombre}}</option>
                @endforeach

              </select>
              <!-- ----------------------------------- PRODUCTO --------------------------- -->

              <select style="width: 265px" name="id_producto"  id="productname" class="btn btn-secondary dropdown-toggle" >

                <option value="0" disabled="true" selected>-- Producto --</option>
              </select>
              </div>

              <b><label>Mínimo</label></b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">#</span>
                </div>
                <input type="number" name="minimo" class="form-control" aria-label="Username" aria-describedby="basic-addon1" style="width: 50%;">
              </div>
              <!-- ----------------------------------- -->
              <!-- <b><label id="prueba" class="prueba">Prueba</label></b> -->
              <b><label>Máximo</label></b>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">#</span>
                </div>
                <input type="number" name="maximo" class="form-control" aria-label="Username" aria-describedby="basic-addon1" style="width: 50%;">
              </div>
            </div>
          </div>
        </div>

        <button type="submit" style="position: absolute; left: 82%; " class="btn btn-primary btn-icon-split">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Añadir</span>
        </button>

      </form>
    </div>

  </div>
</div>

@endsection

<!-- ---------------------------------------------------------- SCRIPTS ------------------------------------------- -->

@section('script')

<script type="text/javascript">
  $(document).ready(function(){

    $(document).on('change','.class_sucursal',function(){
      // console.log("hmm its change");

      var cat_id=$(this).val();
      // console.log(cat_id);
      var div=$(this).parent();

      var op=" ";

      $.ajax({
        type:'get',
        url:'{!!URL::to('findProduct')!!}',
        data:{'id':cat_id},
        success:function(data){

          // console.log('success');
          // console.log(data);
          // console.log(data.length);
          
          op+='<option value="0" selected disabled>Elige un producto</option>';
          for(var i=0;i<data.length;i++){
          op+='<option value="'+data[i].id_producto+'">'+data[i].descripcion_producto+'</option>';
           }

           div.find('#productname').html(" ");
           div.find('#productname').append(op);
        },
        error:function(){

        }
      });
    });

  });
</script>
@endsection

