

<div id="tabla">
  <table class="table" style="text-decoration: none; color: black;">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Código</th>
        <th scope="col">Cantidad</th>
        <th scope="col">Unidad</th>
        <th scope="col">Descripción</th>
        <th>Precio U.</th>
        
        <th>Importe</th>
        <th><i class="fas fa-trash"></i></th>
      </tr>
    </thead>
    <tbody>
    @foreach($compra as $item)
      <tr>
        <td><?php echo $item->codigo ?></td>
        <td> 
          <button type="button" class="btn btn-outline-dark btn-sm" 
          onclick="cantidad_actual(<?php echo $item->precio ?>,<?php echo $item->idProducto ?>,<?php echo $item->id ?>,<?php echo $item->cantidad ?>,1)">
              <i class="fas fa-minus"  ></i>
          </button>  <?php echo $item->cantidad ?>
          <button type="button" class="btn btn-outline-dark btn-sm" 
          onclick="cantidad_actual(<?php echo $item->precio ?>,<?php echo $item->idProducto ?>,<?php echo $item->id ?>,<?php echo $item->cantidad ?>,2)">
              <i class="fas fa-plus"></i>
          </button>
        </td>
        <td><?php echo $item->unidad ?></td>
        <td><?php echo $item->descripcion_producto ?></td>
        <td>$<?php echo $item->precio ?></td>

        <td scope="row">$<?php echo ($item->cantidad)*($item->precio); ?></td>
        <td>
            <button  onclick="eliminar(<?php echo $item->id; ?>)" type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
              <i class="fas fa-trash"></i>
            </button>
        </td>
      </tr>
    @endforeach
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
       
        <td>Total: </td>
        <td>$@foreach($total as $item)<?php echo $item->Total ?>@endforeach</td>
      </tr>    
   </tbody>
  </table>
</div>

