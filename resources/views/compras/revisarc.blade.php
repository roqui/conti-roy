@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div class="container">
    <h3>Proceso de Revisión para la Órden de Compra #{{$idtraslado}}</h3>
    <p class="mb-4">Listado de los artículos asociados a la Órden de Compra con número de folio {{$idtraslado}}.</p>

    @if(session('Mensajeu'))
    
    <div class="alert alert-warning" id="warning-alert">
      <button type="button" class="close" data-dismiss="alert">x</button>
    {{session('Mensajeu')}}</div>

    @endif  

    <div id="alert" class="alert"></div>
    <div id="datos2" class="datos2"></div>

    <div class="form-group col-md-6">
      <label for="inputEmail4">Código del Producto:</label>
      <input type="email" style="width: 300px;"  class="codigo form-control" id="caja" placeholder="">
    </div>

    <input type="hidden" value="{{$idtraslado}}" id="pedido" name="pedido">
    

    <div class="card shadow mb-4">
           
              <div class="card-body">
                <div class="table-responsive">
                          
                       <div id="datos" class="datos"></div> 
                </div>

              

  
              </div>
    </div>

    
    	<button onclick="confirm();" style="margin-bottom: 10px; float: right;"  class="confirmacion btn btn-success btn-icon-split">
		    <span class="icon text-white-50">
		      	<i class="far fa-check-circle"></i>
		    </span>
		    <span class="text">Aceptar Orden</span>
  		</button>
    

    

    <form action="cproximas">
    	<input type="hidden" value="{{$idtraslado}}" name="idpedido">
    	<button type="submit" style="margin-right: 10px; ;margin-bottom: 10px; float: left;"  class="btn btn-outline-secondary">
		      	<i class="fa fa-arrow-left"></i>
		    <span class="text">Regresar</span>
  		</button>
    </form>


    
           
              
</div>

 

@endsection()

@section('script')

<script type="text/javascript">
  function confirm (){
  	$(document).ready(function(){
  		var idpedido = $('#pedido').val();
  		var div=$('#datos2').parent();
  		$.ajax({
		        type:'get',
		        url:'{!!URL::to('aceptarc')!!}',
		        dataType: "json",
		        data:{'idpedido': idpedido},
		        success:function($msj){
		          console.log('Aquí va  al mensaje: ...');
		          console.log($msj);
		          if($msj=="YES"){
		          	location.href ='{!!URL::to('caceptada')!!}';
		          }else{
		          	div.find("#datos2").html($msj);
		          	tabla();
		          }
		          
		        },
		        error:function(xhr, status, error){
		          console.log(xhr);
		          console.log(status);
		          console.log(error);
		        }

		    });

  	});
  }
</script>
<script type="text/javascript">
  $(document).ready(function(){//en este script estoy cargando la tabla 
    	var id = $('#pedido').val();
    	var div=$('#datos').parent();
        $.ajax({
	        type:'get',
	        url:'{!!URL::to('conceptoscp')!!}',
	        dataType: "json",
	        data:{'id': id},
	        success:function($datos){
	          console.log('Primera tabla...');
	          div.find("#datos").html($datos);
	          tabla();
	        },
	        error:function(xhr, status, error){
	          console.log(xhr);
	          console.log(status);
	          console.log(error);
	        }

   		});
  
});
</script>

<script type="text/javascript">
  $(document).ready(function(){
    	$(".codigo").keypress(function(){
    		var codigo=$(this).val();
    		var pedido = $('#pedido').val();
    		var div=$('#alert').parent();
    		 $.ajax({
		        type:'get',
		        url:'{!!URL::to('checadocp')!!}',
		        dataType: "json",
		        data:{'codigo':codigo, 'pedido': pedido},
		        success:function($msj){
		          console.log('Aquí va  al mensaje: ...');
		          console.log($msj);
		          div.find("#alert").html($msj);
		          tabla();
		        },
		        error:function(xhr, status, error){
		          console.log(xhr);
		          console.log(status);
		          console.log(error);
		        }

		    });

    		$('#caja').val('');
    	});
    
  });
</script>
<script type="text/javascript">
	function tabla (){
		$(document).ready(function(){//en este script estoy cargando la tabla 
	    	var id = $('#pedido').val();
	    	var div=$('#datos').parent();
	        $.ajax({
		        type:'get',
		        url:'{!!URL::to('conceptoscp')!!}',
		        dataType: "json",
		        data:{'id': id},
		        success:function($datos){
		          console.log('Imprimiendo tabla...');
		          div.find("#datos").html($datos);
		          
		        },
		        error:function(xhr, status, error){
		          console.log(xhr);
		          console.log(status);
		          console.log(error);
		        }

	   		});
  
		});
	}
</script>



@endsection
