@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="margin:30px;">

  <div class="card shadow mb-4" >
    <div class="card">


      <div style="text-decoration: none; color: black;" class="card-body">
        <b><label style="font-size: 20px;">Compras</label></b>
        <h4>Compras de la sucursal
          <b>
            <?php
            $id = Auth::user()->idSucursal; 

            $sucursal = DB::table('destinos')->select('nombre')->where('id', '=', $id)->first();?>
            @foreach($sucursal as $key)
            {{$key}}
            @endforeach


          </b>
        </h4>


      </div>
      <div class="row" >

        <div class="col-sm">

      @foreach($permisos as $item)
      @if($item->idInterfaz==45)
        @include('compras.infonewcompra')
      @break
      @endif
      @endforeach
      
      @foreach($permisos as $item)
      @if($item->idInterfaz==49)
          <div class="card" style="margin:20px;  ">
            <a href="cproximas" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/calendar.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Fechas de entrega</label></b>
                <h5>Aquí podrás ver las próximas entregas</h5>
              </div>
            </a>
          </div>
      @break
      @endif
      @endforeach

        </div> 

        <div class="col-sm" style=" ;">
      @foreach($permisos as $item)
      @if($item->idInterfaz==13)
          <div class="card" style="margin:20px;  ">
            <a href="proveedores" style="text-decoration: none; color: black; " >
              <div class="card-body">
                <img src="img/provider.jpg" style="width: 100px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px; text-align: center;">Proveedores</label></b>
                <h5>Puedes elegir entre <b> {{$count}}</b> diferentes proveedores</h5>
              </div>
            </a>
          </div>
      @break
      @endif
      @endforeach



      @foreach($permisos as $item)
      @if($item->idInterfaz==50)
          <div class="card" style="margin:20px;  ">
            <a href="maximos" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/estadisticas.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Máximos y mínimos</label></b>
                <h5>Conoce los máximos y mínimos de cada producto</h5>
              </div>
            </a>
          </div>
      @break
      @endif
      @endforeach


        </div>
        <div class="col-sm">

      @foreach($permisos as $item)
      @if($item->idInterfaz==48)
          <div class="card" style="margin:20px;  ">
            <a href="ordencompras" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/ordenes.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;"´>Órdenes de compra</label></b>
                <h5>Échale un vistazo a las órdenes de compra</h5>
              </div>
            </a>
          </div>
      @break
      @endif
      @endforeach
      
      @foreach($permisos as $item)
      @if($item->idInterfaz==48)
          <div class="card" style="margin:20px;  ">
            <a href="work" style="text-decoration: none; color: black;" >
              <div class="card-body">
                <img src="img/return.png" style="width: 80px; margin: 10px;" class="card-img-top" alt="delivered">

                <b><label style="font-size: 20px;">Devoluciones</label></b>
                <h5>Aquí podrás ver las devoluciones de compras</h5>



              </div>
            </a>
          </div>
      @break
      @endif
      @endforeach

        </div> 
      </div>


    </div>
  </div>

  <div>


    @endsection

    
    @section('script')

    <script>
      
      $(document).ready(function() {
        $('.select2').select2();
      });
    </script>

    @endsection