@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')


<div style="margin: 30px; text-decoration: none; color: black;">

	<div  class="card shadow mb-6">

		<div class="card-body">

			<h3 >Órdenes de Compra</h3>
			<h5 style="margin-bottom: 50px;" >Órdenes de compra de la sucursal 
				<b>
					<?php
					$id = Auth::user()->idSucursal; 

					$sucursal = DB::table('destinos')->select('nombre')->where('id', '=', $id)->first();?>
					@foreach($sucursal as $key)
					{{$key}}
					@endforeach


				</b> </h5>

				<div style="margin-top: 30px;" class="table-responsive">

					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th scope="col">Folio</th>
								<th scope="col">Proveedor</th>
								<th scope="col">Fecha de compra</th>
								<th scope="col">Fecha de llegada</th>
								<th>Estado</th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							@foreach($compras as $compra)
							<?php switch ($compra->descripcion) {
                                  case 'Realizada':
                                    $clase="table-primary";
                                    break;
                                  case 'Aceptada':
                                    $clase="table-success";
                                    break;
                                  case 'Rechazada':
                                    $clase="table-danger";
                                    break;
                                  
                                } ?>
							<tr class=<?php echo $clase; ?>>
								<th scope="row">{{$compra->id}}</th>
								<td>{{$compra->nombre}}</td>
								<td>{{$compra->fechaC}}</td>
								<td>{{$compra->fechaLL}}</td>
								<td>{{$compra->descripcion}}</td>
								<td><form action="nota_compras">
                          <input type="hidden" value="{{$compra->id}}" name="id_pedido">
                          <center><button class="btn" type="submit"><i class="fas fa-file-pdf"></i></button></center>
                        </form>
                        </td>

							</tr>

							@endforeach


						</tbody>
					</table>
				</div>

			</div>

		</div>	
	</div> 

	@endsection