@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')



<div style="margin: 30px; text-decoration: none; color: black;">


  <div class="card shadow mb-4">

    <div class="card-body">


      <div class="row">
        <div class="col-sm">


          <div class="card">
            <div class="card-body">

             <div class="row">

               <form action="indexcharts" method="POST">
                <div class="col-sm">
                  {{csrf_field()}} 
                                      <h3>  Máximos y Mínimos Anuales </h3>


                  <b><label>Selecciona un producto</label></b><br>

                  <select name="id" style="width: 300px;" id="id_producto" required class="select2">
                    <option></option>
                    @foreach($productos as $producto)
                    <option name="idProducto" value="{{$producto->id}}">
                     {{ $producto->codigo }} ||     {{ $producto->descripcion_producto }}
                    </option>
                    @endforeach
                  </select>   

                </div>
                <div  class="col-sm">
                  <b><label>Selecciona un año:</label></b> <br>

                  <select name="year" style="margin-bottom: 10px; width: 115px;" class="select2">
                    <option>2019</option>
                  </select> <br>
                  
                  <div style="margin-top: 10px;">
                    <button id="onclickButton" type="submit" class="btn btn-primary btn-icon-split">
                      <span class="icon text-white-50">
                        <i class="fas fa-search"></i>
                      </span>
                      <span class="text">Buscar</span>
                    </button> 
                  </div>                 
                </div>
              </form>
            </div>



          </div>
        </div>



        <div class="card">
          <div class="card-body">
            <h4>Información del producto</h4> <br>

            <div class="row">
                <div class="col-sm">
                  <b><label>Descripción:</label></b> {{$descripcion_producto}} <br>

                  <b><label>Código:  </label></b> {{$codigo}} <br>
                  <b><label id="danibonita">Línea: </label></b> {{$lineas}} <br>

                  <b><label>Proveedor:</label></b> {{$proveedor}}<br>



                </div>
                <div class="col-sm">
                  <b><label>Marca: </label></b> {{$marca}} <br>

                  <b><label>Código SAT: </label></b> {{$codigosat}}<br>
                  <b><label>Sublínea: </label></b> {{$sublineas}}<br>


                  <b><label>Costo:</label></b> ${{$costo}}<br>




                </div>



              </div>



            



            <small>
              <a class="danibonita" href="proveedores">
                Haz click aquí para más información de los proveedores
              </a>
            </small>

          </div>
        </div>
        <div style="margin-top: 50px;">
          <a href="compras" class="btn btn-outline-secondary"><i class="fa fa-arrow-left"></i>   Regresar  </a>

        </div>


      </div>
      <div class="col-sm">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Producto "{{$descripcion_producto}}"</h6>
          </div>
          <div class="card-body">
           


              @include('compras.graficas')


           
            <hr>
            <hr>
            <small>
              <a class="danibonita" href="productos">
                Haz click aquí para ver más información del producto
              </a>
            </small>

          </div>
        </div>

      </div>
    </div>

  </div>

</div>

</div>









@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2();
  });

</script>
@endsection






@endsection