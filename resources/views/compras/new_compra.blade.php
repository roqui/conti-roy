@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="text-decoration: none; color: black; margin:50px;">

  <!-- ------------------- cancelar compra    -->
  <form id="cancelar" action="cancelar_compras" method="get" style="float: right; margin-right: 15px; margin-bottom: 10px;">
      <input type="text" hidden="" value="{{$num_pedido}}" name="id_pedido">
      <button class="btn btn-danger btn-icon-split" >
        <span class="icon text-white-50">
          <i class="far fa-times-circle"></i>
        </span>
        <span class="text">Cancelar</span>
      </button>
    </form>

  <!-- ----------------------- cancelar compra -->

  <div  class="card shadow mb-4" style="width: 100%; margin: 0;">
    <div class="card-body">

      <div style="margin: 30px; background-color: white;">
        <div class="row">
          <div class="col-sm">
            <img style="width: 200px;" src="img/logo2.jpg"><br>
          </div>
          <div class="col-sm">
            <b><label>Email: </label></b> contipapelerias@gmail.com <br>
            <b><label>Teléfono: </label></b> +52 443 313 0740<br>
            <b><label>Sitio web: </label></b> contipapelerias.com
          </div>
          <div class="col-sm">
            <b><label>Producciones Conti, S.A. De C.V.</label></b><br>
            <b><label>Dirección: </label></b> Allende 1013, Centro histórico<br>
            CP. 58000 Morelia, Mich.
          </div>
        </div>
      </div>
      <hr class="sidebar-divider">

      
      <h4>Compra</h4>

      <div class="row">
        <div class="col-sm">
          <b><label>Proveedor: </label></b> {{$proveedor->nombre}}<br>
          <b><label>Dirección: </label></b> {{$proveedor->calle}} #{{$proveedor->num_exterior}} CP. {{$proveedor->codigo_postal}}, 
          <br>{{$proveedor->ciudad}},  México <br>
          <b><label>Teléfono: </label></b> +52 {{$proveedor->telefono}}
        </div>
        <div class="col-sm">
          <b><label>Compra: </label></b> #{{$num_pedido}} <br>
          <b><label>Fecha y Hora: </label></b> {{$date}}<br>
          <b><label>Usuario que expidió: </label></b> {{ Auth::user()->name }}<br>
          <!-- <b><label>Pedido: </label></b> #54321 -->
          <br>
          <!-- ////////////////////////////////////////////// -->
        <!-- AGREGAR NUEVO CONCEPTO -->
          <button href="#" class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#inventario" style="float: right; margin-bottom: 10px;">
            <span class="icon text-white-50">
              <i class="fas fa-plus"></i>
            </span>
            <span class="text">Producto</span>
          </button>

<!-- Modal -->
        <div class="modal fade" id="inventario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><b>Concepto</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                   <b><label>Seleccione un producto: </label></b> <br>
                    <select name="id_prod[]" style="width: 460px;" id="id_producto" required class="select2" multiple="multiple">
                      @foreach($productos as $producto)
                        <option name="idProducto" value="{{$producto->id}}">
                        {{ $producto->descripcion_producto }} || {{ $producto->codigo }} 
                        </option>
                      @endforeach
                    </select>
                    
                    <input hidden value="{{$num_pedido}}" name="idPedido" id="id_pedido">
                      <input hidden value="{{$id_cliente}}" name="id_cliente" id="id_cliente">
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <button type="button" class="btn btn-primary btn-icon-split" data-dismiss="modal" id="guardarnuevo">
                        <span class="icon text-white-50">
                          <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Agregar</span>
                      </button>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- TERMINA AGREGAR NUEVO CONCEPTO -->
        <!-- //////////////////////////////////////////// -->
      <hr class="sidebar-divider">
      <div id="tabla" style="width: 100%;">
        
      </div>
      

      </div>
      <br>
      <div style=" width: -webkit-fill-available;" id="observaciones" hidden="" >
      <form action="observaciones_compras" method="get">
      <div class="input-group" >
        <div class="input-group-prepend">
          <span class="input-group-text">Observaciones</span>
        </div>
        <textarea class="form-control" placeholder="Las observaciones se guardaran al finalizar la compra" id="observaciones_input"></textarea>
      </div>
      <br>
      <button class="btn btn-success btn-icon-split" style="float: right; margin-right: 5px;" onclick="add_obs()" >
        <span class="icon text-white-50">
          <i class="fas fa-save"></i>
        </span>
        <span class="text">Finalizar</span>
      </button>
      </form>
      <!-- --------------------------------------------- IMPRIMIR -->
      <form id="imprimir" hidden="" target="_blank" action="nota_compras" method="get" style="float: right; margin-right: 10px;">
      <input type="text" hidden="" value="{{$num_pedido}}" name="id_pedido">
      <button class="btn btn-primary btn-icon-split" onclick="add_obs()">
        <span class="icon text-white-50">
          <i class="fas fa-print"></i>
        </span>
        <span class="text">PDF</span>
      </button>
    </form>
      </div>
   
 </div>


</div>

@endsection


@section('script')
<script>
  
  $(document).ready(function() {

    //CARGANDO LA TABLA POR PRIMERA VEZ 
    // LE MANDO LOS PARAMETROS ATRAVEZ DE GET "A LA PHP" xd

    num_pedido=$('#id_pedido').val();
    $('#tabla').load('tabla_dinamica_compras?num_pedido='+num_pedido);
    // $('#tabla').on('load','tabla_dinamica');
  //   $('#tabla').load('tabla_dinamica',{
  //   'pedido': num_pedido});
  });

  $(document).ready(function() {
    
    $('#guardarnuevo').click(function(event) {
      // AQUI JALO LOS VALORES CON .val() DE LOS INPUTS Y LOS IMPRIMO PARA VERIFICAR QUE EXISTEN
            id_producto=JSON.stringify($('#id_producto').val());
            num_pedido=$('#id_pedido').val();
            id_cliente=$('#id_cliente').val();
            console.log('id_producto: '+id_producto);
            // console.log('cantidad: '+cantidad);
            // console.log('id_PEDIDO: '+num_pedido);
            // console.log('id_cliente: '+id_cliente);

            // CREANDO EL AJAX PARA MANDAR LA SOLICITUD A UN METODO PARA AGREGAR LOS DATOS A LA BD
            $.ajax({
              url: 'agregar_concepto_compras',
              type: 'post',
              data: {'id_producto': id_producto,
              'num_pedido':num_pedido, 
              _token: '{{csrf_token()}}',
              'id_cliente':id_cliente
               }, 
               success:function(response) {
                response; 
               }
            })
            .done(function(r) {
              // CUANDO SE TERMINA DE EJECUTAR LA FUNCION RECARGO LA TABLA
              alertify.success(r);
              $('#tabla').load('tabla_dinamica_compras?num_pedido='+num_pedido);
              imprimirSiNo(num_pedido);
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              // console.log("complete");
            });

    });
  });

  // ----------------------------- ELIMINAR
    // AQUI, AL IMPRIMIR EL BOTON DE ELIMINAR EN CADA TUPLA LE ASIGNO EL ID DE
    // LA TUPLA EN LA TABLA CONCEPTOS Y EJECUTO UN AJAX PARECIDO AL DE ARRIBA  

  function eliminar(id){
    console.log(id);
    num_pedido=$('#id_pedido').val();
    
    $.ajax({
       url: 'eliminar_concepto_compras',
       type: 'POST',
       data: {'id_concepto': id, _token: '{{csrf_token()}}' },
     })
     .done(function(r) {
      $('#tabla').load('tabla_dinamica_compras?num_pedido='+num_pedido);
       alertify.success(r);
     })
     .fail(function() {
       console.log("error");
     })
     .always(function() {
       // console.log("complete");
     });
      

  }

  // --------------------------------------------------------------
  function imprimirSiNo(id){

    
    $.ajax({
      url: 'imprimirSiNo_compras',
      type: 'get',
      data: {'id_pedido': id},
    })
    .done(function(r) {
      // console.log("tuplas: " + r);
      if (r==1) {
        $('#imprimir').removeAttr('hidden');
        $('#observaciones').removeAttr('disabled');
        $('#observaciones').removeAttr('hidden');

      }
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    

  }
// -----------------------------------------

// -----------------------------------------
$(document).ready(function() {
        $('.select2').select2();
      });
// ----------------------------------------------------

  function cantidad_actual(precio,id_producto,id_concepto,cant,accion){
    num_pedido=$('#id_pedido').val();
    console.log('pedido: '+num_pedido);
    // console.log( "pedido: "+ num_pedido+" id_concepto: "+id_concepto+" Cantidad actual: " + cant + " accion: "+ accion );

    $.ajax({
      url: 'act_cantidad_compras',
      type: 'get',
      data: { 
        'id_concepto':id_concepto,
        'actual': cant, 
        'accion': accion, 
        'id_pedido': num_pedido,
        'id_producto': id_producto,
        'precio': precio }
    })
    .done(function(r) {
      if(r=="Ya no tienes podructo en tu concepto. Se eliminará automaticamente."){
                alertify.error(r);
        }else{
                alertify.success(r);
        }
      $('#tabla').load('tabla_dinamica_compras?num_pedido='+num_pedido);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  }
// -----------------------------------------------------
  
  
    
  
    function add_obs(){
    num_pedido=$('#id_pedido').val();
    observaciones=$('#observaciones_input').val();
    console.log('pedido: '+num_pedido);
    console.log('observaciones: '+observaciones);
    
    $.ajax({
      url: 'observaciones_compras',
      type: 'get',
      data: {'observaciones': observaciones, 'id_pedido':num_pedido },
    })
    .done(function() {
      console.log("success");
      alertify.success('Nota de compra realizada con éxito!');
    })
    
    

    
    
  }
  



</script>


@endsection