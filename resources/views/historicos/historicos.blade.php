@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')



<div style="margin: 50px;">


  <div class="card shadow mb-4">

    <div class="card-body">

      <h3>Histórico de Ventas de Papelerías CONTI</h3>

      <b><label>Filtrar por:</label></b> <br>

      <form method="post" action="historicosbusqueda">
        {{csrf_field()}} 

        <div class="row">
          <div class="col-sm">
            <label>Sucursal</label> <br>
            <select name="id_sucursal" style="width: 300px; height: 30px;" id="id_producto1" required class="select2">
              <option >Selecciona una sucursal</option>
              @foreach($sucursales as $sucursal)
              <option value="{{$sucursal->id}}">{{$sucursal->nombre}}</option>
              @endforeach

            </select> 
          </div>
          <div class="col-sm">
            <label>Producto</label> <br>
            <select name="id_producto" style="width: 300px; height: 30px;" id="id_producto2" required class="select2">
              <option >Selecciona un producto</option>
              @foreach($inventarios as $inventario)
              <option value="{{$inventario->id}}">{{$inventario->codigo}} || {{$inventario->descripcion_producto}}</option>
              @endforeach

            </select> 

          </div>
          <div class="col-sm">
            <label>Mes</label><br>
            <select name="month" style="width: 300px; height: 30px;" id="id_producto3" required class="select2" >
              <option >Selecciona un mes</option>
              <option value="1">Enero</option>
              <option value="2">Febrero</option>
              <option value="3">Marzo</option>
              <option value="4">Abril</option>
              <option value="5">Mayo</option>
              <option value="6">Junio</option>
              <option value="7">Julio</option>
              <option value="8">Agosto</option>
              <option value="9">Septiembre</option>
              <option value="10">Octubre</option>
              <option value="11">Noviembre</option>
              <option value="12">Diciembre</option>

            </select> 
          </div>
          <div class="col-sm">

            <label>Año</label> <br>
            <select style="width: 300px; height: 30px;" id="id_producto4" required class="select2">
              <option >Selecciona un año</option>
              <option value="">2019</option>

            </select> 

            

            <div class="row">
              <div class="col-sm-7"></div>
    <div class="col-sm-5">
                <button style="margin-top: 20px; " id="onclickButton" type="submit" class="btn btn-primary btn-icon-split">
                  <span class="icon text-white-50">
                    <i class="fas fa-search"></i>
                  </span>
                  <span class="text">Buscar</span>
                </button>
              </div>
            </div>


          </div>
          <div class="col-sm">

          </div>
        </div>
      </form>
      <br>
      <hr>

      <div style="margin-top: 30px;">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Código de Producto</th>
            <th scope="col">Descripción de Producto</th>
            <th>Cantidad</th>
            <th>Fecha</th>
            

          </tr>
        </thead>

        <tbody>

          

      </tbody>
    </table>

    
      </div>

    </div>
  </div>

  <div>



    @endsection







    @section('script')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.select2').select2();
      });

    </script>
    @endsection


