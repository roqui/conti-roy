<!-- Button trigger modal -->
<a href=""  data-toggle="modal" data-target="#exampleModal">
  ¿Te gustaría establecer estos valores como máximo y mínimo?

</a>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Establecer máximos y mínimos </b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <input hidden name="id" value="{{$id}}">

        <form action="maximos_hist" method="post">
          {{csrf_field()}} 

          <h4>¿Estás seguro de establecer los siguientes valores para el producto <b>{{$name}}</b>?</h4>
          <div class="container">
            <div class="row">
              <div class="col-sm">

                <input hidden value="{{$infosucursal->id}}" name="id_sucursal">

                <input hidden value="{{$id}}" name="id_producto">

                <div class="form-check">
                  <input class="form-check-input" name="minimo" value="{{$minimo}}" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">
                    <b><label>Mínimo: {{$minimo}}</label></b>
                  </label>
                </div>

              </div>
              <div class="col-sm">

                <div class="form-check">
                  <input class="form-check-input" name="maximo" value="{{$maximo}}"  type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">
                    <b><label>Máximo: {{$maximo}}</label></b>
                  </label>
                </div>

              </div>
            </div>

          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button id="onclickButton" type="submit" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-save"></i>
            </span>
            <span class="text">Guardar</span>
          </button> 
        </form>  
      </div>
    </div>
  </div>
</div>