 @section('menu')

 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index">

    <div class="sidebar-brand-text mx-3" style="padding: 10px; ">
     <div style="width: 210px; height: 62px; background-color: white; border-radius: 3px;">
      <img style="width: 120px; height: 60px;  padding: 10px; border-radius: 5px; " src="img/logo1.png">

    </div>
  </div>
</a>

<hr class="sidebar-divider my-0">







<!-- Divider -->







@foreach($permisos as $item)
@if($item->idInterfaz)


<!--SUBMENU CATALOGOS -->
<li class="nav-item">

  @foreach($permisos as $item)
  @if( $item->idInterfaz==4 || $item->idInterfaz==5 || $item->idInterfaz==10 || $item->idInterfaz==13 || $item->idInterfaz==56 || $item->idInterfaz== 17 )
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#catalogos" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas fa-clipboard-list"></i>
    <span>Catálogos</span>
  </a>
  @break
  @endif
  @endforeach
  <div id="catalogos" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <!-- <h6 class="collapse-header">Productos:</h6> -->
      <!-- --------------------------- -->
      @foreach($permisos as $item)
      @if($item->idInterfaz == 4)
      <a class="collapse-item" href="productos">Catálogo Productos</a>
      @break
      @endif
      @endforeach
        <!-- ----------------------- DE SUPER USUARIO ----------------- -->
       @foreach($permisos as $item)
      @if($item->idInterfaz == 56)
      <a class="collapse-item" href="indexsat">Catálogo Productos SAT</a>
      @break
      @endif
      @endforeach
      <!-- --------------------------- -->
      @foreach($permisos as $item)
      @if($item ->idInterfaz == 5)
      <a class="collapse-item" href="lineas">Catálogo Líneas</a>
      @break
      @endif
      @endforeach
      <!-- --------------------------- -->
      @foreach($permisos as $item)
      @if($item->idInterfaz == 10)
      <a class="collapse-item" href="marca">Catálogo Marcas</a>
      @break
      @endif
      @endforeach
      <!-- --------------------------- -->
      @foreach($permisos as $item)
      @if($item->idInterfaz == 13)
      <a class="collapse-item" href="proveedores">Catálogo Proveedores</a>
      @break
      @endif
      @endforeach
      <!-- --------------------------- -->
      @foreach($permisos as $item)
      @if($item->idInterfaz == 17)
      <a class="collapse-item" href="clientes">Catálogo Clientes</a>
      @break
      @endif
      @endforeach
      <!-- --------------------------- -->
      @foreach($permisos as $item)
      @if($item->idInterfaz)
      <a class="collapse-item" href="users">Catálogo Usuarios</a>  
      @break
      @endif
      @endforeach

      @foreach($permisos as $item)
      @if($item->idInterfaz == 56)
      <a class="collapse-item" href="allindex">Catálogo Usuarios*</a>  
      @break
      @endif
      @endforeach


    </div>
  </div>
</li>
<!--FIN SUBMENU CATALOGOS -->
@foreach($permisos as $item)
@if($item->idInterfaz==4 || $item->idInterfaz==5 || $item->idInterfaz==10 || $item->idInterfaz==13 || $item->idInterfaz==56 || $item->idInterfaz== 17)
<!-- Divider -->
<hr class="sidebar-divider">
@break
@endif
@endforeach

<!--SUBMENU PEDIDOS -->
@foreach($permisos as $item)
@if($item->idInterfaz==26 || $item->idInterfaz==30 || $item->idInterfaz==27 || $item->idInterfaz==32 || $item->idInterfaz==29 )
<!-- ---------------------------------------------- -->
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-share-square"></i>
    <span>Ventas</span>
  </a>
  



  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <!-- ------------------------------------ -->
      @foreach($permisos as $item)
      @if($item->idInterfaz==26 || $item->idInterfaz==30 || $item->idInterfaz==27 || $item->idInterfaz==32 )
      <a class="collapse-item" href="pedidos">Todos los pedidos</a>
      @break
      @endif
      @endforeach
      <!-- ------------------------------------ -->
      @foreach($permisos as $item)
      @if($item->idInterfaz == 27)
      <a class="collapse-item" href="ppendientes">Pedidos pendientes</a>
      @break
      @endif
      @endforeach
      <!-- ------------------------------------ -->
      @foreach($permisos as $item)
      @if($item->idInterfaz==29)
      <a class="collapse-item" href="pasignados">Pedidos asignados</a>
      @break
      @endif
      @endforeach
      <!-- ------------------------------------ -->
      @foreach($permisos as $item)
      @if($item->idInterfaz==30)
      <a class="collapse-item" href="premisionados">Pedidos remisionados</a>
      @break
      @endif
      @endforeach



      <!-- ------------------------------------ -->

    </div>
  </div>
  <!-- FIN SUBMENU PEDIDOS -->
</li>


@break
@endif
@endforeach
<!--FIN SUBMENU PEDIDOS -->


<!--SUBMENU TRANSACCIONES  -->
@foreach($permisos as $item)
@if($item->idInterfaz==34 ||$item->idInterfaz==35 || $item->idInterfaz==35 || $item->idInterfaz==38 || $item->idInterfaz==40 || $item->idInterfaz==41 || $item->idInterfaz==37)
<li class="nav-item">
        
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#transacciones" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas fa-exchange-alt"></i>
    <span>Traslados</span>
  </a>

  <div id="transacciones" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
     <!-- --------------------------------- -->
     @foreach($permisos as $item)
     @if($item->idInterfaz==34 ||$item->idInterfaz==35 || $item->idInterfaz==35 || $item->idInterfaz==38 || $item->idInterfaz==40 || $item->idInterfaz==41)
     <a class="collapse-item" href="traslados">Ver Traslados</a>
     @break
     @endif
     @endforeach
     <!-- --------------------------------- -->
     @foreach($permisos as $item)
     @if($item->idInterfaz==35)
     <a class="collapse-item" href="tpendientes">Traslados Pendientes</a>
     @break
     @endif
     @endforeach
     <!-- --------------------------------- -->
     @foreach($permisos as $item)
     @if($item->idInterfaz == 37)
     <a class="collapse-item" href="tasignados">Traslados Asignados</a>
     @break
     @endif
     @endforeach
     <!-- --------------------------------- -->
     @foreach($permisos as $item)
     @if($item->idInterfaz== 38)
     <a class="collapse-item" href="tremisionados">Traslados Remisionados</a>
     @break
     @endif
     @endforeach

     <!-- --------------------------------- -->
     @foreach($permisos as $item)
     @if($item->idInterfaz==41)
     <a class="collapse-item" href="tproximos">Traslados Próximos</a>
     @break
     @endif
     @endforeach
     <!-- --------------------------------- -->
   </div>




 </li>

      <!--SUBMENU ORD DE COMPRA -->
@foreach($permisos as $ordenes)
@if($ordenes->idInterfaz==45 || $ordenes->idInterfaz==49 || $ordenes->idInterfaz==13  || $ordenes->idInterfaz==50 || $ordenes->idInterfaz==48)
     <li class="nav-item">
      <a class="nav-link collapsed" href="compras"  aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-money-check-alt"></i>
        <span>Compras</span>
      </a>
    </li>
    <hr class="sidebar-divider">
 @break
 @endif
 @endforeach











@foreach($permisos as $item)
@if($item->idInterfaz==42 || $item->idInterfaz==56)
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#inventarios" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas fa-boxes"></i>
    <span>Inventarios</span>
  </a>

  <div id="inventarios" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
     
    @foreach($permisos as $item)
    @if($item->idInterfaz==56)
      <a class="collapse-item" href="indexinventarios">Inventario*</a>
    @break
    @endif
    @endforeach
    <!-- ------------------------------------ -->
    @foreach($permisos as $item)
    @if($item->idInterfaz==42)
      <a class="collapse-item" href="inventarios">Inventario</a>
    @break
    @endif
    @endforeach
      
      <!-- --------------------------------- -->
      @foreach($permisos as $item)
      @if($item->idInterfaz==42)
      <a class="collapse-item" href="historicos">Históricos Mensuales</a>
      @break
      @endif
      @endforeach
      <!-- --------------------------------- -->

      <a class="collapse-item" href="maximos"><span>Estadísticas Anuales</span></a>

    </div>
  </div>
</li>
@break
@endif
@endforeach

<!--FIN SUBMENU INVENTARIOS -->

@break
@endif
@endforeach




  
@break
@endif
@endforeach
<!-- FIN SUBMENU ORD DE COMPRA -->
<!-- Heading -->


<!--SUBMENU sucursales -->

@foreach($permisos as $item)
@if($item->idInterfaz==54)
<li class="nav-item">
  <a class="nav-link collapsed" href="sucursales"  data-target="#sucursales" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas fa-home"></i>
    <span>Sucursales</span>
  </a>
</li>
@break
@endif
@endforeach
<!--FIN SUBMENU sucursales -->


<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">


<li class="nav-item">
  <a  class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#info" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas fa-cogs"></i> 
    <span>Herramientas</span>
  </a>

  <div id="info" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">

      <a class="collapse-item" href="manual">Manual de Usuario</a>

      @foreach($permisos as $item)
      @if($item->idInterfaz==55)
      <a class="collapse-item" href="backlogindex">Bitácora</a>
      @break
      @endif
      @endforeach

      @foreach($permisos as $item)
      @if($item->idInterfaz==56)
      <a class="collapse-item" href="backlogAll">Bitácora*</a>
      @break
      @endif
      @endforeach
    </div>
  </div>
</li>



<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">
<!-- Sidebar Toggler (Sidebar) -->

<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>


</ul>

<script type="text/javascript">

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
</script>


<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>

  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>





@endsection