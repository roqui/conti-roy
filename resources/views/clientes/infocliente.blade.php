<!-- Button trigger modal -->
<button type="button"  data-toggle="modal" data-target="#info{{$cliente->id}}" class="btn"><i class="fa fa-info-circle"></i></button>

<!-- Modal -->
<div class="modal fade" id="info{{$cliente->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Información del cliente <b>       <b>{{$cliente->nombre}}</b> 
        </b> </b></h5> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">

       <div class="container">






            <h5>Información Personal</h5>


            <label><b>Email:</b> {{$cliente->email}}</label> <br>
            <b>Num. Teléfono: </b>{{$cliente->telefono}} <br>
            <br>

            <h5>Datos de Facturación</h5>

            <label><b>RFC:</b> {{$cliente->rfc}}</label> <br>


            Calle {{$cliente->calle}} #{{$cliente->num_exterior}} 

            CP. {{$cliente->codigo_postal}}<br>

            {{$cliente->ciudad}}, {{$cliente->estado}} 
            {{$cliente->pais}} <br>
            <br>


            <h5>Información Adicional</h5>

            
            <b>Días Plazo:</b> {{$cliente->dias_plazo}} <br>

            <b>Descuento:</b> {{$cliente->descuento}}% <br>
            <b>Observaciones:</b> {{$cliente->observaciones}} 


      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
</div>
</div>



