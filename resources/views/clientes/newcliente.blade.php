@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')


<div style="margin:50px;">





  <div  class="card shadow mb-6">

    <div class="card-body">


     <h3>Agregar Cliente </h3><br>

     <h5>Datos Personales</h5>



     <form action="new_cliente"  method="POST">
      {{csrf_field()}} 
      <div class="row">
        <div class="col-sm">

          <b><label>Nombre:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> </span>
            </div>

            <input required  type="text" placeholder="Nombre Completo" name="nombre" class="form-control" aria-nombreibedby="basic-addon1">
          </div>   

          <b><label>Correo Electrónico:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> @ </span>
            </div>

            <input required  type="email" placeholder="Correo Elcetrónico" name="email" class="form-control" aria-nombreibedby="basic-addon1">
          </div>



           <input name="foto" value="img/users.png" hidden>
        </div>
        <div class="col-sm">


          <b><label>Número de Teléfono:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> +52</span>
            </div>

            <input required  type="number" placeholder="Ejem. 4431685412" name="telefono" class="form-control" aria-nombreibedby="basic-addon1">
          </div>

          <b><label>RFC:</label></b> <br>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"> </span>
            </div>

            <input required type="text" placeholder="RFC" name="rfc" pattern="^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))((-)?([A-Z\d]{3}))?$" class="form-control" aria-nombreibedby="basic-addon1">
          </div>

          
        </div>
      </div>
      <hr>
      <h5>Dirección de Facturación</h5>

      <div class="row">
        <div class="col-sm">
         <b><label>Calle:</label></b> <br>
         <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"> </span>
          </div>

          <input required  type="text" placeholder="Colonia" name="calle" class="form-control" aria-nombreibedby="basic-addon1">
        </div>

      </div>
      
      <div class="col-sm">
       <b><label>Num. Exterior:</label></b> <br>
       <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="basic-addon1"> # </span>
        </div>
        
        <input required  type="number" placeholder="Número Exterior" name="num_exterior" class="form-control" aria-nombreibedby="basic-addon1">
      </div>    
    </div>
    <div class="col-sm">
     <b><label>Código Postal:</label></b> <br>
     <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> CP.</span>
      </div>
      
      <input required  type="text" placeholder="Código Postal" name="codigo_postal" class="form-control" aria-nombreibedby="basic-addon1">
    </div>    
  </div>
</div>

<div class="row">
  <div class="col-sm">
    <b><label>País:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  type="text" placeholder="País" name="pais" class="form-control" aria-nombreibedby="basic-addon1">
    </div>
  </div>
  <div class="col-sm">
    <b><label>Estado:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  type="text" placeholder="Estado" name="estado" class="form-control" aria-nombreibedby="basic-addon1">
    </div>
  </div>
  <div class="col-sm">
    <b><label>Ciudad:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  type="text" placeholder="Ciudad" name="ciudad" class="form-control" aria-nombreibedby="basic-addon1">
    </div>
  </div>
</div>
<hr>
<h5>Información Adicional</h5>

<div class="row">
  <div class="col-sm">
    <b><label>Días de Plazo:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  type="text" placeholder="Días de Plazo" name="dias_plazo" class="form-control" aria-nombreibedby="basic-addon1">
    </div>

    <b><label>Observaciones:</label></b> <br>
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> </span>
      </div>
      
      <input required  class="form-control" name="observaciones" aria-nombreibedby="basic-addon1">

      </input>
    </div>

        <div >
              <a href="clientes" class="btn btn-outline-danger"><i class="fa fa-arrow-left"></i>   Cancelar  </a>

            </div>
  </div>
  <div class="col-sm">
    <b><label>Descuento:</label></b> <br>
    <div class="input-group mb-3">


      <input type="text" required placeholder="Descuento" name="descuento" class="form-control" aria-nombreibedby="basic-addon1">

      <div class="input-group-prepend">
        <span class="input-group-text" id="basic-addon1"> %</span>
      </div>
    </div>
 


    <div style=" float: right; margin-top: 90px; ">
    <button type="submit"  class="btn btn-primary btn-icon-split">
      <span class="icon text-white-50">
        <i class="fas fa-check"></i>
      </span>
      <span class="text">Agregar cliente</span>
    </button>
    </div>

  </div>
  
</div>

</form>







<div>




</div>


</div>


@endsection