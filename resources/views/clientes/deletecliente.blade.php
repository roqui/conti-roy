<!-- Button trigger modal -->
<button type="button"  data-toggle="modal" data-target="#delete{{$cliente->id}}" class="btn"><i class="fas fa-trash-alt"></i></button>

<!-- Modal -->
<div class="modal fade" id="delete{{$cliente->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Eliminar cliente</b></h5> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">


        <form action="eliminarcliente" method="POST">

        {{csrf_field()}} 
        <div class="container">

          <h5>¿Estás seguro de eliminar al cliente <b>{{$cliente->nombre}}?</b></h5>

          <small>Recuerda que sólo podrás eliminar clientes que no tengan pedidos en proceso</small>

          <input hidden value="{{$cliente->id}}" name="id">
        </div>




      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-danger btn-icon-split">
          <span class="icon text-white-50">
            <i class="fas fa-times"></i>
          </span>
          <span class="text">Eliminar</span>
        </button>s
      </div>
    </form>
  </div>
</div>
</div>



<!-- Button trigger modalmmmmmksdfjñlkdjfñlkajsdflñkdnfñlsdnflñkdnflkñdnf -->