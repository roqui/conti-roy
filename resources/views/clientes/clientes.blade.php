@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')



<div style="margin: 50px;">
	
	@if(session('Mensaje'))

	<div class="alert alert-success" id="success-alert">
		<button type="button" class="close" data-dismiss="alert">x</button>
	{{session('Mensaje')}}</div>

	@endif
	@if(session('Mensajee'))

	<div class="alert alert-danger" id="danger-alert">
		<button type="button" class="close" data-dismiss="alert">x</button>
	{{session('Mensajee')}}</div>

	@endif
	@if(session('Mensajea'))

	<div class="alert alert-primary" id="warning-alert">
		<button type="button" class="close" data-dismiss="alert">x</button>
	{{session('Mensajea')}}</div>

	@endif

	

	<div class="card shadow mb-4">

		<div class="card-body">

			<h3>Clientes</h3>
			<h4 style="float: left;">Listado de clientes de Papelerías CONTI</h4>
			@foreach($permisos as $item)
			@if($item->idInterfaz==18)
			<div style="margin-bottom: 20px; text-align-last: right;">
				<a href="agregar_cliente"  class="btn btn-primary btn-icon-split">
					<span class="icon text-white-50">
						<i class="fas fa-plus"></i>
					</span>
					<span class="text">Agregar cliente</span>
				</a>
			</div>
			@break
			@endif
			@endforeach



			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th scope="col">ID</th>
							<th scope="col">Nombre</th>
							<th scope="col">RFC</th>
							<th scope="col">Num. Teléfono</th>
							<th scope="col">Ciudad</th>
							<th></th>
							@foreach($permisos as $item)
							@if($item->idInterfaz==19)
							<th></th>
							<th></th>
							@break
							@endif
							@endforeach
						</tr>
					</thead>

					<tbody>

						@foreach($clientes as $cliente)
						<tr>
							<th scope="row">{{$cliente->id}}</th>
							<td>{{$cliente->nombre}}</td>
							<td>{{$cliente->rfc}}</td>
							<td>{{$cliente->telefono}}</td>
							<td>{{$cliente->ciudad}}</td>
							<td>@include('clientes.infocliente')</td>
							@foreach($permisos as $item)
							@if($item->idInterfaz==19)
							<td>
								
									<form action="editar_cliente" method="POSTS">
										{{csrf_field()}} 

										<input type="hidden" name="id" value="{{$cliente->id}}">
										<button class="btn" type="submit"><i class="fa fa-edit"></i></button>
									</form>
								
							</td>
							<td>@include('clientes.deletecliente')</td>
							@break
							@endif
							@endforeach
						</tr>
						@endforeach
					</tbody>
				</table>

			</div>

		</div>
	</div>




</center>

<div>



	@endsection