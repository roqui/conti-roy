@extends('layouts.plantilla')
@extends('layouts.menu')
@section('main')




	<!-- Begin Page Content -->
        <div class="container-fluid">

        	

          <!-- Page Heading -->
          <h1 class="h3 mb-1 text-gray-800">Permisos del usuario: "{{$user}}"</h1>
          <p class="mb-4">Listado de las funciones a las que el usuario "{{$user}}" tiene acceso.</p>


          <!-- Content Row -->
          <div class="row">

          	<!-- DataTales Example -->
          <div class="card shadow mb-4" style="margin: 10px; width: 100%;">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Permisos</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th scope="col" >ID</th>
				      <th scope="col">Descripción</th>
				      <th scope="col" width="80" >¿Asignada?</th>
				      <th scope="col" width="100"></th>
                     

                    </tr>
                  </thead>

                  <tbody>
                    
                    @foreach ($interf as $interfaz)
                    <tr>
				      <th scope="row">{{$interfaz->id}}</th>
				      <td>{{$interfaz->nombre}</td>
				      <td><div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-info"><i class="fas fa-thumbs-up"></i></div></td>
				      <td>

				          <center><a href="#" class="btn btn-success" style="width: 120px;"><i class="fa fa-plus-square"></i> Agregar</a></center>

				      </td>
				    </tr>
				    @endforeach
                    

                    
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
            

            

            

          </div>

        </div>
        <!-- /.container-fluid -->



@endsection

