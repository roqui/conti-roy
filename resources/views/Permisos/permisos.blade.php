@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')


	<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-1 text-gray-800">Permisos del usuario: </h1>
          @foreach($nombre as $nomb)
          <h1 class="h3 mb-1 text-gray-800">{{$nomb->name}}</h1>
          @endforeach
          <p class="mb-4">Listado de las funciones a las que el usuario tiene acceso.</p>


          <!-- Content Row -->
          <div class="row">

          	

			<div class="accordion" id="accordionExample" style="margin: 10px; width: 100%;">
			  <div class="card">
			  	
			    <div class="card-header" id="headingOne">
			      <h2 class="mb-0">
			        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			          Permisos Para el Surtidor
			        </button>
			      </h2>

					@foreach($permisos as $item)
					@if($item->idInterfaz==24)
			      <form action="{{route('createall')}}">
			      	<input type="hidden" name="idrol" value="1">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-plus-square"></i> Todos</button>
			      </form>

			      <form action="{{route('elimall')}}">
			      	<input type="hidden" name="idrol" value="1">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-minus-square"></i> Todos</button>
			      </form>
			      @break
				  @endif
				  @endforeach
			    </div>


			    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
			      <div class="card-body">

						      	<!--___________________PARA SURTIDORES ______________________ -->

			          	<!-- DataTales Example -->
			          
			            <div class="card-body">
			              <div class="table-responsive">

			                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			                  <thead>
			                    <tr>
			                      <th scope="col" >ID</th>
							      <th scope="col">Descripción</th>
							      <th scope="col" width="80" >¿Asignada?</th>
							      @foreach($permisos as $modif)
							      @if($modif->idInterfaz==24)
							      <th scope="col" width="50">Agregar</th>
							      <th scope="col" width="50">Quitar</th>
							      @break
							      @endif
							      @endforeach
			                     

			                    </tr>
			                  </thead>
			                  
			                  <tbody>
			                  	<?php $cont=0; ?>
			                    @foreach ($surtidores as $interfaz)
			                    <tr>
							      <th scope="row">{{$interfaz->id}}</th>
							      <td>{{$interfaz->descripcion}}</td>
							      <td>

							      	<?php 

							      		if($sur[$cont]=="[]"){
							      			?>
							      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-danger"><i class="fas fa-thumbs-down"></i></div></td>
							      			<?php 
							      		}else{
							      			?>
							      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-success"><i class="fas fa-thumbs-up"></i></div></td>
							      			<?php 
							      		}

							      		$cont++;
							      	?>
							      	
							      	@foreach($permisos as $modif)
							      	@if($modif->idInterfaz==24)
							      <td>


							          <form action="{{route('createpermiso')}}">
							          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
							          	<input type="hidden" name="idusuario" value="{{$user}}">
							          	<center><button class="btn btn-primary " style="width: 50px;" type="submit"> <i class="fa fa-plus-square"></i> </button></center>
							          </form>

							      </td>

							      <td>


							          <form action="{{route('elimpermiso')}}">
							          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
							          	<input type="hidden" name="idusuario" value="{{$user}}">
							          	<center><button class="btn btn-warning " style="width: 50px;" type="submit"> <i class="fa fa-minus-square"></i></button></center>
							          </form>

							      </td>
							      @break
							      @endif
							      @endforeach
							    </tr>
			                    
							    @endforeach
			                    

							    
			                    
			                  </tbody>
			                </table>
			              </div>
			            </div>
			        


			      </div>
			    </div>
			  </div>

			  <div class="card">
			    <div class="card-header" id="headingTwo">
			      <h2 class="mb-0">
			        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsedist" aria-expanded="false" aria-controls="collapseTwo">
			          Permisos para el Jefe de Distribución
			        </button>
			      </h2>
					@foreach($permisos as $item)
					@if($item->idInterfaz==24)
			      <form action="{{route('createall')}}">
			      	<input type="hidden" name="idrol" value="2">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-plus-square"></i> Todos</button>
			      </form>

			      <form action="{{route('elimall')}}">
			      	<input type="hidden" name="idrol" value="2">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-minus-square"></i> Todos</button>
			      </form>
				@break
				@endif
				@endforeach
			    </div>
			    <div id="collapsedist" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
			      <div class="card-body">
			        
						         <!--___________________PARA JEFE DE DISTRIBUCIÓN ______________________ -->

			          	<!-- DataTales Example -->
			          
			            <div class="card-body">
			              <div class="table-responsive">
			                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			                  <thead>
			                    <tr>
			                      <th scope="col" >ID</th>
							      <th scope="col">Descripción</th>
							      <th scope="col" width="80" >¿Asignada?</th>
							      @foreach($permisos as $modif)
							      @if($modif->idInterfaz==24)
							      <th scope="col" width="100">Agregar</th>
							      <th scope="col" width="100">Quitar</th>
							      @break
							      @endif
							      @endforeach
			                     

			                    </tr>
			                  </thead>
			                  
			                  <tbody>
			                  	<?php $cont=0; ?>
			                    @foreach ($distribuidores as $interfaz)
			                    <tr>
							      <th scope="row">{{$interfaz->id}}</th>
							      <td>{{$interfaz->descripcion}}</td>
							      <td>

							      	<?php 

							      		if($dist[$cont]=="[]"){
							      			?>
							      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-danger"><i class="fas fa-thumbs-down"></i></div></td>
							      			<?php 
							      		}else{
							      			?>
							      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-success"><i class="fas fa-thumbs-up"></i></div></td>
							      			<?php 
							      		}

							      		$cont++;
							      	?>
							      	
							      	@foreach($permisos as $modif)
							      	@if($modif->idInterfaz==24)
							      <td>


							          <form action="{{route('createpermiso')}}">
							          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
							          	<input type="hidden" name="idusuario" value="{{$user}}">
							          	<center><button class="btn btn-primary " style="width: 50px;" type="submit"> <i class="fa fa-plus-square"></i> </button></center>
							          </form>

							      </td>

							      <td>


							          <form action="{{route('elimpermiso')}}">
							          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
							          	<input type="hidden" name="idusuario" value="{{$user}}">
							          	<center><button class="btn btn-warning " style="width: 50px;" type="submit"> <i class="fa fa-minus-square"></i></button></center>
							          </form>

							      </td>
							      @break
							      @endif
							      @endforeach
							    </tr>
			                    
							    @endforeach
			                    

							    
			                    
			                  </tbody>
			                </table>
			              </div>
			            </div>
			          



			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" id="headingThree">
			      <h2 class="mb-0">
			        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			          Permisos para el Jefe de Almacén
			        </button>
			      </h2>
				@foreach($permisos as $modif)
				@if($modif->idInterfaz==24)
			      <form action="{{route('createall')}}">
			      	<input type="hidden" name="idrol" value="3">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-plus-square"></i> Todos</button>
			      </form>

			      <form action="{{route('elimall')}}">
			      	<input type="hidden" name="idrol" value="3">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-minus-square"></i> Todos</button>
			      </form>
			      @break
			      @endif
			      @endforeach

			    </div>
			    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
			      <div class="card-body">
			        
						      		<!--___________________PARA JEFE DE ALMACÉN ______________________ -->

			          	<!-- DataTales Example -->
			          
			            <div class="card-body">
			              <div class="table-responsive">
			                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			                  <thead>
			                    <tr>
			                      <th scope="col" >ID</th>
							      <th scope="col">Descripción</th>
							      <th scope="col" width="80" >¿Asignada?</th>
							      @foreach($permisos as $modif)
							      @if($modif->idInterfaz==24)
							      <th scope="col" width="100">Agregar</th>
							      <th scope="col" width="100">Quitar</th>
							      @break
							      @endif
							      @endforeach
			                     

			                    </tr>
			                  </thead>
			                  
			                  <tbody>
			                  	<?php $cont=0; ?>
			                    @foreach ($almacenistas as $interfaz)
			                    <tr>
							      <th scope="row">{{$interfaz->id}}</th>
							      <td>{{$interfaz->descripcion}}</td>
							      <td>

							      	<?php 

							      		if($alm[$cont]=="[]"){
							      			?>
							      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-danger"><i class="fas fa-thumbs-down"></i></div></td>
							      			<?php 
							      		}else{
							      			?>
							      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-success"><i class="fas fa-thumbs-up"></i></div></td>
							      			<?php 
							      		}

							      		$cont++;
							      	?>
							      	
							      	@foreach($permisos as $modif)
							      	@if($modif->idInterfaz==24)
							      <td>


							          <form action="{{route('createpermiso')}}">
							          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
							          	<input type="hidden" name="idusuario" value="{{$user}}">
							          	<center><button class="btn btn-primary " style="width: 50px;" type="submit"> <i class="fa fa-plus-square"></i> </button></center>
							          </form>

							      </td>

							      <td>


							          <form action="{{route('elimpermiso')}}">
							          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
							          	<input type="hidden" name="idusuario" value="{{$user}}">
							          	<center><button class="btn btn-warning " style="width: 50px;" type="submit"> <i class="fa fa-minus-square"></i></button></center>
							          </form>

							      </td>
							      @break
							      @endif
							      @endforeach
							    </tr>
			                    
							    @endforeach
			                    

							    
			                    
			                  </tbody>
			                </table>
			              </div>
			            </div>
          


			      </div>
			    </div>
			  </div>

			  <div class="card">
			    <div class="card-header" id="headingfor">
			      <h2 class="mb-0">
			        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsefor" aria-expanded="false" aria-controls="collapsefor">
			          Permisos para el Comprador
			        </button>
			      </h2>
					@foreach($permisos as $modif)
					@if($modif->idInterfaz==24)
			      <form action="{{route('createall')}}">
			      	<input type="hidden" name="idrol" value="5">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-plus-square"></i> Todos</button>
			      </form>

			      <form action="{{route('elimall')}}">
			      	<input type="hidden" name="idrol" value="5">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-minus-square"></i> Todos</button>
			      </form>
			      @break
			      @endif
			      @endforeach

			    </div>
			    <div id="collapsefor" class="collapse" aria-labelledby="headingfor" data-parent="#accordionExample">
			      <div class="card-body">
						        	<!--___________________PARA JEFE DE COMPRADOR ______________________ -->

			          	<!-- DataTales Example -->
			          
			            <div class="card-body">
			              <div class="table-responsive">
			                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			                  <thead>
			                    <tr>
			                      <th scope="col" >ID</th>
							      <th scope="col">Descripción</th>
							      <th scope="col" width="80" >¿Asignada?</th>
							      @foreach($permisos as $modif)
							      @if($modif->idInterfaz==24)
							      <th scope="col" width="100">Agregar</th>
							      <th scope="col" width="100">Quitar</th>
							      @break
							      @endif
							      @endforeach
			                     

			                    </tr>
			                  </thead>
			                  
			                  <tbody>
			                  	<?php $cont=0; ?>
			                    @foreach ($compradores as $interfaz)
			                    <tr>
							      <th scope="row">{{$interfaz->id}}</th>
							      <td>{{$interfaz->descripcion}}</td>
							      <td>

							      	<?php 

							      		if($comp[$cont]=="[]"){
							      			?>
							      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-danger"><i class="fas fa-thumbs-down"></i></div></td>
							      			<?php 
							      		}else{
							      			?>
							      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-success"><i class="fas fa-thumbs-up"></i></div></td>
							      			<?php 
							      		}

							      		$cont++;
							      	?>
							      	
							      	@foreach($permisos as $modif)
							      	@if($modif->idInterfaz==24)
							      <td>


							          <form action="{{route('createpermiso')}}">
							          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
							          	<input type="hidden" name="idusuario" value="{{$user}}">
							          	<center><button class="btn btn-primary " style="width: 50px;" type="submit"> <i class="fa fa-plus-square"></i> </button></center>
							          </form>

							      </td>

							      <td>


							          <form action="{{route('elimpermiso')}}">
							          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
							          	<input type="hidden" name="idusuario" value="{{$user}}">
							          	<center><button class="btn btn-warning " style="width: 50px;" type="submit"> <i class="fa fa-minus-square"></i></button></center>
							          </form>

							      </td>
							      @break
							      @endif
							      @endforeach
							    </tr>
			                    
							    @endforeach
			                    

							    
			                    
			                  </tbody>
			                </table>
			              </div>
			            </div>



			      </div>
			    </div>
			  </div>

			  <div class="card">
				    <div class="card-header" id="headingfiv">
				      <h2 class="mb-0">
				        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsefiv" aria-expanded="false" aria-controls="collapsefiv">
				          Permisos para el Gerente de Operaciones
				        </button>
				      </h2>
					@foreach($permisos as $modif)
					@if($modif->idInterfaz==24)
				      <form action="{{route('createall')}}">
			      	<input type="hidden" name="idrol" value="4">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-plus-square"></i> Todos</button>
			      </form>

			      <form action="{{route('elimall')}}">
			      	<input type="hidden" name="idrol" value="4">
					<input type="hidden" name="idusuario" value="{{$user}}">
			      	<button type="submit" class="btn" style="border: 3px; float: right;"><i class="fa fa-minus-square"></i> Todos</button>
			      </form>
			      @break
			      @endif
			      @endforeach

				    </div>
				    <div id="collapsefiv" class="collapse" aria-labelledby="headingfiv" data-parent="#accordionExample">
				      <div class="card-body">
				        	
									      		<!--___________________PARA GERENTE DE OPERACIONES ______________________ -->

					          	<!-- DataTales Example -->
					          
					            <div class="card-body">
					              <div class="table-responsive">
					                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					                  <thead>
					                    <tr>
					                      <th scope="col" >ID</th>
									      <th scope="col">Descripción</th>
									      <th scope="col" width="80" >¿Asignada?</th>
									      @foreach($permisos as $modif)
									      @if($modif->idInterfaz==24)
									      <th scope="col" width="100">Agregar</th>
									      <th scope="col" width="100">Quitar</th>
									      @break
									      @endif
									      @endforeach
					                     

					                    </tr>
					                  </thead>
					                  
					                  <tbody>
					                  	<?php $cont=0; ?>
					                    @foreach ($gerente as $interfaz)
					                    <tr>
									      <th scope="row">{{$interfaz->id}}</th>
									      <td>{{$interfaz->descripcion}}</td>
									      <td>

									      	<?php 

									      		if($ger[$cont]=="[]"){
									      			?>
									      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-danger"><i class="fas fa-thumbs-down"></i></div></td>
									      			<?php 
									      		}else{
									      			?>
									      			<div style="text-align: center; width: 100px" class="m-0 font-weight-bold text-success"><i class="fas fa-thumbs-up"></i></div></td>
									      			<?php 
									      		}

									      		$cont++;
									      	?>
									      	
									      	@foreach($permisos as $modif)
									      	@if($modif->idInterfaz==24)
									      <td>


									          <form action="{{route('createpermiso')}}">
									          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
									          	<input type="hidden" name="idusuario" value="{{$user}}">
									          	<center><button class="btn btn-primary " style="width: 50px;" type="submit"> <i class="fa fa-plus-square"></i> </button></center>
									          </form>

									      </td>

									      <td>


									          <form action="{{route('elimpermiso')}}">
									          	<input type="hidden" name="idinterfaz" value="{{$interfaz->id}}">
									          	<input type="hidden" name="idusuario" value="{{$user}}">
									          	<center><button class="btn btn-warning " style="width: 50px;" type="submit"> <i class="fa fa-minus-square"></i></button></center>
									          </form>

									      </td>
									      @break
									      @endif
									      @endforeach
									    </tr>
					                    
									    @endforeach
					                    

									    
					                    
					                  </tbody>
					                </table>
					              </div>
					            </div>

				      </div>
				    </div>
				  </div>



			</div>



            

         
            
         



         </div><!--class row-->

        </div>
        <!-- /.container-fluid -->



@endsection