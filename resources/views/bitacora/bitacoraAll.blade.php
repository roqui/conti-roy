@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')



<div style="margin: 50px;">
	

	<div class="card shadow mb-4">

		<div class="card-body">
			<h3>Bitácora</h3>
	<h4>Bitácora de procesos en todas las sucursales de Papelerías CONTI</h4>
	
		@if(session('Mensaje'))
  
  <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensaje')}}</div>

  @endif
  @if(session('Mensajee'))
  
  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajee')}}</div>

  @endif
  @if(session('Mensajea'))
  
  <div class="alert alert-primary" id="warning-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajea')}}</div>

  @endif

	
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th scope="col">ID</th>
							<th scope="col">Sucursal</th>
							<th scope="col">Usuario</th>
							<th scope="col">Acción</th>
							<th>Fecha</th>
						</tr>
					</thead>

					<tbody>
						
						@foreach($bitacoras as $bitacora)

						<tr>
							<td>{{$bitacora->id}}</td>
							<td>{{$bitacora->nombre}}</td>
							<td>{{$bitacora->usuario}}</td>
							<td>{{$bitacora->descripcion}}</td>
							<td>{{$bitacora->created_at}}</td>
						</tr>

						@endforeach

						

					</tbody>
				</table>

			</div>

		</div>
	</div>




</center>

<div>



	@endsection