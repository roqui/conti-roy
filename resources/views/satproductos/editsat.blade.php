<!-- Button trigger modal -->
<button  class="btn" data-toggle="modal"

data-target="#editproducto{{$producto->id}}"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Editar línea"></i>

</button>

<!-- Modal -->
<div class="modal fade" id="editproducto{{$producto->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar línea <b>{{$producto->descripcion}}</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form  action="editarsat" method="POST">
        {{csrf_field()}} 
        <input type="hidden" name="_method" value="POST">

        <label>Nuevo Código:</label> <br>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"> </span>
          </div>
          <input value="{{$producto->id}}" hidden name="id">
          <input type="text" name="codigo" class="form-control" placeholder="{{$producto->codigo}}" value="{{$producto->codigo}}"  aria-descripcionibedby="basic-addon1">
        </div>

        <label>Nueva descripción:</label> <br>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"> </span>
          </div>
          <input value="{{$producto->id}}" hidden name="id">
          <input type="text" name="descripcion" class="form-control" placeholder="{{$producto->descripcion}}" value="{{$producto->descripcion}}"  aria-descripcionibedby="basic-addon1">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary btn-icon-split">
          <span class="icon text-white-50">
            <i class="fas fa-check"></i>
          </span>
          <span class="text">Actualizar Producto</span>
        </button>
      </div>
    </form>
  </div>
</div>
</div>
