@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div style="margin:50px;" >


  @if(session('Mensaje'))
  
  <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensaje')}}</div>

  @endif
  @if(session('Mensajee'))
  
  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajee')}}</div>

  @endif
  @if(session('Mensajea'))
  
  <div class="alert alert-primary" id="warning-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajea')}}</div>

  @endif



  <div class="card shadow mb-4" style="width: 100%; margin: 0;">




    <div class="card-body" >

      <h3>Productos SAT</h3>
      <h5> Listado de Productos SAT</h5>
      <div style="margin: 10px; float: right;">
       @include('satproductos.nuevosat')

     </div>
     <div class="table-responsive" >
      <table  class="table table-bordered" id="dataTable"   cellspacing="0">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Código SAT</th>
            <th scope="col">Descripción</th>
            <th></th>
            <th></th>
          </tr>
        </thead>

        

        <tbody>
          @foreach($satproductos as $producto)
          <tr>
          <td>{{$producto->id}}</td>
          <td>{{$producto->codigo}}</td>
          <td>{{$producto->descripcion}}</td>
      
            <th style="width: 50px;">
              @include('satproductos.editsat')
            </th>
            <th style="width: 50px;">
              @include('satproductos.deletesat')
            </th>
         
          </tr>
            @endforeach
        </tbody>
      </table>

    </div>

  </div>
</div>





</div>



@endsection