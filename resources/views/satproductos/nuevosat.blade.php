<button href="#" class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#exampleModal">
	<span class="icon text-white-50">
		<i class="fas fa-plus"></i>
	</span>
	<span class="text">Agregar Producto</span>
</button>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Nuevo Producto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<form action="nuevosat" method="post">
					{{csrf_field()}} 

					<b><label>Código</label></b> <br>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1"></span>
						</div>
						<input type="text" name="codigo" class="form-control" placeholder="Código SAT del producto" aria-label="Username" aria-describedby="basic-addon1">
					</div>

					<b><label>Descipción</label></b>

					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1"></span>
						</div>
						<input type="text" name="descripcion" class="form-control" placeholder="Descripción del producto" aria-label="Username" aria-describedby="basic-addon1">
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary btn-icon-split">
						<span class="icon text-white-50">
							<i class="fas fa-check"></i>
						</span>
						<span class="text">Agregar</span>
					</button>				
				</div>
			</form>
		</div>
	</div>
</div>