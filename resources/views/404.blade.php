@extends('layouts.plantilla')
@extends('layouts.menu')
@section('main')

 <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- 404 Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="404">404</div>
            <p class="lead text-gray-800 mb-5">Página en construcción</p>
            
            <p class="text-gray-500 mb-0">Seguimos trabajando</p>
            <a href="index">&larr; Regresar al inicio</a>
          </div>

        </div>
        <!-- /.container-fluid -->


@endsection