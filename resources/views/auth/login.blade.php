
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> CONTI Papelerias | Iniciar Sesión</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body style="background-image: url('img/back.png'); background-repeat: no-repeat;background-size: cover;" >

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9" >

        <div style="width: 70%; position: relative; left: 130px;"  class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div style="margin: 30px;">
              <div class="text-center">
                <h4>¡Bienvenido de nuevo!</h4>
                    <div>
                      <img style="margin: 10px;" src="img/logo1.png">
                    </div>
                  </div>
                  <!-- inicio de sesion con usuario-->
                  <label>Usuario</label>
                  <form class="user" method="POST" action="{{ route('login') }}" method="POST">
                      @csrf 
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1">  <i class="fas fa-user-circle"></i></span>
                        </div>
                        <input id="login" type="login" class="form-control @error('login') is-invalid @enderror" name="login" value="{{ old('login') }}" required autocomplete placeholder="Introduce tu usuario" autofocus">

                        @error('login')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Usuario y/o contraseña incorrecta, verifique de nuevo</strong>
                                    </span>
                                @enderror

                                               </div>



                    <label>Contraseña</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1">  <i class="fas fa-unlock-alt"></i></span>
                        </div>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete placeholder="Introduce tu contraseña">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        
                                    </span>
                                @enderror
                      </div>

                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"{{ old('remember') ? 'checked' : '' }} >

                                    <label class="form-check-label" for="remember">
                                        Recuérdame
                                    </label>
                      </div>
                    </div>
                    
                    <button  type="submit" style="position: relative; float: right; margin-bottom:20px; margin-top: 20px; " class="btn btn-info btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-sign-in-alt"></i>
                    </span>
                    <span class="text">Iniciar sesión</span>
                  </button>

                    
                  </form>
                  <hr>
                  
            </div>

          </div>
        </div>

      </div>

    </div>

  </div>

 

</body>

</html>
