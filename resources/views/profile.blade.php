@extends('layouts.plantilla')
@extends('layouts.menu')

@section('main')

<div class="content" style="margin: 15px;">

	<div class="jumbotron" style="background-color: white;">
  
    <h3> <b>Información de empleado </h3></b> 

     @if(session('Mensaje'))
      
      <div class="alert alert-success" id="success-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensaje')}}</div>

    @endif
    @if(session('Mensajee'))
      
      <div class="alert alert-danger" id="danger-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajee')}}</div>

    @endif
    @if(session('Mensajea'))
      
      <div class="alert alert-primary" id="warning-alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
      {{session('Mensajea')}}</div>

    @endif


  <hr class="my-4">

  <div class="container">
  <div class="row">
    <div class="col">



			          <b>Nombre: </b> {{ Auth::user()->name }} <br>

                <b>Usuario:</b> {{ Auth::user()->usuario }} <br>

      <b>Email:</b> {{ Auth::user()->email }} <br>
      
            <b>Teléfono: </b> +52 {{ Auth::user()->telefono }} <br>
      <b>Fecha de registro: </b> {{ Auth::user()->created_at }} <br>

      <b>Dirección:</b> {{ Auth::user()->calle }} 

    </div>
    <div class="col">
      	<div style="margin:15px;">
  		<img style="width: 200px; height: 200px; border-radius: 5px; " src="{{ Auth::user()->foto }}"><br>
  	</div>
       <form style="position: relative; left: 5px;" method="POST" enctype="multipart/form-data" action="/photo">
       @csrf
         <h5>¿quieres cambiar tu foto?</h5>
       <input type="file" class="btn-sm" style="border-radius: 10px;" name="avatar"> <br> 


       <button type="submit" style="position: relative;left: 9px;" class="btn btn-info tn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-check-circle"></i>
                    </span>
                    <span class="text">Aceptar</span>
                  </button>
       </form>
    </div>
  </div>
</div>

</div>
	
</div> 

@endsection