@extends('layouts.plantilla')
@extends('layouts.menu')
@section('main')
<div style="margin:30px; text-decoration: none;">

  @if(session('Mensajeu'))

  <div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajeu')}}</div>

  @endif
  @if(session('Mensajeue'))

  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajeue')}}</div>

  @endif
  @if(session('Mensajee'))

  <div class="alert alert-danger" id="danger-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajee')}}</div>

  @endif
  @if(session('Mensajeua'))

  <div class="alert alert-warning" id="warning-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
  {{session('Mensajeua')}}</div>

  @endif



  <div class="card shadow mb-4">

    <div class="card-body">
      <h3>Usuarios</h3>
      <h4 style="float: left;">Lista de usuarios del sistema de <b>
        <?php
        $id = Auth::user()->idSucursal; 

        $sucursal = DB::table('destinos')->select('nombre')->where('id', '=', $id)->first();?>
        @foreach($sucursal as $key)
        {{$key}}
        @endforeach


      </b>
    </h4>

    @foreach($permisos as $add_empleado)
    @if($add_empleado->idInterfaz==21)
    <div style="float: right; margin-bottom: 10px; margin-top: 10px;" >
     <a href="newusers" class="btn btn-primary btn-icon-split">
      <span class="icon text-white-50">
        <i class="fas fa-plus"></i>
      </span>
      <span class="text">Agregar Usuario</span>

    </a> <br>
    </div>
    @break
    @endif
    @endforeach



  <div class="table-responsive">
    <table style="text-decoration: none;" class="table table-bordered" id="dataTable"  cellspacing="0">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th>Foto</th>
          <th scope="col">Nombre</th>
          <th scope="col">Puesto</th>
          <th scope="col">Sucursal</th>
          <th scope="col">Usuario</th>
          <th></th>
          @foreach($permisos as $item)
          @if($item->idInterfaz==22)
          <th></th>
          <th></th>
          @break
          @endif
          @endforeach
          @foreach($permisos as $item)
          @if($item->idInterfaz==23)
          <th></th>
          @break
          @endif
          @endforeach
        </tr>
      </thead>

      <tbody>
        @foreach ($users as $user)
        <tr>
          <th scope="row">{{$user->id}}</th>

          <th scope="row">
            <img style="width: 50px;" src="{{$user->foto}}">
          </th>
          <td>{{$user->name}}</td>
          <td>{{$user->descripcion}}</td>
          <td>{{$user->nombre}}</td>
          <td>{{$user->usuario}}</td>

          <td>
            @include('editusers')
          </td>
          @foreach($permisos as $item)
          @if($item->idInterfaz==22)
          <td>
            <form action="actuser">
              <input type="hidden" name="id" value="{{$user->id}}">
              <button class="btn" type="submit"><i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Editar usuario"></i></button>
            </form>        
          </td>
          <td>
            @include('delete_usuario') 
          </td> 
          @break
          @endif
          @endforeach
          @foreach($permisos as $ver_permisos)
          @if($ver_permisos->idInterfaz==23)
          <td>
            <center>
              <form action="{{route('permiso')}}">
                <input type="hidden" name="usuario" value="{{$user->id}}">

                <button class="btn " type="submit" > <i class="fas fa-check-square" data-toggle="tooltip" data-placement="top" title="Permisos de usuario"> </i></button>
              </form>

            </center>
          </td>
          @break
          @endif
          @endforeach
        </tr>
        @endforeach
      </tbody>
    </table>

  </div>

</div>
</div>



<div style="float: right;">
 <a href="userexcel" style="margin-bottom:10px; " class="btn btn-success btn-icon-split">
  <span class="icon text-white-50">
    <i class="fas fa-download"></i>
  </span>
  <span class="text">Obtener Excel</span>    
</a> <br>
</div>

</div>


@endsection


