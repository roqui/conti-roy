<?php

use Illuminate\Database\Seeder;

class PuestosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('puestos')->insert([
        	'id'=>'1',
            'descripcion'=>'Surtidor'
        ]);

        \DB::table('puestos')->insert([
        	'id'=>'2',
            'descripcion'=>'Jefe de Distribución'
        ]);

        \DB::table('puestos')->insert([
        	'id'=>'3',
            'descripcion'=>'Jefe de Almacén'
        ]);

        \DB::table('puestos')->insert([
        	'id'=>'4',
            'descripcion'=>'Gerente de Operaciones'
        ]);

        \DB::table('puestos')->insert([
        	'id'=>'5',
            'descripcion'=>'Comprador'
        ]);

        \DB::table('puestos')->insert([
            'id'=>'6',
            'descripcion'=>'Conductor'
        ]);
    }
}
