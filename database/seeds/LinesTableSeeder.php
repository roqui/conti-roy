<?php

use Illuminate\Database\Seeder;

class LinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	\DB::table('lineas')->insert([
    		'id'=>'1',
            'descr' => 'Arte'
           
        ]);

        \DB::table('lineas')->insert([
            'id'=>'2',
            'descr' => 'Artículos de limpieza'
           
        ]);

        \DB::table('lineas')->insert([
            'id'=>'3',
            'descr' => 'Artículos escolares'
           
        ]);

        \DB::table('lineas')->insert([
            'id'=>'4',
            'descr' => 'Artículos temporales'
           
        ]);

        \DB::table('lineas')->insert([
            'id'=>'5',
            'descr' => 'Cómputo y consumibles'
           
        ]);

        \DB::table('lineas')->insert([
            'id'=>'6',
            'descr' => 'Cuadernos'
           
        ]);

        \DB::table('lineas')->insert([
            'id'=>'7',
            'descr' => 'Encuadernación'
           
        ]);
       
       \DB::table('lineas')->insert([
            'id'=>'8',
            'descr' => 'Escritura'
           
        ]);
       \DB::table('lineas')->insert([
            'id'=>'9',
            'descr' => 'Formas'
           
        ]);

       \DB::table('lineas')->insert([
            'id'=>'10',
            'descr' => 'Manualidades, mercería y fiesta'
           
        ]);
       \DB::table('lineas')->insert([
            'id'=>'11',
            'descr' => 'Material didáctico'
           
        ]);

       \DB::table('lineas')->insert([
            'id'=>'12',
            'descr' => 'Mobiliario'
           
        ]);
       \DB::table('lineas')->insert([
            'id'=>'13',
            'descr' => 'Mochilas y portafolios'
           
        ]);
       \DB::table('lineas')->insert([
            'id'=>'14',
            'descr' => 'Oficina'
           
        ]);

       \DB::table('lineas')->insert([
            'id'=>'15',
            'descr' => 'Papel'
           
        ]);

       \DB::table('lineas')->insert([
            'id'=>'16',
            'descr' => 'Pegamentos y adhesivos'
           
        ]);

       \DB::table('lineas')->insert([
            'id'=>'17',
            'descr' => 'Servicios'
           
        ]);
    }
}
