<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TransaccionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hora=Carbon::now();

        DB::table('transaccions')->insert([
            'id'=>'1',
            'costoT'=>'800',
            'Estado'=>'1',
            'id_origen'=>'1',
            'id_destino'=>'3',
            'observaciones'=>'Transacción Importante',
            'usuario'=>'Maria Fernanda Morales Medina',

            'created_at'=>$hora
        ]);

        DB::table('transaccions')->insert([
            'id'=>'2',
            'costoT'=>'6700',
            'Estado'=>'2',
            'id_origen'=>'1',
            'id_destino'=>'2',
            'observaciones'=>'Transacción Nueva',
            'usuario'=>'Rodrigo Alejandro Quintana Gaytán',

            'created_at'=>$hora
        ]);

        DB::table('transaccions')->insert([
            'id'=>'3',
            'costoT'=>'890',
            'Estado'=>'4',
            'id_origen'=>'1',
            'id_destino'=>'2',
            'observaciones'=>'Transacción Pequeña',
            'usuario'=>'Karen Daniela Olivo Solorio',

            'created_at'=>$hora
        ]);

        DB::table('transaccions')->insert([
            'id'=>'4',
            'costoT'=>'5000',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Transacción Reciente',
            'usuario'=>'Jeanette Monserrat Avilés Dávalos',

            'created_at'=>$hora
        ]);

        DB::table('transaccions')->insert([
            'id'=>'5',
            'costoT'=>'650',
            'Estado'=>'2',
            'id_origen'=>'1',
            'id_destino'=>'1',
            'observaciones'=>'Transacción 1',
            'usuario'=>'Fernando Raya Jacobo',

            'created_at'=>$hora
        ]);

        DB::table('transaccions')->insert([
            'id'=>'6',
            'costoT'=>'800',
            'Estado'=>'4',
            'id_origen'=>'1',
            'id_destino'=>'1',
            'observaciones'=>'Transacción 2',
            'usuario'=>'Arnulfo Damián Sosa',

            'created_at'=>$hora
        ]);

        DB::table('transaccions')->insert([
            'id'=>'7',
            'costoT'=>'1000',
            'Estado'=>'2',
            'id_origen'=>'1',
            'id_destino'=>'1',
            'observaciones'=>'Transacción 3',
            'usuario'=>'Jose Miguel Trejo Covian',

            'created_at'=>$hora
        ]);

        DB::table('transaccions')->insert([
            'id'=>'8',
            'costoT'=>'250',
            'Estado'=>'1',
            'id_origen'=>'1',
            'id_destino'=>'3',
            'observaciones'=>'Transacción 4',
            'usuario'=>'David El Tres Apellidos',

            'created_at'=>$hora
        ]);

        DB::table('transaccions')->insert([
            'id'=>'9',
            'costoT'=>'5500',
            'Estado'=>'2',
            'id_origen'=>'1',
            'id_destino'=>'3',
            'observaciones'=>'Transacción 5',
            'usuario'=>'Brandon Ceja Cruz',

            'created_at'=>$hora
        ]);

        DB::table('transaccions')->insert([
            'id'=>'10',
            'costoT'=>'6000',
            'Estado'=>'2',
            'id_origen'=>'1',
            'id_destino'=>'3',
            'observaciones'=>'Transacción 6',
            'usuario'=>'Alan Martin Fuentes',

            'created_at'=>$hora
        ]);
        
        
    }
}