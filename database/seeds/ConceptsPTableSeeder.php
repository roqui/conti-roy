<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ConceptsPTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('concepts')->insert([
            'id'=>'61',
            'idPedido'=>'1',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'id'=>'1',
            'idPedido'=>'1',
            'idProducto'=>'2',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
        	'id'=>'2',
            'idPedido'=>'1',
            'idProducto'=>'3',
            'cantidad'=>'105',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'3',
            'idPedido'=>'1',
            'idProducto'=>'4',
            'cantidad'=>'200',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'4',
            'idPedido'=>'1',
            'idProducto'=>'5',
            'cantidad'=>'150',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'5',
            'idPedido'=>'1',
            'idProducto'=>'6',
            'cantidad'=>'896',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);



         \DB::table('concepts')->insert([
            'id'=>'62',
            'idPedido'=>'2',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-02-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'id'=>'6',
            'idPedido'=>'2',
            'idProducto'=>'2',
            'cantidad'=>'120',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-02-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'7',
            'idPedido'=>'2',
            'idProducto'=>'3',
            'cantidad'=>'30',
            'cantidadcheck'=>'35',
            'descuento'=>'6',
            'subtotal'=>'350',
            'estado'=>'1',
            'created_at'=>'2019-02-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'8',
            'idPedido'=>'2',
            'idProducto'=>'4',
            'cantidad'=>'852',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-02-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'9',
            'idPedido'=>'2',
            'idProducto'=>'5',
            'cantidad'=>'987',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-02-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'10',
            'idPedido'=>'2',
            'idProducto'=>'6',
            'cantidad'=>'89',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-02-20 04:33:05'
        ]);
        \DB::table('concepts')->insert([
            'id'=>'63',
            'idPedido'=>'3',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-03-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'id'=>'11',
            'idPedido'=>'3',
            'idProducto'=>'2',
            'cantidad'=>'196',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-03-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'12',
            'idPedido'=>'3',
            'idProducto'=>'3',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-03-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'13',
            'idPedido'=>'3',
            'idProducto'=>'4',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-03-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'14',
            'idPedido'=>'3',
            'idProducto'=>'5',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-03-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'15',
            'idPedido'=>'3',
            'idProducto'=>'6',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-03-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'64',
            'idPedido'=>'4',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-04-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'id'=>'16',
            'idPedido'=>'4',
            'idProducto'=>'2',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-04-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'17',
            'idPedido'=>'4',
            'idProducto'=>'3',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-04-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'18',
            'idPedido'=>'4',
            'idProducto'=>'4',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-04-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'19',
            'idPedido'=>'4',
            'idProducto'=>'5',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-04-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'20',
            'idPedido'=>'4',
            'idProducto'=>'6',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-04-20 04:33:05'
        ]);

         \DB::table('concepts')->insert([
            'id'=>'65',
            'idPedido'=>'5',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-05-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'id'=>'21',
            'idPedido'=>'5',
            'idProducto'=>'2',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-05-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'22',
            'idPedido'=>'5',
            'idProducto'=>'3',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-05-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'23',
            'idPedido'=>'5',
            'idProducto'=>'4',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-05-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'24',
            'idPedido'=>'5',
            'idProducto'=>'5',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-05-20 04:33:05'
        ]);

          \DB::table('concepts')->insert([
            'id'=>'25',
            'idPedido'=>'5',
            'idProducto'=>'6',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-05-20 04:33:05'
        ]);

           \DB::table('concepts')->insert([
            'id'=>'66',
            'idPedido'=>'6',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-06-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
        	'id'=>'26',
            'idPedido'=>'6',
            'idProducto'=>'2',
            'cantidad'=>'105',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-06-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'27',
            'idPedido'=>'6',
            'idProducto'=>'3',
            'cantidad'=>'200',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-06-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'28',
            'idPedido'=>'6',
            'idProducto'=>'4',
            'cantidad'=>'150',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-06-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'29',
            'idPedido'=>'6',
            'idProducto'=>'5',
            'cantidad'=>'896',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-06-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'30',
            'idPedido'=>'6',
            'idProducto'=>'6',
            'cantidad'=>'120',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-06-20 04:33:05'
        ]);

         \DB::table('concepts')->insert([
            'id'=>'67',
            'idPedido'=>'7',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-07-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'31',
            'idPedido'=>'7',
            'idProducto'=>'2',
            'cantidad'=>'30',
            'cantidadcheck'=>'35',
            'descuento'=>'6',
            'subtotal'=>'350',
            'estado'=>'1',
            'created_at'=>'2019-07-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'32',
            'idPedido'=>'7',
            'idProducto'=>'3',
            'cantidad'=>'852',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-07-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'33',
            'idPedido'=>'7',
            'idProducto'=>'4',
            'cantidad'=>'987',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-07-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'34',
            'idPedido'=>'7',
            'idProducto'=>'5',
            'cantidad'=>'89',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-07-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'35',
            'idPedido'=>'7',
            'idProducto'=>'6',
            'cantidad'=>'196',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-07-20 04:33:05'
        ]);

         \DB::table('concepts')->insert([
            'id'=>'68',
            'idPedido'=>'8',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-08-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'id'=>'36',
            'idPedido'=>'8',
            'idProducto'=>'2',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-08-20 04:33:05'
        ]);

          \DB::table('concepts')->insert([
            'id'=>'37',
            'idPedido'=>'8',
            'idProducto'=>'3',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-08-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
        	'id'=>'38',
            'idPedido'=>'8',
            'idProducto'=>'4',
            'cantidad'=>'105',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-08-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'39',
            'idPedido'=>'8',
            'idProducto'=>'5',
            'cantidad'=>'200',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-08-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'40',
            'idPedido'=>'8',
            'idProducto'=>'6',
            'cantidad'=>'150',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-08-20 04:33:05'
        ]);

         \DB::table('concepts')->insert([
            'id'=>'69',
            'idPedido'=>'9',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-09-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'id'=>'41',
            'idPedido'=>'9',
            'idProducto'=>'2',
            'cantidad'=>'896',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-09-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'42',
            'idPedido'=>'9',
            'idProducto'=>'3',
            'cantidad'=>'120',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-09-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'43',
            'idPedido'=>'9',
            'idProducto'=>'4',
            'cantidad'=>'30',
            'cantidadcheck'=>'35',
            'descuento'=>'6',
            'subtotal'=>'350',
            'estado'=>'1',
            'created_at'=>'2019-09-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'44',
            'idPedido'=>'9',
            'idProducto'=>'5',
            'cantidad'=>'852',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-09-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'45',
            'idPedido'=>'9',
            'idProducto'=>'6',
            'cantidad'=>'987',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-09-20 04:33:05'
        ]);


         \DB::table('concepts')->insert([
            'id'=>'70',
            'idPedido'=>'10',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-10-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'id'=>'46',
            'idPedido'=>'10',
            'idProducto'=>'2',
            'cantidad'=>'89',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-10-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'47',
            'idPedido'=>'10',
            'idProducto'=>'3',
            'cantidad'=>'196',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-10-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'48',
            'idPedido'=>'10',
            'idProducto'=>'4',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-10-20 04:33:05'
        ]);

          \DB::table('concepts')->insert([
            'id'=>'49',
            'idPedido'=>'10',
            'idProducto'=>'5',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-10-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
        	'id'=>'50',
            'idPedido'=>'10',
            'idProducto'=>'6',
            'cantidad'=>'105',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-10-20 04:33:05'
        ]);

          \DB::table('concepts')->insert([
            'id'=>'71',
            'idPedido'=>'11',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-11-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'51',
            'idPedido'=>'11',
            'idProducto'=>'2',
            'cantidad'=>'200',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-11-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'52',
            'idPedido'=>'11',
            'idProducto'=>'3',
            'cantidad'=>'150',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-11-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'53',
            'idPedido'=>'11',
            'idProducto'=>'4',
            'cantidad'=>'896',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-11-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'54',
            'idPedido'=>'11',
            'idProducto'=>'5',
            'cantidad'=>'120',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-11-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'55',
            'idPedido'=>'11',
            'idProducto'=>'6',
            'cantidad'=>'30',
            'cantidadcheck'=>'35',
            'descuento'=>'6',
            'subtotal'=>'350',
            'estado'=>'1',
            'created_at'=>'2019-11-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'72',
            'idPedido'=>'12',
            'idProducto'=>'1',
            'cantidad'=>'100',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'56',
            'idPedido'=>'12',
            'idProducto'=>'2',
            'cantidad'=>'852',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'57',
            'idPedido'=>'12',
            'idProducto'=>'3',
            'cantidad'=>'987',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'58',
            'idPedido'=>'12',
            'idProducto'=>'4',
            'cantidad'=>'89',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'59',
            'idPedido'=>'12',
            'idProducto'=>'5',
            'cantidad'=>'196',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'id'=>'60',
            'idPedido'=>'12',
            'idProducto'=>'6',
            'cantidad'=>'230',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-12-20 04:33:05'
        ]);


         \DB::table('concepts')->insert([
            'idPedido'=>'13',
            'idProducto'=>'1',
            'cantidad'=>'259',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'idPedido'=>'13',
            'idProducto'=>'2',
            'cantidad'=>'254',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'13',
            'idProducto'=>'3',
            'cantidad'=>'102',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'13',
            'idProducto'=>'4',
            'cantidad'=>'852',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'13',
            'idProducto'=>'5',
            'cantidad'=>'123',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'13',
            'idProducto'=>'6',
            'cantidad'=>'896',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


         \DB::table('concepts')->insert([
            'idPedido'=>'14',
            'idProducto'=>'1',
            'cantidad'=>'147',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'idPedido'=>'14',
            'idProducto'=>'2',
            'cantidad'=>'48',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'14',
            'idProducto'=>'3',
            'cantidad'=>'87',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'14',
            'idProducto'=>'4',
            'cantidad'=>'102',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'14',
            'idProducto'=>'5',
            'cantidad'=>'201',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'14',
            'idProducto'=>'6',
            'cantidad'=>'106',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


         \DB::table('concepts')->insert([
            'idPedido'=>'15',
            'idProducto'=>'1',
            'cantidad'=>'852',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'idPedido'=>'15',
            'idProducto'=>'2',
            'cantidad'=>'874',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'15',
            'idProducto'=>'3',
            'cantidad'=>'25',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'15',
            'idProducto'=>'4',
            'cantidad'=>'236',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'15',
            'idProducto'=>'5',
            'cantidad'=>'85',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'15',
            'idProducto'=>'6',
            'cantidad'=>'452',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

         \DB::table('concepts')->insert([
            'idPedido'=>'16',
            'idProducto'=>'1',
            'cantidad'=>'254',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'idPedido'=>'16',
            'idProducto'=>'2',
            'cantidad'=>'102',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'16',
            'idProducto'=>'3',
            'cantidad'=>'85',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'16',
            'idProducto'=>'4',
            'cantidad'=>'96',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'16',
            'idProducto'=>'5',
            'cantidad'=>'87',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'16',
            'idProducto'=>'6',
            'cantidad'=>'102',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

         \DB::table('concepts')->insert([
            'idPedido'=>'17',
            'idProducto'=>'1',
            'cantidad'=>'254',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'idPedido'=>'17',
            'idProducto'=>'2',
            'cantidad'=>'102',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'17',
            'idProducto'=>'3',
            'cantidad'=>'85',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);
        
        \DB::table('concepts')->insert([
            'idPedido'=>'17',
            'idProducto'=>'4',
            'cantidad'=>'96',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'17',
            'idProducto'=>'5',
            'cantidad'=>'87',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'17',
            'idProducto'=>'6',
            'cantidad'=>'102',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'18',
            'idProducto'=>'1',
            'cantidad'=>'147',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'idPedido'=>'18',
            'idProducto'=>'2',
            'cantidad'=>'48',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'18',
            'idProducto'=>'3',
            'cantidad'=>'87',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'18',
            'idProducto'=>'4',
            'cantidad'=>'102',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'18',
            'idProducto'=>'5',
            'cantidad'=>'201',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'18',
            'idProducto'=>'6',
            'cantidad'=>'106',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


          \DB::table('concepts')->insert([
            'idPedido'=>'19',
            'idProducto'=>'1',
            'cantidad'=>'852',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);


        \DB::table('concepts')->insert([
            'idPedido'=>'19',
            'idProducto'=>'2',
            'cantidad'=>'874',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'19',
            'idProducto'=>'3',
            'cantidad'=>'25',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'19',
            'idProducto'=>'4',
            'cantidad'=>'236',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'19',
            'idProducto'=>'5',
            'cantidad'=>'85',
            'cantidadcheck'=>'15',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('concepts')->insert([
            'idPedido'=>'19',
            'idProducto'=>'6',
            'cantidad'=>'452',
            'cantidadcheck'=>'0',
            'descuento'=>'20',
            'subtotal'=>'200',
            'estado'=>'1',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        

       }
}