<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class InventariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $hora=Carbon::now();

        \DB::table('inventarios')->insert([
            'id'=>'1',
            'id_producto'=>'1',
            'id_sucursal'=>'1',
            'cant_disponible'=>'250',
            'cant_apartada'=>'0',
            'maximos'=>'500', 
            'minimos'=>'10',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'2',
            'id_producto'=>'2',
            'id_sucursal'=>'1',
            'cant_disponible'=>'235',
            'cant_apartada'=>'0',
            'maximos'=>'680',
            'minimos'=>'20',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'3',
            'id_producto'=>'3',
            'id_sucursal'=>'1',
            'cant_disponible'=>'120',
            'cant_apartada'=>'0',
            'maximos'=>'300',
            'minimos'=>'50',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'4',
            'id_producto'=>'4',
            'id_sucursal'=>'1',
            'cant_disponible'=>'600',
            'cant_apartada'=>'0',
            'maximos'=>'1000',
            'minimos'=>'200',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'5',
            'id_producto'=>'5',
            'id_sucursal'=>'1',
            'cant_disponible'=>'275',
            'cant_apartada'=>'0',
            'maximos'=>'430',
            'minimos'=>'50',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'6',
            'id_producto'=>'6',
            'id_sucursal'=>'1',
            'cant_disponible'=>'90',
            'cant_apartada'=>'0',
            'maximos'=>'180',
            'minimos'=>'20',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'7',
            'id_producto'=>'7',
            'id_sucursal'=>'1',
            'cant_disponible'=>'110',
            'cant_apartada'=>'0',
            'maximos'=>'200',
            'minimos'=>'15',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'8',
            'id_producto'=>'8',
            'id_sucursal'=>'1',
            'cant_disponible'=>'270',
            'cant_apartada'=>'0',
            'maximos'=>'400',
            'minimos'=>'80',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'9',
            'id_producto'=>'9',
            'id_sucursal'=>'1',
            'cant_disponible'=>'70',
            'cant_apartada'=>'0',
            'maximos'=>'200',
            'minimos'=>'10',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'10',
            'id_producto'=>'10',
            'id_sucursal'=>'1',
            'cant_disponible'=>'550',
            'cant_apartada'=>'0',
            'maximos'=>'700',
            'minimos'=>'200',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'11',
            'id_producto'=>'11',
            'id_sucursal'=>'1',
            'cant_disponible'=>'245',
            'cant_apartada'=>'0',
            'maximos'=>'650',
            'minimos'=>'100',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'12',
            'id_producto'=>'12',
            'id_sucursal'=>'1',
            'cant_disponible'=>'690',
            'cant_apartada'=>'0',
            'maximos'=>'800',
            'minimos'=>'200',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'13',
            'id_producto'=>'13',
            'id_sucursal'=>'1',
            'cant_disponible'=>'400',
            'cant_apartada'=>'0',
            'maximos'=>'500',
            'minimos'=>'100',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'14',
            'id_producto'=>'14',
            'id_sucursal'=>'1',
            'cant_disponible'=>'180',
            'cant_apartada'=>'0',
            'maximos'=>'345',
            'minimos'=>'20',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'15',
            'id_producto'=>'15',
            'id_sucursal'=>'1',
            'cant_disponible'=>'190',
            'cant_apartada'=>'0',
            'maximos'=>'290',
            'minimos'=>'60',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'16',
            'id_producto'=>'16',
            'id_sucursal'=>'1',
            'cant_disponible'=>'380',
            'cant_apartada'=>'0',
            'maximos'=>'700',
            'minimos'=>'150',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'17',
            'id_producto'=>'17',
            'id_sucursal'=>'1',
            'cant_disponible'=>'60',
            'cant_apartada'=>'0',
            'maximos'=>'300',
            'minimos'=>'100',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'18',
            'id_producto'=>'18',
            'id_sucursal'=>'1',
            'cant_disponible'=>'800',
            'cant_apartada'=>'0',
            'maximos'=>'1200',
            'minimos'=>'200',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'19',
            'id_producto'=>'19',
            'id_sucursal'=>'1',
            'cant_disponible'=>'150',
            'cant_apartada'=>'0',
            'maximos'=>'200',
            'minimos'=>'10',
            'created_at'=> $hora
        ]);

        \DB::table('inventarios')->insert([
            'id'=>'20',
            'id_producto'=>'20',
            'id_sucursal'=>'1',
            'cant_disponible'=>'300',
            'cant_apartada'=>'0',
            'maximos'=>'400',
            'minimos'=>'80',
            'created_at'=> $hora
        ]);
    }
}