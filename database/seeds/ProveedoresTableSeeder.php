<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProveedoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$hora=Carbon::now();


        \DB::table('proveedores')->insert([
            'id'=>'1',
            'nombre'=>'Domingo Quintana Herrera',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);
        
        \DB::table('proveedores')->insert([
            'id'=>'2',  
            'nombre'=>'Julio Cesar Quintana Gaytan',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        \DB::table('proveedores')->insert([
            'id'=>'3',
            'nombre'=>'Manuel Adolfo Alvarez',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        \DB::table('proveedores')->insert([
            'id'=>'4',
            'nombre'=>'Carlos Perez Perez',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        \DB::table('proveedores')->insert([
            'id'=>'5',
            'nombre'=>'Mauricio Saucedo',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',             'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        \DB::table('proveedores')->insert([
            'id'=>'6',
            'nombre'=>'Alejandro Avilés',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        \DB::table('proveedores')->insert([
            'id'=>'7',
            'nombre'=>'Luis Zavala',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);


        \DB::table('proveedores')->insert([
            'id'=>'8',
            'nombre'=>'Sergio López',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        \DB::table('proveedores')->insert([
            'id'=>'9',
            'nombre'=>'Indalecio Matsuoka',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        \DB::table('proveedores')->insert([
            'id'=>'10',
            'nombre'=>'Cármen Sotelo',
            'calle'=>'Educacion',
            'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'rfc'=>'XAXX010101000',           'foto'=>'img/proveedores.jpg',

            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);
        
    }
}