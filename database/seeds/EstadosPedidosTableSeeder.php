<?php

use Illuminate\Database\Seeder;

class EstadosPedidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('estadopedidos')->insert([
        	'id'=>'1',
            'descripcion'=>'Pendiente'
        ]);

        \DB::table('estadopedidos')->insert([
        	'id'=>'2',
            'descripcion'=>'Surtiendo'
        ]);

        \DB::table('estadopedidos')->insert([
        	'id'=>'3',
            'descripcion'=>'Remisionado'
        ]);

        \DB::table('estadopedidos')->insert([
        	'id'=>'4',
            'descripcion'=>'Enviado'
        ]);

        \DB::table('estadopedidos')->insert([
        	'id'=>'5',
            'descripcion'=>'Cancelado'
        ]);

        \DB::table('estadopedidos')->insert([
            'id'=>'6',
            'descripcion'=>'Aceptado'
        ]);
    }
}
