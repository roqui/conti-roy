<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InterfazTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    // catalogo Productos-----------------------------------------------------------------


        \DB::table('interfazs')->insert([
        	'id'=>'1',
            'descripcion'=>'Modificar y Eliminar Producto',
            'desc_buscar'=>'Catálogos -> Productos -> Modificar y Eliminar',
            'url'=>'productos'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'2',
            'descripcion'=>'Añadir Producto',
            'desc_buscar'=>'Catálogos -> Productos -> Agregar Producto',
            'url'=>'productos'

        ]);

        DB::table('interfazs')->insert([
            'id'=>'3',
            'descripcion'=>'Añadir Producto a Inventario',
            'desc_buscar'=>'Catálogos -> Productos -> Agregar',
            'url'=>'productos'
        ]);

        DB::table('interfazs')->insert([
            'id'=>'4',
            'descripcion'=>'Ver Productos',
            'desc_buscar'=>'Catálogos -> Productos',
            'url'=>'productos'

        ]);


    //catálogo de lineas y sublineas
        \DB::table('interfazs')->insert([
            'id'=>'5',
            'descripcion'=>'Ver líneas',
            'desc_buscar'=>'Catálogos -> Líneas',
            'url'=>'lineas'

        ]);


        \DB::table('interfazs')->insert([
            'id'=>'6',
            'descripcion'=>'Añadir Línea',
            'desc_buscar'=>'Catálogos -> Líneas -> Agregar Línea',
            'url'=>'lineas'

        ]);

        \DB::table('interfazs')->insert([
            'id'=>'7',
            'descripcion'=>'Ver Sublíneas',
            'desc_buscar'=>'Catálogos -> Líneas -> Btn Ver Sublíneas',
            'url'=>'lineas'


        ]);

        \DB::table('interfazs')->insert([
            'id'=>'8',
            'descripcion'=>'Añadir Sublínea',
            'desc_buscar'=>'Catálogos -> Líneas -> Btn Ver Sublíneas',
            'url'=>'lineas'

        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'9',
            'descripcion'=>'Modificar y Eliminar Sublíneas',
            'desc_buscar'=>'Catálogos -> Líneas -> Btn Ver Sublíneas',
            'url'=>'lineas'

        ]);

    //catalogo de marcas
        \DB::table('interfazs')->insert([
            'id'=>'10',
            'descripcion'=>'Ver Marcas',
            'desc_buscar'=>'Catálogos -> Marcas',
            'url'=>'marca'

        ]);


        \DB::table('interfazs')->insert([
        	'id'=>'11',
            'descripcion'=>'Añadir Marca',
            'desc_buscar'=>'Catálogos -> Marca -> Agregar Marca',
            'url'=>'marca'

        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'12',
            'descripcion'=>'Modificar y Eliminar Marca',
            'desc_buscar'=>'Catálogos -> Marca -> Modificar y Eliminar Marca',
            'url'=>'marca'

        ]);

        //catálogo de proveedores
        \DB::table('interfazs')->insert([
            'id'=>'13',
            'descripcion'=>'Ver Proveedores',
            'desc_buscar'=>'',
            'url'=>'proveedores'

        ]);


        \DB::table('interfazs')->insert([
        	'id'=>'14',
            'descripcion'=>'Agregar Proveedor',
            'desc_buscar'=>'',
            'url'=>'proveedores'

        ]);


        \DB::table('interfazs')->insert([
        	'id'=>'15',
            'descripcion'=>'Modificar y Eliminar Proveedor',
            'desc_buscar'=>'',
            'url'=>'proveedores'

        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'16',
            'descripcion'=>'Excel Proveedores',
            'desc_buscar'=>'',
            'url'=>'proveedores'

        ]);

//catálogo clientes

        \DB::table('interfazs')->insert([
        	'id'=>'17',
            'descripcion'=>'Ver Clientes',
            'desc_buscar'=>'',
            'url'=>'clientes'

        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'18',
            'descripcion'=>'Agregar Cliente',
            'desc_buscar'=>'',
            'url'=>'clientes'

        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'19',
            'descripcion'=>'Modificar y Eliminar Cliente',
            'desc_buscar'=>'',
            'url'=>'clientes'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'20',
            'descripcion'=>'Excel Clientes',
            'desc_buscar'=>'',
            'url'=>'clientes'
        ]);

        //catalogo de Usuarios---------------------------------------------------------------

        \DB::table('interfazs')->insert([
        	'id'=>'21',
            'descripcion'=>'Añadir Usuario',
            'desc_buscar'=>'',
            'url'=>'users'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'22',
            'descripcion'=>'Modificar y Eliminar Usuario',
            'desc_buscar'=>'',
            'url'=>'users'
        ]);


        \DB::table('interfazs')->insert([
        	'id'=>'23',
            'descripcion'=>'Ver Permisos de Usuario',
            'desc_buscar'=>'',
            'url'=>'users'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'24',
            'descripcion'=>'Modificar Permisos',
            'url'=>'users'

        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'25',
            'descripcion'=>'Excel Usuarios',
            'url'=>'users'
        ]);

//pedidos

        \DB::table('interfazs')->insert([
        	'id'=>'26',
            'descripcion'=>'Agregar Pedido (Venta)',
            'url'=>'pedidos'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'27',
            'descripcion'=>'Ver Pedidos Pendientes',
            'url'=>'ppendientes'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'28',
            'descripcion'=>'Asignar Pedido a un Surtidor',
            'url'=>'ppendientes'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'29',
            'descripcion'=>'Ver Pedidos Asignados',
            'url'=>'pasignados'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'30',
            'descripcion'=>'Ver Pedidos Remisionados',
            'url'=>'premisionados'
        ]);


        \DB::table('interfazs')->insert([
        	'id'=>'31',
            'descripcion'=>'Asignar Pedido a un Conductor',
            'url'=>'premisionados'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'32',
            'descripcion'=>'Ver todos los Pedidos',
            'url'=>'todos'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'33',
            'descripcion'=>'Ver Cancelaciones de Pedidos',
            'url'=>'pedidos'
        ]);

//Traslados

        \DB::table('interfazs')->insert([
            'id'=>'34',
            'descripcion'=>'Agregar Traslado',
            'url'=>'traslados'
        ]);

        \DB::table('interfazs')->insert([
            'id'=>'35',
            'descripcion'=>'Ver Traslados Pendientes',
            'url'=>'tpendientes'
        ]);

        \DB::table('interfazs')->insert([
            'id'=>'36',
            'descripcion'=>'Asignar Traslado a un Surtidor',
            'url'=>'tpendientes'
        ]);


        \DB::table('interfazs')->insert([
            'id'=>'37',
            'descripcion'=>'Ver Traslados Asignados',
            'url'=>'tasignados'
        ]);

        \DB::table('interfazs')->insert([
            'id'=>'38',
            'descripcion'=>'Ver Traslados Remisionados',
            'url'=>'tremisionados'
        ]);


        \DB::table('interfazs')->insert([
            'id'=>'39',
            'descripcion'=>'Asignar Traslado a un Conductor',
            'url'=>'tremisionados'
        ]);

        \DB::table('interfazs')->insert([
            'id'=>'40',
            'descripcion'=>'Ver todos los Traslados',
            'url'=>'todost'
        ]);

        \DB::table('interfazs')->insert([
            'id'=>'41',
            'descripcion'=>'Ver Próximos de Traslados',
            'url'=>'tproximos'
        ]);

//Inventarios
        \DB::table('interfazs')->insert([
            'id'=>'42',
            'descripcion'=>'Ver Inventario',
            'url'=>'inventarios'
        ]);

        \DB::table('interfazs')->insert([
            'id'=>'43',
            'descripcion'=>'Añadir Producto a Inventario',
            'url'=>'inventarios'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'44',
            'descripcion'=>'Ver Max y Min',
            'url'=>'maximos'
        ]);

//orden de compra

        \DB::table('interfazs')->insert([
        	'id'=>'45',
            'descripcion'=>'Agregar Orden de Compra',
            'url'=>'compras'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'46',
            'descripcion'=>'Ver Ordenes de Compra Pendientes',
            'url'=>'ordencompras'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'47',
            'descripcion'=>'Aceptar/Rechazar Orden de Compra (Revisar)',
            'url'=>'cproximas'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'48',
            'descripcion'=>'Ver todas las Órdenes de Compra',
            'url'=>'ordencompras'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'49',
            'descripcion'=>'Ver Fechas de Entrega',
            'url'=>'ordencompras'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'50',
            'descripcion'=>'Ver Maximos y Mínimos en Compras',
            'url'=>'maximos'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'51',
            'descripcion'=>'Ver Devoluciones de Ord. de Compra',
            'url'=>'ordencompras'
        ]);
//sucursales

        \DB::table('interfazs')->insert([
        	'id'=>'52',
            'descripcion'=>'Modificar y Eliminar Sucursal',
            'url'=>'sucursales'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'53',
            'descripcion'=>'Agregar Sucursal',
            'url'=>'sucursales'
        ]);



        \DB::table('interfazs')->insert([
        	'id'=>'54',
            'descripcion'=>'Ver Sucursales',
            'url'=>'sucursales'
        ]);

        \DB::table('interfazs')->insert([
        	'id'=>'55',
            'descripcion'=>'Bitácoras',
            'url'=>'backlogindex'
        ]);


        \DB::table('interfazs')->insert([
            'id'=>'56',
            'descripcion'=>'Super Usuario'
        ]);

        


        

       
        



    }
}
