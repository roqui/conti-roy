<?php

use Illuminate\Database\Seeder;

class checkStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('check_states')->insert([
        	'id'=>'1',
            'descripcion'=>'Sin Checar'
        ]);

        \DB::table('check_states')->insert([
        	'id'=>'2',
            'descripcion'=>'Checado'
        ]);
    }
}
