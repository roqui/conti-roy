<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $hora=Carbon::now();
    	

    	\DB::table('users')->insert([
    		'id'=>'1',
            'name' => 'Maria Fernanda Morales Medina',
            'foto'=>'img/user.png',
            'email' => 'mafervamp@gmail.com',
            'puesto' => '4',
            'usuario' => 'MaferM22',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Fracc Albatros Paseo de la Noria 28 Col Ex-hacienda La huerta',
            'telefono' => '4431331414',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
    		'id'=>'2',
            'name' => 'Rodrigo Alejandro Quintana Gaytan',
            'foto'=>'img/user.png',
            'email' => 'roy98123@gmail.com',
            'puesto' => '3',
            'usuario' => 'roy98123',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Educacion 297 Col Molino de Parras Morelia Mich',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
    		'id'=>'3',
            'name' => 'Karen Daniela Olivo Solorio',
            'foto'=>'img/user.png',
            'email' => 'dany@gmail.com',
            'puesto' => '2',
            'usuario' => 'danypandax',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Calle noseque 198 Col Prados Verdes',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
    		'id'=>'4',
            'name' => 'Jeanette Monserrat Avilés Davalos',
            'foto'=>'img/user.png',
            'email' => 'monse@gmail.com',
            'puesto' => '1',
            'usuario' => 'MonseAvd',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Calle noseque 198 Col Prados Verdes',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
    		'id'=>'5',
            'name' => 'Fernando Rayado Jacobo',
            'foto'=>'img/user.png',
            'email' => 'Ticky@gmail.com',
            'puesto' => '5',
            'usuario' => 'Ticky',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Calle Pinos 198 Col Prados Verdes',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
            'id'=>'6',
            'name' => 'Arnulfo Damian Sosa',
            'foto'=>'img/user.png',
            'email' => 'ufiin@gmail.com',
            'puesto' => '4',
            'usuario' => 'ufiin',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Av Tecnologico 2800',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
            'id'=>'7',
            'name' => 'Jose Miguel Trejo Covian',
            'foto'=>'img/user.png',
            'email' => 'mike@gmail.com',
            'puesto' => '4',
            'usuario' => 'josemi',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Av Tecnologico 2800',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
            'id'=>'8',
            'name' => 'David El tres Apellidos',
            'foto'=>'img/user.png',
            'email' => 'david@gmail.com',
            'puesto' => '4',
            'usuario' => 'david',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Av Tecnologico 2800',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
            'id'=>'9',
            'name' => 'Brandon Ceja Cruz',
            'foto'=>'img/user.png',
            'email' => 'brandon@gmail.com',
            'puesto' => '4',
            'usuario' => 'brandon',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Av Tecnologico 2800',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
            'id'=>'10',
            'name' => 'Alan Martin Fuentes',
            'foto'=>'img/user.png',
            'email' => 'alan@gmail.com',
            'puesto' => '4',
            'usuario' => 'alan',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Av Tecnologico 2800',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
            'id'=>'11',
            'name' => 'Alex Gaytan',
            'foto'=>'img/user.png',
            'email' => 'alex@gmail.com',
            'puesto' => '4',
            'usuario' => 'alex',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Av Tecnologico 2800',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
            'id'=>'12',
            'name' => 'Sebastian Monroy Maciel',
            'foto'=>'img/user.png',
            'email' => 'monroy@gmail.com',
            'puesto' => '6',
            'usuario' => 'monroy',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Av Tecnologico 2800',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
            'id'=>'13',
            'name' => 'Luis Angel Rojas García',
            'foto'=>'img/user.png',
            'email' => 'rojas@gmail.com',
            'puesto' => '6',
            'usuario' => 'Rojas',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Av Tecnologico 2800',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);

        \DB::table('users')->insert([
            'id'=>'14',
            'name' => 'Juan Carlos García Villaseñor',
            'foto'=>'img/user.png',
            'email' => 'güero@gmail.com',
            'puesto' => '6',
            'usuario' => 'Guero',
            'password' => '$2y$10$xXfIg.SIBVmeWhaDOvNfmuCNOjUgqNWna/5AoSRV6zvM.iyvDvGT6',
            'calle' => 'Av Tecnologico 2800',
            'telefono' => '4433902492',
            'idSucursal' => '1',

            'created_at'=>$hora

        ]);



        

        
    }
}
