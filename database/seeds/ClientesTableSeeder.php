<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$hora=Carbon::now();


    	DB::table('clientes')->insert([
           'id'=>'1',
           'nombre'=>'Domingo Quintana Herrera',
           'calle'=>'Educacion',
           'num_exterior'=>'297',
           'codigo_postal'=>'58010',
           'rfc'=>'XAXX010101000',
           'telefono'=>'4431684356',
           'ciudad'=>'Morelia',
           'estado'=>'Michoacán',
           'pais'=>'Mexico',
           'foto'=>'img/user.png',
           'dias_plazo'=>'30',
           'observaciones'=>'Ninguna',
           'descuento'=>'0.0',
           'email'=>'example@gmail.com',
           'created_at'=>$hora
       ]);

        DB::table('clientes')->insert([
        	'id'=>'2',	
            'nombre'=>'Julio Cesar Quintana Gaytan',
            'calle'=>'Educacion',
             'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
                       'foto'=>'img/user.png',

            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        DB::table('clientes')->insert([
        	'id'=>'3',
            'nombre'=>'Manuel Adolfo Alvarez',
            'calle'=>'Educacion',
             'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
                       'foto'=>'img/user.png',

            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        DB::table('clientes')->insert([
        	'id'=>'4',
            'nombre'=>'Carlos Perez Perez',
            'calle'=>'Emiliano Zapata',
            'calle'=>'Educacion',
             'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
                       'foto'=>'img/user.png',

            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        DB::table('clientes')->insert([
        	'id'=>'5',
            'nombre'=>'Mauricio Saucedo',
            'calle'=>'Educacion',
             'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
                       'foto'=>'img/user.png',

            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        DB::table('clientes')->insert([
            'id'=>'6',
            'nombre'=>'Cármen Rámirez',
            'calle'=>'Educacion',
             'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
                       'foto'=>'img/user.png',

            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        DB::table('clientes')->insert([
            'id'=>'7',
            'nombre'=>'Ulises López',
            'calle'=>'Educacion',
             'num_exterior'=>'297',
            'codigo_postal'=>'58010',
                       'foto'=>'img/user.png',

            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        DB::table('clientes')->insert([
            'id'=>'8',
            'nombre'=>'Alejandra Avilés',
            'calle'=>'Educacion',
             'num_exterior'=>'297',
            'codigo_postal'=>'58010',
                       'foto'=>'img/user.png',

            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        DB::table('clientes')->insert([
            'id'=>'9',
            'nombre'=>'Valeria Barragán',
                       'foto'=>'img/user.png',

            'calle'=>'Educacion',
             'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

        DB::table('clientes')->insert([
            'id'=>'10',
            'nombre'=>'Octavio Gil',
            'calle'=>'Educacion',
             'num_exterior'=>'297',
            'codigo_postal'=>'58010',
            'rfc'=>'XAXX010101000',
            'telefono'=>'4431684356',
                       'foto'=>'img/user.png',

            'ciudad'=>'Morelia',
            'estado'=>'Michoacán',
            'pais'=>'Mexico',
            'dias_plazo'=>'30',
            'observaciones'=>'Ninguna',
            'descuento'=>'0.0',
            'email'=>'example@gmail.com',
            'created_at'=>$hora
        ]);

    }
}
