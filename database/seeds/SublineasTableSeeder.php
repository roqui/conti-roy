<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SublineasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hora=Carbon::now();

        // ------- #1 ARTE

        DB::table('sublineas')->insert([
            'id'=>'1',
            'descrip'=>'Accesorios de Arte',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'2',
            'descrip'=>'Acuarelas',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'3',
            'descrip'=>'Bastidores',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'4',
            'descrip'=>'Caballetes',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'5',
            'descrip'=>'Carbon',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'6',
            'descrip'=>'Colores',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'7',
            'descrip'=>'Estilografos',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'8',
            'descrip'=>'Lienzos',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'9',
            'descrip'=>'Pinceles',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'10',
            'descrip'=>'Pintura',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'11',
            'descrip'=>'Rodetes',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'12',
            'descrip'=>'Rotuladores',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'13',
            'descrip'=>'Tizas',
            'id_linea'=>'1',
            'created_at'=>$hora
        ]);

        // ------ #2 ARTICULOS DE LIMPIEZA --------------------------

        DB::table('sublineas')->insert([
            'id'=>'14',
            'descrip'=>'Artículos Varios',
            'id_linea'=>'2',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'15',
            'descrip'=>'Desechables',
            'id_linea'=>'2',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'16',
            'descrip'=>'Gel Antibacterial',
            'id_linea'=>'2',
            'created_at'=>$hora
        ]);

        //----------------#3 Articulos Escolares ----------------------

        DB::table('sublineas')->insert([
            'id'=>'17',
            'descrip'=>'Articulos Preescolar',
            'id_linea'=>'3',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'18',
            'descrip'=>'Borradores',
            'id_linea'=>'3',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'19',
            'descrip'=>'Gomas',
            'id_linea'=>'3',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'20',
            'descrip'=>'Juegos de Geometría',
            'id_linea'=>'3',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'21',
            'descrip'=>'Reglas',
            'id_linea'=>'3',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'22',
            'descrip'=>'Sacapuntas',
            'id_linea'=>'3',
            'created_at'=>$hora
        ]);

        //------------------- #4 Articulos Temporales ----------------

        DB::table('sublineas')->insert([
            'id'=>'23',
            'descrip'=>'Agendas',
            'id_linea'=>'4',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'24',
            'descrip'=>'Artículos de Muertos',
            'id_linea'=>'4',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'25',
            'descrip'=>'Artículos Fiestas Patrias',
            'id_linea'=>'4',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'26',
            'descrip'=>'Artículos Navideños',
            'id_linea'=>'4',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'27',
            'descrip'=>'Artículos San Valentín',
            'id_linea'=>'4',
            'created_at'=>$hora
        ]);

//------------ #5 Computo y consumibles --------------

        DB::table('sublineas')->insert([
            'id'=>'28',
            'descrip'=>'Accesorios Computo',
            'id_linea'=>'5',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'29',
            'descrip'=>'Calculadoras',
            'id_linea'=>'5',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'30',
            'descrip'=>'Cartucho de Tinta',
            'id_linea'=>'5',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'31',
            'descrip'=>'Cintas para Equipos',
            'id_linea'=>'5',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'32',
            'descrip'=>'Discos',
            'id_linea'=>'5',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'33',
            'descrip'=>'Equipos',
            'id_linea'=>'5',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'34',
            'descrip'=>'Memorias',
            'id_linea'=>'5',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'35',
            'descrip'=>'Pilas',
            'id_linea'=>'5',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'36',
            'descrip'=>'Toner',
            'id_linea'=>'5',
            'created_at'=>$hora
        ]); 

      //------------#6 Cuadernos ------------------

        DB::table('sublineas')->insert([
            'id'=>'37',
            'descrip'=>'Blocks',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'38',
            'descrip'=>'Cuaderno Collage',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'39',
            'descrip'=>'Cuaderno Francés',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'40',
            'descrip'=>'Cuaderno Italiano',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'41',
            'descrip'=>'Cuaderno Profesional Económico',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'42',
            'descrip'=>'Cuaderno Profesional',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'43',
            'descrip'=>'Cuaderno Especiales',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'44',
            'descrip'=>'Cuaderno Preescolar',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'45',
            'descrip'=>'Forros para Cuaderno',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'46',
            'descrip'=>'Libretas Ejecutivas',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'47',
            'descrip'=>'Repuestos de Hoja',
            'id_linea'=>'6',
            'created_at'=>$hora
        ]);

//--------- #7 Encuadernación ------------------

        DB::table('sublineas')->insert([
            'id'=>'48',
            'descrip'=>'Arillos',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'49',
            'descrip'=>'Contac',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'50',
            'descrip'=>'Engargoladoras',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'51',
            'descrip'=>'Enmicadoras',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'52',
            'descrip'=>'Forros',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'53',
            'descrip'=>'Guillotinas',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'54',
            'descrip'=>'Micas',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'55',
            'descrip'=>'Pastas para Engargolar',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'56',
            'descrip'=>'Plástico Cristal',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'57',
            'descrip'=>'Postes de Aluminio',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'58',
            'descrip'=>'Refuerzos para Hoja',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'59',
            'descrip'=>'Trituradoras de Papel',
            'id_linea'=>'7',
            'created_at'=>$hora
        ]);

//----------#8 Escritura ---------------

        DB::table('sublineas')->insert([
            'id'=>'60',
            'descrip'=>'Bolígrafos',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'61',
            'descrip'=>'Bolígrafos Finos',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'62',
            'descrip'=>'Colores',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'63',
            'descrip'=>'Crayones',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'64',
            'descrip'=>'Lápiz de Colores',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'65',
            'descrip'=>'Lápiz Grafito',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'66',
            'descrip'=>'Marcador de Agua',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'67',
            'descrip'=>'Marcador Industrial',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'68',
            'descrip'=>'Marcador Permanente',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'69',
            'descrip'=>'Marcador Pintarrón',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'70',
            'descrip'=>'Marcador Vidrio',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'71',
            'descrip'=>'Marcatextos',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'72',
            'descrip'=>'Minas',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'73',
            'descrip'=>'Portaminas',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'74',
            'descrip'=>'Repuestos de Escritura',
            'id_linea'=>'8',
            'created_at'=>$hora
        ]);

//---------- #9 Formas ---------------------------------

        DB::table('sublineas')->insert([
            'id'=>'75',
            'descrip'=>'Cajas de Empaque',
            'id_linea'=>'9',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'76',
            'descrip'=>'Formas Contables',
            'id_linea'=>'9',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'77',
            'descrip'=>'Libros Contables',
            'id_linea'=>'9',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'78',
            'descrip'=>'Rollos',
            'id_linea'=>'9',
            'created_at'=>$hora
        ]);

//------- #10 Manualidades merceria y fiesta
        DB::table('sublineas')->insert([
            'id'=>'79',
            'descrip'=>'Adornos',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'80',
            'descrip'=>'Arte de Corte y Confección',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'81',
            'descrip'=>'Bolsa y Cajas de Regalo',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'82',
            'descrip'=>'Calcomanias',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'83',
            'descrip'=>'Desechables Fiesta',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'84',
            'descrip'=>'Foamy',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'85',
            'descrip'=>'Globos',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'86',
            'descrip'=>'Invitaciones',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'87',
            'descrip'=>'Material de Repujado',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'88',
            'descrip'=>'Merceria',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'89',
            'descrip'=>'Moños y Listones',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'90',
            'descrip'=>'Pelotas',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'91',
            'descrip'=>'Pistola Silicón',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'92',
            'descrip'=>'Plastilina y Masas',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'93',
            'descrip'=>'Plumas',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'94',
            'descrip'=>'PVC',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'95',
            'descrip'=>'Rafias',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'96',
            'descrip'=>'Silicón',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'97',
            'descrip'=>'Unicel',
            'id_linea'=>'10',
            'created_at'=>$hora
        ]);

//----------------#11 Material Didáctico--------------------

        DB::table('sublineas')->insert([
            'id'=>'98',
            'descrip'=>'Abacos',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'99',
            'descrip'=>'Arte para Maquetas',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'100',
            'descrip'=>'Diccionarios',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'101',
            'descrip'=>'Figuras Geométricas',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'102',
            'descrip'=>'Flauta',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'103',
            'descrip'=>'Foamy',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'104',
            'descrip'=>'Juegos de Mesa',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'105',
            'descrip'=>'Libros de Colorear',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'106',
            'descrip'=>'Libros Lectura',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'107',
            'descrip'=>'Lupas',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'108',
            'descrip'=>'Monografías',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'109',
            'descrip'=>'Palillos y Tablas de Madera',
            'id_linea'=>'11',
            'created_at'=>$hora
        ]);

//-------------- #12 Mobiliario -------------------------

        DB::table('sublineas')->insert([
            'id'=>'110',
            'descrip'=>'Archiveros',
            'id_linea'=>'12',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'111',
            'descrip'=>'Bancos',
            'id_linea'=>'12',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'112',
            'descrip'=>'Escritorios',
            'id_linea'=>'12',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'113',
            'descrip'=>'Estantería',
            'id_linea'=>'12',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'114',
            'descrip'=>'Locker',
            'id_linea'=>'12',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'115',
            'descrip'=>'Mesas',
            'id_linea'=>'12',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'116',
            'descrip'=>'Otros',
            'id_linea'=>'12',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'117',
            'descrip'=>'Pizarrones',
            'id_linea'=>'12',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'118',
            'descrip'=>'Sillas',
            'id_linea'=>'12',
            'created_at'=>$hora
        ]);

//--------- #13 Mochilas y Portafolios

        DB::table('sublineas')->insert([
            'id'=>'119',
            'descrip'=>'Funda para Computadora',
            'id_linea'=>'13',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'120',
            'descrip'=>'Loncheras',
            'id_linea'=>'13',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'121',
            'descrip'=>'Mochilas',
            'id_linea'=>'13',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'122',
            'descrip'=>'Porta Lapiceras',
            'id_linea'=>'13',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'123',
            'descrip'=>'Portafolios',
            'id_linea'=>'13',
            'created_at'=>$hora
        ]);

//----------- #14 Oficina ------------------------------

        DB::table('sublineas')->insert([
            'id'=>'124',
            'descrip'=>'Accesorios de Oficina',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'125',
            'descrip'=>'Broches',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'126',
            'descrip'=>'Calendarios y Directorios',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'127',
            'descrip'=>'Carpetas de Argollas',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'128',
            'descrip'=>'Cestos de Basura',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'129',
            'descrip'=>'Clips',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'130',
            'descrip'=>'Correctores',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'131',
            'descrip'=>'Cuenta Fácil',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'132',
            'descrip'=>'Cutter',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'133',
            'descrip'=>'Dadales y Esponjeros',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'134',
            'descrip'=>'Desengrapadores',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'135',
            'descrip'=>'Engrapadoras',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'136',
            'descrip'=>'Folder',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'137',
            'descrip'=>'Grapas',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'138',
            'descrip'=>'Ligas',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'139',
            'descrip'=>'Organizadores',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'140',
            'descrip'=>'Papeleras',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'141',
            'descrip'=>'Perforadoras',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'142',
            'descrip'=>'Planificadores',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'143',
            'descrip'=>'Porta Documentos',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'144',
            'descrip'=>'Registradores',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'145',
            'descrip'=>'Repuestos Correctores',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'146',
            'descrip'=>'Repuestos de Navaja',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'147',
            'descrip'=>'Revisteros',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'148',
            'descrip'=>'Rollos',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'149',
            'descrip'=>'Rotuladores',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'150',
            'descrip'=>'Sellos Separadores',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'151',
            'descrip'=>'Sobres',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'152',
            'descrip'=>'Tablas Agarra Papel',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'153',
            'descrip'=>'Tijeras',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'154',
            'descrip'=>'Tintas',
            'id_linea'=>'14',
            'created_at'=>$hora
        ]);

//----------- #15 Papel ---------------

        DB::table('sublineas')->insert([
            'id'=>'155',
            'descrip'=>'Cartulinas',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'156',
            'descrip'=>'Papel América',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'157',
            'descrip'=>'Papel Bond t/c',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'158',
            'descrip'=>'Papel Bond t/o',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'159',
            'descrip'=>'Papel Carbón',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'160',
            'descrip'=>'Papel Cartoncillo',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'161',
            'descrip'=>'Papel Cascarón',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'162',
            'descrip'=>'Papel China',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'163',
            'descrip'=>'Papel Continuo',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'164',
            'descrip'=>'Papel Crepé',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'165',
            'descrip'=>'Papel de Colores',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'166',
            'descrip'=>'Papel Especiales',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'167',
            'descrip'=>'Papel Extendidos',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'168',
            'descrip'=>'Papel Kraft y Corrugados',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'169',
            'descrip'=>'Papel Lustre',
            'id_linea'=>'15',
            'created_at'=>$hora
        ]);

//-------#16 Pegamentos y Adhesivos------------------

        DB::table('sublineas')->insert([
            'id'=>'170',
            'descrip'=>'Cinta Doble Cara',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'171',
            'descrip'=>'Cintas Adhesivas',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'172',
            'descrip'=>'Cintas Empaque',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'173',
            'descrip'=>'Cintas Industriales',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'174',
            'descrip'=>'Cintas Invisibles',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'175',
            'descrip'=>'Cintas Masking Tape',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'176',
            'descrip'=>'Despachadores Cinta',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'177',
            'descrip'=>'Etiquetadoras',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'178',
            'descrip'=>'Etiquetas',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'179',
            'descrip'=>'Notas Adhesivas',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'180',
            'descrip'=>'Pegamento Blanco',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'181',
            'descrip'=>'Pegamento en Barra',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'182',
            'descrip'=>'Pegamento en Gel',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'183',
            'descrip'=>'Pegamento Transparente',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'184',
            'descrip'=>'Pegamento Industrial',
            'id_linea'=>'16',
            'created_at'=>$hora
        ]);

//---------#17 Servicios -----------------

        DB::table('sublineas')->insert([
            'id'=>'185',
            'descrip'=>'Apoyos',
            'id_linea'=>'17',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'186',
            'descrip'=>'Copias',
            'id_linea'=>'17',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'187',
            'descrip'=>'Engargolados y Enmicados',
            'id_linea'=>'17',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'188',
            'descrip'=>'Fletes',
            'id_linea'=>'17',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'189',
            'descrip'=>'Ofertas',
            'id_linea'=>'17',
            'created_at'=>$hora
        ]);

        DB::table('sublineas')->insert([
            'id'=>'190',
            'descrip'=>'Tarimas',
            'id_linea'=>'17',
            'created_at'=>$hora
        ]);

    }
}
