<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MarcasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$hora=Carbon::now();

    	DB::table('marcas')->insert([
            'id'=>'1',
            'descripcion' => 'BIC'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'2',
            'descripcion' => 'Azor'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'3',
            'descripcion' => 'UHU'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'4',
            'descripcion' => 'Scribe'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'5',
            'descripcion' => 'Berol'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'6',
            'descripcion' => 'Kores'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'7',
            'descripcion' => 'Kimberly-Clark'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'8',
            'descripcion' => 'Stabilo'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'9',
            'descripcion' => 'Paper Mate'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'10',
            'descripcion' => 'Pelikan'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'11',
            'descripcion' => 'Norma'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'12',
            'descripcion' => '3M'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'13',
            'descripcion' => 'Staedtler'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'14',
            'descripcion' => 'Crayola'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'15',
            'descripcion' => 'Sharpie'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'16',
            'descripcion' => 'Maped'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'17',
            'descripcion' => 'Berol'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'18',
            'descripcion' => 'Vinci'
            
        ]);

        DB::table('marcas')->insert([
            'id'=>'19',
            'descripcion' => 'MAE'
            
        ]);
    
    	DB::table('marcas')->insert([
            'id'=>'20',
            'descripcion' => 'Totto'
            
        ]);
    }
}