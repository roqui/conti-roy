<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OrdenCompraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hora=Carbon::now();

        DB::table('ordencompras')->insert([
            'id'=>'1',
            'id_proveedor'=>'1',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'2000',
            'estado'=>'1',
            'usuario'=>'Maria Fernanda Morales Medina',
            'observaciones'=>'Orden 1',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

        DB::table('ordencompras')->insert([
            'id'=>'2',
            'id_proveedor'=>'2',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'5000',
            'estado'=>'2',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'observaciones'=>'Orden 2',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

        DB::table('ordencompras')->insert([
            'id'=>'3',
            'id_proveedor'=>'3',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'3000',
            'estado'=>'3',
            'usuario'=>'Rodrigo Alejandro Quintana Gaytán',
            'observaciones'=>'Orden 3',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

        DB::table('ordencompras')->insert([
            'id'=>'4',
            'id_proveedor'=>'4',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'4000',
            'estado'=>'1',
            'usuario'=>'Jeanette Monserrat Avilés Dávalos',
            'observaciones'=>'Orden 4',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

        DB::table('ordencompras')->insert([
            'id'=>'5',
            'id_proveedor'=>'5',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'8000',
            'estado'=>'2',
            'usuario'=>'Fernando Raya Jacobo',
            'observaciones'=>'Orden 5',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

        DB::table('ordencompras')->insert([
            'id'=>'6',
            'id_proveedor'=>'6',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'5500',
            'estado'=>'3',
            'usuario'=>'Alan Martin Fuentes',
            'observaciones'=>'Orden 6',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

        DB::table('ordencompras')->insert([
            'id'=>'7',
            'id_proveedor'=>'7',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'2000',
            'estado'=>'1',
            'usuario'=>'Brandon Ceja Cruz',
            'observaciones'=>'Orden 7',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

        DB::table('ordencompras')->insert([
            'id'=>'8',
            'id_proveedor'=>'8',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'9000',
            'estado'=>'2',
            'usuario'=>'Arnulfo Damián Sosa',
            'observaciones'=>'Orden 8',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

        DB::table('ordencompras')->insert([
            'id'=>'9',
            'id_proveedor'=>'9',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'6000',
            'estado'=>'3',
            'usuario'=>'Jose Miguel Trejo Covian',
            'observaciones'=>'Orden 9',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

        DB::table('ordencompras')->insert([
            'id'=>'10',
            'id_proveedor'=>'10',
            'fechaC'=>'2019-10-20 04:33:05',
            'fechaLL'=>'2019-12-20 04:33:05',
            'Total'=>'7000',
            'estado'=>'1',
            'usuario'=>'David El Tres Apellidos',
            'observaciones'=>'Orden 10',
            'id_origen'=>'1',

            'created_at'=>$hora
        ]);

    }
}