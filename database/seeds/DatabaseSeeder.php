<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
          $this->call(EstadosOrdenTableSeeder::class);
        $this->call(EstadosPedidosTableSeeder::class);
        $this->call(checkStatesTableSeeder::class);   
        $this->call(ClientesTableSeeder::class);
        $this->call(InterfazTableSeeder::class);
        $this->call(LinesTableSeeder::class);
        $this->call(MarcasTableSeeder::class);
        $this->call(ProveedoresTableSeeder::class);
        $this->call(PuestosTableSeeder::class);
        $this->call(SublineasTableSeeder::class);
        $this->call(SucursalTableSeeder::class);
        $this->call(TransaccionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PedidosTableSeeder::class);
        $this->call(PermisosTableSeeder::class);
        $this->call(RolesDefaultTableSeeder::class);
                $this->call(SatTableSeeder::class);

        $this->call(ProductosTableSeeder::class);
        $this->call(ConceptsPTableSeeder::class);
        $this->call(InventariosTableSeeder::class);
        $this->call(OrdenCompraTableSeeder::class);

    }

}
