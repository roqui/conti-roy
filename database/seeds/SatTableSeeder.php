<?php

use Illuminate\Database\Seeder;

class SatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	\DB::table('satproductos')->insert([
        	'codigo'=>'123456',
            'descripcion'=>'Lápices'

        ]);
        \DB::table('satproductos')->insert([
        	'codigo'=>'12345678200',
            'descripcion'=>'Pintura para papel'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'1234567891235',
            'descripcion'=>'Calcomanías Preescolar'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'123456789126',
            'descripcion'=>'Engrapadora'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'147852147852',
            'descripcion'=>'Folder Oficio'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'123654780',
            'descripcion'=>'Estambre negro'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'8521479874',
            'descripcion'=>'Calculadora Básica'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'8796541258',
            'descripcion'=>'Silicón Azul'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'7896541230',
            'descripcion'=>'Contact 10m'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'7893214560',
            'descripcion'=>'Marcatextos amarillo'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'852147963',
            'descripcion'=>'Cinta invisible 5m'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'12345678',
            'descripcion'=>'Cutter'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'123456785242',
            'descripcion'=>'Cuaderno Profesional'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'1234567242789',
            'descripcion'=>'Moño guinda'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'135678524234',
            'descripcion'=>'Cartulina Gruesa'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'135676524234',
            'descripcion'=>'Acuarela'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'234562432565',
            'descripcion'=>'Lápiz de Puntillas'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'135670024234',
            'descripcion'=>'Cuaderno Italiano'

        ]);\DB::table('satproductos')->insert([
        	'codigo'=>'135642524234',
            'descripcion'=>'19'

        ]);
        \DB::table('satproductos')->insert([
        	'codigo'=>'124567777234',
            'descripcion'=>'Colores Bicolor'

        ]);

        //desde aquí empeiza lo vacío

        \DB::table('satproductos')->insert([
            'codigo'=>'44111801',
            'descripcion'=>'Ayudas para esténciles o textos'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111802',
            'descripcion'=>'Película de dibujo'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111803',
            'descripcion'=>'Compases'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111804',
            'descripcion'=>'Papeles de dibujo'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111805',
            'descripcion'=>'Curvas para dibujo'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111806',
            'descripcion'=>'Transportadores'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111807',
            'descripcion'=>'Escalas'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111808',
            'descripcion'=>'Reglas T'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111809',
            'descripcion'=>'Plantillas'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111810',
            'descripcion'=>'Triángulos'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111812',
            'descripcion'=>'Kits o sets de dibujo'

        ]);

        \DB::table('satproductos')->insert([
            'codigo'=>'44111813',
            'descripcion'=>'Puntos o cintas de dibujo'

        ]);
    }

}
