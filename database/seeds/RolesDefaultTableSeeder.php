<?php

use Illuminate\Database\Seeder;

class RolesDefaultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    //SURTIDOR--------------------------------------------------
        \DB::table('rol_defaults')->insert([
        	'id'=>'1',
        	'id_rol'=>'1',
            'id_interfaz'=>'29'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'2',
        	'id_rol'=>'1',
            'id_interfaz'=>'37'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'3',
            'id_rol'=>'1',
            'id_interfaz'=>'4'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'4',
            'id_rol'=>'1',
            'id_interfaz'=>'55'

        ]);



    //JEFE DE DISTRIBUCION

        \DB::table('rol_defaults')->insert([
        	'id'=>'5',
        	'id_rol'=>'2',
            'id_interfaz'=>'30'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'6',
        	'id_rol'=>'2',
            'id_interfaz'=>'31'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'7',
        	'id_rol'=>'2',
            'id_interfaz'=>'32'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'8',
        	'id_rol'=>'2',
            'id_interfaz'=>'38'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'9',
            'id_rol'=>'2',
            'id_interfaz'=>'39'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'10',
            'id_rol'=>'2',
            'id_interfaz'=>'40'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'37',
            'id_rol'=>'2',
            'id_interfaz'=>'55'

        ]);

    //JEFE DE ALMACÉN-------------------------------------------------------------------------------

        

        \DB::table('rol_defaults')->insert([
        	'id'=>'11',
        	'id_rol'=>'3',
            'id_interfaz'=>'4'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'12',
        	'id_rol'=>'3',
            'id_interfaz'=>'5'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'13',
        	'id_rol'=>'3',
            'id_interfaz'=>'7'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'14',
        	'id_rol'=>'3',
            'id_interfaz'=>'10'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'15',
        	'id_rol'=>'3',
            'id_interfaz'=>'27'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'16',
        	'id_rol'=>'3',
            'id_interfaz'=>'28'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'17',
        	'id_rol'=>'3',
            'id_interfaz'=>'32'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'18',
        	'id_rol'=>'3',
            'id_interfaz'=>'33'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'19',
        	'id_rol'=>'3',
            'id_interfaz'=>'35'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'20',
        	'id_rol'=>'3',
            'id_interfaz'=>'36'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'21',
        	'id_rol'=>'3',
            'id_interfaz'=>'40'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'22',
        	'id_rol'=>'3',
            'id_interfaz'=>'41'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'23',
        	'id_rol'=>'3',
            'id_interfaz'=>'42'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'24',
        	'id_rol'=>'3',
            'id_interfaz'=>'46'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'25',
        	'id_rol'=>'3',
            'id_interfaz'=>'47'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'26',
        	'id_rol'=>'3',
            'id_interfaz'=>'48'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'27',
        	'id_rol'=>'3',
            'id_interfaz'=>'49'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'28',
        	'id_rol'=>'3',
            'id_interfaz'=>'50'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'29',
        	'id_rol'=>'3',
            'id_interfaz'=>'51'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'30',
        	'id_rol'=>'3',
            'id_interfaz'=>'39'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'31',
        	'id_rol'=>'3',
            'id_interfaz'=>'42'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'32',
        	'id_rol'=>'3',
            'id_interfaz'=>'43'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'33',
        	'id_rol'=>'3',
            'id_interfaz'=>'44'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'34',
        	'id_rol'=>'3',
            'id_interfaz'=>'47'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'35',
        	'id_rol'=>'3',
            'id_interfaz'=>'50'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'36',
        	'id_rol'=>'3',
            'id_interfaz'=>'55'

        ]);

        

        //COMPRADORA--------------------------------------------------------------------------------------------

        \DB::table('rol_defaults')->insert([
        	'id'=>'38',
        	'id_rol'=>'5',
            'id_interfaz'=>'1'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'39',
        	'id_rol'=>'5',
            'id_interfaz'=>'2'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'40',
        	'id_rol'=>'5',
            'id_interfaz'=>'3'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'41',
        	'id_rol'=>'5',
            'id_interfaz'=>'4'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'42',
        	'id_rol'=>'5',
            'id_interfaz'=>'5'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'43',
        	'id_rol'=>'5',
            'id_interfaz'=>'6'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'44',
        	'id_rol'=>'5',
            'id_interfaz'=>'7'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'45',
        	'id_rol'=>'5',
            'id_interfaz'=>'8'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'46',
        	'id_rol'=>'5',
            'id_interfaz'=>'9'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'47',
        	'id_rol'=>'5',
            'id_interfaz'=>'10'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'48',
        	'id_rol'=>'5',
            'id_interfaz'=>'11'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'49',
        	'id_rol'=>'5',
            'id_interfaz'=>'12'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'50',
        	'id_rol'=>'5',
            'id_interfaz'=>'13'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'51',
        	'id_rol'=>'5',
            'id_interfaz'=>'14'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'52',
        	'id_rol'=>'5',
            'id_interfaz'=>'15'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'53',
        	'id_rol'=>'5',
            'id_interfaz'=>'16'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'54',
        	'id_rol'=>'5',
            'id_interfaz'=>'17'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'55',
        	'id_rol'=>'5',
            'id_interfaz'=>'18'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'56',
        	'id_rol'=>'5',
            'id_interfaz'=>'20'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'57',
        	'id_rol'=>'5',
            'id_interfaz'=>'26'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'58',
        	'id_rol'=>'5',
            'id_interfaz'=>'32'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'59',
        	'id_rol'=>'5',
            'id_interfaz'=>'34'

        ]);

        \DB::table('rol_defaults')->insert([
        	'id'=>'60',
        	'id_rol'=>'5',
            'id_interfaz'=>'40'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'61',
            'id_rol'=>'5',
            'id_interfaz'=>'42'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'62',
            'id_rol'=>'5',
            'id_interfaz'=>'43'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'63',
            'id_rol'=>'5',
            'id_interfaz'=>'44'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'64',
            'id_rol'=>'5',
            'id_interfaz'=>'45'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'65',
            'id_rol'=>'5',
            'id_interfaz'=>'48'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'66',
            'id_rol'=>'5',
            'id_interfaz'=>'49'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'67',
            'id_rol'=>'5',
            'id_interfaz'=>'50'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'68',
            'id_rol'=>'5',
            'id_interfaz'=>'51'

        ]);

        \DB::table('rol_defaults')->insert([
            'id'=>'69',
            'id_rol'=>'5',
            'id_interfaz'=>'55'

        ]);

        //GERENTE DE OPERACIONES------------------------------------------------------------------


        $num=70;
        for ($i=1; $i<=56 ; $i++) {
        	\DB::table('rol_defaults')->insert([
        	'id'=>$num,
        	'id_rol'=>'4',
            'id_interfaz'=>$i

        ]); 

        	$num++;
        	
        }





    }
}
