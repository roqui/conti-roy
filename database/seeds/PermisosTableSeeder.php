<?php

use Illuminate\Database\Seeder;

class PermisosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $id=1;
        for ($cont=1; $cont <= 56 ; $cont++) { 
        		\DB::table('permisos')->insert([
		        	'id'=>$id,
		            'idInterfaz'=>$cont,
		            'idUsuario'=>'1'

		        ]);

		        $id++;
        }

        for ($cont=1; $cont <= 56 ; $cont++) { 
        		\DB::table('permisos')->insert([
		        	'id'=>$id,
		            'idInterfaz'=>$cont,
		            'idUsuario'=>'2'

		        ]);

		        $id++;
        }

        for ($cont=1; $cont <= 56 ; $cont++) { 
        		\DB::table('permisos')->insert([
		        	'id'=>$id,
		            'idInterfaz'=>$cont,
		            'idUsuario'=>'3'

		        ]);

		        $id++;
        }

        for ($cont=1; $cont <= 56 ; $cont++) { 
        		\DB::table('permisos')->insert([
		        	'id'=>$id,
		            'idInterfaz'=>$cont,
		            'idUsuario'=>'4'

		        ]);

		        $id++;
        }

        for ($cont=1; $cont <= 56 ; $cont++) { 
        		\DB::table('permisos')->insert([
		        	'id'=>$id,
		            'idInterfaz'=>$cont,
		            'idUsuario'=>'5'

		        ]);

		        $id++;
        }

    }
}
