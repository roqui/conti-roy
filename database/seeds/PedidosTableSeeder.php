<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PedidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('pedidos')->insert([
            'id'=>'1',
            'costoT'=>'2000',
            'Estado'=>'1',
            'id_origen'=>'1',
            'id_destino'=>'4',
            'observaciones'=>'Gran Pedido',
            'usuario'=>'Maria Fernanda Morales Medina',
            'created_at'=>'2019-01-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'2',
            'costoT'=>'450',
            'Estado'=>'2',
            'id_origen'=>'1',
            'id_destino'=>'3',
            'observaciones'=>'Otro Pedido',
            'usuario'=>'Rodrigo Alejandro Quintana Gaytán',
            'created_at'=>'2019-02-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'3',
            'costoT'=>'8000',
            'Estado'=>'5',
            'id_origen'=>'1',
            'id_destino'=>'3',
            'observaciones'=>'Pedido con Proveedor Importante',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'created_at'=>'2019-03-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'4',
            'costoT'=>'690',
            'Estado'=>'4',
            'id_origen'=>'1',
            'id_destino'=>'2',
            'observaciones'=>'Pedido Pequeño',
            'usuario'=>'Jeanette Monserrat Avilés Dávalos',
            'created_at'=>'2019-04-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'5',
            'costoT'=>'700',
            'Estado'=>'1',
            'id_origen'=>'1',
            'id_destino'=>'3',
            'observaciones'=>'Pedido Costoso',
            'usuario'=>'Fernando Rayado Jacobo',
            'created_at'=>'2019-05-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'6',
            'costoT'=>'2500',
            'Estado'=>'5',
            'id_origen'=>'1',
            'id_destino'=>'4',
            'observaciones'=>'Pedido Urgente',
            'usuario'=>'Arnulfo Damián Sosa',
            'created_at'=>'2019-06-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'7',
            'costoT'=>'2000',
            'Estado'=>'1',
            'id_origen'=>'1',
            'id_destino'=>'4',
            'observaciones'=>'Pedido Tardado',
            'usuario'=>'Jose Miguel Trejo Covian',
            'created_at'=>'2019-07-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'8',
            'costoT'=>'980',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'2',
            'observaciones'=>'Pedido Grande',
            'usuario'=>'David El Tres Apellidos',
            'created_at'=>'2019-08-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'9',
            'costoT'=>'10000',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'3',
            'observaciones'=>'Pedido Inmediato',
            'usuario'=>'Brandon Ceja Cruz', 
            'created_at'=>'2019-09-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'10',
            'costoT'=>'360',
            'Estado'=>'2',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Pedido de Libretas',
            'usuario'=>'Alan Martin Fuentes',
            'created_at'=>'2019-10-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'11',
            'costoT'=>'500',
            'Estado'=>'1',
            'id_origen'=>'1',
            'id_destino'=>'3',
            'observaciones'=>'Pedido de Papel',
            'usuario'=>'Alan Martin Fuentes',
            'created_at'=>'2019-11-20 04:33:05'
        ]);

        \DB::table('pedidos')->insert([
            'id'=>'12',
            'costoT'=>'360',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Pedido Urgente',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

         \DB::table('pedidos')->insert([
            'id'=>'13',
            'costoT'=>'360',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Pedido Urgente',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'created_at'=>'2019-12-20 04:33:05'
        ]);


         \DB::table('pedidos')->insert([
            'id'=>'14',
            'costoT'=>'360',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Pedido Urgente',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'created_at'=>'2019-12-20 04:33:05'
        ]);


         \DB::table('pedidos')->insert([
            'id'=>'15',
            'costoT'=>'360',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Pedido Urgente',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

         \DB::table('pedidos')->insert([
            'id'=>'16',
            'costoT'=>'360',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Pedido Urgente',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

         \DB::table('pedidos')->insert([
            'id'=>'17',
            'costoT'=>'360',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Pedido Urgente',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

         \DB::table('pedidos')->insert([
            'id'=>'18',
            'costoT'=>'360',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Pedido Urgente',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'created_at'=>'2019-12-20 04:33:05'
        ]);

         \DB::table('pedidos')->insert([
            'id'=>'19',
            'costoT'=>'360',
            'Estado'=>'3',
            'id_origen'=>'1',
            'id_destino'=>'5',
            'observaciones'=>'Pedido Urgente',
            'usuario'=>'Karen Daniela Olivo Solorio',
            'created_at'=>'2019-12-20 04:33:05'
        ]);




    }
}