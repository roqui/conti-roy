<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$hora=Carbon::now();

    	DB::table('productos')->insert([
           'codigo'=>'123456',
           'codigosat'=>'1',
           'descripcion_producto' => 'Lápices',
           'precio' => '4.00',
           'costo' => '2.50',
           'id_proveedor' => '1',
           'id_marca' => '1',
           'unidad'=>'pieza',
           'iva' => '16',
           'id_sublinea'=>'65',
           'created_at' => $hora
       ]);

        DB::table('productos')->insert([
            'codigo'=>'1234567891234',
            'codigosat'=>'2',
            'descripcion_producto' => 'Pintura para papel',
            'precio' => '8.00',
            'costo' => '3.50',
            'iva' => '16',
            'id_proveedor' => '2',
            'id_marca' => '18',
            'id_sublinea'=>'10',
            'unidad'=>'pieza',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'1234567891235',
            'codigosat'=>'3',
            'descripcion_producto' => 'Calcomanias Preescolar',
            'precio' => '25.00',
            'costo' => '18.00',
            'id_proveedor' => '9',
            'id_marca' => '10',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_sublinea'=>'82',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'1234567891236',
            'codigosat'=>'4',
            'descripcion_producto' => 'Engrapadora',
            'precio' => '30.00',
            'costo' => '16.00',
            'id_proveedor' => '5',
            'id_marca' => '6',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_sublinea'=>'135',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'147852147852',
            'codigosat'=>'5',
            'descripcion_producto' => 'Folder Oficio',
            'precio' => '5.00',
            'costo' => '2.00',
            'id_proveedor' => '2',
            'id_marca' => '8',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_sublinea'=>'136',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'123654780',
            'codigosat'=>'6',
            'descripcion_producto' => 'Estambre Negro',
            'precio' => '12.00',
            'costo' => '7.00',
            'id_proveedor' => '8',
            'id_marca' => '14',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_sublinea'=>'88',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'8521479874',
            'codigosat'=>'7',
            'descripcion_producto' => 'Calculadora Básica',
            'precio' => '16.00',
            'costo' => '8.00',
            'id_proveedor' => '1',
            'id_marca' => '10',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_sublinea'=>'29',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'8796541258',
            'codigosat'=>'8',
            'descripcion_producto' => 'Silicón Azúl',
            'precio' => '3.00',
            'costo' => '1.50',
            'id_proveedor' => '1',
            'id_marca' => '4',
            'id_sublinea'=>'96',
            'unidad'=>'pieza',
            'iva' => '16',

            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'7896541230',
            'codigosat'=>'9',
            'descripcion_producto' => 'Contac 10m',
            'precio' => '60.00',
            'costo' => '40.00',
            'id_proveedor' => '6',
            'id_marca' => '11',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_sublinea'=>'49',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'7893214560',
            'codigosat'=>'10',
            'descripcion_producto' => 'Marcatextos Amarillo',
            'precio' => '8.00',
            'costo' => '5.00',
            'id_proveedor' => '7',
            'id_marca' => '2',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_sublinea'=>'71',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'852147963',
            'codigosat'=>'11',
            'descripcion_producto' => 'Cinta Invisible 5m',
            'precio' => '14.00',
            'costo' => '9.00',
            'id_proveedor' => '7',
            'id_marca' => '2',
            'unidad'=>'caja',
            'iva' => '16',

            'id_sublinea'=>'174',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'12365478',
            'codigosat'=>'12',
            'descripcion_producto' => 'Cutter',
            'precio' => '22.00',
            'costo' => '13.00',
            'unidad'=>'caja',
            'iva' => '16',

            'id_proveedor' => '9',
            'id_marca' => '15',
            'id_sublinea'=>'132',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'123456785242',
            'codigosat'=>'13',
            'descripcion_producto' => 'Cuaderno Profesional',
            'precio' => '9.00',
            'costo' => '4.00',
            'id_proveedor' => '4',
            'unidad'=>'caja',
            'iva' => '16',
            'id_marca' => '11',
            'id_sublinea'=>'42',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'123456784789',
            'codigosat'=>'14',
            'descripcion_producto' => 'Moño Guinda',
            'precio' => '12.00',
            'costo' => '7.00',
            'unidad'=>'caja',
            'iva' => '16',

            'id_proveedor' => '6',
            'id_marca' => '19',
            'id_sublinea'=>'89',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'124567524234',
            'codigosat'=>'15',
            'descripcion_producto' => 'Cartulina Gruesa',
            'precio' => '6.00',
            'costo' => '3.00',
            'unidad'=>'caja',
            'iva' => '16',

            'id_proveedor' => '8',
            'id_marca' => '7',
            'id_sublinea'=>'155',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'124565424234',
            'codigosat'=>'16',
            'descripcion_producto' => 'Acuarela',
            'precio' => '20.00',
            'costo' => '8.00',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_proveedor' => '5',
            'id_marca' => '5',
            'id_sublinea'=>'129',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'234567654327',
            'codigosat'=>'17',
            'descripcion_producto' => 'Lápiz de Puntillas',
            'precio' => '8.00',
            'costo' => '3.00',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_proveedor' => '3',
            'id_marca' => '2',
            'id_sublinea'=>'100',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'124560024234',
            'codigosat'=>'18',
            'descripcion_producto' => 'Cuaderno Italiano',
            'precio' => '15.00',
            'costo' => '8.00',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_proveedor' => '1',
            'id_marca' => '6',
            'id_sublinea'=>'80',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'124986524234',
            'codigosat'=>'19',
            'descripcion_producto' => 'Papel China',
            'precio' => '5.00',
            'costo' => '2.00',
            'unidad'=>'pieza',
            'iva' => '16',

            'id_proveedor' => '3',
            'id_marca' => '2',
            'id_sublinea'=>'170',
            'created_at' => $hora
        ]);

        DB::table('productos')->insert([
            'codigo'=>'124567777234',
            'codigosat'=>'20',
            'descripcion_producto' => 'Colores Bicolor',
            'precio' => '60.00',
            'costo' => '39.00',
            'unidad'=>'caja',
            'iva' => '16',

            'id_proveedor' => '5',
            'id_marca' => '1',
            'id_sublinea'=>'66',
            'created_at' => $hora
        ]);





    }

}