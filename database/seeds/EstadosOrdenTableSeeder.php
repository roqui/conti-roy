<?php

use Illuminate\Database\Seeder;

class EstadosOrdenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('estadoords')->insert([
        	'id'=>'1',
            'descripcion'=>'Realizada'
        ]);

        \DB::table('estadoords')->insert([
        	'id'=>'2',
            'descripcion'=>'Aceptada'
        ]);

        \DB::table('estadoords')->insert([
        	'id'=>'3',
            'descripcion'=>'Rechazada'
        ]);
    }
}
