<?php

use Illuminate\Database\Seeder;

class SucursalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('destinos')->insert([
        	'id'=>'1',
            'nombre'=>'Sucursal Almacén Central',
            'telefono'=>'4431675431',
            'calle'=>'Paseo de las Vacas',
            'num_exterior'=>'165',
            'codigo_postal'=>'58110',
            'colonia'=>'Ranchito',
            'ciudad'=>'Morelia'
        ]);

        \DB::table('destinos')->insert([
        	'id'=>'2',
            'nombre'=>'Sucursal Almacén Capital',
            'telefono'=>'4431675431',
            'calle'=>'Paseo de las Vacas',
            'num_exterior'=>'165',
            'codigo_postal'=>'58110',
            'colonia'=>'Ranchito',
            'ciudad'=>'Ciudad de México'
        ]);

        \DB::table('destinos')->insert([
        	'id'=>'3',
            'nombre'=>'Sucursal Carrillo',
            'telefono'=>'4431675431',
            'calle'=>'Paseo de las Vacas',
            'num_exterior'=>'165',
            'codigo_postal'=>'58110',
            'colonia'=>'Ranchito',
            'ciudad'=>'Morelia'
        ]);

        \DB::table('destinos')->insert([
        	'id'=>'4',
            'nombre'=>'Sucursal Centro Histórico',
            'telefono'=>'4431675431',
            'calle'=>'Paseo de las Vacas',
            'num_exterior'=>'165',
            'codigo_postal'=>'58110',
            'colonia'=>'Ranchito',
            'ciudad'=>'Morelia'
        ]);

        \DB::table('destinos')->insert([
        	'id'=>'5',
            'nombre'=>'Sucursal Zamora',
            'telefono'=>'4431675431',
            'calle'=>'Paseo de las Vacas',
            'num_exterior'=>'165',
            'codigo_postal'=>'58110',
            'colonia'=>'Ranchito',
            'ciudad'=>'Zamora'
        ]);

        \DB::table('destinos')->insert([
        	'id'=>'6',
            'nombre'=>'Sucursal Patzcuaro',
              'telefono'=>'4431675431',
            'calle'=>'Paseo de las Vacas',
            'num_exterior'=>'165',
            'codigo_postal'=>'58110',
            'colonia'=>'Ranchito',
            'ciudad'=>'Patzcuaro'
        ]);

        \DB::table('destinos')->insert([
        	'id'=>'7',
            'nombre'=>'Sucursal La Huerta',
            'telefono'=>'4431675431',
            'calle'=>'Paseo de las Vacas',
            'num_exterior'=>'165',
            'codigo_postal'=>'58110',
            'colonia'=>'Ranchito',
            'ciudad'=>'Morelia'
        ]);

        \DB::table('destinos')->insert([
        	'id'=>'8',
            'nombre'=>'Sucursal Estadio',
            'telefono'=>'4431675431',
            'calle'=>'Paseo de las Vacas',
            'num_exterior'=>'165',
            'codigo_postal'=>'58110',
            'colonia'=>'Ranchito',
            'ciudad'=>'Morelia'
        ]);
    }
}
