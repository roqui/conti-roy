<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marcas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion');
            $table->timestamps();
        });

        Schema::create('lineas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descr');
            $table->timestamps();
        });

        Schema::create('sublineas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descrip');

            $table->unsignedBigInteger('id_linea');
            $table->foreign('id_linea')->references('id')->on('lineas');
            $table->timestamps();
        });

        


        Schema::create('estadopedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',255);
            $table->timestamps();
        });

        Schema::create('proveedores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rfc');  
            $table->string('nombre', 50);
            $table->string('calle', 200);
            $table->string('ciudad', 50);
            $table->string('estado', 50);    
            $table->string('pais', 50);
            $table->string('foto', 255);    
            $table->string('num_exterior', 200);
            $table->string('codigo_postal', 200);
            $table->string('telefono', 50);
            $table->string('email'); 
            $table->string('observaciones');
            $table->integer('dias_plazo');
            $table->double('descuento');
            $table->timestamps();
        });


        Schema::create('satproductos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo', 25)->unique();
            $table->string('descripcion');
            $table->timestamps();

        });



        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo', 25)->unique();
             $table->unsignedBigInteger('codigosat');
            $table->foreign('codigosat')->references('id')->on('satproductos');
            $table->string('descripcion_producto');
            $table->double('precio', 8, 2);
            $table->double('costo', 8, 2);
            $table->double('iva', 8, 2);
            $table->string('unidad');
            $table->unsignedBigInteger('id_proveedor');
            $table->foreign('id_proveedor')->references('id')->on('proveedores');            
            $table->unsignedBigInteger('id_marca');
            $table->foreign('id_marca')->references('id')->on('marcas');
            $table->unsignedBigInteger('id_sublinea');
            $table->foreign('id_sublinea')->references('id')->on('sublineas');
            $table->timestamps();
        });






        Schema::create('inventarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_producto');
            $table->foreign('id_producto')->references('id')->on('productos');
            $table->unsignedBigInteger('id_sucursal');
            $table->foreign('id_sucursal')->references('id')->on('destinos');
            $table->integer('cant_disponible');
            $table->integer('cant_apartada');
            $table->integer('maximos');
            $table->integer('minimos');
            $table->timestamps();


        });




        Schema::create('clientes', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->string('rfc');  
            $table->string('nombre', 50);
            $table->string('calle', 200);
            $table->string('ciudad', 50);
            $table->string('estado', 50);    
            $table->string('pais', 50); 
            $table->string('foto', 255);   
            $table->string('num_exterior', 200);
            $table->string('codigo_postal', 200);
            $table->string('telefono', 50);
            $table->string('email'); 
            $table->string('observaciones');
            $table->integer('dias_plazo');
            $table->double('descuento');
            $table->timestamps();
        });

        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('CostoT', 8, 2);
            $table->unsignedBigInteger('Estado');
            $table->foreign('Estado')->references('id')->on('estadopedidos');
            $table->unsignedBigInteger('id_origen');
            $table->foreign('id_origen')->references('id')->on('destinos');
            $table->unsignedBigInteger('id_destino');
            $table->foreign('id_destino')->references('id')->on('clientes');
            $table->string('observaciones',300)->nullable();
            $table->string('usuario',50);
            


            $table->timestamps();
        });

        Schema::create('surtirs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idPedido');
            $table->foreign('idPedido')->references('id')->on('pedidos');
            $table->unsignedBigInteger('idSurtidor');
            $table->foreign('idSurtidor')->references('id')->on('users');
            $table->string('estado');
            $table->timestamps();
        });

        Schema::create('checars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idSurtido');
            $table->foreign('idSurtido')->references('id')->on('surtirs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marcas');

        Schema::dropIfExists('lineas');

        Schema::dropIfExists('sublineas');

        Schema::dropIfExists('estadopedidos');

        Schema::dropIfExists('proveedores');

                Schema::dropIfExists('satproductos');



        Schema::dropIfExists('productos');



        Schema::dropIfExists('inventarios');

        Schema::dropIfExists('clientes');

        Schema::dropIfExists('pedidos');

        Schema::dropIfExists('surtirs');

        Schema::dropIfExists('checars');
    }
}
