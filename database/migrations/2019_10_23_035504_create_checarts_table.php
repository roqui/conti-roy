<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checarts', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->unsignedBigInteger('idSurtido');
            $table->foreign('idSurtido')->references('id')->on('surtirts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checarts');
    }
}
