<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConceptoordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estadoords', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion');

            $table->timestamps();
        });

        Schema::create('check_states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',255);
            $table->timestamps();
        });

     

        Schema::create('ordencompras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_proveedor');
            $table->foreign('id_proveedor')->references('id')->on('proveedores');
            $table->date('fechaC')->nullable();
            $table->date('fechaLL')->nullable();
            $table->double('Total', 8, 2);
            $table->unsignedBigInteger('estado');
            $table->foreign('estado')->references('id')->on('estadoords');
            $table->unsignedBigInteger('id_origen');
            $table->foreign('id_origen')->references('id')->on('destinos');
            $table->timestamps();
            $table->string('observaciones',300)->nullable();
            $table->string('usuario',50);
        });

        Schema::create('conceptoords', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idOrden');
            $table->foreign('idOrden')->references('id')->on('ordencompras');
            $table->unsignedBigInteger('idProducto');
            $table->foreign('idProducto')->references('id')->on('productos');
            $table->integer('cantidad');
            $table->integer('cantidadrec');
            $table->unsignedBigInteger('estado');
            $table->foreign('estado')->references('id')->on('check_states');
            $table->unsignedBigInteger('subtotal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                Schema::dropIfExists('estadoords');

                Schema::dropIfExists('check_states');

                Schema::dropIfExists('ordencompras');

        Schema::dropIfExists('conceptoords');
    }
}
