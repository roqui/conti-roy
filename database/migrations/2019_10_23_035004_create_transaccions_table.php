<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaccions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_origen');
            $table->foreign('id_origen')->references('id')->on('destinos');
            $table->double('CostoT', 8, 2);
            $table->unsignedBigInteger('Estado');
            $table->foreign('Estado')->references('id')->on('estadopedidos');
            $table->unsignedBigInteger('id_destino');
            $table->foreign('id_destino')->references('id')->on('destinos');
            $table->string('observaciones',300)->nullable();
            $table->string('usuario',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaccions');
    }
}
