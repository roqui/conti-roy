<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnviartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enviarts', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->unsignedBigInteger('idChecado');
            $table->foreign('idChecado')->references('id')->on('checarts');
            $table->unsignedBigInteger('idConductor');
            $table->foreign('idConductor')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enviarts');
    }
}
