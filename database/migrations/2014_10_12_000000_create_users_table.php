<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('destinos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 50);
            $table->string('calle', 200);
            $table->string('colonia', 200);
            $table->string('num_exterior', 200);
            $table->string('codigo_postal', 200);
            $table->string('telefono', 10);
            $table->string('ciudad', 50);
            $table->timestamps();

        });


        Schema::create('puestos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',255);
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 40);
            $table->string('foto', 255);
            $table->string('email',100)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->unsignedBigInteger('puesto');
            $table->foreign('puesto')->references('id')->on('puestos');
            $table->string('usuario', 20)->unique();
            $table->string('password', 255);
            $table->string('calle', 255);
            $table->string('telefono', 10);
            $table->unsignedBigInteger('idSucursal');
            $table->foreign('idSucursal')->references('id')->on('destinos');
            $table->rememberToken();
            $table->timestamps();
        });
    

    Schema::create('notificacions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idUsuario');
                        $table->foreign('idUsuario')->references('id')->on('users');

            $table->string('contenido', 255);
            $table->timestamps();
        });
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('puestos');
                        Schema::dropIfExists('destinos');

        Schema::dropIfExists('users');

                        Schema::dropIfExists('notificacions');

    }
}
